Privacy Policy

This bot does not collect or store any personal information, except as described below:

User Submissions
When users submit their LowFuelMotorsport (LFM) ID using the !add command, the ID will be stored in the bot's private database. This allows the bot to reference the user's profile for other commands.

Prediction Scores
The !predict command will track users' prediction scores for the AOR Prediction game. Scores are stored anonymously without any personal details.

Keyword Monitoring
The bot monitors and collects messages that contain specific keywords as predetermined by the server administrators. These keyword-containing messages are sent to a designated channel within the "main" Discord server for review by server administrators. Certain Users may be able to reply to these messages.

Access and Data Retention
Only the two designated Administrators of the bot have access to view the database and server logs. User submissions and predictions are retained indefinitely for bot functionality unless a user requests their data be removed. Messages containing specific keywords are stored temporarily for monitoring purposes.

Data Sharing
No personal user information is ever shared with any third parties. Server logs, the private database containing user submissions, and the messages containing specific keywords are only accessible by the two Administrators.

Changes to this Policy
This policy may be updated from time to time. Continued use of the bot's commands means acceptance of any changes to this privacy policy.

Contact
If you have any other privacy or data access questions, please contact one of the bot Administrators.

By using this bot's commands, you agree to this Privacy Policy and consent to how your submissions, predictions, and keyword-containing messages may be stored and used as described above.