import re
import sys

path = sys.argv[1]
regex = sys.argv[2]
subst = sys.argv[3]

content = ""
with open(path, "r") as fp:
    lines = fp.readlines()
    content = "".join(lines)

with open(path, "w") as fp:
    result = re.sub(regex, subst, content, 0, re.MULTILINE)
    fp.write(result)
