import os
import requests
from typing import Any
from dataclasses import asdict, dataclass

@dataclass
class PortainerEnvVar:
    name: str
    value: str

@dataclass
class PortainerStack:
    
    portainerUrl: str
    portainerUsername: str
    portainerPassword: str
    portainerEndpointId: int
    stackName: str
    stackType: int
    composeFile: str
    prune: bool
    pullImage: bool
    delete: bool
    verifySSL: bool
    env_variables: list[PortainerEnvVar]

    @property
    def apiUrl(self) -> str:
        return f"{self.portainerUrl}/api"

    def deployStack(self) -> None:

        token = self.login()
        
        header = {
                    "Content-Type": "application/json",
                    "Authorization": f"Bearer {token}"
                 }

        stack_id = self.getStackIdByName(header)

        if self.delete:
            print("Deleting stack")
            if stack_id != -1:
                self.deleteStack(header, stack_id)
            return
            
        composeContent = ""
        with open(self.composeFile, "r") as fp:
            composeContent = fp.read()

        if stack_id != -1:
            print("Updating stack")
            self.updateStack(header, stack_id, composeContent)
        else:
            print("Creating stack")
            self.createStack(header, composeContent)

    def login(self) -> str:

        auth_json = {
                    "username": self.portainerUsername,
                    "password": self.portainerPassword
                }

        login_request = requests.post(f"{self.apiUrl}/auth", json=auth_json, verify=self.verifySSL)
        if login_request.status_code != 200:
            print(f"Failed to login {login_request.status_code}: {login_request.text}")
            exit(1)

        return login_request.json()["jwt"]
    
    def listStacks(self, header: dict[str, str]) -> list[dict[str, Any]]:

        stacks_request = requests.get(f"{self.apiUrl}/stacks", headers=header, verify=self.verifySSL)
        if stacks_request.status_code != 200:
            print(f"Failed to list stacks, {stacks_request.status_code}: {stacks_request.text}")

        return stacks_request.json()

    def getStackIdByName(self, header: dict[str, str]) -> int:

        stacks = self.listStacks(header)
        for stack in stacks:
            if stack["Name"] == self.stackName:
                return stack["Id"]

        return -1   

    def createStack(self, header: dict[str, str], composeContent: str) -> None:
 
        create_data = {
                "name": self.stackName,
                "swarmID": str(self.portainerEndpointId),
                "stackFileContent": composeContent,
                "env": [asdict(var) for var in self.env_variables]
            }
        create_request = requests.post(
                f"{self.apiUrl}/stacks?type={self.stackType}&method=string&endpointId={self.portainerEndpointId}",
                headers=header,
                json=create_data,
                verify=self.verifySSL)

        if create_request.status_code != 200:
            print(f"Failed to create stack, {create_request.status_code}: {create_request.text}")
            exit(1)


    def updateStack(self, header: dict[str, str], stackId: int, composeContent: str) -> None:

        update_data = {
                "prune": True,
                "pullImage": True,
                "stackFileContent": composeContent,
                "env": [asdict(var) for var in self.env_variables]
            }

        update_request = requests.put(
                f"{self.apiUrl}/stacks/{stackId}?endpointId={self.portainerEndpointId}",
                headers=header,
                json=update_data,
                verify=self.verifySSL)

        if update_request.status_code != 200:
            print(f"Failed to update stack, {update_request.status_code}: {update_request.text}")
            exit(1)
        
    def deleteStack(self, header: dict[str, str], stackId: int) -> None:
        
        del_request = requests.delete(
                f"{self.apiUrl}/stacks/{stackId}?external=false&endpointId={self.portainerEndpointId}",
                headers=header,
                verify=self.verifySSL)

        if del_request.status_code != 204:
            print(f"Failed to delete stack, {del_request.status_code} {del_request.text}")
            exit(1)


def get_bool_env(env_name: str) -> bool:

    input = os.environ[env_name].lower()

    if input not in ["true", "false"]:
        raise ValueError("Invalid argument for bool type, must be true or false")

    return input == "true"

def get_int_env(env_name: str) -> int:

    input = os.environ[env_name]

    if not input.isnumeric():
        raise ValueError("Invalid argument for int type, must be a number")

    return int(input)


def main():
    
    env_keys = [env_var for env_var in os.environ.keys() if env_var.startswith("ENV_")]
    envs = []
    for key in env_keys:
        name = key.split("_", maxsplit=1)[1]
        envs.append(PortainerEnvVar(name, os.environ[key]))
 
    stackInfo = PortainerStack(os.environ["PORTAINER_URL"],
                               os.environ["PORTAINER_USERNAME"],
                               os.environ["PORTAINER_PASSWORD"],
                               get_int_env("PORTAINER_ENDPOINT_ID"),
                               os.environ["STACK_NAME"], 
                               get_int_env("STACK_TYPE"),
                               os.environ["COMPOSE_FILE"],
                               get_bool_env("PRUNE"),
                               get_bool_env("PULL_IMAGE"),
                               get_bool_env("DELETE"),
                               get_bool_env("VERIFY_SSL"),
                               envs)

    stackInfo.deployStack()

if __name__ == "__main__":
    main()

