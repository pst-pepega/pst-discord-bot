By using this bot, you agree to the following terms:

1. Command Use. The bot should only be used to execute its intended commands, which are primarily the LFM BOP commands and other LFM/ACC Commands.

2. Copypastas. Any copypastas submitted through the bot's commands should not contain personal insults, threats of violence, illegal plans/instructions, or confidential information. Moderators reserve the right to remove any submissions deemed inappropriate. (!addpasta Command)

3. LFM Profiles. When adding your LFM profile to the database via the !add command, you must provide accurate details and not impersonate another user.

4. No Liability. The bot is provided solely for its intended commands. The owner is not liable for any issues arising from bot use.

5. Changes. These terms may be updated by the owner. Continued use of commands means accepting any changes.

6. Law. These terms are governed by the laws where the owner resides. By using commands, you agree to the owner's local jurisdiction.
