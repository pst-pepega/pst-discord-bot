import requests
import json
import sys
from datetime import datetime, timedelta
import os

# Function to get the season data from the LFM API
def get_season_data(series_id):
    url = f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonWeeks/{series_id}"
    response = requests.get(url)
    
    # Check if the request was successful
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Failed to retrieve data. Status code: {response.status_code}")
        return None

# Function to process and extract data
def process_season_data(season_data, series_name):
    formatted_data = []
    
    # Loop over the weeks (assuming 0 to 11 weeks)
    for week in season_data[:12]:
        
        try:
            # Extract the date from the "from" field and format it as DD/MM/YYYY
            date_str = week['from'].split()[0]  # Get only the date portion (e.g., 2024-10-07)
            date_obj = datetime.strptime(date_str, "%Y-%m-%d")
            print(date_obj)
            if date_obj.weekday() == 6 and "sprint" in series_name.lower():
                date_obj = date_obj + timedelta(days=1)
            formatted_date = date_obj.strftime("%d/%m/%Y")
            
            # Get the track name
            track_name = week['track']['track_name']
            
            # Create a dictionary entry with the formatted data
            formatted_data.append({
                "date": formatted_date,
                "track": track_name,
                "series": series_name
            })
        except:
            print("No data for this week.")
    
    return formatted_data

# Function to append to the JSON file (or create it if it doesn't exist)
def append_to_json(data, filename="season_schedule.json"):
    # If the file already exists, load existing data and append the new data
    if os.path.exists(filename):
        with open(filename, "r") as json_file:
            existing_data = json.load(json_file)
        existing_data.extend(data)
    else:
        existing_data = data
    
    # Write the updated data back to the file
    with open(filename, "w") as json_file:
        json.dump(existing_data, json_file, indent=4, ensure_ascii=False)
    
    print(f"Data appended to {filename}")

# Main function to get data and append it to the file
def main(series_id, series_name):
    season_data = get_season_data(series_id)
    
    if season_data:
        formatted_data = process_season_data(season_data, series_name)
        append_to_json(formatted_data)

# Example usage:
if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python script.py <series_id> <series_name>")
        sys.exit(1)
    
    series_id = sys.argv[1]  # Get series_id from command-line arguments
    series_name = sys.argv[2]  # Get series_name from command-line arguments
    main(series_id, series_name)
