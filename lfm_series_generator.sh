#!/bin/bash

python3 lfm_schedule_generator.py 503 "Pro Series"
python3 lfm_schedule_generator.py 505 "GT3 Series"
python3 lfm_schedule_generator.py 507 "Sprint Series"
python3 lfm_schedule_generator.py 509 "GT4 Series"
python3 lfm_schedule_generator.py 513 "Single Make Series"
python3 lfm_schedule_generator.py 510 "GT3+GT4 Multiclass Series"
python3 lfm_schedule_generator.py 511 "Duo Cup"
python3 lfm_schedule_generator.py 512 "Endurance Series"