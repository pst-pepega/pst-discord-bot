* Add F1 automated reminder based on calendar
* ~~Add AOR Ballast proposal~~
* ~~Add Ranking system based on AOR (1 column after Zolder)~~
* ~~Add Lap time recording + if lap time is beat make announcement~~
* Add PST Rankings


* Automated ACC Down
* * Save current status
* * Save when status is stable, and when changed print how long it was offline/online for
* * If changed post in channels
* * * Command to post, need manage message rights
* * * * channel
* * * * message when on again
* * * * message when off again 
* * save longest/record for offline/online time


FIX:
* favourite command doesnt do anything if no lfm-id added
* race results when "king of car" calculate distance to second place
* make multiclass series have all classes available in standings