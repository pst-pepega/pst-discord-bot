import csv
import json

# Input CSV data
csv_data = '''Wet Chance,Dry Chance,Mixed Chance,Avg Temp,Temp,Clouds,Rain,Date and Time,Track,id,Event Name
0.152,0.137,0.711,17.57,16,0.45,0.15,2025-01-09 20:00:00,Circuit de Spa Francorchamps,165480,Caseking Pro Series
0.245,0.257,0.498,19.736,17,0.4,0.2,2025-01-16 20:00:00,Nürburgring,165481,Caseking Pro Series
0.0,1.0,0.0,22.676,18,0.2,0,2025-01-23 20:00:00,Kyalami,165482,Caseking Pro Series
0.0,0.999,0.001,22.152,21,0.2,0.1,2025-01-30 20:00:00,Watkins Glen,165483,Caseking Pro Series
0.173,0.17,0.657,20.709,20,0.5,0.1,2025-02-06 20:00:00,Suzuka Circuit,165484,Caseking Pro Series
0.0,1.0,0.0,29.106,25,0.2,0,2025-02-13 20:00:00,Autodromo Enzo e Dino Ferrari,165485,Caseking Pro Series
0.311,0.32,0.369,18.678,18,0.55,0.05,2025-02-20 20:00:00,Misano,165486,Caseking Pro Series
1.0,0.0,0.0,21.189,21,0.65,0.2,2025-02-27 20:00:00,Silverstone,165487,Caseking Pro Series
0.06,0.597,0.343,25.329,24,0.3,0.2,2025-03-06 20:00:00,Zandvoort,165488,Caseking Pro Series
0.0,1.0,0.0,20.664,26,0,0,2025-03-13 20:00:00,Circuit Ricardo Tormo,165489,Caseking Pro Series
0.0,1.0,0.0,16.636,18,0.8,0,2025-03-20 20:00:00,Circuit de Paul Ricard,165490,Caseking Pro Series
0.119,0.47,0.411,15.323,20,0.4,0.15,2025-03-27 20:00:00,Mount Panorama Circuit,165491,Caseking Pro Series
'''

# Split data by rows
rows = csv_data.strip().split("\n")

# Initialize list to hold transformed data
json_data = []

# Process each row in the CSV
for row in rows[1:]:  # Skip header
    cols = row.split(",")
    wet = float(cols[0]) * 100
    dry = float(cols[1]) * 100
    mixed = float(cols[2]) * 100
    
    # Parse date and time
    date_time = cols[7]
    date, time = date_time.split(" ")
    hour = time.split(":")[0]
    
    json_entry = {
        "Date": date,
        "Track": cols[8],
        "°C": int(cols[4]),
        "Hour": int(hour),
        "Avg. °C": cols[3],
        "Clouds": float(cols[5]),
        "Rain": float(cols[6]),
        "Dry": f"{dry:.1f}%",
        "Wet": f"{wet:.1f}%",
        "Mixed": f"{mixed:.1f}%"
    }
    
    json_data.append(json_entry)

# Write JSON data to 'lfmproweather.json'
with open("lfmproweather.json", "w") as json_file:
    json.dump(json_data, json_file, indent=4, ensure_ascii=False)

print("Data has been written to lfmproweather.json.")