import json
from datetime import datetime, timedelta

# Function to increase date by 91 days
def increase_date_by_91_days(data):
    for entry in data:
        date_str = entry['date']
        date_obj = datetime.strptime(date_str, '%d/%m/%Y')
        new_date_obj = date_obj + timedelta(days=91)
        entry['date'] = new_date_obj.strftime('%d/%m/%Y')
    return data

# Load JSON data from file
with open('lfm_schedule.json', 'r') as f:
    data = json.load(f)

# Update dates
updated_data = increase_date_by_91_days(data)

# Save updated JSON data back to the file
with open('lfm_schedule.json', 'w') as f:
    json.dump(updated_data, f, indent=4)

print("Updated data saved to 'lfm_schedule.json'")
