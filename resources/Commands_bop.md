**BOP**
Usage: !bop [track]

Prints LFM Predicted BOP for a given track
\_\_\_\_

**GetBopFile**
Usage: !bopfile [track]/all

Uploads the bop.json for usage in servers for a given track or for all tracks in a single file.
\_\_\_\_

**BopSmall**
Refer to BOP.

Same concept, just smaller, for mobile usually.
\_\_\_\_

**BopAdjust**
Usage: !bopadjust [car] - [laptime] - [track]

Recalculates the bop with your inputs.

Note: The car needs to 100% match the name it has in !bop command. Track will be matched.
\_\_\_\_

**RealBOP**
Usage: !realbop [track]
Also: !boplast, !bopcurrent, !bopchanged

Prints the current active bop with the changes from last version.
\_\_\_\_

**BOPCar**
Usage: !bopcar [car]

Prints laptime, bop information for given car. Matches input car best to all available cars.
\_\_\_\_

**GT4BOP**
Usage: !gt4bop [track]

Tries to calculate a bop for gt4 cars.
\_\_\_\_
**BOPNext**
Usage: !bopnext [track]
Also: !boppredict

Prints the bop that will be active next week. Similar to !bop but also prints the difference in the bop.