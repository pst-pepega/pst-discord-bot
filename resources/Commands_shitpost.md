**SendCopyPasta**
Usage: !sendpasta [title] : [copypasta]

Sends the copypasta to the PST Server for evaluation and then will be added to the database.
\_\_\_\_

**GetPastas**
Usage: !getpastas [searchword]

Searches for copypastas in the database and sends the search results as a file.
\_\_\_\_

**GNU**
Usage: !gnu

Has no use.
\_\_\_\_

**Neofetch**
Usage: !neofetch

Has no use either.
\_\_\_\_

**SDLSubmission**
Usage: !sdlcrash [link]

Sends SDL Crash Video-link to the PST Server. Zabbzi thanks you for your contribution.
\_\_\_\_

**ITBSubmission**
Usage: !itbcrash [link]

Sends ITB Crash Video-link to the PST Server. Zabbzi thanks you for your contribution.
\_\_\_\_