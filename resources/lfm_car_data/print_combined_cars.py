import json
import sys

# Load data from combined_cars_sorted.json
json_file = "combined_cars_sorted.json"
try:
    with open(json_file, "r") as file:
        data = json.load(file)
except FileNotFoundError:
    print(f"Error: File '{json_file}' not found.")
    sys.exit(1)

# Input prompt for the number of entries to print
if len(sys.argv) > 1:
    num_entries = int(sys.argv[1])
else:
    num_entries = int(input("How many entries would you like to print? "))
entries_to_print = data[:num_entries]

# Determine max character lengths for formatting based on selected entries
name_length = max(len(entry["name"]) for entry in entries_to_print) + 2
km_length = max(len(f"{entry['kilometers']:.2f}") for entry in entries_to_print) + 2
laps_length = max(len(str(entry["laps"])) for entry in entries_to_print) + 2
car_length = max(len(entry["car_name"]) for entry in entries_to_print) + 2

# Header
print(f"{'Name'.ljust(name_length)}{'Kilometers'.ljust(km_length)}{'Laps'.ljust(laps_length)}{'Car'.ljust(car_length)}")
print("=" * (name_length + km_length + laps_length + car_length))

# Printing each entry
for entry in entries_to_print:
    name = entry["name"].ljust(name_length)
    kilometers = f"{entry['kilometers']:.2f}".ljust(km_length)
    laps = str(entry["laps"]).ljust(laps_length)
    car = entry["car_name"].ljust(car_length)
    
    print(f"{name}{kilometers}{laps}{car}")
