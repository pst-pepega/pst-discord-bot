# -*- coding: utf-8 -*-

import json
# import urllib library
from urllib.request import urlopen 
import sys
import time
import json
from urllib.request import urlopen
import sys
import time

#import send_discord_message

LIST_LENGTH = 100

def time_conversion(sec):
   sec_value = sec % (24 * 3600)
   hour_value = sec_value // 3600
   sec_value %= 3600
   min = sec_value // 60
   sec_value %= 60
   print(str(hour_value) + ":" + str(min) + ":" + str(sec_value))


def Sort(sub_li):
    return(sorted(sub_li, key = lambda x: x[1], reverse=True))

def PruneNDict(d, N):
    return {k: d[k] for k in list(d)[:N]}

def stats():
    if(sys.argv[1] == "help" or sys.argv[1] == "--help" or sys.argv[1] == "-help" or len(sys.argv) < 1):
        for x in range(1, 62):
            print(str(x) + " = " + Car_lookup(x))
        print("arg1 minimum races")
    else:
        url = "https://api2.lowfuelmotorsport.com/api/statistics/safetyrating"
        response = urlopen(url)
        data = json.loads(response.read())
        
        all_user_data = []
        
        for x in data:
            if(x["races"] >= int(sys.argv[1])):
                all_user_data.append(x)

        y = 0
        length = len(all_user_data)
        percent_of_length = int(length/10)        
        results = [[] for _ in range(LIST_LENGTH)]

        for x in all_user_data:
            time.sleep(0.25)
            y = y + 1
            completed_percent = round(1 - ((length - y) / length), 5) * 100
            print(completed_percent, end='\r')
            name = f'{x["vorname"]} {x["nachname"]}'
            user_id = x["id"]

#            if y % percent_of_length == 0:
#                send_discord_message.send_discord_message(f"{int(completed_percent+0.1)}/100")
            
            if len(all_user_data) < 100:
                print(name + " " + str(user_id))
            
            try:
                url = f"https://api2.lowfuelmotorsport.com/api/users/getUsersFavouriteCars/{user_id}"
                response = urlopen(url)
                user_data = json.loads(response.read())

                for user_car in user_data:
                    current_car_id = user_car["car_id"]
                    if current_car_id < LIST_LENGTH:
                        laps = user_car["laps"]
                        kilometers = round(float(user_car["km"]), 3)
                        result = {
                            "name": name,
                            "kilometers": kilometers,
                            "laps": laps,
                            "id": user_id
                        }
                        results[current_car_id].append(result)
            except Exception as e:
                print(e)
            
        for i, element in enumerate(results): 
            element = sorted(element, key=lambda x: x["kilometers"], reverse=True)
            car_name = Car_lookup(i)
            # Save the sorted results to a JSON file with Unicode encoding
            with open(f'{car_name}.json', 'w', encoding="utf-8") as file:
                json.dump(element, file, indent=4, ensure_ascii=False)


def Car_lookup(car_id):
    if(car_id == 0):
        return "Porsche 991 GT3 R"
    if(car_id == 1):
        return "Mercedes-AMG GT3 old"
    if(car_id == 2):
        return "Ferrari 488 GT3"
    if(car_id == 3):
        return "Audi R8 LMS"
    if(car_id == 4):
        return "Lamborghini Huracan GT3"
    if(car_id == 5):
        return "McLaren 650S GT3"
    if(car_id == 6):
        return "Nissan GT-R Nismo GT3"
    if(car_id == 7):
        return "BMW M6 GT3"
    if(car_id == 8):
        return "Bentley Continental"
    if(car_id == 9):
        return "Porsche 991 II GT3 Cup"
    if(car_id == 10):
        return "Nissan GT-R Nismo GT3 old"
    if(car_id == 11):
        return "Bentley Continental old"
    if(car_id == 12):
        return "AMR V12 Vantage GT3"
    if(car_id == 13):
        return "Reiter Engineering R-EX GT3"
    if(car_id == 14):
        return "Emil Frey Jaguar G3"
    if(car_id == 15):
        return "Lexus RC F GT3"
    if(car_id == 16):
        return "Lamborghini Huracan GT3 Evo"
    if(car_id == 17):
        return "Honda NSX GT3"
    if(car_id == 18):
        return "Lambo ST"
    if(car_id == 19):
        return "Audi R8 LMS Evo"
    if(car_id == 20):
        return "AMR V8 Vantage GT3"
    if(car_id == 21):
        return "Honda NSX GT3 Evo"
    if(car_id == 22):
        return "McLaren 720S GT3"
    if(car_id == 23):
        return "Porsche 991II GT3 R"
    if(car_id == 24):
        return "Ferrari 488 GT3 Evo"
    if(car_id == 25):
        return "Mercedes-AMG GT3"
    if(car_id == 26):
        return "Ferrari 488 Challenge Evo"
    if(car_id == 27):
        return "BMW M2 CS Racing"
    if(car_id == 28):
        return "Porsche 911 GT3 Cup (Type 992)"
    if(car_id == 29):
        return "Lamborghini Huracán Super Trofeo EVO2"
    if(car_id == 30):
        return "BMW M4 GT3"
    if(car_id == 31):
        return "Audi R8 LMS evo II"
    if(car_id == 32):
        return "Ferrari 296 GT3"
    if(car_id == 33):
        return "Lamborghini Huracan GT3 EVO 2"
    if(car_id == 34):
        return "Porsche 992 GT3 R"
    if(car_id == 35):
        return "McLaren 720S GT3 Evo"
    if(car_id == 36):
        return "Ford Mustang GT3"
    if(car_id == 50):
        return "Alpine A110 GT4"
    if(car_id == 51):
        return "Aston Martin Vantage GT4"
    if(car_id == 52):
        return "Audi R8 LMS GT4"
    if(car_id == 53):
        return "BMW M4 GT4"
    if(car_id == 55):
        return "Chevrolet Camaro GT4"
    if(car_id == 56):
        return "Ginetta G55 GT4"
    if(car_id == 57):
        return "KTM X-Bow GT4"
    if(car_id == 58):
        return "Maserati MC GT4"
    if(car_id == 59):
        return "McLaren 570S GT4"
    if(car_id == 60):
        return "Mercedes AMG GT4"
    if(car_id == 61):
        return "Porsche 718 Cayman GT4 Clubsport"
    if(car_id == 80):
        return "Audi R8 LMS GT2"
    if(car_id == 82):
        return "KTM XBOX GT2"
    if(car_id == 83):
        return "Maserati MC20 GT2"
    if(car_id == 84):
        return "Mercedes AMG GT2"
    if(car_id == 85):
        return "Porsche 911 GT2 RS CS Evo"
    if(car_id == 86):
        return "Porsche 935"
    return ""


def rank(dictio):
    x = {key: val for key, val in sorted(dictio.items(), key = lambda ele: ele[1], reverse=True)}
    y = {key: rank for rank, key in enumerate(sorted(x, key=x.get, reverse=True), 1)}
    return y


if __name__ == "__main__":
    stats()
