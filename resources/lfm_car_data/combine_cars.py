import json
import glob

# Assuming all json files follow the pattern 'car_name.json'
files = glob.glob('*.json')

combined_data = []

for file in files:
    if file != "combined_cars_sorted.json":
        car_name = file.split('.')[0]  # Get the car name from the file name
        with open(file, 'r') as f:
            data = json.load(f)
            for entry in data:
                entry['car_name'] = car_name  # Add the car_name field
            combined_data.extend(data)

# Sort the combined data by kilometers in descending order
combined_data_sorted = sorted(combined_data, key=lambda x: x['kilometers'], reverse=True)

# Save the sorted combined data to a new file
with open('combined_cars_sorted.json', 'w') as f:
    json.dump(combined_data_sorted, f, indent=4, ensure_ascii=False)
