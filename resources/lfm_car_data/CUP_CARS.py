import json
import glob

result = {}

GT2_CARS = ["Ferrari 488 Challenge Evo", "Lambo ST", "Lamborghini Huracán Super Trofeo EVO2", "Porsche 911 GT3 Cup (Type 992)", "Porsche 991 II GT3 Cup"]

for file in GT2_CARS:
    if file == "CUP_CARS.json":
        continue
    file = f"{file}.json"
    with open(file, "r") as f:
        #print(file)
        data = json.load(f)
        for item in data:
            id_ = item["id"]
            name = item["name"]
            if id_ not in result:
                result[id_] = {"name": name, "kilometers": 0, "laps": 0, "id": id_}
            #print(result[id_])
            result[id_]["kilometers"] += round(item["kilometers"], 3)
            result[id_]["laps"] += round(item["laps"], 3)


# Remove duplicates by keeping only the highest kilometers value
unique_result = {}
for id_, values in result.items():
    if id_ not in unique_result or values["kilometers"] > unique_result[id_]["kilometers"]:
        unique_result[id_] = values

# Sort the result by kilometers in descending order
sorted_result = sorted(unique_result.values(), key=lambda x: x["kilometers"], reverse=True)

# Write the sorted result to GT4.json
with open("CUP_CARS.json", "w") as f:
    json.dump(sorted_result, f, indent=4, ensure_ascii=False)

print("Result written to CUP_CARS.json")
