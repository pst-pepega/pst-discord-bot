#!/bin/bash

cd ~/pst-discord-bot/resources/lfm_car_data

# Run the first Python file with an argument
python3 lfm_car_data.py $1

# Run the second Python file
python3 GT4.py

# Run the third Python file
python3 GT2.py

python3 combine_cars.py

git add .
git commit -m "new car data."
git push