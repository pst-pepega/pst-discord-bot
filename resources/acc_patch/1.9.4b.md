### AMR V8 VANTAGE:
* -10kg; Power buff Donington

### BENTLEY 2018:
* Yaw update
* Power buff Donington; Kyalami, Indianapolis

### BMW M4:
* Increased braking torque
* -30kg Donington

### FERRARI 296:
* Increased fuel consumption
* Global turbo buff low range

### HONDA NSX EVO:
* Yaw update
* Suspension update
* -30kg; Power buff Donington

### LAMBORGHINI HURACAN EVO2:
* ARBs update

### MCLAREN 720S EVO:
* Lift coefficient update
* Power buff from 6000 to max Oulton Park, Donington, Snetterton; Watkins Glen, Circuit of The Americas; Suzuka, Laguna Seca, Indianapolis, Kyalami
* +20kg Donington

### MERCEDES AMG EVO:
* Yaw update
* +20kg; Power buff mid range Donington

### PORSCHE 992 GT3R:
* -40kg; Power nerf Donington