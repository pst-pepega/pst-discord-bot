### AMR V8 VANTAGE:
* -10kg; power adjustment Laguna Seca
* Power adjustement Suzuka
 
### AUDI R8 LMS EVO 2:
* -15kg Laguna Seca
 
### BENTLEY 2018:
* -20kg Monza
* -10kg Laguna Seca
 
### BMW M4:
* -35kg Monza
* -25kg Oulton Park
* -20kg Kyalami
 
### FERRARI 296:
* +30kg Mount Panorama
* +20kg Paul Ricard/Spa-Francorchamps/Kyalami; Turbo buff
 
### HONDA NSX EVO:
* -15kg Monza
 
### LAMBORGHINI HURACAN EVO2:
* Power nerf Monza
* -20kg Oulton Park
* -20kg Mount Panorama/Indianapolis
* -20kg Paul Ricard; Max fuel 120L
* -20kg Spa-Francorchamps; Power nerf
* -20kg Nurburgring/Barcelona; Max fuel 120L
* 20kg Brands Hatch, Zolder, Valencia/Misano, Zandvoort
* 10kg Snetterton 
* Max fuel 120L Kyalami
 
### MCLAREN 720S
* 10kg Mount Panorama
* 10kg EU C 2020
* Power buff Monza/Brands Hatch, Zolder, Valencia/EU C [???]; Oulton Park; Mount Panorama/Suzuka/Laguna Seca/Kyalami/Indianapolis
 
### MERCEDES AMG EVO:
* Reduced front & rear drag generation
* Turbo (ram-air) adjustment on all tracks