import sys

# Define the mappings
mappings = {
    "EU A": "Monza",
    "EU B1": "Paul Ricard",
    "EU B2": "Spa-Francorchamps",
    "EU C2020": "Imola, Silverstone (no 2021 update)",
    "EU C1": "Nurburgring",
    "EU C2": "Barcelona",
    "EU D0": "Hungaroring (no 2021 update)",
    "EU D1": "Brands Hatch, Zolder, Valencia",
    "EU D2": "Misano, Zandvoort",
    "IGT A": "Mount Panorama",
    "IGT C": "Suzuka",
    "IGT D": "Laguna Seca",
    "IGT E": "Kyalami",
    "IGT F": "Indianapolis",
    "BGT A": "Oulton Park",
    "BGT B": "Donington",
    "BGT D": "Snetterton",
    "GTAM B": "Watkins Glen",
    "GTAM C": "Circuit of The Americas"
}

# Read the input file
with open(sys.argv[1], 'r') as file:
    lines = file.readlines()

# Perform the replacements
updated_lines = []
for line in lines:
    for key, value in mappings.items():
        if key in line:
            line = line.replace(key, value)
    updated_lines.append(line)

# Write the updated content to a new file
with open(sys.argv[1].replace(".txt",".md"), 'w') as file:
    file.writelines(updated_lines)
