import json

def print_weather_data_from_json(file_path):
    with open(file_path, 'r') as json_file:
        weather_data = json.load(json_file)

    # Print header
    print("Date,Track,°C,Hour,Avg. °C,Clouds,Rain,Dry,Wet,Mixed")

    for entry in weather_data:
        print(f"{entry['Date']},{entry['Track']},{entry['°C']},{entry['Hour']},{entry['Avg. °C']},{entry['Clouds']},{entry['Rain']},{entry['Dry']},{entry['Wet']},{entry['Mixed']}")

if __name__ == "__main__":
    # Assuming your JSON file is named 'weather_data.json'
    print_weather_data_from_json('weather_data.json')
