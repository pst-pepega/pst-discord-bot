import requests
import lfm_race_data
import database
from datetime import datetime, timedelta, timezone
import logging
import pytz

async def add_race_to_database(race_id, description, channel_id):
    # Fetch JSON data from the API
    url = f"https://api3.lowfuelmotorsport.com/api/race/{race_id}"
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        # Extract the race_date field
        race_date_time = data.get("race_date", "Key not found")
        logging.info(f"Race Date: {race_date_time}")
    else:
        logging.info(f"Failed to fetch data. HTTP Status Code: {response.status_code}")

    race_info = lfm_race_data.get_race_reminder_info(race_id)

    # Calculate time differences
    cest_timezone = pytz.timezone("Europe/Berlin")  # Berlin follows CET/CEST
    utc_timezone = pytz.utc

    race_time = cest_timezone.localize(datetime.strptime(race_date_time, "%Y-%m-%d %H:%M:%S"))
    race_time = race_time.astimezone(utc_timezone)
    
    #race_time = datetime.strptime(race_date_time, '%Y-%m-%d %H:%M:%S')
    now = datetime.now(timezone.utc)

    hours_12 = (race_time - now) <= timedelta(hours=12)
    hours_6 = (race_time - now) <= timedelta(hours=6)
    hours_3 = (race_time - now) <= timedelta(hours=3)
    hour_1 = (race_time - now) <= timedelta(hours=1)
    minutes_30 = (race_time - now) <= timedelta(minutes=30)
    minutes_15 = (race_time - now) <= timedelta(minutes=15)

    # Generate SQL
    query = f"""
    INSERT INTO race_reminders (
        race_id, channel_id, hours_12, hours_6, hours_3, hour_1, minutes_30, minutes_15, race_date_time, race_info, description
    ) VALUES (
        '{race_id}', {channel_id}, {hours_12}, {hours_6}, {hours_3}, {hour_1}, {minutes_30}, {minutes_15}, '{race_time.replace(tzinfo=None) }', '{race_info}', '{description}'
    );
    """
    logging.info(query)

    pool = await database.create_pool()
    await database.execute_query(query, pool)

    ## fetch_values to fetch all of the race_reminders, then iterate over them