import requests
import logging

logger = logging.getLogger(__name__)

def format_name(vorname, nachname, max_length=30):
    combined_name = f"{vorname} {nachname}"
    if len(combined_name) > max_length:
        return combined_name[:max_length]
    else:
        return combined_name.ljust(max_length)

def get_user_info(search_query):
    # Make the API request
    result = ""
    api_url = f"https://api2.lowfuelmotorsport.com/api/search/{search_query}"
    response = requests.get(api_url)
    
    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        data = response.json()

        # Check if the response contains data and drivers
        if 'data' in data and 'drivers' in data['data']:
            drivers = data['data']['drivers']
            result_list = []

            # Check if there are any drivers in the response
            if drivers:
            # Iterate over each driver in the list
                for driver in drivers:
                    try:
                        # Extract information from the current driver
                        vorname = driver['vorname']
                        nachname = driver['nachname']
                        rating = driver['rating_by_sim'][0]['rating']
                        safety_rating = driver['safety_rating']

                        driver_dict = {
                            'Name': format_name(vorname, nachname),
                            'Rating': rating,
                            'Safety Rating': safety_rating
                        }
                    except:
                        logger.info("That user is fucketh.")
                    
                    # Append the result string to the result list
                    result_list.append(driver_dict)

                # Return the result list
                return result_list
                # Output the information

            else:
                print("No drivers found in the response.")
        else:
            print("Invalid response format.")
    else:
        print(f"Error: {response.status_code}")