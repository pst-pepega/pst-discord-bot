import datetime
import json


def getweather(input: int, offset: int = 0) -> dict:
    # List of objects with dates
    with open('resources/json/enduranceweather.json', 'r') as file:
        data = json.load(file)

    if input:
        return data[int(input)-1]
    # Get the current date
    current_date = datetime.date.today()

    # Check if today is Sunday (replace '6' with the day of the week you want, where Monday is 0 and Sunday is 6)
    if current_date.weekday() < 6:
        # Calculate the date of the upcoming Thursday
        days_until_thursday = (6 - current_date.weekday()) % 7
        thursday_date = current_date + datetime.timedelta(days=days_until_thursday)
    else:
        # It's Sunday, use the current date
        thursday_date = current_date

    thursday_date += datetime.timedelta(weeks=offset)

    # Convert the date to the format in your data (assuming it's in the format "dd/mm/yy")
    thursday_date_str = thursday_date.strftime("%d/%m/%y")

    # Search for the object with the matching date
    matching_object = None
    for obj in data:
        if obj["Date"] == thursday_date_str:
            matching_object = obj
            break

    # Print the matching object
    if matching_object:
        return matching_object
    else:
        return None
