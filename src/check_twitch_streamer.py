import os
import requests
from dotenv import load_dotenv

# Load .env variables
load_dotenv(dotenv_path=os.path.join(os.path.dirname(__file__), "..", ".env"))

# Get credentials
CLIENT_ID = os.getenv("TWITCH_CLIENT_ID")
CLIENT_SECRET = os.getenv("TWITCH_CLIENT_SECRET")


def check_live_status(STREAMER_NAME):

    # Request OAuth token
    auth_url = "https://id.twitch.tv/oauth2/token"
    auth_params = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "grant_type": "client_credentials",
    }

    response = requests.post(auth_url, data=auth_params)
    auth_response = response.json()
    access_token = auth_response["access_token"]


    # Check if the stream is live
    streamer_name = STREAMER_NAME
    headers = {
        "Client-ID": CLIENT_ID,
        "Authorization": f"Bearer {access_token}",
    }
    response = requests.get(
        f"https://api.twitch.tv/helix/streams?user_login={streamer_name}",
        headers=headers
    ).json()

    if response.get("data"):
        print(f"{streamer_name} is LIVE!")
    else:
        print(f"{streamer_name} is OFFLINE.")

check_live_status("xiSCHUBERT")