def get_username_by_id(user_id):
    try:
        with open("resources/user_data.txt", 'r') as file:
            for line in file:
                parts = line.strip().split(':')
                if len(parts) == 2:
                    id_in_file, name = parts
                    if user_id == id_in_file.strip():
                        return name.strip()
    except FileNotFoundError:
        # Handle file not found error here.
        return None
    return None
