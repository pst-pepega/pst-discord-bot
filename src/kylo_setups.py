import requests
import lfmhl
import logging
import os
from dotenv import load_dotenv


key = os.getenv("GITLAB_KEY")
logger = logging.getLogger(__name__)
# Set your GitLab project's ID
project_id = "48334246"
branch = "kylo"
access_token = key
headers = {"PRIVATE-TOKEN": access_token}
gitlab_api_url = "https://gitlab.com/api/v4/projects/{project_id}/repository/tree".format(project_id=project_id)

def get_response(path, page = 1):
    # Parameters for the API request
    params = {
        "path": path,
        "ref": branch,
        "page": page
    }   
    # Headers with the access token
    response = requests.get(gitlab_api_url, params=params, headers=headers)
    return response

def get_setup_link(track, car):
    # Make a GET request to the GitLab API with the access token
    response = get_response("")
    folder_list = response.json()
    #print(folder_list)
    folder_names = [folder['name'] for folder in folder_list]
    print(folder_names)

    car = lfmhl.find_closest_car(folder_names, car)


    # Make a GET request to the GitLab API with the access token
    response = get_response(f"{car}")
    folder_list = response.json()
    folder_names = [folder['name'] for folder in folder_list]
    print(folder_names)

    track = lfmhl.find_closest_car(folder_names, track)

    folder_path = (f"{car}/{track}")
    print(folder_path)

    # Make a GET request to the GitLab API with the access token
    response = get_response(folder_path)

    # Check if the response was successful
    if response.status_code == 200:
        files = response.json()

        # Initialize variables to track the last updated file and its commit date
        last_updated_file = None
        last_commit_date = None

        # Iterate through the files and fetch commit information
        for file in files:
            # Fetch commit information for the file
            commit_url = "https://gitlab.com/api/v4/projects/{project_id}/repository/commits".format(project_id=project_id)
            commit_params = {"path": file["path"], "ref_name": branch}
            commit_response = requests.get(commit_url, params=commit_params, headers=headers)

            if commit_response.status_code == 200:
                commit_data = commit_response.json()
                if commit_data:
                    # Extract the commit date (use the first commit as GitLab sorts in descending order)
                    commit_date = commit_data[0]["committed_date"]
                    if last_commit_date is None or commit_date > last_commit_date:
                        last_commit_date = commit_date
                        last_updated_file = file

        if last_updated_file:
            print("Last updated file:")
            print(last_updated_file)
        else:
            print("No files found in the specified folder.")

    else:
        print("Error: Unable to retrieve files")

    base_url = 'https://gitlab.com/Kylo_69/Setups/-/raw/'

    file_name = last_updated_file['name']

    link = f'{base_url}/kylo/{folder_path}/{file_name}?inline=false'

    print(link)

    return link