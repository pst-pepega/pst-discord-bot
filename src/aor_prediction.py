import csv
from tabulate import tabulate
import itertools

def format_csv_as_table(file_path, num_rows=15):
    with open(file_path, 'r', newline='') as csv_file:
        csv_reader = csv.reader(csv_file)
        headers = next(csv_reader)  # Assuming the first row contains column headers
        data = [row[:3] for row in itertools.islice(csv_reader, num_rows)]  # Selecting only the first three columns and first num_rows rows
    
    return tabulate(data, headers=headers[:3], tablefmt='grid')

# if __name__ == "__main__":
#     file_path = f"resources/aor_prediction_results/aor_s9.csv"
#     table_string = format_csv_as_table(file_path)
#     print(table_string)

def printerino(a):
    file_path = f"resources/aor_prediction_results/aor_s{a}.csv"
    table_string = format_csv_as_table(file_path)
    print(table_string)
    return table_string
