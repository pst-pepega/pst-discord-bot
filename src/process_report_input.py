import re

def process(input_string):
	# Define regular expressions to capture the variables
	name_pattern = re.compile(r'name:\s*(\S+)', re.IGNORECASE)
	sr_pattern = re.compile(r'sr:\s*([\d.]+)', re.IGNORECASE)
	tp_pattern = re.compile(r'tp:\s*([\d.]+)', re.IGNORECASE)
	ban_pattern = re.compile(r'ban:', re.IGNORECASE)
	pp_pattern = re.compile(r'penalty\s*point|pp:', re.IGNORECASE)
	permaban_pattern = re.compile(r'permaban:', re.IGNORECASE)
	btr_pattern = re.compile(r'back\sto\srookies|btr:', re.IGNORECASE)

	# Initialize variables with default values
	name = ""
	sr = ""
	tp = ""
	ban = 0
	pp = 0
	permaban = "FALSE"
	btr = "FALSE"

	# Extract variables using regular expressions
	name_match = name_pattern.search(input_string)
	sr_match = sr_pattern.search(input_string)
	tp_match = tp_pattern.search(input_string)
	ban_match = ban_pattern.search(input_string)
	pp_match = pp_pattern.search(input_string)
	permaban_match = permaban_pattern.search(input_string)
	btr_match = btr_pattern.search(input_string)

	# Assign values if matches were found
	if name_match:
		name = name_match.group(1)
	if sr_match:
		sr = sr_match.group(1)
	if tp_match:
		tp = tp_match.group(1)
	if ban_match:
		ban = ban_match
	if pp_match:
		pp = pp_match
	if permaban_match:
		permaban = "TRUE"
	if btr_match:
		btr = "TRUE"

	# Print the extracted variables
	return {	
		'name': name,
        'sr': sr,
        'tp': tp,
        'ban': ban,
        'pp': pp,
        'permaban': permaban,
        'btr': btr,
	}
