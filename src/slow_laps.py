import lfmhl
import time
from fuzzywuzzy import fuzz, process
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def convert_string_lap_to_float(lap):
	return round((int(lap[1:2]) * 60 + float(lap[3:])), 3)

def get_matching_car(input_text, car_list):
    # Dictionary to store car names and their matching scores
    car_scores = {}

    # Calculate the fuzzy matching score for each car in the list
    for car in car_list:
        score = fuzz.ratio(input_text.lower(), car.lower())
        car_scores[car] = score

    # Find the car with the highest matching score
    best_match = max(car_scores, key=car_scores.get)

    # Return the best-matching car and its score
    car_list = []
    car_list.append(best_match)
    return car_list

def get_records(input_car):
	still_available = ""

	track_list = lfmhl.get_all_tracks()
	car_list = {
		"Audi R8 LMS GT3 evo II 2022", 
		"AMR V8 Vantage 2019", 
		"Ferrari 296 GT3 2023", 
		"Bentley Continental 2018", 
		"Honda NSX GT3 Evo 2019", 
		"Lamborghini Huracan GT3 EVO 2 2023", 
		"Mercedes-AMG GT3 2020", 
		"BMW M4 GT3 2021", 
		"Porsche 992 GT3 R 2023", 
		"McLaren 720S GT3 Evo 2023",
		"Ford Mustang GT3",
		"Nissan GT-R Nismo GT3"
}
	if len(input_car) > 1:
		car_list = (get_matching_car(input_car, car_list))
		#print(car_list)

	for track in track_list:
		hotlap_data = lfmhl.get_hotlaps(track)
		world_record = lfmhl.world_record(track)
		for car in car_list:
			for hotlap in hotlap_data:
				#logger.info(f'{hotlap["car_name"]} {hotlap["car_year"]} {int(hotlap["gameversion"])}')
				if f'{hotlap["car_name"]} {hotlap["car_year"]}' == car and int(hotlap["gameversion"]) > 49:
					# Treat it as "Still available" and add or increment
					lap_time = hotlap['lap']
					#print(f"{convert_string_lap_to_float(lap_time)} > {convert_string_lap_to_float(world_record['lap'])}")
					if convert_string_lap_to_float(lap_time) > round((convert_string_lap_to_float(world_record['lap'])*1.01), 3):
						still_available += f"{car} + {track} — {hotlap['lap']}\n"
						#print(f"{car} + {track} — {hotlap['lap']}\n")
						#print("### ADDED TO LIST ###")
						#time.sleep(1)
					break
				
	return still_available

#print(get_records("Honda"))