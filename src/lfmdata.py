import json
# import urllib library
from urllib.request import urlopen 
import sys

#try:
#    for x in range(limit):
#        time = data[x]["race_date"]
#        car = data[x]["car_name"]
#        event_name = data[x]["event_name"]
#        start_pos = data[x]["start_pos"]
#        finishing_pos = data[x]["finishing_pos"]
#        ip = data[x]["incidents"]
#        elo = data[x]["rating_change"]
#        track = data[x]["track_name"]
#        sr = data[x]["sr_change"]
    #    print("You completed a " + event_name + " race on " + track + " with the " + car + ", you started " + start_pos + " and finished " + finishing_pos + ". You gained " + elo + " Elo and " + sr + " SR with " + ip + " incident points.\n")
#        tits = time + "\t" + car + "\t" + event_name + "\t" + track + "\t" + str(start_pos)  + "->" + str(finishing_pos) + "\t" + str(elo) + "\t" +  str(sr)  + "(" + str(ip) + ")"
#        print(tits)
#        counter += 1
#except:
#    print(str(counter) + "/" + str(limit) + " tracks")
# counter = 0

def find_tracks(data, limit):
    unique_tracks = []
    try:
        for x in range(limit):
            if data[x]["track_name"] not in unique_tracks:
                unique_tracks.append(data[x]["track_name"])
     #           print(data[x]["track_name"])
    except:
        print()
    return unique_tracks    


def count_tracks(unique_tracks, data, limit):
    track_counter = []

    for x in unique_tracks:
        track_counter.append(int(0))

    try:
        for x in range(limit):
            if(data[x]["track_name"]) in unique_tracks:
                # index = unique_tracks.index(data[x]["track_name"])
                track_counter[unique_tracks.index(data[x]["track_name"])] += 1
                # print(test =+ 1)
                # print(data[x]["track_name"] + " " + str(track_counter[unique_tracks.index(data[x]["track_name"])]))
    except:
       print()

    return track_counter

def find_cars(data, limit):
    unique_cars = []
    try:
        for x in range(limit):
            if data[x]["car_name"] not in unique_cars:
                unique_cars.append(data[x]["car_name"])
    except:
        print()
    return unique_cars        

def count_cars(unique_cars, data, limit):
    car_counter = []

    for x in unique_cars:
        car_counter.append(int(0))

    try:
        for x in range(limit):
            if(data[x]["car_name"]) in unique_cars:
                # index = unique_tracks.index(data[x]["track_name"])
                car_counter[unique_cars.index(data[x]["car_name"])] += 1
                # print(test =+ 1)
                # print(data[x]["track_name"] + " " + str(track_counter[unique_tracks.index(data[x]["track_name"])]))
    except:
       print()

    return car_counter

def Convert(lst1, lst2):
    it1 = iter(lst1)
    it2 = iter(lst2)
    res_dct = dict(zip(it1, it2))
    return res_dct

def Convert3(lst1, lst2, lst3):
    it1 = iter(lst1)
    it2 = iter(lst2)
    it3 = iter(lst3)
    res_dct = dict(zip(zip(it1, it2), it3))
    return res_dct    

def count_tracks_and_cars(unique_tracks, unique_cars, data, limit):
    counter = []

    for x in unique_tracks:
        for y in unique_cars:
            counter.append(int(0))

    try:
        for x in range(limit):
            if(data[x]["track_name"]) in unique_tracks:
                if(data[x]["car_name"] in unique_cars):
                # index = unique_tracks.index(data[x]["track_name"])
                    counter[(unique_cars.index(data[x]["car_name"])) * (len(unique_tracks)) + (unique_tracks.index(data[x]["track_name"]))] += 1
                # print(test =+ 1)
                # print(data[x]["track_name"] + " " + str(track_counter[unique_tracks.index(data[x]["track_name"])]))
    except:
       print()

#    for x in range(len(track_counter)):
#        print(unique_tracks[x] + " = " + str(track_counter[x]))

    return counter

def count_something(unique_tracks, unique_cars, something, data, limit):
    counter = []

    for x in unique_tracks:
        for y in unique_cars:
            counter.append(int(0))

    try:
        for x in range(limit):
            if(data[x]["track_name"]) in unique_tracks:
                if(data[x]["car_name"] in unique_cars):
                # index = unique_tracks.index(data[x]["track_name"])
                    counter[(unique_cars.index(data[x]["car_name"])) * (len(unique_tracks)) + (unique_tracks.index(data[x]["track_name"]))] += data[x][something]

                # print(test =+ 1)
                # print(data[x]["track_name"] + " " + str(track_counter[unique_tracks.index(data[x]["track_name"])]))
    except:
       print()

#    for x in range(len(track_counter)):
#        print(unique_tracks[x] + " = " + str(track_counter[x]))
    i = range(len(counter))
    for j in i:
          counter[j] = round(counter[j], 3)
    
    return counter


def find_car_track(cars, tracks):
    lst = []
    for x in tracks:
        for y in cars:
            input = (x + " at " + y)
            if input not in lst:
                lst.append(input)
    return lst

def per_race(key, count, data):
    values = []
    length = range(len(key)-1)
    for x in length:
        if(count[x] >= 3):
            values.append(round(data[x] / count[x], 3))

        else:
            values.append(-10000)

    return values

def start_finish(start, finish):
    values = []
    length = range(len(start)-1)
    for x in length:
        values.append(round(finish[x] - start[x], 3))
    return values;        

def Sort(dictio):
    dic2=dict(sorted(dictio.items(),key= lambda x:x[1], reverse=True))
    return dic2

def Sort_inverse(dictio):
    dic2=dict(sorted(dictio.items(),key= lambda x:x[1], reverse=False))
    return dic2    

def PruneDict(MyDict, number):
    MyDict = {key:val for key, val in MyDict.items() if val != number}
    return MyDict

def PruneNDict(MyDict, n):
    MyDict = dict(list(MyDict.items())[:n])
    return MyDict

def averageSOF(data, limit):
    i = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    counter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    j = 0
    counters = 0
    result = ""

    try:
        for x in range(limit):
            if(data[x]["user_split"] == 1):
                i[0] = i[0]+data[x]["sof"]
               # print(str(++counter[0]) + " " + str(data[x]["sof"]))
                counter[0] = counter[0] + 1
            else:
                i[data[x]["user_split"]-1] = i[data[x]["user_split"]-1]+data[x]["split" + str(data[x]["user_split"]) + "_sof"]
                #print(str(counter[data[x]["user_split"]-1]) + " " + str(data[x]["split" + str(data[x]["user_split"]) + "_sof"]))
                counter[data[x]["user_split"]-1] = counter[data[x]["user_split"]-1] + 1
    except:
        print()
    for x in range(10):
        if(counter[x] > 0):
            result += f"{counter[x]} Races in Split {x+1} with an average SOF of {(round(i[x]/counter[x], 3))}\n"
            j = j+i[x] 
            counters = counters + counter[x]
    result += (str(counters) + " Races. " + str(round(j/counters, 3)) + " Average SOF.\n")        
    return result

def EloRank(alldata, lfmid):
    counter1 = 0
    counter2 = 0
    elo = 0
    firstname = ""
    lastname = ""
    for x in alldata:
        if(x["id"] == lfmid):
            elo = x["cc_rating"]
            firstname = x["vorname"]
            lastname = x["nachname"]
    for x in alldata:
        counter1 += 1
        if(x["cc_rating"] > elo):
            counter2 += 1

    return (firstname + " " + lastname + ", your Elo is " + str(elo) + ", you're in the top " + str(round(counter2/counter1*100, 6)) + "%.")        

def countIP(data):
    counterIP = 0
    counterRace = 0
    counterCleanRace = 0

    for x in data:
        counterRace += 1
        counterIP += x["incidents"]
        if(x["incidents"] == 0):
            counterCleanRace += 1


    return (str(counterRace) + " Races, " + str(counterCleanRace) + " of them clean, " + str(counterIP) + " Incident Points, which makes " + str(round(counterIP/counterRace, 5)) + " Incidents per Race")


async def give_data(user_id: int, everything: bool):
        
    limit = 10000
    url = "https://api2.lowfuelmotorsport.com/api/users/getUsersPastRaces/" + str(user_id) + "?start=0&limit=" + str(limit)
    
    # store the response of URL
    response = urlopen(url)
    counter = 0
    data = json.loads(response.read())

    url = "https://api2.lowfuelmotorsport.com/api/statistics/safetyrating"
    response = urlopen(url)
    alldata = json.loads(response.read())
    all_data = ""
    compromised_data = ""
    try:
        if(len(sys.argv) > 1):
            print("LFM ID: " + sys.argv[1] + "\n Loading...\n")
    except:
        print("Did you forget your lfm id?")
        return

    unique_tracks = find_tracks(data, limit)
    unique_cars = find_cars(data, limit)
    unique_car_and_track = find_car_track(unique_tracks, unique_cars)
    
    track_counter = count_tracks(unique_tracks, data, limit)
    car_counter = count_cars(unique_cars, data, limit)
    car_and_track = count_tracks_and_cars(unique_tracks, unique_cars, data, limit)
    
    elo_car_track = count_something(unique_tracks, unique_cars, "rating_change", data, limit)
    sr_car_track = count_something(unique_tracks, unique_cars, "sr_change", data, limit)
    ip_car_track = count_something(unique_tracks, unique_cars, "incidents", data, limit)
    start_car_track = count_something(unique_tracks, unique_cars, "start_pos", data, limit)
    finish_car_track = count_something(unique_tracks, unique_cars, "finishing_pos", data, limit)

    elo_car_track_per_race = per_race(unique_car_and_track, car_and_track, elo_car_track)
    ip_car_track_per_race = per_race(unique_car_and_track, car_and_track, ip_car_track)
    sr_car_track_per_race = per_race(unique_car_and_track, car_and_track, sr_car_track)
    start_car_track_per_race = per_race(unique_car_and_track, car_and_track, start_car_track)
    finish_car_track_per_race = per_race(unique_car_and_track, car_and_track, finish_car_track)

    start_finish_data = start_finish(start_car_track_per_race, finish_car_track_per_race)

    if(everything):
        all_data += ("Races done in given track, car or track & car combo\n")
        all_data += (json.dumps(Sort(Convert(unique_tracks, track_counter)), indent=4, ensure_ascii=False))
        all_data += (json.dumps(Sort(Convert(unique_cars, car_counter)), indent=4, ensure_ascii=False))
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, car_and_track)), 0), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")

        all_data += ("Elo gain or loss per car & track combo\n")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, elo_car_track)), 0), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")

        all_data += ("Safety Rating gain or loss per car & track combo\n")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, sr_car_track)), 0), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")

        all_data += ("Incident Points per car & track combo\n")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, ip_car_track)), 0), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")
        
        all_data += ("Elo gain/loss per race for car+track with more or equal than 3 races")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, elo_car_track_per_race)), -10000), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")
        
        all_data += ("Incident Points per race for car+track with more or equal than 3 races")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, ip_car_track_per_race)), -10000), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")
        
        all_data += ("Safety Rating gain/loss per race for car+track with more or equal than 3 races")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, sr_car_track_per_race)), -10000), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")
        
        all_data += ("Average Start position per race for car+track with more or equal than 3 races")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, start_car_track_per_race)), -10000), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")
        
        all_data += ("Average Finish position per race for car+track with more or equal than 3 races")
        all_data += (json.dumps(PruneDict(Sort(Convert(unique_car_and_track, finish_car_track_per_race)), -10000), indent=4, ensure_ascii=False))
        all_data += ("_____________________________________________________________\n")
        
        all_data += ("Difference Start -> Finish Position per race for car+track with more or equal than 3 races")
        all_data += (json.dumps(Sort(Convert(unique_car_and_track, start_finish_data)), indent=4, ensure_ascii=False))
        all_data += "\n"
        all_data += countIP(data)
        all_data += "\n"
        all_data += EloRank(alldata, user_id)   
        all_data += "\n"
        all_data += ("Average SOF")   
        all_data += "\n"
        all_data += averageSOF(data, limit)

        return all_data

    else:
        compromised_data += ("Races done in given track, car or track & car combo\n")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_tracks, track_counter)),5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_cars, car_counter)),5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, car_and_track)),5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")

        compromised_data += ("Elo gain or loss per car & track combo\n")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, elo_car_track)),5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(PruneDict(Sort_inverse(Convert(unique_car_and_track, elo_car_track)), -10000), 5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")

        compromised_data += ("Safety Rating gain or loss per car & track combo\n")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, sr_car_track)),5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(PruneDict(Sort_inverse(Convert(unique_car_and_track, sr_car_track)), -10000), 5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")

        compromised_data += ("Elo gain/loss per race for car+track with more or equal than 3 races")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, elo_car_track_per_race)), 5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(PruneDict(Sort_inverse(Convert(unique_car_and_track, elo_car_track_per_race)), -10000), 5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")
        
        compromised_data += ("Incident Points per race for car+track with more or equal than 3 races")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, ip_car_track_per_race)), 5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(PruneDict(Sort_inverse(Convert(unique_car_and_track, ip_car_track_per_race)), -10000), 5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")
        
        compromised_data += ("Safety Rating gain/loss per race for car+track with more or equal than 3 races")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, sr_car_track_per_race)), 5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(PruneDict(Sort_inverse(Convert(unique_car_and_track, sr_car_track_per_race)), -10000), 5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")
        
        compromised_data += ("Average Start position per race for car+track with more or equal than 3 races")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, start_car_track_per_race)), 5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(PruneDict(Sort_inverse(Convert(unique_car_and_track, start_car_track_per_race)), -10000), 5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")
        
        compromised_data += ("Average Finish position per race for car+track with more or equal than 3 races")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, finish_car_track_per_race)), 5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(PruneDict(Sort_inverse(Convert(unique_car_and_track, finish_car_track_per_race)), -10000), 5), indent=4, ensure_ascii=False))
        compromised_data += ("_____________________________________________________________\n")
        
        compromised_data += ("Difference Start -> Finish Position per race for car+track with more or equal than 3 races")
        compromised_data += (json.dumps(PruneNDict(Sort(Convert(unique_car_and_track, start_finish_data)), 5), indent=4, ensure_ascii=False))
        compromised_data += (json.dumps(PruneNDict(Sort_inverse(Convert(unique_car_and_track, start_finish_data)), 5), indent=4, ensure_ascii=False))
        compromised_data += "\n"
        compromised_data += countIP(data)
        compromised_data += "\n"
        compromised_data += EloRank(alldata, user_id)   
        compromised_data += "\n"
        compromised_data += ("Average SOF")
        compromised_data += "\n"
        compromised_data += averageSOF(data, limit)

        return compromised_data