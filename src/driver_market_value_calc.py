import math
import requests
from datetime import datetime
import pandas as pd

def fetch_age(date):
    # Define the start date (January 1, 2007)
    start_date = datetime.strptime(date, "%d/%m/%Y")

    # Get the current date
    current_date = datetime.now()

    # Calculate the difference in years
    years_since = current_date.year - start_date.year

    # Check if we've passed the start date anniversary this year
    if (current_date.month, current_date.day) < (start_date.month, start_date.day):
        years_since -= 1

    return years_since
    

def fetch_user_stats(user_id):
    url = f"https://api2.lowfuelmotorsport.com/api/users/getUserStats/{user_id}"
    
    try:
        # Send GET request
        response = requests.get(url)
        # Check if request was successful
        response.raise_for_status()
        # Parse the JSON response
        data = response.json()
        return data
    except requests.exceptions.HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
    except Exception as err:
        print(f"An error occurred: {err}")
    return None


def fetch_elo_and_titles(user_id):
    url = f"https://api2.lowfuelmotorsport.com/api/users/getUserData/{user_id}"
    
    try:
        # Send GET request
        response = requests.get(url)
        # Check if request was successful
        response.raise_for_status()
        # Parse the JSON response
        data = response.json()
        elo = data['rating_by_sim'][0]['rating']
    except requests.exceptions.HTTPError as http_err:
        print(f"HTTP error occurred: {http_err}")
        return None
    except Exception as err:
        print(f"An error occurred: {err}")
        return None

    try:
        titles = data['winning_achievements']
        pro_series = 0
        other_titles = 0

        for title in titles:
            pro_series += check_pro_series(title['text'])

        for title in titles:
            other_titles += check_lfm_titles(title['text'])

        return elo, pro_series, other_titles
    
    except:
        return elo, 0, 0


def check_pro_series(text):
    # Check if "Caseking" is in the text and any of the other words
    if "Pro Series" in text and "Champion" in text:
        return 1
    if "Pro Series" in text and "second" in text:
        return 0.5
    if "Pro Series" in text and "third" in text:
        return 0.25
    return False


def check_lfm_titles(text):
    # Check if "Caseking" is in the text and any of the other words
    if "Champion" in text and not "Pro Series" in text:
        return 1
    #if "second" in text and not "Pro Series" in text:
    #    return 0.5
    #if "third" in text and not "Pro Series" in text:
    #    return 0.25
    return False


def calculate_score(lfm_trophies_P1, pro_series_titles, talent, podiums, poles, wins, races, elo, age):
    # Formula breakdown
    base_value = (
        (lfm_trophies_P1 * 500) + 
        (pro_series_titles * 10000) + 
        (talent * 700) + 
        (podiums * 10) + 
        (poles * 12) + 
        (wins / races * 1000) + 
        (elo * 0.85) - 
        (races * 5)
    )
    
    # Scale by 20:Age
    scaled_value = base_value * (20 / age)
    
    # Ceiling the value to the nearest 250
    final_value = math.ceil(scaled_value / 250) * 250
    
    return final_value



def calc(row) -> int:

    user_id = row['user_id']
    name = row['name']
    birthday = row['birthday']
    data = fetch_user_stats(user_id)

    # Data:
    talent = float(row['talent'])
    poles = data['poles']
    wins = data['wins']
    podiums = data['podium'] + wins
    races = data['starts']
    elo, pro_series_titles, lfm_trophies_P1 = fetch_elo_and_titles(user_id)
    age = fetch_age(birthday)


    score = calculate_score(lfm_trophies_P1, pro_series_titles, talent, podiums, poles, wins, races, elo, age)
    #print(name, lfm_trophies_P1, pro_series_titles, talent, podiums, poles, wins, races, elo, age)
    #print(f"The market value for {name}: {score}")
    return score
