import json
import requests
import logging
from Utils import get_lfm_week
from datetime import datetime
import lfm_penaltypoints
from constants import Series_to_ID

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_series_start_date(series):
    date = ""
    if series == 507: ## Sprint
        date = "2025-01-06 00:15:00"
    elif series == 505: ## GT3
        date = "2025-01-06 00:00:00"
    elif series == 513: ## Single
        date = "2025-01-06 17:00:00"
    elif series == 509: ## GT4
        date = "2025-01-06 16:00:00"
    elif series == 503: ## Pro
        date = "2025-01-09 19:00:00"
    return date


def get_standings(series: int, rows = 10) -> str:
    table_data = None
    logger.info(f"Series: {series} — Rows: {rows}")
    id = series

    # Specify the API/URL from which to fetch JSON data
    url = f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonStandings/{id}"  # Replace with the actual URL
    logger.info(url)

    # Fetch JSON data from the URL
    response = requests.get(url)
    if response.status_code == 200:
        json_data = response.json()
        logger.info("got data.")

        if "GT3" in json_data:
            logger.info("GT3 Data")
            # Extract data for the table
            table_data = json_data["GT3"]["1"]
        elif "TCX" in json_data:
            logger.info("M2 Data")
            # Extract data for the table
            table_data = json_data["TCX"]["1"]
        elif "GT4" in json_data:
            logger.info("GT4 Data")
            # Extract data for the table
            table_data = json_data["GT4"]["1"]
        else:
            logger.info("OVERALL Data")
            # Extract data for the table
            table_data = json_data["OVERALL"]["1"]

    else:
        logging.info(f"Failed to fetch data. Status code: {response.status_code}")
        return "The API didn't respond :("
    
    standings_data = dict()
    found_driver = None
    #print(table_data)

    if table_data:
        max_weeks = table_data[0]['weeks_counted']
        logger.info(max_weeks)
        max_weeks = get_lfm_week.calculate_current_week(get_series_start_date(series), datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        logger.info(max_weeks)
        logger.info(f"Max Weeks: {max_weeks}")
        cleaned_scores = get_cleaned_score(series, max_weeks)
        logger.info(max_weeks)
        for entry in table_data:
            if cleaned_scores:
                for rank, (user_id, total_score) in enumerate(cleaned_scores, start=1):
                    if int(user_id) == entry['user_id']:
                        found_driver = (user_id, total_score, rank)
            if found_driver:
                cleaned_score = found_driver[1]
            else:
                cleaned_score = "??"

            standings_data[entry['user_id']] = (entry['vorname'], entry['nachname'], entry['races'], entry['weeks_counted'], entry['points'], cleaned_score, entry['elo'], entry['origin'])
            logger.info(standings_data[entry['user_id']])

            # Sort standings_data by the 6th key (index 5 in a zero-based index)
    sorted_standings = dict(sorted(standings_data.items(), key=lambda x: x[1][5], reverse=True))

    # Print the sorted standings
    #for driver, data in sorted_standings.items():
        #print(f"{driver}: {data}")
  
    # Format header names
    position = "Pos."
    vorname = "Firstname"
    nachname = "Lastname"
    races = "Races"
    weeeks_counted = "Weeks"
    points = "Points"
    elo = "Elo"
    origin = "Origin"
    cleaned = "Points*"

    # Print formatted headers
    result = f"{position:4}\t{vorname[:15]:<15}\t{nachname[:15]:<15}\t{races:5}\t{weeeks_counted:5}\t{points:6}\t{cleaned:8}\t{elo:4}\t{origin[:6]:<6}\n"
    limiter = rows
    counter = 0
    # Print data
    for driver, data in sorted_standings.items():
        row = [
        f"{counter+1:4}",    # 2-character wide for position
        f"{data[0][:15]:<15}",  # 10-character wide for vorname
        f"{data[1][:15]:<15}",  # 10-character wide for nachname
        f"{data[2]:5}",  # 2-character wide for races
        f"{data[3]:5}",  # 1-character wide for weeks counted
        f"{data[4]:6}",  # 4-character wide for points
        f"{data[5]:8}", # cleaned score
        f"{data[6]:4}",  # 4-character wide for elo
        f"{data[7][:6]:<6}",  # 5-character wide for origin
    ]
        result += "\t".join(row)
        result += "\n"
        counter += 1
        logger.info(f"{counter} — {limiter}")
        if counter == limiter:
            return result
    return result


def get_cleaned_score(series, weeks):
    id = series  
    logger.info(f"{id} - {weeks}")
    if series == "Multiclass" or series == "Duo":
        weeks = weeks-2
    else:
        weeks = weeks-4
    logger.info(f"-- Weeks: {weeks} --")

     # Specify the API/URL from which to fetch JSON data
    url = f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonStandings/{id}"  # Replace with the actual URL
    logger.info(url)

    # Fetch JSON data from the URL
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        logger.info("got data.")

        car_class = ""

        series_name = Series_to_ID.get_name(series)
        
        if series_name == "GT4":
            logger.info("GT4 Data")
            # Extract data for the table
            car_class = "GT4"
            # Extracting individual driver scores
            driver_scores = get_driver_scores(data, car_class, weeks)
        elif series_name == "M2":
            logger.info("M2 Data")
            # Extract data for the table
            car_class = "TCX"
            # Extracting individual driver scores
            driver_scores = get_driver_scores(data, car_class, weeks)
        else:
            try:
                logger.info("GT3 Data")
                # Extract data for the table
                car_class = "GT3"       
                # Extracting individual driver scores
                driver_scores = get_driver_scores(data, car_class, weeks)
            except:
                logger.info("OVERALL Data")
                # Extract data for the table
                car_class = "OVERALL"       
                # Extracting individual driver scores
                driver_scores = get_driver_scores(data, car_class, weeks)



        dirty_list = lfm_penaltypoints.get_point_reduction_list()

        for user_id, points in driver_scores.items():
            temp = int(user_id)
            if dirty_list:
                if temp in dirty_list:
                    driver_scores[user_id] = round(points*0.9,1)

        # Order the drivers by their total score
        sorted_drivers = sorted(driver_scores.items(), key=lambda x: x[1], reverse=True)
        #print(sorted_drivers)
        return sorted_drivers
    
def get_driver_scores(data, car_class, weeks):
    driver_scores = {}

    for driver_data in data[car_class]["1"]:
        user_id = f'{driver_data["user_id"]}'  
        weekly_scores = [driver_data[f"week_{i}"] for i in range(1, 13)]
        best_three_scores = sorted(weekly_scores, reverse=True)[:weeks]
        if user_id not in driver_scores:
            driver_scores[user_id] = 0
        driver_scores[user_id] += sum(best_three_scores)

    return driver_scores
    
def get_csv():
    series = "pro"
    table_data = None
    id = Series_to_ID.get_id("Pro")    

    # Specify the API/URL from which to fetch JSON data
    url = f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonStandings/{id}"  # Replace with the actual URL
    logger.info(url)

    # Fetch JSON data from the URL
    response = requests.get(url)
    if response.status_code == 200:
        json_data = response.json()
        logger.info("got data.")

        if ( 
            id == 206 or
            id == 205 or
            id == 204
        ):
            logger.info("GT3 Data")
            # Extract data for the table
            table_data = json_data["GT3"]["1"]
        elif id == 210:
            logger.info("M2 Data")
            # Extract data for the table
            table_data = json_data["TCX"]["1"]
        elif id == 209:
            logger.info("GT4 Data")
            # Extract data for the table
            table_data = json_data["GT4"]["1"]

    else:
        print(f"Failed to fetch data. Status code: {response.status_code}")
        return "The API didn't respond :("
    
    standings_data = dict()
    found_driver = None
    if table_data:
        for entry in table_data:
            standings_data[entry['user_id']] = (entry['vorname'], entry['nachname'], entry['week_1'], entry['week_2'], entry['week_3'], entry['week_4'], entry['week_5'], entry['week_6'], entry['week_7'], entry['week_8'], entry['week_9'], entry['week_10'], entry['week_11'], entry['week_12'], entry['points'])
    
        # Define the headers for the CSV file
    headers = ['User ID', 'First Name', 'Last Name', 'Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7', 'Week 8', 'Week 9', 'Week 10', 'Week 11', 'Week 12', 'Total Points']

    import csv
    # Write data to CSV file
    with open('standings.csv', 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(headers)  # Write headers
        for user_id, data in standings_data.items():
            row = [user_id] + list(data)
            writer.writerow(row)


#print(get_standings(509))