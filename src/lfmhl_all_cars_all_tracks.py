import lfmhl
from datetime import datetime
import re
import json
import operator


def format_time(time_str: str) -> float:
    # Use a regular expression to extract the time part
    match = re.search(r'(\d{2}:\d{2}.\d{3})', time_str)
    
    if match:
        # Extract the matched time string
        time_part = match.group(1)
        
        # Parse the time string into a datetime object
        time_obj = datetime.strptime(time_part, "%M:%S.%f")
        
        # Calculate the total seconds
        total_seconds = (time_obj.minute * 60 +
                         time_obj.second + time_obj.microsecond / 1e6)
        
        return total_seconds
    else:
        # Handle cases where no valid time is found
        raise ValueError("No valid time found in the input string")


def get_data():
	track_list = lfmhl.get_all_tracks()

	Records = dict()

	for track in track_list:
		Records[track] = lfmhl.car_records(track)

	car_data = {}

	# Extract and process the data
	for circuit, cars in Records.items():
		for car, time_str in cars.items():
			total_seconds = format_time(time_str)
			
			if car not in car_data:
				car_data[car] = {
					'Count': 1,
					'Total Time': total_seconds,
				}
			else:
				car_data[car]['Count'] += 1
				car_data[car]['Total Time'] += total_seconds
				car_data[car]['Total Time'] = round(car_data[car]['Total Time'], 3)          
				

	# Calculate averages
	for car, stats in car_data.items():
		stats['Average Time'] = round(stats['Total Time'] / stats['Count'], 3)

	# Convert the dictionary to a list
	car_list = [{'Car': car, **stats} for car, stats in car_data.items()]

	# Convert to JSON
	json_data2 = json.dumps(car_list, indent=4)
	json_data = eval(json_data2)

	# Print or save the JSON data
	sorted_data = sorted(json_data, key=lambda x: (-x["Count"], x["Total Time"]))

	return sorted_data
	# Now, sorted_data contains your JSON data sorted by "Count" and then by "Total Time" as a secondary criterion