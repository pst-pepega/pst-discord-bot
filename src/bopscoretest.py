import bop
import pandas as pd

def get_score(track):
    track_bop = bop.get_bop(track)
    lap_values = track_bop.loc[track_bop['relevant'] == 'X', 'lap'].tolist() # type: ignore
    bop_values = track_bop.loc[track_bop['relevant'] == 'X', 'bop'].tolist() # type: ignore
    for x in range(len(bop_values)):
        bop_values[x] = int(bop_values[x])

    average_bop = (sum(bop_values)/len(bop_values))

    for x in range(len(lap_values)):
        lap_values[x] = float(bop.time_to_seconds(lap_values[x]))

    kg = (bop.get_kg_per_second(track))
    average_lap = (sum(lap_values)/len(lap_values))
    overflow = (average_lap - lap_values[0]) / kg
    lap_time_diff = lap_values[0] - lap_values[len(lap_values)-1]

    score = (100 + (average_bop) - overflow - int(bop_values[0]) - lap_time_diff)/10 + 40/int(bop_values[0])
    print(f"(100 + {average_bop} - {overflow} - {bop_values[0]} - {lap_time_diff})/10 + 40/{bop_values[0]}")
    # (100 + (-0.1) - 14 - 14 - 0.699)/10 + 40/14
    return score

def get_all():
    return_string = ""
    track_score_dict = {}
    adjust = 40

    track_list = bop.get_all_tracks()
    # Calculate the score for each track and store it in the dictionary
    if track_list:
        for track in track_list:
            print(track)
            score = get_score(track)
            track_score_dict[track] = round(score, 3)

    # Sort the dictionary by scores in descending order
    sorted_track_score = dict(sorted(track_score_dict.items(), key=lambda item: item[1], reverse=True))

    # Print the sorted results
    for track, score in sorted_track_score.items():
        return_string += (f"{track.ljust(adjust)}: {score}")