from collections.abc import Sequence
import math
from LfmAPI import LfmApi, LfmRace

import json
from urllib.request import urlopen
import matplotlib.pyplot as plt
import csv

import lfmsearch


def compute_elo_gain(drivers: Sequence[int], elo: int, factor: float) -> list[float]:
    rBase = 1600 / math.log(2)
    elogain = 0
    drivers_count = len(drivers)

    elo_gains = []
    for y in range(1, drivers_count + 1):
        elogain = 0
        fudgeFactor = ((drivers_count - 0) / 2 - y) / 100
        for entry in drivers:
            if entry != elo:
                a = (1 - math.exp(-elo / rBase)) * math.exp(-entry / rBase)
                b = (1 - math.exp(-entry / rBase)) * math.exp(-elo / rBase)
                c = (1 - math.exp(-elo / rBase)) * math.exp(-entry / rBase)
                score = a / (b + c)
                elogain = elogain + score

        elo_gains.append(
            (drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * factor
        )

    return elo_gains


def generate_message(
    race_data: LfmRace, elo_gains: Sequence[float], short: bool
) -> str:
    message = f"\nRace at {race_data.track.name} at {race_data.date}"
    message += f"\n{race_data.server_settings.settings['data']['serverName']}"
    message += f"\nK-factor: {race_data.event.k_factor}"

    for i, elo_gain in enumerate(elo_gains):
        if short and (i != 1 and abs(elo_gain) > 20):
            continue
        message += f"\nP {i}: {round(elo_gain, 1)}"

    return message


def calculate_elo_by_id(race: int, id: int, short: bool = False) -> str:
    data = LfmApi().get_race(race)
    if data is None:
        return f"Failed get race data for #{race}"
    
    car_class = ""
    drivers_elo = []
    driver = -1
    for entry in data.participants.entries:
        if entry.user_id == id:
            car_class = entry.car_class

    for entry in data.participants.entries:
        if entry.car_class == car_class:
            drivers_elo.append(entry.elo)
        if entry.user_id == id:
            driver = entry.elo

    elo_gains = compute_elo_gain(drivers_elo, driver, data.event.k_factor)
    return generate_message(data, elo_gains, short)


def calculate_elo_by_name(
    race: int, firstname: str, lastname: str, short: bool = False
) -> str:
    data = LfmApi().get_race(race)
    if data is None:
        return f"Failed get race data for #{race}"

    car_class = ""
    drivers_elo = []
    driver = -1
    for entry in data.participants.entries:
        if entry.first_name == firstname and entry.last_name == lastname:
            car_class = entry.car_class

    for entry in data.participants.entries:
        if car_class == entry.car_class:
            drivers_elo.append(entry.elo)
        if entry.first_name == firstname and entry.last_name == lastname:
            driver = entry.elo

    if driver == -1:
        return "Driver not found"

    elo_gains = compute_elo_gain(drivers_elo, driver, data.event.k_factor)
    return generate_message(data, elo_gains, short)


def main(race, name_or_id: bool, short: bool, id_firstname: str | int, lastname: str = ""):
    return_string = ""
    small_result_string = ""
    basic_info = ""

    data = LfmApi().get_race(race)
    if data is None:
        return

    basic_info += f"\nRace at {data.track.name} at {data.date}"
    basic_info += f"\n{data.server_settings.settings['data']['serverName']}"

    drivers = []
    driver = 0
    car_class = ""
    
    for entry in data.participants.entries:
        if int(name_or_id) == 1 and entry.user_id == int(id_firstname):
            car_class = entry.car_class
        if (
            int(name_or_id) == 0
            and entry.first_name == id_firstname
            and entry.last_name == lastname
        ):
            car_class = entry.car_class

    for entry in data.participants.entries:
        if car_class == entry.car_class:
            drivers.append(entry.elo)
        if int(name_or_id) == 1 and entry.user_id == int(id_firstname):
            driver = entry.elo
            basic_info += f"\nYour user is {entry.first_name} {entry.last_name} with {entry.elo} elo."

        if (
            int(name_or_id) == 0
            and entry.first_name == id_firstname
            and entry.last_name == lastname
        ):
            driver = entry.elo
            basic_info += f"\nYour user is {entry.first_name} {entry.last_name} with {entry.elo} elo."

    rBase = 1600 / math.log(2)
    elogain = 0
    k_factor = data.event.k_factor
    drivers_count = len(drivers)

    basic_info += "\n" + ("K-factor: " + str(k_factor))

    for y in range(1, drivers_count + 1):
        elogain = 0
        fudgeFactor = ((drivers_count - 0) / 2 - y) / 100
        for entry in drivers:
            if entry != driver:
                a = (1 - math.exp(-driver / rBase)) * math.exp(-entry / rBase)
                b = (1 - math.exp(-entry / rBase)) * math.exp(-driver / rBase)
                c = (1 - math.exp(-driver / rBase)) * math.exp(-entry / rBase)
                score = a / (b + c)
                elogain = elogain + score

        elogain = (
            (drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * k_factor
        )

        if y == 1 or abs(elogain) < 20:
            small_result_string += f"P{str(y)}: {str(round(elogain, 4))} \n"

        return_string += f"\nP{y}: {round(elogain, 4)}"

    return (basic_info, small_result_string, return_string)
    

def calc_for_position(race: int, lfm_name: str, position: int, car_class: str = "GT3") -> str:
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race}"
    return_string = ""
    small_result_string = ""
    basic_info = ""

    response = urlopen(url)
    data = json.loads(response.read())

    basic_info += f"\n{data['track']['track_name']} at {data['race_date']}"

    # drivers = [5368, 5018, 4922, 4726, 4697, 4319, 4170, 3799, 3633, 3624, 3409, 3322, 3092, 3060, 2913, 2757, 2735, 2701, 2611, 2516, 2372, 2368, 2358, 2299, 2290, 2277, 2257, 2255, 2249, 2232, 2226, 2190, 2168, 2102, 2097]
    drivers = []
    driver = 0

    for x in data["participants"]["entries"]:
        if f'{x["vorname"]} {x["nachname"]}' == lfm_name:
            car_class = x["car_class"]

    for x in data["participants"]["entries"]:
        if car_class == x['car_class']:
            drivers.append(x["elo"])
        if f'{x["vorname"]} {x["nachname"]}' == lfm_name:
            driver = x["elo"]
            basic_info += f" with {x['vorname']} {x['nachname']} [{x['elo']}] — "

    if driver == 0:
        drivers_data = lfmsearch.get_user_info(lfm_name)
        if drivers_data is None:
            return "Driver not found"
        else:
            drivers_data = drivers_data[:1]
            print(drivers_data)
            driver = drivers_data[0]["Rating"]
            basic_info += f" with {drivers_data[0]['Name'].replace('    ','')} [{drivers_data[0]['Rating']}] — "
            drivers.append(driver)
    
    rBase = 1600 / math.log(2)
    elogain = 0
    k_factor =  float(data["event"]["k_factor"])
    drivers_count = len(drivers)
    print(drivers_count)

    for y in range(1, drivers_count+1):    
        elogain = 0
        fudgeFactor = ((drivers_count - 0) / 2 - y) / 100    
        for x in drivers:
            if (x != driver):
                a = (1 - math.exp(-driver / rBase)) * math.exp(-x / rBase)
                b = (1 - math.exp(-x / rBase)) * math.exp(-driver / rBase)
                c = (1 - math.exp(-driver / rBase)) * math.exp(-x / rBase)
                score = a / (b + c)
                #score = (drivers_count - driver_pos - score - fudgeFactor) * 200 / drivers_count * k_factor
                # print(str(score))
                elogain = elogain + score
        
        elogain = (drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * k_factor
        #print(abs(elogain))
        #print(y)

        if y == 1 or abs(elogain) < 20:
            small_result_string += f"P{str(y)}: {str(round(elogain, 4))} \n"            

        if y == position:
            return_string += f"For Position {y}, you gain {round(elogain, 4)}"

    return f"{basic_info} {return_string}" 


def calculate_per_class(race, id, car_class):
    car_class = car_class.upper()
    return_string = ""
    small_result_string = ""
    basic_info = ""

    data = LfmApi().get_race(race)
    if data is None:
        return

    basic_info += f"\nRace at {data.track.name} at {data.date}"
    basic_info += f"\n{data.server_settings.settings['data']['serverName']}"

    drivers = []
    driver = 0

    for entry in data.participants.entries:
        if entry.car_class == car_class:
            drivers.append(entry.elo)
        if entry.user_id == int(id):
            driver = entry.elo
            basic_info += f"\nYour user is {entry.first_name} {entry.last_name} with {entry.elo} elo, you are driving the {entry.car_class} class."

    rBase = 1600 / math.log(2)
    elogain = 0
    k_factor = data.event.k_factor
    drivers_count = len(drivers)

    basic_info += "\n" + ("K-factor: " + str(k_factor))

    for y in range(1, drivers_count + 1):
        elogain = 0
        fudgeFactor = ((drivers_count - 0) / 2 - y) / 100
        for entry in drivers:
            if entry != driver:
                a = (1 - math.exp(-driver / rBase)) * math.exp(-entry / rBase)
                b = (1 - math.exp(-entry / rBase)) * math.exp(-driver / rBase)
                c = (1 - math.exp(-driver / rBase)) * math.exp(-entry / rBase)
                score = a / (b + c)
                elogain = elogain + score

        elogain = (
            (drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * k_factor
        )

        if y == 1 or abs(elogain) < 20:
            small_result_string += f"P{str(y)}: {str(round(elogain, 4))} \n"

        return_string += f"\nP {y}: {round(elogain, 4)}"
    
    return (basic_info, small_result_string, return_string)


def table(race, car_class):
    basic_info = ""
    drivers_dict = dict()

    data = LfmApi().get_race(race)
    if data is None:
        return

    basic_info += f"\nRace at {data.track.name} at {data.date}"
    basic_info += f"\n{data.server_settings.settings['data']['serverName']}"

    for entry in data.participants.entries:
        if entry.car_class == car_class:
            name = f"{entry.first_name} {entry.last_name}"
            name = name.split(' |')[0]
            drivers_dict[name] = entry.elo

    rBase = 1600 / math.log(2)
    elogain = 0
    k_factor = data.event.k_factor
    drivers_count = len(drivers_dict)

    basic_info += "\n" + ("K-factor: " + str(k_factor))
    result_list = []
    for name, elo in drivers_dict.items():
        temp_list = []
        driver = elo
        for y in range(1, drivers_count + 1):
            elogain = 0
            fudgeFactor = ((drivers_count - 0) / 2 - y) / 100
            for name2, elo2 in drivers_dict.items():
                if elo2 != driver:
                    a = (1 - math.exp(-driver / rBase)) * math.exp(-elo2 / rBase)
                    b = (1 - math.exp(-elo2 / rBase)) * math.exp(-driver / rBase)
                    c = (1 - math.exp(-driver / rBase)) * math.exp(-elo2 / rBase)
                    score = a / (b + c)
                    elogain = elogain + score
            elogain = (
                (drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * k_factor
            )
            temp_list.append(round(elogain, 1))
        result_list.append(temp_list)
    return (result_list, drivers_dict)

import os
def get_image(race, car_class = "GT3"):
    car_class = car_class.upper()
    data, drivers = table(race, car_class) # type: ignore
    with open('data.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(data)

    fontsize = 800 / len(drivers)
    # Sample data dictionary 
    data_dict = drivers

    # Open CSV file for writing
    with open('output.csv', 'w', newline='') as f:

        # Create CSV writer
        writer = csv.writer(f)

    # Write header row 
        writer.writerow(["Driver"] + list(range(1,len(drivers)+1)))
    
    # Write first column from data dict
    #writer.writerow(list(data_dict.values()))

        # Read CSV data line by line
        with open('data.csv') as csv_file:
            csv_reader = csv.reader(csv_file)

            # Write each line, prepend column from data dict using index
            for i, row in enumerate(csv_reader):  
                writer.writerow([list(data_dict.keys())[i]] + row)

    data = []
    with open('output.csv') as f:
        reader = csv.reader(f)
        headers = next(reader)[1:] # slice from index 1
        for row in reader:
            data.append(row)
            #print(row)


    fig, ax = plt.subplots(figsize=(50,30))

    ax.set_xticks(range(len(headers)))
    #print(headers)
    ax.set_xticklabels(headers, fontsize=max(fontsize/4, 20), rotation=45) 
    ax.set_xlim(-0.5, len(headers)-0.5) 

    print(data)
    
    ax.set_yticks(range(len(data)))

    labels = [row[0].replace(' ', '\n', 1) for row in data]
    print(labels)
    ax.set_yticklabels(labels, fontsize=max(fontsize/4, 20))
    #ax.set_yticks(range(len(data)))  
    #ax.set_yticklabels([row[0] for row in data], fontsize=fontsize)
    ax.set_ylim(-0.5, len(data)-0.5)


    for i, row in enumerate(data):
    #print(row)
        for j, val in enumerate(row):

            if j == 0:
                continue

            if isinstance(float(val), float):   

                color = 'green' if float(val) > 0 else 'red'

                ax.broken_barh([(j-1.5, 1)], (i-0.5, 1), facecolors=color)
                ax.text(j-1, i, val, 
                            ha='center', va='center', 
                            fontsize=fontsize) # increase font size
    plt.savefig('elo_distribution.png', bbox_inches='tight')
    os.remove("data.csv")
    os.remove("output.csv")
    print(fontsize)
