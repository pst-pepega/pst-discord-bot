import csv
import requests
from operator import itemgetter
from datetime import datetime

# Fetch the CSV data from the URL
url = "https://gitlab.com/pst-pepega/pst-scripts/-/raw/71dde1447326df0d3ab0e03f0c04667fa046613e/lfm_driver_market_value/driver_data.csv"
response = requests.get(url)
decoded_content = response.content.decode('utf-8').splitlines()

# Read the CSV data
reader = csv.DictReader(decoded_content)

# Function to format a row with specific width constraints
def format_row_birthday_talent(name, birthday, talent):
    name = (name[:32] + '...') if len(name) > 35 else name.ljust(35)
    birthday = birthday.ljust(10)
    talent = f"{float(talent):.1f}".rjust(9)
    return f"{name}{birthday}{talent}"

# Parse the birthday to a consistent format
def parse_birthday(birthday_str):
    for fmt in ("%d/%m/%Y", "%d/%m/%y"):
        try:
            return datetime.strptime(birthday_str, fmt)
        except ValueError:
            continue
    return None  # Return None if no valid format found

# Collect and sort data by either 'birthday' or 'talent'
def sort_and_print_birthday_talent(data, sort_by):
    message = ""
    if sort_by == 'birthday':
        # Parse and sort by birthday as datetime
        sorted_data = sorted(data, key=lambda row: parse_birthday(row['birthday']) or datetime.max)
    elif sort_by == 'talent':
        # Convert talent to float and sort
        sorted_data = sorted(data, key=lambda row: float(row['talent']), reverse=True)
    else:
        print("Invalid sort_by value. Use 'birthday' or 'talent'.")
        return
    
    # Print the formatted rows
    message += (f"{'Name'.ljust(35)}{'Birthday'.ljust(10)}{'Talent'.rjust(9)}\n")
    message += ('-' * 54)
    message += "\n"
    for row in sorted_data:
        message += (format_row_birthday_talent(row['name'], row['birthday'], row['talent']))
        message += "\n"

    return message

# Convert CSV data into a list of dictionaries, excluding 'user_id'
driver_data = [row for row in reader if 'user_id' in row]

# Sort by 'birthday' or 'talent' (change as needed)
sort_by = 'talent'  # or 'birthday'
message = sort_and_print_birthday_talent(driver_data, sort_by)
print(message)

# Fetch the CSV data from the URL
url = "https://gitlab.com/pst-pepega/pst-scripts/-/raw/71dde1447326df0d3ab0e03f0c04667fa046613e/lfm_driver_market_value/scores.csv"
response = requests.get(url)
decoded_content = response.content.decode('utf-8').splitlines()

# Read the CSV data
reader = csv.DictReader(decoded_content)

# Function to format a row with specific width constraints
def format_row_score(name, score):
    name = (name[:32] + '...') if len(name) > 35 else name.ljust(35)
    score = f"{float(score):.1f}".rjust(9)
    return f"{name}{score}"

# Collect and sort data by 'score'
def sort_and_print_score(data):
    message = ""
    # Convert score to float and sort
    sorted_data = sorted(data, key=lambda row: float(row['score']), reverse=True)
    
    # Print the formatted rows
    message += (f"{'Name'.ljust(35)}{'Score'.rjust(9)}\n")
    message += ('-' * 44)
    message += "\n"
    for row in sorted_data:
        message += (format_row_score(row['name'], row['score']))
        message += "\n"

    return message

# Convert CSV data into a list of dictionaries
driver_data = [row for row in reader]

# Sort by 'score' and print
message = sort_and_print_score(driver_data)
print(message)
