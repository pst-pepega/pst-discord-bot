import asyncio
from concurrent.futures import ProcessPoolExecutor
import time

def cpu_bound_task(n):
    # Simulate a CPU-bound task
    print(f"Starting CPU-bound task with argument {n}")
    time.sleep(2)
    print(f"Completed CPU-bound task with argument {n}")
    return n * n

async def task_processor(queue, executor):
    while True:
        task = await queue.get()
        if task is None:
            # If None is received, it's a signal to shut down
            break
        n = task
        result = await asyncio.get_event_loop().run_in_executor(executor, cpu_bound_task, n)
        print(f"Processed task: {n}, Result: {result}")
        queue.task_done()

async def main():
    queue = asyncio.Queue()
    executor = ProcessPoolExecutor()

    # Start the task processor
    processor = asyncio.create_task(task_processor(queue, executor))

    # Add tasks to the queue
    for i in range(21):
        await queue.put(i)

    # Other tasks can be performed here
    print("Main runner is doing other things.")

    for i in range(11):
        print(f"Sleeping for {i} seconds.")
        await asyncio.sleep(i)

    # Wait for all tasks to be processed
    await queue.join()

    # Signal the processor to shut down
    await queue.put(None)
    await processor

    # Shutdown the executor
    executor.shutdown()


# Run the main function
asyncio.run(main())
