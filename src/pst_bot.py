import asyncio
import importlib
import os
import aiohttp
import discord
from typing import Iterable
from discord import app_commands
from discord.ext import tasks
import random
from datetime import datetime, timedelta
import logging

import constants
import database

import handle_pst_roles

import Commands

import pst_lfm

logger = logging.getLogger(__name__)


def get_app_commands() -> Iterable[app_commands.Command]:
    commands = []
    files = os.listdir("src/Commands/")
    for file in files:
        if file.startswith("_"):
            continue
        module = importlib.import_module(f"Commands.{file.rsplit('.')[0]}")
        print(module)
        for attr in dir(module):
            if attr.startswith("_") or not attr.endswith("_cmd"):
                continue
            thing = getattr(module, attr)
            if isinstance(thing, discord.app_commands.Command):
                commands.append(thing)
    return commands


def get_pst_commands() -> Iterable[app_commands.Command]:
    commands = []
    files = os.listdir("src/Commands/")
    for file in files:
        if file.startswith("_"):
            continue
        module = importlib.import_module(f"Commands.{file.rsplit('.')[0]}")
        print(module)
        for attr in dir(module):
            if attr.startswith("_") or not attr.endswith("_pstcmd"):
                continue
            thing = getattr(module, attr)
            if isinstance(thing, discord.app_commands.Command):
                commands.append(thing)
    return commands


class PstBot(discord.Client):
    def __init__(self, *args, **kwargs):
        self.command_manager = Commands.CommandManager()

        intents = discord.Intents.default()
        intents.message_content = False
        super().__init__(intents=intents, *args, **kwargs)
        self.tree = app_commands.CommandTree(self)

    @tasks.loop(minutes=10)
    async def lfm_race_result(self):
        if not self.is_ready():
            logger.warning("Not logged in yet, skipping race result step")
            return

        pool = await database.create_pool()
        conn = await pool.acquire()

        rows = await conn.fetch("""
                                SELECT lfm_ids.discord_id, lfm_ids.lfmid,
                                race_results.channel_id
                                FROM lfm_ids JOIN race_results
                                ON race_results.discord_id = lfm_ids.discord_id
                                """)
        await pool.release(conn)
        message_cache: dict[int: str] = {}
        for row in rows:
            discord_id = row.get("discord_id")
            lfm_id = row.get("lfmid")
            channel_id = row.get("channel_id")

            logger.info(f"Checking last race for {discord_id}")

            if lfm_id not in message_cache.keys():
                message = await pst_lfm.check_last_race(pool, discord_id, lfm_id)
                if message is None:
                    continue
                message_cache[lfm_id] = message

            channel = self.get_channel(channel_id)
            if channel is None:
                logger.error(f"Channel {channel_id} not found for {discord_id}"
                             f" (lfm_id: {lfm_id})")
                continue

            try:
                await channel.send(message)
            except discord.errors.Forbidden:
                logger.error(f"Not sufficient permissions to send to channel {channel.name}")
            except discord.errors.HTTPException as http_error:
                logger.error(f"HTTP error during message send {http_error}")

        if pool:
            await pool.close()


    @tasks.loop(minutes=15)
    async def lfm_race_reminder(self):
        if not self.is_ready():
            logger.warning("Not logged in yet, skipping race reminder step")
            return
        
        query = "select * from race_reminders;"
        pool = await database.create_pool()
        races = await database.fetch_values(query, pool)
        now = datetime.now()
        pool = await database.create_pool()

        logging.info(f"{len(races)} Races to check")

        for race in races:

            logging.info(race)

            try:

                table_id = race['table_id']
                race_time = datetime.strptime(race['race_date_time'], "%Y-%m-%d %H:%M:%S")
                channel = self.get_channel(race['channel_id'])

                if race_time < now:
                    query = f'DELETE FROM race_reminders WHERE table_id = {table_id};'
                    await database.execute_query(query, pool)
                    continue

                if not race['hours_12']:
                    if (race_time - now) <= timedelta(hours=12):
                        await channel.send(f"{race['description']}\n\n{race['race_info']}")
                        query = f''' UPDATE race_reminders
                                    SET hours_12 = TRUE
                                    WHERE table_id = {table_id};'''
                        
                        await database.execute_query(query, pool)
                        continue
                if not race['hours_6']:
                    if (race_time - now) <= timedelta(hours=6):
                        await channel.send(f"{race['description']}\n\n{race['race_info']}")
                        query = f''' UPDATE race_reminders
                                    SET hours_6 = TRUE
                                    WHERE table_id = {table_id};'''
                        
                        await database.execute_query(query, pool)
                        continue
                if not race['hours_3']:
                    if (race_time - now) <= timedelta(hours=3):
                        await channel.send(f"{race['description']}\n\n{race['race_info']}")
                        query = f''' UPDATE race_reminders
                                    SET hours_3 = TRUE
                                    WHERE table_id = {table_id};'''
                        
                        await database.execute_query(query, pool)
                        continue
                if not race['hour_1']:
                    if (race_time - now) <= timedelta(hours=1):
                        await channel.send(f"{race['description']}\n\n{race['race_info']}")
                        query = f''' UPDATE race_reminders
                                    SET hour_1 = TRUE
                                    WHERE table_id = {table_id};'''
                        
                        await database.execute_query(query, pool)
                        continue
                if not race['minutes_30']:
                    if (race_time - now) <= timedelta(minutes=30):
                        await channel.send(f"{race['description']}\n\n{race['race_info']}")
                        query = f''' UPDATE race_reminders
                                    SET minutes_30 = TRUE
                                    WHERE table_id = {table_id};'''
                        
                        await database.execute_query(query, pool)
                        continue
                if not race['minutes_15']:
                    if (race_time - now) <= timedelta(minutes=15):
                        await channel.send(f"{race['description']}\n\n{race['race_info']}")
                        query = f''' UPDATE race_reminders
                                    SET minutes_15 = TRUE
                                    WHERE table_id = {table_id};'''
                        
                        await database.execute_query(query, pool)
                        continue
                
            except Exception as e:
                temp_channel = self.get_channel(1240300121254531072)
                await temp_channel.send(f"Got an error...\n{e}")

        if pool:
            await pool.close()


    async def setup_hook(self):

        ### UNCOMMENT TO DELETE COMMANDS
        # global_commands = await self.http.get_global_commands(self.application_id)
        # for command in global_commands:
        #     print(command['name'])
        #     #logging.info(f"Deleted {command['name']}")
        #     #await self.http.delete_global_command(self.application_id, command['id'])

        # guild_commands = await self.http.get_guild_commands(self.application_id, constants.PST_GUILD_ID)
        # for command in guild_commands:
        #     logging.info(f"Deleted {command['name']}")
        #     await self.http.delete_guild_command(self.application_id, constants.PST_GUILD_ID, command['id'])

        commands = get_app_commands()
        pstcommands = get_pst_commands()

        for command in commands:
            logger.info(f"Adding command {command.name}")
            self.tree.add_command(command)

        for command in pstcommands:
            logger.info(f"Adding PST only command {command.name}")
            self.tree.add_command(command, guild=discord.Object(id=constants.PST_GUILD_ID))
        await self.tree.sync(guild=discord.Object(id=constants.PST_GUILD_ID))

        for command in pstcommands:
            if command.name == "overtake":
                logger.info(f"Adding PST only command {command.name} to test guild.")
                self.tree.add_command(command, guild=discord.Object(id=constants.KYLO_TEST_GUILD_ID))
        await self.tree.sync(guild=discord.Object(id=constants.KYLO_TEST_GUILD_ID))

        await self.tree.sync()

        # for guild in GUILDS:
        #    try:
        #        self.tree.copy_global_to(guild=guild)
        #        await self.tree.sync(guild=guild)
        #    except discord.Forbidden:
        #        logger.error(f"Failed to sync app command to {guild.id}")

        self.lfm_race_result.start()
        self.lfm_race_reminder.start()

        logger.info("Setup done")
        self.user_join_times = dict()


    async def on_ready(self):
        logger.info(f"We have logged in as {self.user}")
        logger.info("Registering commands")
        logger.info("### Counters ###")
        pool = await database.create_pool()
        logger.info(await database.print_counters(pool))
        try:
            self.dict1 = await handle_pst_roles.get_sorted_message_counts()
        except Exception as e:
            pass
        if pool:
            await pool.close()

        activeservers = self.guilds
        for guild in activeservers:
            logger.info(guild)

        logger.info(
            f"All {len(self.command_manager.get_commands())} commands registered."
        )
        
        self.temp_message_dict = dict()


    async def on_message(self, message: discord.Message):
        # logger.info(f"Message: {message.guild} in {message.channel} from {message.author} said {message.content}")
        if message.author == self.user:
            return

        if message.author.id == 1044699864530944060:
            return

        if message.guild.id == constants.PST_GUILD_ID:
            if message.author.bot == False:

                pool = await database.create_pool()
                await database.update_last_1000_messages(message.author.id, pool)

                total_messages = await database.get_last_1000_messages_message_count(pool)

                logging.info(f"{total_messages}/1000")
                
                if total_messages >= 1000:
                    temp_dict = await database.fetch_last_1000_messages_dict(pool)
                    await database.recreate_last_1000_messages_table(pool)
                    file_name = await handle_pst_roles.make_funny_graph(temp_dict, message)
                    temp_channel = self.get_channel(1015601499117727856)
                    files = [discord.File(file_name[i]) for i in range(3)]
                    await temp_channel.send(files=files)

                    for file in file_name:
                        os.remove(file)

                if pool:
                    await pool.close()

            message_count = await handle_pst_roles.get_message_count(message)
            if message_count:
                if message_count % 100 == 0:
                    await message.add_reaction("💯")

                    #await message.channel.send(await handle_pst_roles.get_overtake_counts_from_message(message))

                    try:
                        self.dict2 = await handle_pst_roles.get_sorted_message_counts()
                        temp_channel = self.get_channel(1240300121254531072)

                        changes = handle_pst_roles.compare_message_counts(self.dict1, self.dict2)
                        message_to_send = ""
                        for user_id, overtaken_users in changes.items():
                            for overtaken_user in overtaken_users:
                                if overtaken_user != user_id:
                                    message_to_send += (f"{await self.fetch_user(user_id)} overtook {await self.fetch_user(overtaken_user)}\n")

                        temp_channel = self.get_channel(constants.PST_GENERAL_CHANNEL_ID)
                        await temp_channel.send(message_to_send)
                        self.dict1 = self.dict2
                    except Exception as e:
                        temp_channel = self.get_channel(1240300121254531072)
                        await temp_channel.send(f"Got an error...\n{e}")

                if message_count % 1000 == 0 and message_count <= 100000:
                    temp_channel = self.get_channel(constants.PST_GENERAL_CHANNEL_ID)
                    await temp_channel.send(f"{message_count}th message for <@{message.author.id}>!")
                
                if message_count % 10000 == 0 and message_count <= 1000000 and message_count > 100000:
                    temp_channel = self.get_channel(constants.PST_GENERAL_CHANNEL_ID)
                    await temp_channel.send(f"{message_count}th message for <@{message.author.id}>!")

            # THIS SHOULD WORK WITHOUT MESSAGE CONTENT
            if message.author.name == "pran_":
                # THIS WILL WORK
                if random.randint(1, 100) == 100:
                    await message.reply(
                        "https://tenor.com/view/youre-brown-brown-black-saudia-arabia-blm-gif-21420652")
                    

    async def on_raw_reaction_add(self, payload):
        # Get the emoji object
        logger.info(f"Reaction:  {payload}")
        emoji = payload.emoji
        text = await self.get_channel(payload.channel_id).fetch_message(
            payload.message_id
        )

        # Check if the emoji name is "skull" or "\uD83D\uDC80"
        if emoji.name in ("skull", "\uD83D\uDC80", "💀"):
            await text.add_reaction(emoji)

        if emoji.name in ("❌", "\u274C"):
            if text.author == self.user:
                await text.delete()
                logger.info("Deleted my message, because someone reacted with ❌")

        # Check if the emoji name contains "copium"
        if "copium" in emoji.name.lower() and payload.guild_id == constants.PST_GUILD_ID:
            # Do something when the message is reacted with "copium" emoji
            pool = await database.create_pool()
            logger.info("Database connected.")

            start_date = datetime.strptime("2023-06-01", "%Y-%m-%d")
            current_date = datetime.now()
            days_passed = (current_date - start_date).days

            # Read the counters.json file
            copium = await database.get_some_value("copium", pool)
            logging.info(f"copium:  {copium}")

            copium_remaining = 2 - copium * 0.001 + days_passed * 0.001
            copium_remaining = round(copium_remaining, 3)
            if copium_remaining > 0.0:
                copium += 1
                await database.update_some_value("copium", copium, pool)
            else:
                channel_to_send_message_to = self.get_channel(constants.PST_GENERAL_CHANNEL_ID)
                if payload.member.nick is None:
                    name = payload.member.name
                else:
                    name = payload.member.nick
                await channel_to_send_message_to.send(
                    f"Unfortunately, we are all out of copium, but {name} tried to use some copium. Hopefully they can find another way to stimulate his senses."
                )
                logger.info(f"Copium remaining: {copium_remaining}")
                return

            if pool:
                await pool.close()
            logger.info("Database disconnected.")


    async def on_guild_join(self, guild):
        channel = self.get_channel(constants.PST_GENERAL_CHANNEL_ID)
        if channel is not None:
            await channel.send(f"{guild.name} has been infiltrated. [{len(self.guilds)}]\nhttps://tenor.com/view/dj-khaled-another-one-one-gif-5057861")
        else:
            logger.info(f"Channel with ID Failed to send message to PST HQ.")


    async def on_guild_remove(self, guild):
        channel = self.get_channel(constants.PST_GENERAL_CHANNEL_ID)
        if channel is not None:
            await channel.send(f"{guild.name} has left the project. [{len(self.guilds)}/100]")
        else:
            logger.info(f"Channel with ID Failed to send message to PST HQ.")


    async def on_voice_state_update(
        self,
        member: discord.Member, 
        before: discord.VoiceState, 
        after: discord.VoiceState
    ) -> None:
        
        if member.guild.id != 1015601496743751740:
            return
        
        if before.channel is not None:
            if before.channel.id == 1015601499117727857 and after.channel is not None:
                temp_channel = self.get_channel(1015601499117727856)
                voice_channel_info = f"Yooo guys Dalking here, informing you that {member.name} has joined the <#{after.channel.id}> voice channel, ready to talk to you."
                await temp_channel.send(voice_channel_info)
        
        # Check if the user has joined a voice channel
        if before.channel is None and after.channel is not None:
            # Store the current time when the user joins
            self.user_join_times[member.id] = datetime.now()
            logging.info(f"{member.name} joined voice channel at {self.user_join_times[member.id]}")
            temp_channel = self.get_channel(1015601499117727856)
            
            if after.channel.id != 1015601499117727857:
                voice_channel_info = f"Yooo guys Dalking here, informing you that {member.name} has joined the <#{after.channel.id}> voice channel, ready to talk to you."
                await temp_channel.send(voice_channel_info)

        # Check if the user has left a voice channel
        elif before.channel is not None and after.channel is None:
            if member.id in self.user_join_times: 
                result1 = await handle_pst_roles.get_sorted_voice_activity()
                # Calculate the time spent in the voice channel
                join_time = self.user_join_times.pop(member.id)
                time_in_voice = datetime.now() - join_time
                logging.info(f"{member.name} left voice channel. Time spent: {time_in_voice}")
                time_in_voice_seconds = int(time_in_voice.total_seconds())
                await handle_pst_roles.handle_voice_channel_activity(member, time_in_voice_seconds)

                days, seconds = divmod(time_in_voice_seconds, 86400)    # 86400 seconds in a day
                hours, seconds = divmod(seconds, 3600)    # 3600 seconds in an hour
                minutes, seconds = divmod(seconds, 60)    # 60 seconds in a minute
                time_string = f'{days}d:{hours:02}h:{minutes:02}m:{seconds:02}s'
                
                voice_channel_info =f"Bye {member.name} {constants.Emotes.AYAYA} [{time_string}]"
                temp_channel = self.get_channel(1015601499117727856)
                await temp_channel.send(voice_channel_info)
                result2 = await handle_pst_roles.get_sorted_voice_activity()
                try:
                    temp_channel = self.get_channel(1015601499117727856)
                    changes = handle_pst_roles.compare_voice_activity(result1, result2) 
                    message_to_send = ""
                    for user_id, overtaken_users in changes.items():
                        for overtaken_user in overtaken_users:
                            if overtaken_user != user_id:
                                message_to_send += f"{await self.fetch_user(overtaken_user)}, "

                                
                    message_to_send = (f"{await self.fetch_user(user_id)} overtook {message_to_send[:-2]} in voice activity\n")
                    temp_channel = self.get_channel(constants.PST_GENERAL_CHANNEL_ID)
                    await temp_channel.send(message_to_send)
                except Exception as e:
                    temp_channel = self.get_channel(1240300121254531072)
                    await temp_channel.send(f"Got an error...\n{e}")


            else:
                logging.info(f"Couldn't find join time for {member.name}")


    async def on_automod_action(self, action: discord.AutoModAction):
        if action.guild_id == 1015601496743751740:
        # Check if AutoMod was triggered by bad language
            if action.rule_trigger_type == discord.AutoModRuleTriggerType.keyword_preset or action.rule_id == 1105565891418132622:
                logging.info(f"AutoMod Rule: {action.rule_trigger_type} broken.")
                author = action.user_id  # The user who triggered AutoMod
                channel = action.channel  # The channel where it occurred
                channel_id = channel.id

                random_number = random.random()

                if random_number < 0.1:

                    # Respond with a message similar to an FIA statement
                    response_message = (
                        f"**Stewards Decision**\n\n"
                        f"**User:** <@{author}>\n"
                        f"**Session:** <#{channel_id}>\n"
                        f"**Issue:** Inappropriate language detected. Usage of the word **{action.matched_keyword}**\n\n"
                        f"**Decision:** You’re required to be more mindful of language in public forums.\n\n"
                        f"**Reasoning:**\n"
                        f"The moderators reviewed your language, which may be deemed unsuitable for public discussion.\n"
                        f"While this language wasn’t directed at anyone, please remember your role in maintaining a positive environment.\n"
                        f"**Let’s aim for respectful conversations moving forward. Thank you!**"
                    )
                    await channel.send(response_message)

                elif random_number < 0.09:

                    # Respond with a message similar to an FIA statement
                    response_message = (
                        f"**Stewards Decision**\n\n"
                        f"**User:** <@{author}>\n"
                        f"**Session:** <#{channel_id}>\n"
                        f"**Issue:** Inappropriate language detected. Usage of the word **{action.matched_keyword}**\n\n"
                        f"**Decision:** You’re required to be more mindful of language.\n\n"
                    )
                    await channel.send(response_message)

                elif random_number < 0.01:
                    # Respond with a message similar to an FIA statement
                    response_message = (
                        f"**Stewards Decision**\n\n"
                        f"**User:** <@{author}>\n"
                        f"**Session:** <#{channel_id}>\n"
                        f"**Issue:** Inappropriate language detected. Usage of the word **{action.matched_keyword}**\n\n"
                        f"**Decision:** Donald Trump has pardoned you. Please move along.\n\n"
                    )
                    await channel.send(response_message)
                else: 
                    message_id = action.message_id
                    message = await channel.fetch_message(message_id)
                    if message is None:
                        logging.info("Message not found.")            
                    # Add the reaction
                    await message.add_reaction("⚠")


                pool = await database.create_pool()
                haram_count = await database.get_haram_count(author, pool)
                if haram_count == None:
                    haram_count = 0

                haram_count += 1
                await database.update_haram_count(author, haram_count, pool)
                logger.info(f"{author} violations: {haram_count}")

                if pool:
                    await pool.close()

                if haram_count % 10 == 0:
                    timeout_duration = haram_count                    
                    guild = self.get_guild(action.guild_id)
                    if guild:
                        logging.info(guild)
                        member = await guild.fetch_member(action.user_id) 

                    logging.info(member)

                    try:
                        # Set the timeout duration
                        until = discord.utils.utcnow() + timedelta(seconds=timeout_duration)
                        await member.edit(timed_out_until=until, reason="Haram.") # type: ignore
                        logging.info(f"User {member} has been timed out for {timeout_duration:.1f} seconds.")
                    except discord.Forbidden:
                        logging.info("I don't have permission to time out this user.")
                    except discord.HTTPException as e:
                        logging.info(f"Failed to time out user: {e}")

                else:
                    timeout_duration = haram_count / 10                    
                    guild = self.get_guild(action.guild_id)
                    if guild:
                        logging.info(guild)
                        member = await guild.fetch_member(action.user_id) 

                    logging.info(member)

                    try:
                        # Set the timeout duration
                        until = discord.utils.utcnow() + timedelta(seconds=timeout_duration)
                        await member.edit(timed_out_until=until, reason="Haram.") # type: ignore
                        logging.info(f"User {member} has been timed out for {timeout_duration:.1f} seconds.")
                    except discord.Forbidden:
                        logging.info("I don't have permission to time out this user.")
                    except discord.HTTPException as e:
                        logging.info(f"Failed to time out user: {e}")