def test(message):
    parts = message.split(':')
            
    if len(parts) != 3:
        print('Invalid format. Please use: !season tracks: xxx, yyy, zzz drivers: Jimmy Holm, Teemu Karppinen')
        return

    # Extract tracks and drivers
    track_part, driver_part = parts[1], parts[2]

    # Clean up and split tracks and drivers
    tracks = [track.strip() for track in track_part.split(',')]
    drivers = [driver.strip() for driver in driver_part.split(',')]

    print(tracks)
    print(drivers)

    # Do something with the extracted data (e.g., store it in variables)
    track_data = ', '.join(tracks)
    driver_data = ', '.join(drivers)

    # Print or use the data as needed
    print(f'Tracks: {track_data}')
    print(f'Drivers: {driver_data}')

    # You can also send a message back to the channel if needed
    print(f'Tracks: {track_data}\nDrivers: {driver_data}')

test("!season tracks: xxx, yyy, zzz, ..., ... drivers: Jimmy Holm, Teemu Karppinen")