import json
import math
import requests
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import os
import logging

logger = logging.getLogger(__name__)


def get_ordinal(number: int) -> str:
    if 10 <= number % 100 <= 20:
        suffix = "th"
    else:
        suffix = {1: "st", 2: "nd", 3: "rd"}.get(number % 10, "th")

    return str(number) + suffix

def find_closest_car(car_list, input_car):
    closest_match, _ = process.extractOne(input_car, car_list, scorer=fuzz.partial_ratio)
    return closest_match

def round_to_nearest_hundred(x):
    return math.floor(x / 100) * 100

def round_to_nearest_tenth(x):
    return math.floor(x / 10) * 10

def round_to_nearest(x):
    return math.floor(x)

def get_data(lfm_id, car_name, author):
    logging.info(f"{lfm_id}, {car_name}")
    result = ""
    response = requests.get(f'https://api2.lowfuelmotorsport.com/api/users/getUsersFavouriteCars/{lfm_id}')

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        input_data = response.json()

        response1 = requests.get(f"https://api2.lowfuelmotorsport.com/api/users/getUserData/{lfm_id}?with_achievements=true").json()
        driver_name = f"{response1['vorname']} {response1['nachname']}"

        LFM_carlist = []

        for car in input_data:
            LFM_carlist.append(car["car_name"])

        with open(f'resources/lfm_car_data/{car_name}.json', 'r') as file:
            data = json.load(file)

        total_km = 0
        total_laps = 0   


        if car_name == "GT4":     
            for car in input_data:
                if "GT4" in car['car_name']:
                    total_km   += float(car['km'])
                    total_laps += int(car['laps'])
        elif car_name == "GT2":
            for car in input_data:
                if int(car['car_id']) >= 80:
                    print(car['car_name'])
                    total_km   += float(car['km'])
                    total_laps += int(car['laps'])
        else:
            for car in input_data:
                if car['car_name'] == car_name:
                    total_km   = float(car['km'])
                    total_laps = int(car['laps'])
                    break
        
        
        input_data = {
            "name": author,
            "kilometers": total_km,
            "laps": total_laps
        }

        data.append(input_data)

        for datapoint in data:
            if datapoint['name'] == driver_name:
                data.remove(datapoint)
                break

        data = sorted(data, key=lambda x: x["kilometers"], reverse = True)

        # Find the index of the input data
        input_index = next((i for i, item in enumerate(data) if item["name"] == input_data["name"]), None)
        
        if input_index is not None:
            # Define the number of records to show above and below the input
            num_records = 3

            # Calculate the start and end indices based on the input index
            start_index = max(0, input_index - num_records)
            end_index = min(len(data), input_index + num_records + 1)  # Add 1 to include the input index

            # Print the sorted data with the calculated range of records
            for i, record in enumerate(data[start_index:end_index]):
                result += (f"{i+1+start_index}. {str(record['name']):30} \t {str(round(record['kilometers'], 3)):10} \t {record['laps']}\n")

        return result
    

def get_milestone(lfm_id, car_name):
    logging.info(f"{lfm_id}, {car_name}")
    response = requests.get(f'https://api2.lowfuelmotorsport.com/api/users/getUsersFavouriteCars/{lfm_id}')

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        input_data = response.json()

        response1 = requests.get(f"https://api2.lowfuelmotorsport.com/api/users/getUserData/{lfm_id}?with_achievements=true").json()
        driver_name = f"{response1['vorname']} {response1['nachname']}"

        LFM_carlist = []

        for car in input_data:
            LFM_carlist.append(car["car_name"])
            
        with open(f'resources/lfm_car_data/{car_name}.json', 'r') as file:
            data = json.load(file)

        total_km = 0
        total_laps = 0   

        if car_name == "GT4":     
            for car in input_data:
                if "GT4" in car['car_name']:
                    total_km   += float(car['km'])
                    total_laps += int(car['laps'])

        elif car_name == "GT2":     
            for car in input_data:
                if "GT2" in car['car_name'] or car['car_name'] == "Porsche 935":
                    total_km   += float(car['km'])
                    total_laps += int(car['laps'])
        else:
            for car in input_data:
                if car['car_name'] == car_name:
                    total_km   = float(car['km'])
                    total_laps = int(car['laps'])
                    break
        
        
        input_data = {
            "name": "author",
            "kilometers": total_km,
            "laps": total_laps
        }

        data.append(input_data)

        for datapoint in data:
            if datapoint['name'] == driver_name:
                data.remove(datapoint)
                break

        data = sorted(data, key=lambda x: x["kilometers"], reverse = True)

        # Find the index of the input data
        input_index = next((i for i, item in enumerate(data) if item["name"] == input_data["name"]), None)

        if input_index:
            input_index += 1

            if input_index > 100: # type: ignore
                milestone = round_to_nearest_hundred(input_index)
                kilometres_next = data[milestone-1]['kilometers']
            elif input_index > 10: # type: ignore
                milestone = round_to_nearest_tenth(input_index)
                kilometres_next = data[milestone-1]['kilometers']
            elif input_index > 1: # type: ignore
                milestone = input_index - 1 # type: ignore
                kilometres_next = data[milestone-1]['kilometers'] # type: ignore
            
            kilometres_user = data[input_index-1]['kilometers'] # type: ignore
            print(kilometres_user)
            kilometres_milestone = round(kilometres_next - kilometres_user, 3)
            
            if 10 > milestone > 1:
                return f"{kilometres_milestone} kilometres to be the {get_ordinal(milestone)} biggest {car_name} enjoyer."
            elif milestone >= 10:
                return f"{kilometres_milestone} kilometres to be the {get_ordinal(milestone)} biggest {car_name} enjoyer."
            else:
                return f"{kilometres_milestone} kilometres to be the biggest {car_name} enjoyer."
        else:
            return f"You're the King of the {car_name} 👑"
        

def get_combined_cars_sorted_milestone(lfm_id, car_name) -> str: 
    logging.info(f"{lfm_id}, {car_name}") 
    response = requests.get(f'https://api2.lowfuelmotorsport.com/api/users/getUsersFavouriteCars/{lfm_id}')

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        input_data = response.json()

        response1 = requests.get(f"https://api2.lowfuelmotorsport.com/api/users/getUserData/{lfm_id}?with_achievements=true").json()
        driver_name = f"{response1['vorname']} {response1['nachname']}"

        LFM_carlist = []

        for car in input_data:
            LFM_carlist.append(car["car_name"])
            
        with open(f'resources/lfm_car_data/combined_cars_sorted.json', 'r') as file:
            data = json.load(file)

        total_km = 0
        total_laps = 0   

        for car in input_data:
            if car['car_name'] == car_name:
                total_km   = float(car['km'])
                total_laps = int(car['laps'])
                break
        
        if total_km <= 100000:
            return ""
        
        input_data = {
            "name": "author",
            "kilometers": total_km,
            "laps": total_laps
        }

        data.append(input_data)

        for datapoint in data:
            if datapoint['name'] == driver_name:
                data.remove(datapoint)
                break

        data = sorted(data, key=lambda x: x["kilometers"], reverse = True)

        # Find the index of the input data
        input_index = next((i for i, item in enumerate(data) if item["name"] == input_data["name"]), None)

        if input_index:
            input_index += 1

            if input_index > 100: # type: ignore
                milestone = round_to_nearest_hundred(input_index)
                kilometres_next = data[milestone-1]['kilometers']
            elif input_index > 10: # type: ignore
                milestone = round_to_nearest_tenth(input_index)
                kilometres_next = data[milestone-1]['kilometers']
            elif input_index > 1: # type: ignore
                milestone = input_index - 1 # type: ignore
                kilometres_next = data[milestone-1]['kilometers'] # type: ignore
            
            kilometres_user = data[input_index-1]['kilometers'] # type: ignore
            print(kilometres_user)
            kilometres_milestone = round(kilometres_next - kilometres_user, 3)
            
            if 10 > milestone > 1:
                return f"{kilometres_milestone} kilometres in the {car_name} to be the {get_ordinal(milestone)} in most distance for a single car on LFM."
            elif milestone >= 10:
                return f"{kilometres_milestone} kilometres in the {car_name} to be the {get_ordinal(milestone)} in most distance for a single car on LFM."
            else:
                return f"{kilometres_milestone} kilometres in the {car_name} to be the 1st in most distance for a single car on LFM.."
        else:
            return f"You're first in most distance for a single car on LFM. 👑"
    
    return ""
        

