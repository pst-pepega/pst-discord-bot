import pandas as pd
import re

def filter_data(df, track=None, min_avg_temp=None, max_avg_temp=None, min_mixed_chance=None, min_dry_chance=None, min_wet_chance=None, past_date=None):
    # Convert 'Date and Time' column to datetime
    df['Date and Time'] = pd.to_datetime(df['Date and Time'])

    # Apply filters based on provided criteria
    if track is not None:
        df = df[df['Track'] == track]
    
    if min_avg_temp is not None:
        df = df[df['Avg Temp'] >= min_avg_temp]

    if max_avg_temp is not None:
        df = df[df['Avg Temp'] <= max_avg_temp]

    if min_mixed_chance is not None:
        df = df[df['Mixed Chance'] >= min_mixed_chance]

    if min_dry_chance is not None:
        df = df[df['Dry Chance'] >= min_dry_chance]

    if min_wet_chance is not None:
        df = df[df['Wet Chance'] >= min_wet_chance]

    if past_date is not None:
        if past_date.lower() == 'now':
            past_date = pd.to_datetime('now')
        else:
            past_date = pd.to_datetime(past_date)
        df = df[df['Date and Time'] >= past_date]

    return df


def parse_string(input_string: str):
    # Extracting values using regular expressions
    pattern = r'(track=([^=]+))?\s+(mintemp=(\d+))?\s?+(maxtemp=(\d+))?\s?+(mixed=(\d+))?\s?+(dry=(\d+))?\s?+(wet=(\d+))?'

    match = re.findall(pattern, input_string)
    match = match[0] if len(match) == 1 else match[1]

    if match:
        track_name = match[1] if match[1] != "" else None
        min_temp = match[3] if match[3] != "" else None
        max_temp = match[5] if match[5] != "" else None
        mixed = match[7] if match[7] != "" else None
        dry = match[9] if match[9] != "" else None
        wet = match[11] if match[11] != "" else None
    else:
        track_name = None
        min_temp = None
        max_temp = None
        mixed = None
        dry = None
        wet = None

    if min_temp:
        min_temp = int(min_temp)
    if max_temp:
        max_temp = int(max_temp)
    if mixed:
        mixed = float(int(mixed)/100)
    if dry:
        dry = float(int(dry)/100)
    if wet:
        wet = float(int(wet)/100)

    return [track_name, min_temp, max_temp, mixed, dry, wet]

def get_data(input_string):
    result = ""
    #input_string = "!find_race mintemp=27 maxtemp=30 dry=88"
    returned_list = parse_string(input_string)
    print(returned_list)
    # Read data from CSV file
    df = pd.read_csv('resources/race_data/sprints.csv')

    # Example filtering
    filtered_df = filter_data(df, track=returned_list[0], min_avg_temp=returned_list[1], max_avg_temp=returned_list[2], min_mixed_chance=returned_list[3], min_dry_chance=returned_list[4], min_wet_chance=returned_list[5], past_date='now')
    # Display the filtered DataFrame manually formatted as a table with tabs and headers
    result += (f'{"Wet":5}\t{"Dry":5}\t{"Mixed":5}\t{"Temp":3}\t{"Date and Time":17}\t{"Track":3}\n')
    limit = 10
    counter = 1
    for _, row in filtered_df.iterrows():
        if limit == counter:
            return result
        result += (f"{row['Wet Chance']:.2f}\t{row['Dry Chance']:.2f}\t{row['Mixed Chance']:.2f}\t{row['Avg Temp']:.3f}\t{row['Date and Time']}\t{row['Track']}\n")
        counter += 1
    return result