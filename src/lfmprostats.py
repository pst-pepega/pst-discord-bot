import pandas as pd

def get_lfmprostats(sort, number_of_results: int, columns_to_keep: list, order='DESC', minimum_races = 0):
    # Load the CSV file into a pandas DataFrame
    df = pd.read_csv('resources/race_results.csv')

    # Print the column names to verify exact names
    print(df.columns)

    # Group by columns 'vorname', 'nachname', 'user_id'
    grouped = df.groupby(['vorname', 'nachname', 'user_id'])

    # Calculate the requested metrics
    result = grouped.agg({
        'user_id': 'count',            # Races
        'position': lambda x: (x == 1).sum(),   # Wins
        'pole_position': 'sum',        # Pole Positions
        'fastest_lap': 'sum',          # Fastest Laps
        'ratingGain': 'sum',          # Elo
        'sr_change': 'sum',          # SR
        'incidents': 'sum',          # Incidents
    })

    # Rename the columns
    result.columns = ['Races', 'Wins', 'Pole Positions', 'Fastest Laps', 'Elo', 'SR', 'Incidents']

    # Add additional calculated columns with distinct names
    result['Inc/Race'] = round((result['Incidents'] / result['Races']), 3)
    result['Podiums'] = grouped.apply(lambda group: (group['position'] <= 3).sum())
    result['Top5s'] = grouped['position'].apply(lambda x: (x <= 5).sum())
    result['Top10s'] = grouped['position'].apply(lambda x: (x <= 10).sum())
    result['Winrate'] = round((result['Wins'] / result['Races']) * 100, 3)
    result['Podiumrate'] = round(result['Podiums'] / result['Races'] * 100, 3)
    result['Pole Position Rate'] = round(result['Pole Positions'] / result['Races'] * 100, 3)
    result['Fastest Lap Rate'] = round(result['Fastest Laps'] / result['Races'] * 100, 3)


    # Reset the index to make 'vorname', 'nachname', 'user_id' columns
    result.reset_index(inplace=True)

    #columns_to_keep = ['First Name', 'Last Name', 'Elo', 'SR', 'Inc.', 'Inc./Race']

    # Rename the columns according to your requirement
    result.columns = ['First Name', 'Last Name', 'user_id', 'Races', 'Wins', 'Poles', 'Purple', 'Elo', 'SR', 
                      'Inc.', 'Inc./Race', 'Podiums', 'Top5s', 'Top10s', 'Winrate', 'Podium %', 'Poles %', 'F. Lap %']
    result = result.drop('user_id', axis=1)

    # Drop the non-specified columns
    result = result.loc[:, columns_to_keep]

    # Sort the DataFrame based on user input
    sort_column = sort
    if order=='DESC':
        result = result.sort_values(by=sort_column, ascending=False)
    else:
        result = result.sort_values(by=sort_column, ascending=True)

    result = result[result['Races'] >= minimum_races]

    # Select the top 10 rows, including headers
    result_top10 = result.head(number_of_results)

    result_top10.reset_index(drop=True, inplace=True)

    # Print or save the result as needed
    return result_top10


def lfm_pro_car_stats(sort, number_of_results: int):
     # Load the CSV file into a pandas DataFrame
    df = pd.read_csv('resources/race_results.csv')

    # Print the column names to verify exact names
    print(df.columns)

    # Group by columns 'vorname', 'nachname', 'user_id'
    grouped = df.groupby(['car_name'])

    print(grouped)

    # Calculate the requested metrics
    result = grouped.agg({
        'car_name': 'count',            # Races
        'position': lambda x: (x == 1).sum(),   # Wins
        'pole_position': 'sum',        # Pole Positions
        'fastest_lap': 'sum',          # Fastest Laps
    })

    # Rename the columns
    result.columns = ['Races', 'Wins', 'Pole Positions', 'Fastest Laps']

    # Add additional calculated columns with distinct names
    result['Podiums'] = grouped.apply(lambda group: (group['position'] <= 3).sum())
    result['Top5s'] = grouped['position'].apply(lambda x: (x <= 5).sum())
    result['Top10s'] = grouped['position'].apply(lambda x: (x <= 10).sum())
    result['Winrate'] = round((result['Wins'] / result['Wins'].sum()) * 100, 3)
    result['Podiumrate'] = round(result['Podiums'] / result['Podiums'].sum() * 100, 3)
    result['Pole Position Rate'] = round(result['Pole Positions'] / result['Pole Positions'].sum() * 100, 3)
    result['Fastest Lap Rate'] = round(result['Fastest Laps'] / result['Fastest Laps'].sum() * 100, 3)

    # Reset the index to make 'vorname', 'nachname', 'user_id' columns
    result.reset_index(inplace=True)

    # Rename the columns according to your requirement
    result.columns = ['car_name', 'Races', 'Wins', 'Poles', 'F. Laps', 'Podiums', 'Top5s', 'Top10s', 'Winrate', 'Podium %', 'Poles %', 'F. Lap %']

    # Sort the DataFrame based on user input
    sort_column = sort
    result = result.sort_values(by=sort_column, ascending=False)

    # Select the top 10 rows, including headers
    result_top10 = result.head(number_of_results)

    result_top10.reset_index(drop=True, inplace=True)

    # Print or save the result as needed
    return result_top10


def get_lfmprostats_by_car_and_user(sort, number_of_results: int, car_filter = None):
    # Load the CSV file into a pandas DataFrame
    df = pd.read_csv('resources/race_results.csv')

    # Print the column names to verify exact names
    print(df.columns)

    # Group by columns 'vorname', 'nachname', 'user_id'
    grouped = df.groupby(['vorname', 'nachname', 'user_id', 'car_name'])

    # Calculate the requested metrics
    result = grouped.agg({
        'user_id': 'count',            # Races
        'position': lambda x: (x == 1).sum(),   # Wins
        'pole_position': 'sum',        # Pole Positions
        'fastest_lap': 'sum',          # Fastest Laps
    })

    # Rename the columns
    result.columns = ['Races', 'Wins', 'Pole Positions', 'Fastest Laps']

    # Add additional calculated columns with distinct names
    result['Podiums'] = grouped.apply(lambda group: (group['position'] <= 3).sum())
    result['Top5s'] = grouped['position'].apply(lambda x: (x <= 5).sum())
    result['Top10s'] = grouped['position'].apply(lambda x: (x <= 10).sum())
    result['Winrate'] = round((result['Wins'] / result['Races']) * 100, 3)
    result['Podiumrate'] = round(result['Podiums'] / result['Races'] * 100, 3)
    result['Pole Position Rate'] = round(result['Pole Positions'] / result['Races'] * 100, 3)
    result['Fastest Lap Rate'] = round(result['Fastest Laps'] / result['Races'] * 100, 3)

    # Reset the index to make 'vorname', 'nachname', 'user_id' columns
    result.reset_index(inplace=True)

    # Rename the columns according to your requirement
    result.columns = ['First Name', 'Last Name', 'user_id', 'car_name', 'Races', 'Wins', 'Poles', 'F. Laps', 'Podiums', 'Top5s', 'Top10s', 'Winrate', 'Podium %', 'Poles %', 'F. Lap %']
    result = result.drop('user_id', axis=1)

    # Sort the DataFrame based on user input
    sort_column = sort
    result = result.sort_values(by=sort_column, ascending=False)


    if car_filter:
        result = result[result['car_name'] == car_filter]

    # Select the top 10 rows, including headers
    result_top10 = result.head(number_of_results)

    result_top10.reset_index(drop=True, inplace=True)


    result_top10 = result_top10.drop('F. Laps', axis=1)
    result_top10 = result_top10.drop('F. Lap %', axis=1)


    # Print or save the result as needed
    return result_top10
