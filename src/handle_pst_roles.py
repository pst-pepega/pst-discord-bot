from datetime import datetime, timedelta
import discord
from matplotlib import pyplot as plt
import database
import logging
import pytz
from typing import List, Dict

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

message_count_roles = {
    1: {'add_role_id': 1037054580212641862},
    25: {'add_role_id': 1023892576035684364, 'remove_role_id': 1037054580212641862},
    29: {'add_role_id': 1043174578807115776, 'remove_role_id': 1023892576035684364},
    59: {'add_role_id': 1017439810148909106, 'remove_role_id': 1043174578807115776},
    96: {'add_role_id': 1043174923620855939, 'remove_role_id': 1017439810148909106},
    169: {'add_role_id': 1043175408465616997, 'remove_role_id': 1043174923620855939},
    290: {'add_role_id': 1017443331573305465, 'remove_role_id': 1043175408465616997},
    503: {'add_role_id': 1043176193463177226, 'remove_role_id': 1017443331573305465},
    868: {'add_role_id': 1105083016319074304, 'remove_role_id': 1043176193463177226},
    1501: {'add_role_id': 1043176499152433202, 'remove_role_id': 1105083016319074304},
    2596: {'add_role_id': 1035629297488117781, 'remove_role_id': 1043176499152433202},
    4489: {'add_role_id': 1104489207403118712, 'remove_role_id': 1035629297488117781},
    7763: {'add_role_id': 1046880606560202763, 'remove_role_id': 1104489207403118712},
    13424: {'add_role_id': 1104490204909273208, 'remove_role_id': 1046880606560202763},
    23215: {'add_role_id': 1130795034338414705, 'remove_role_id': 1104490204909273208},
    40147: {'add_role_id': 1043177978244378726, 'remove_role_id': 1130795034338414705},
    69420: {'add_role_id': 1019350397229469727, 'remove_role_id': 1043177978244378726},
    100000: {'add_role_id': 1196051047970517103, 'remove_role_id': 1019350397229469727},
    # Add more entries as needed
}


async def get_role_name(guild, role_id):
    role = guild.get_role(role_id)
    return role.name if role else None


def convert_minutes_to_string(total_minutes):
    # Define conversion constants
    minutes_per_hour = 60
    hours_per_day = 24
    days_per_week = 7
    days_per_month = 30.44  # Approximate average
    days_per_year = 365.25  # Accounting for leap years

    # Calculate each component
    years = int(total_minutes // (minutes_per_hour * hours_per_day * days_per_year))
    total_minutes %= (minutes_per_hour * hours_per_day * days_per_year)

    months = int(total_minutes // (minutes_per_hour * hours_per_day * days_per_month))
    total_minutes %= (minutes_per_hour * hours_per_day * days_per_month)

    weeks = int(total_minutes // (minutes_per_hour * hours_per_day * days_per_week))
    total_minutes %= (minutes_per_hour * hours_per_day * days_per_week)

    days = int(total_minutes // (minutes_per_hour * hours_per_day))
    total_minutes %= (minutes_per_hour * hours_per_day)

    hours = int(total_minutes // minutes_per_hour)
    minutes = int(total_minutes % minutes_per_hour)

    # Construct the result string, omitting zero values
    time_components = []
    if years > 0:
        time_components.append(f"{years} year{'s' if years > 1 else ''}")
    if months > 0:
        time_components.append(f"{months} month{'s' if months > 1 else ''}")
    if weeks > 0:
        time_components.append(f"{weeks} week{'s' if weeks > 1 else ''}")
    if days > 0:
        time_components.append(f"{days} day{'s' if days > 1 else ''}")
    if hours > 0:
        time_components.append(f"{hours} hour{'s' if hours > 1 else ''}")
    if minutes > 0:
        time_components.append(f"{minutes} minute{'s' if minutes > 1 else ''}")

    return ' '.join(time_components)


async def add_role(message: discord.Message, role_id: int, message_count: int):
    # Check if the role exists in the server
    guild = message.guild
    logger.info(guild)
    if guild:
        member = await guild.fetch_member(message.author.id)
        role = guild.get_role(role_id)
        logger.info(member)
        logger.info(role)

        if role:
            # Add the role to the member
            if member:
                await member.add_roles(role, reason='Reached a new treshold for messages sent on server.')
                await message.channel.send(f'{member.mention} has reached {message_count} messages on this server, time for a new role! You are now (a) {role.name}')
                logger.info(f"Added role to {member.name}")
        else:
            await logger.info(f'The role with ID {role_id} does not exist in this server.') # type: ignore


async def remove_role(message: discord.Message, role_id: int): # type: ignore
    # Check if the role exists in the server
    guild = message.guild
    if guild:
        member = await guild.fetch_member(message.author.id)
        role = guild.get_role(role_id)
        logger.info(member)
        logger.info(role)

        if role:
            # Add the role to the member
            if member:
                await member.remove_roles(role)
        else:
            await logger.info(f'The role with ID {role_id} does not exist in this server.') # type: ignore


async def check_message_count(message: discord.Message, message_count: int):
     if message_count in message_count_roles:
        role_info = message_count_roles[message_count]
        
        # Add the role
        logger.info(f"Adding the role with id {role_info['add_role_id']} to {message.author.name}")
        await add_role(message, role_info['add_role_id'], message_count)
        
        # Remove the role
        logger.info(f"Removing the role with id {role_info['remove_role_id']} from {message.author.name}")
        await remove_role(message, role_info['remove_role_id'])


async def get_message_count(message: discord.Message):
    if message.author.bot:
        return
    pool = await database.create_pool()
    user_id = message.author.id
    message_count = await database.get_message_count(user_id, pool)
    if message_count == None:
        message_count = 0
    message_count += 1
    await database.update_messages_count(user_id, message_count, pool)
    logger.info(f"{message.author.name} messages: {message_count}")
    if pool:
        await pool.close()
    await check_message_count(message, message_count)
    return message_count


async def print_messages_leaderboard(message: discord.Interaction, limit : int = 10):
    pool = await database.create_pool()
    result = await database.get_all_message_count(limit, pool)
    max_user_length = 32
    user = f"Member"
    message_count = "Messages"
    resultstring = ""
    resultstring += f"{user.ljust(max_user_length)}: {message_count}\n"
    counter = 1

    for row in result:
        logger.info(row)
        user_id = str(row['user_id'])
        message_count = row['message_count']
        try:
            user = await message.user.guild.fetch_member(user_id) # type: ignore
            user = f"{counter}. {user.name}"
        except:
            user = f"{counter}. Deleted User"
        counter += 1
        logger.info(f"{user}: {user_id}")
        resultstring += f"{user.ljust(max_user_length)}: {message_count}\n"
    
    if pool:
        await pool.close()
        
    return resultstring


async def print_haram_leaderboard(message: discord.Interaction, limit : int = 10):
    pool = await database.create_pool()
    result = await database.get_all_haram_count(limit, pool)
    max_user_length = 32
    user = f"Member"
    message_count = "Violations"
    resultstring = ""
    resultstring += f"{user.ljust(max_user_length)}: {message_count}\n"
    counter = 1

    for row in result:
        logger.info(row)
        user_id = str(row['user_id'])
        message_count = row['violations']
        try:
            user = await message.user.guild.fetch_member(user_id) # type: ignore
            user = f"{counter}. {user.name}"
        except:
            user = f"{counter}. Deleted User"
        counter += 1
        logger.info(f"{user}: {user_id}")
        resultstring += f"{user.ljust(max_user_length)}: {message_count}\n"
    
    if pool:
        await pool.close()
        
    return resultstring


async def print_relative_messages_leaderboard(message: discord.Interaction):
    result = ""
    max_user_length = 32
    pool = await database.create_pool()
    user_id = message.user.id
    query = f"WITH RankedMessages AS (SELECT *, ROW_NUMBER() OVER (ORDER BY message_count DESC) AS row_position FROM messages) SELECT row_position FROM RankedMessages WHERE user_id = {user_id};"
    position = await database.fetch_value(query, pool)
    if position-2 < 1:
        position = 0
    else:
        position -= 3
    query = f"SELECT * FROM messages order by message_count DESC LIMIT 5 OFFSET {position};"
    rows = await database.fetch_values(query, pool)

    for row in rows:
        logger.info(row)
        user_id = str(row['user_id'])
        message_count = row['message_count']
        try:
            user = await message.user.guild.fetch_member(user_id) # type: ignore
            user = f"{position+1}. {user.name}"
        except:
            user = f"{position+1}. Deleted User"
        position += 1
        logger.info(f"{user}: {user_id}")
        result += f"{user.ljust(max_user_length)}: {message_count}\n"

    return result


async def print_relative_haram_leaderboard(message: discord.Interaction):
    result = ""
    max_user_length = 32
    pool = await database.create_pool()
    user_id = message.user.id
    query = f"WITH RankedMessages AS (SELECT *, ROW_NUMBER() OVER (ORDER BY violations DESC) AS row_position FROM haram_words) SELECT row_position FROM RankedMessages WHERE user_id = {user_id};"
    position = await database.fetch_value(query, pool)
    if position-2 < 1:
        position = 0
    else:
        position -= 3
    query = f"SELECT * FROM haram_words order by violations DESC LIMIT 5 OFFSET {position};"
    rows = await database.fetch_values(query, pool)

    for row in rows:
        logger.info(row)
        user_id = str(row['user_id'])
        message_count = row['violations']
        try:
            user = await message.user.guild.fetch_member(user_id) # type: ignore
            user = f"{position+1}. {user.name}"
        except:
            user = f"{position+1}. Deleted User"
        position += 1
        logger.info(f"{user}: {user_id}")
        result += f"{user.ljust(max_user_length)}: {message_count}\n"

    return result


async def find_difference(message: discord.Interaction):
    pool = await database.create_pool()
    user_id = message.user.id
    input_number = await database.get_message_count(user_id, pool)
    number_list = message_count_roles
    # Filter numbers greater than the input number
    filtered_numbers = [num for num in number_list if num > input_number]

    if not filtered_numbers:
        return None  # No bigger number found
    
    message_per_day = 1.0

    try:
        user_messages, user_minutes = await database.fetch_user_last_1000_message_count(pool, user_id)
        message_per_day = (user_messages/user_minutes)*1440
    except:
        logging.info("User is not in the 1000 message table.")

        server_timezone = pytz.utc
        try:
            user = await message.user.guild.fetch_member(user_id) # type: ignore
            if user.joined_at:
                joined_date = user.joined_at.replace(tzinfo=server_timezone)
                current_date = datetime.utcnow().replace(tzinfo=pytz.utc)
                days_since_join = (current_date - joined_date).days

                # Avoid division by zero
                message_per_day = input_number / max(1, days_since_join)
        except:
            logger.info("User not found.")


    # Find the smallest number among the filtered numbers
    next_biggest_number = min(filtered_numbers)
    role_id = message_count_roles.get(next_biggest_number, {}).get('add_role_id', None)
    role_name = "???"
    if role_id != None:    
        guild = message.guild
        logger.info(guild)
        if guild:
            role = guild.get_role(role_id)
            if role:
                role_name = role.name

    # Calculate the difference
    difference = next_biggest_number - input_number
    days_until_next_role = round(difference/message_per_day,3)
    logger.info(f"difference: {difference} — messages: {input_number}")
    logger.info(f"<@{user_id}>, you have {input_number} messages, your next rank ({role_name}) will be in {difference} messages. [{days_until_next_role} days]")

    return f"<@{user_id}>, you have {input_number} messages, your next rank ({role_name}) will be in {difference} messages. [{days_until_next_role} days]"



def reduce_dict_around_key(input_dict, target_key: str):
    # Find the position of the key in the dictionary
    key_index = list(input_dict.keys()).index(target_key)
    low = max(key_index-2,0)
    if low == 0:
        high = 5
    else:
        high = min(key_index+3, len(input_dict))

    # Create a new dictionary with elements 2 positions before and 2 positions ahead
    reduced_dict = {}
    for i in range(low, high):
        if 0 <= i < len(input_dict):
            key = list(input_dict.keys())[i]
            reduced_dict[key] = input_dict[key]

    return reduced_dict


async def messages_per_day(message: discord.Interaction) -> str:
    limit = 50
    pool = await database.create_pool()
    result = await database.get_all_message_count(limit, pool)
    max_user_length = 32
    user = f"Member"
    message_count = "Messages/Day"
    resultstring = ""
    resultstring += f"{user.ljust(max_user_length)}: {message_count}\n"
    counter = 1
    # Check if the role exists in the server
    guild = message.guild
    logger.info(guild)
    server_timezone = pytz.utc

    message_per_day_dict = {}

    for row in result:
        user_id = str(row['user_id'])
        message_count = row['message_count']
        if guild:
            try:
                user = await message.user.guild.fetch_member(user_id) # type: ignore
                if user.joined_at:
                    joined_date = user.joined_at.replace(tzinfo=server_timezone)
                    current_date = datetime.utcnow().replace(tzinfo=pytz.utc)
                    days_since_join = (current_date - joined_date).days

                    # Avoid division by zero
                    message_per_day = message_count / max(1, days_since_join)

                    message_per_day_dict[user_id] = round(message_per_day, 3)
            except:
                logger.info("User not found, I guess.")

    # Sort the dictionary based on message_per_day values (highest to lowest)
    sorted_message_per_day = dict(sorted(message_per_day_dict.items(), key=lambda item: item[1], reverse=True))
    
    counter = list(sorted_message_per_day.keys()).index(str(message.user.id))
    if counter == 2:
        counter = 1
    elif counter > 2:
        counter -= 2

    sorted_message_per_day = reduce_dict_around_key(sorted_message_per_day, str(message.user.id))

    for user_id, messages_per_day in sorted_message_per_day.items():
        user = await message.user.guild.fetch_member(user_id) # type: ignore
        user = f"{counter+1}. {user.name}"
        logger.info(f"{user}: {user_id}")
        resultstring += f"{user.ljust(max_user_length)}: {messages_per_day}\n"
        counter += 1

    logger.info(resultstring)
    return resultstring


async def get_ranks(message: discord.Interaction) -> str:
    return_string = ""
    guild = message.guild
    max_role_length = 40
    if guild:
        for number, roles_info in message_count_roles.items():
            add_role_id = roles_info.get('add_role_id')
            role_name = await get_role_name(guild, add_role_id)

            if role_name:
                return_string += (f"{number:40}: {role_name}\n")
    return return_string


async def find_difference_all(message: discord.Interaction):
    results = ""
    user_data = []
    pool = await database.create_pool()
    limit = 69
    result = await database.get_all_message_count(limit, pool)
    number_list = message_count_roles

    for row in result:
        user_id = str(row['user_id'])
        input_number = row['message_count']

        # Filter numbers greater than the input number
        filtered_numbers = [num for num in number_list if num > input_number]

        if not filtered_numbers:
            filtered_numbers = [1000000]  # No bigger number found
    
        message_per_day = 0.0
        server_timezone = pytz.utc
        try:
            user = await message.user.guild.fetch_member(user_id) # type: ignore
            user_name = user.name
            if user.joined_at:
                joined_date = user.joined_at.replace(tzinfo=server_timezone)
                current_date = datetime.utcnow().replace(tzinfo=pytz.utc)
                days_since_join = (current_date - joined_date).days

                # Avoid division by zero
                message_per_day = input_number / max(1, days_since_join)

            # Find the smallest number among the filtered numbers
            next_biggest_number = min(filtered_numbers)
            # Calculate the difference
            difference = next_biggest_number - input_number
            days_until_next_role = round(difference/message_per_day,3)
            logger.info(f"difference: {difference} — messages: {input_number} — user: {user_name} — messages per day: {message_per_day}")
            user_data.append({"user_name": user_name, "days_until_next_role": days_until_next_role, "difference": difference})
            
        except:
            logger.info("User not found.")

    sorted_user_data = sorted(user_data, key=lambda x: x['days_until_next_role'])
    results += (f"{'User Name':<15}{'Days':<10}{'Difference':<10}\n")
    for user in sorted_user_data[:30]:
        results += (f"{user['user_name'][:15]:<15}{user['days_until_next_role']:<10}{user['difference']:<10}\n")

    logger.info(results)
    return results


async def get_sorted_message_counts() -> Dict[int, int]:
    pool = await database.create_pool()
    result = await database.get_all_message_count(100, pool)
    if pool:
        await pool.close()
    message_counts = {row['user_id']: row['message_count'] for row in result}
    message_counts = {user_id: message_count for user_id, message_count in message_counts.items() if message_count >= 10}
    sorted_message_counts = dict(sorted(message_counts.items(), key=lambda item: item[1], reverse=True))
    return sorted_message_counts


def compare_message_counts(dict1: Dict[int, int], dict2: Dict[int, int]) -> Dict[int, List[int]]:
    changes = {}
    # Compare the message counts in dict2 to dict1
    for user_id, count2 in dict2.items():
        count1 = dict1.get(user_id, 0)
        print(f"{user_id}: {count2} - {count1}")
        if count2 > count1:
            overtaken_users = []
            for uid, count in dict1.items():
                new_count = dict2.get(uid, 0)
                print(f"{new_count} - {count}")
                if new_count < count2 and count >= count1:
                    overtaken_users.append(uid)
            if overtaken_users:
                changes[user_id] = overtaken_users
    return changes


async def get_overtake_counts(message: discord.Interaction, user_id, pool) -> str:
    pool = await database.create_pool()
    result = await database.get_overtake_message_count(pool)

    message_counts = {row['user_id']: row['message_count'] for row in result}
    user_messages, user_minutes = await database.fetch_user_last_1000_message_count(pool, user_id)

    previous_user_id, difference, position = find_previous_user_difference_and_rank(user_id, message_counts)
    previous_messages, previous_minutes = await database.fetch_user_last_1000_message_count(pool, previous_user_id)

    user = await message.user.guild.fetch_member(user_id) # type: ignore
    
    try:
        previous = await message.user.guild.fetch_member(previous_user_id) # type: ignore
    except: 
        previous = "Some Bozo that left the Server"


    difference_messages = user_messages - previous_messages

    if user_messages == 0:
        catch_up = "Send a message and we might be able to find out when that will happen."

    elif difference_messages < 0:
        catch_up = f"You send fewer messages than {previous}, mate. It's not happening."

    elif previous_messages == 0 and difference:
        minutes_until_caught_up = difference / (user_messages / user_minutes)
        minutes_until_caught_up = round(minutes_until_caught_up, 1)
        catch_up = f"You need around {convert_minutes_to_string(minutes_until_caught_up)} until you have caught up with {previous}."
    
    elif difference:
        user_rate = user_messages / user_minutes
        previous_rate = previous_messages / previous_minutes
        time_to_catch_up = round(difference / (user_rate - previous_rate), 2)
        if time_to_catch_up < 0:
            catch_up = "Your message sending pace is too slow.. Unlucky."
        else:
            catch_up = f"You need around {convert_minutes_to_string(time_to_catch_up)} until you have caught up with {previous}."

    return f"{user} you need {difference} messages to overtake {previous} for position {position-1}\n{catch_up}"


async def get_overtake_counts_from_message(message: discord.Message) -> str:
    user_id = message.author.id
    pool = await database.create_pool()
    result = await database.get_overtake_message_count(pool)

    message_counts = {row['user_id']: row['message_count'] for row in result}
    user_messages, user_minutes = await database.fetch_user_last_1000_message_count(pool, user_id)

    previous_user_id, difference, position = find_previous_user_difference_and_rank(user_id, message_counts)
    previous_messages, previous_minutes = await database.fetch_user_last_1000_message_count(pool, previous_user_id)

    user = await message.author.guild.fetch_member(user_id) # type: ignore
    
    try:
        previous = await message.author.guild.fetch_member(previous_user_id) # type: ignore
    except: 
        previous = "Some Bozo that left the Server"


    difference_messages = user_messages - previous_messages

    if user_messages == 0:
        catch_up = "Send a message and we might be able to find out when that will happen."

    elif difference_messages < 0:
        #catch_up = f"You send fewer messages than {previous}, mate. It's not happening."
        return ""

    elif previous_messages == 0 and difference:
        minutes_until_caught_up = difference / (user_messages / user_minutes)
        minutes_until_caught_up = round(minutes_until_caught_up, 1)
        catch_up = f"You need around {convert_minutes_to_string(minutes_until_caught_up)} until you have caught up with {previous}."
    
    elif difference:
        user_rate = user_messages / user_minutes
        previous_rate = previous_messages / previous_minutes
        time_to_catch_up = round(difference / (user_rate - previous_rate), 2)
        if time_to_catch_up < 0:
            catch_up = "Your message sending pace is too slow.. Unlucky."
        else:
            catch_up = f"You need around {convert_minutes_to_string(time_to_catch_up)} until you have caught up with {previous}."

    return f"{user} you need {difference} messages to overtake {previous} for position {position-1}\n{catch_up}"


def find_previous_user_difference_and_rank(target_user_id, message_counts):
    # Convert the dictionary to a sorted list of tuples (user_id, message_count)
    sorted_message_counts = sorted(message_counts.items(), key=lambda item: item[1], reverse=True)
    
    # Extract user_ids and their message_counts separately
    user_ids = [user_id for user_id, _ in sorted_message_counts]
    message_counts_sorted = {user_id: count for user_id, count in sorted_message_counts}
    
    # Find the index (rank) of the target user
    rank = user_ids.index(target_user_id) + 1  # Rank is 1-based
    
    if rank > 1:
        previous_user_id = user_ids[rank - 2]
        difference = message_counts_sorted[previous_user_id] - message_counts_sorted[target_user_id]
        return previous_user_id, difference, rank
    else:
        return None, None, rank  # If the target user is the first in the list
    

async def make_funny_graph(data: dict, message: discord.Message):
    name_dict = {}
    
    # Fetch and map user names to message counts
    for id, count in data.items():
        try:
            member = await message.guild.fetch_member(id)
            name = member.name
        except:
            name = "Unknown User"
        name_dict[name] = count
        logging.info(f"{name}: {count}")
    
    # Sort the dictionary by message count in descending order
    sorted_name_dict = dict(sorted(name_dict.items(), key=lambda item: item[1], reverse=True))

    total_messages = sum(int(count) for count in sorted_name_dict.values())
    others_count = 0
    others_others_count = 0
    filtered_names = []
    filtered_counts = []
    others_names = []
    others_counts = []
    others_others_names = []
    others_others_counts = []
    


    # Separate users with less than 1% messages into "Others"
    for name, count in sorted_name_dict.items():
        if (int(count) / total_messages) >= 0.08:
            filtered_names.append(name)
            filtered_counts.append(count)
        
        elif (int(count) / total_messages) >= 0.016:
            others_count += int(count)
            others_names.append(name)
            others_counts.append(count)

        else:
            others_others_count += int(count)
            others_others_names.append(name)
            others_others_counts.append(count)
    
    if others_count > 0:
        filtered_names.append("Others")
        filtered_counts.append(others_count+others_others_count)
    
    if others_others_count > 0:
        others_names.append("Others")
        others_counts.append(others_others_count)
    
    
    # Create the pie chart for proper people
    plt.figure(figsize=(8, 8))
    plt.pie(filtered_counts, labels=filtered_names, autopct='%1.1f%%', startangle=140, colors=plt.cm.Paired.colors)
    plt.title('Messages by Users in last 1000 Server Messages', y=1.02)
    
    # Adjust layout and remove some borders
    plt.subplots_adjust(top=0.99, bottom=0.00)
    
    # Save the chart to a file
    plt.savefig("funny_pie_chart.png")
    plt.close()

    ########

    # Create the pie chart for others
    plt.figure(figsize=(8, 8))
    plt.pie(others_counts, labels=others_names, autopct='%1.1f%%', startangle=140, colors=plt.cm.Paired.colors)
    plt.title(f'Messages by Others in {sum(others_counts)} of the last 1000 Server Messages', y=1.02)
    
    # Adjust layout and remove some borders
    plt.subplots_adjust(top=0.99, bottom=0.00)
    
    # Save the chart to a file
    plt.savefig("others.png")
    plt.close()

    ########

    # Create the pie chart for others
    plt.figure(figsize=(8, 8))
    plt.pie(others_others_counts, labels=others_others_names, autopct='%1.1f%%', startangle=140, colors=plt.cm.Paired.colors)
    plt.title(f'Messages by Others in {sum(others_others_counts)} of the last 1000 Server Messages', y=1.02)
    
    # Adjust layout and remove some borders
    plt.subplots_adjust(top=0.99, bottom=0.00)
    
    # Save the chart to a file
    plt.savefig("others_others.png")
    plt.close()

    return "funny_pie_chart.png", "others.png", "others_others.png"


async def handle_voice_channel_activity(
        member: discord.Member, 
        time_spend_in_voice: int
        ):
    
    pool = await database.create_pool()
    current_time = await database.get_voice_activity(member.id, pool)
    new_time = current_time + time_spend_in_voice
    await database.update_voice_activity(member.id, new_time, pool)
    
    if pool:
        await pool.close()


async def print_voice_leaderboard(message: discord.Interaction, limit : int = 10):
        
    def convert_seconds_to_dhms(seconds: int) -> str:
        days, seconds = divmod(seconds, 86400)    # 86400 seconds in a day
        hours, seconds = divmod(seconds, 3600)    # 3600 seconds in an hour
        minutes, seconds = divmod(seconds, 60)    # 60 seconds in a minute
        return f"{days}d:{hours:02}h:{minutes:02}m:{seconds:02}s"

    
    pool = await database.create_pool()
    result = await database.get_all_voice_activity(pool, limit)
    max_user_length = 32
    user = f"Member"
    voice_activity = "Time spend in voice"
    resultstring = ""
    resultstring += f"{user.ljust(max_user_length)}: {voice_activity}\n"
    counter = 1

    for row in result:
        logger.info(row)
        user_id = str(row['user_id'])
        voice_activity = row['time_in_voice_channel']
        try:
            user = await message.user.guild.fetch_member(user_id) # type: ignore
            user = f"{counter}. {user.name}"
        except:
            user = f"{counter}. Unknown Bozo"
        counter += 1
        logger.info(f"{user}: {user_id}")
        resultstring += f"{user.ljust(max_user_length)}: {convert_seconds_to_dhms(voice_activity)}\n"
        logging.info(resultstring)
    
    if pool:
        await pool.close()
        
    return resultstring


def compare_voice_activity(dict1: Dict[int, int], dict2: Dict[int, int]) -> Dict[int, List[int]]:
    changes = {}
    # Compare the message counts in dict2 to dict1
    for user_id, count2 in dict2.items():
        count1 = dict1.get(user_id, 0)
        logging.info(f"{user_id}: {count2} - {count1}")
        if count2 > count1:
            overtaken_users = []
            for uid, count in dict1.items():
                new_count = dict2.get(uid, 0)
                logging.info(f"{new_count} - {count}")
                if new_count < count2 and count >= count1:
                    overtaken_users.append(uid)
            if overtaken_users:
                changes[user_id] = overtaken_users
    return changes


async def get_sorted_voice_activity() -> Dict[int, int]:
    pool = await database.create_pool()
    result = await database.get_all_voice_activity(pool, 100)
    if pool:
        await pool.close()
    message_counts = {row['user_id']: row['time_in_voice_channel']for row in result}
    message_counts = {user_id: message_count for user_id, message_count in message_counts.items() if message_count >= 10}
    sorted_message_counts = dict(sorted(message_counts.items(), key=lambda item: item[1], reverse=True))
    logging.info(sorted_message_counts)
    return sorted_message_counts