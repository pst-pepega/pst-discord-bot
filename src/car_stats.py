import requests
import json
import logging
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from constants import LFM_SEASON

import bop

logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def get_track_id(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 223 <= track['track_id'] <= 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}
    logger.info(track_dict)

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = bop.find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def get_stats(track, car_class, sorting = 'wins'):
    if sorting == "entries":
        sorting = "races"
    # URL to fetch JSON data from
    if track == 'all':
        url = f"https://api2.lowfuelmotorsport.com/api/statistics/carStats?class={car_class}&season={LFM_SEASON}&sim=1"
    else:
        url = f'{LFM_API}/statistics/carStats?class={car_class}&season={LFM_SEASON}&track={get_track_id(track)}&sim=1'
    print(url)
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
    except:
        logger.info(f"{url} didn't load.")
        return
    
    total_entries = 0
    for entry in data:
        total_entries += int(entry['races'])

    total_wins = sum(entry['wins'] for entry in data)

    for entry in data:
        entry['podium'] = entry['podium'] + entry['wins']
        entry['top5'] = entry['top5'] + entry['podium']
        entry['top10'] = entry['top10'] + entry['top5']
        try:
            entry['wins_per_entry'] = round(entry['wins'] / entry['races'], 3)
        except: 
            entry['wins_per_entry'] = 0.00 
        try:
            entry['podium_per_entry'] = round(entry['podium'] / entry['races'], 3)
        except: 
            entry['podium_per_entry'] = 0.00 
        try:
            entry['top5_per_entry'] = round(entry['top5'] / entry['races'], 3)
        except: 
            entry['top5_per_entry'] = 0.00 
        try:
            entry['top10_per_entry'] = round(entry['top10'] / entry['races'], 3)
        except: 
            entry['top10_per_entry'] = 0.00 

            
        try:
            entry['wins_per_race'] = round(entry['wins'] / total_wins, 3)
        except: 
            entry['wins_per_race'] = 0.00 
        try:
            entry['podium_per_race'] = round(entry['podium'] / total_wins, 3)
        except: 
            entry['podium_per_race'] = 0.00 
        try:
            entry['top5_per_race'] = round(entry['top5'] / total_wins, 3)
        except: 
            entry['top5_per_race'] = 0.00 
        try:
            entry['top10_per_race'] = round(entry['top10'] / total_wins, 3)
        except: 
            entry['top10_per_race'] = 0.00 
        try:
            entry['entry_rate'] = round(entry['races'] / total_entries, 3)
        except: 
            entry['entry_rate'] = 0.00 

    # Now your_list contains the additional calculated fields for each entry
    
    data = sorted(data, key=lambda x: x[sorting], reverse=True)

    return data


def get_image(data):
    # Filter out entries where "races" is 0 (if needed)
    #data = [entry for entry in data if entry['races'] > 0]

    # Keys to remove
    keys_to_remove = ['car_id', 'server_value', 'year', 'class', 'content_link', 'content_link_name', 'modder_name', 'modder_link', 'sim_id', 'non_base_content']

    # Loop through the list and create new dictionaries with filtered keys
    #filtered_data = [ {key: value for key, value in item.items() if key not in keys_to_remove} for item in data]
    filtered_data = data

    # Define the data
    data = {
        "Car Name": [entry['car_name'] for entry in filtered_data],
        "Entries": [entry['races'] for entry in filtered_data],
        "Wins": [entry['wins'] for entry in filtered_data],
        "Podiums": [entry['podium'] for entry in filtered_data],
        "Top 5": [entry['top5'] for entry in filtered_data],
        "Top 10": [entry['top10'] for entry in filtered_data],
        "Entry Rate": [entry['entry_rate'] for entry in filtered_data],
        "Wins/Race": [entry['wins_per_race'] for entry in filtered_data],
        "Podiums/Race": [entry['podium_per_race'] for entry in filtered_data],
        "Top 5/Race": [entry['top5_per_race'] for entry in filtered_data],
        "Top 10/Race": [entry['top10_per_race'] for entry in filtered_data],
        "Wins/Entry": [entry['wins_per_entry'] for entry in filtered_data],
        "Podiums/Entry": [entry['podium_per_entry'] for entry in filtered_data],
        "Top 5/Entry": [entry['top5_per_entry'] for entry in filtered_data],
        "Top 10/Entry": [entry['top10_per_entry'] for entry in filtered_data]
    }

    # Create a DataFrame
    df = pd.DataFrame(data)

    # Define colors for highlighting
    colors = ['#D4AF37', '#C0C0C0', '#CD7F32']  # Gold, Silver, Bronze

    # Function to highlight top 3 values
    def highlight_top3(s):
        if pd.api.types.is_numeric_dtype(s):
            is_top3 = s.nlargest(3).index
            return ['background-color: {}'.format(colors[is_top3.get_loc(i)]) if i in is_top3 else '' for i in s.index]
        return [''] * len(s)

    # Apply the highlighting function to numeric columns
    highlighted_df = df.apply(highlight_top3)

    # Create the plot
    fig_width = 16
    fig_height = fig_width / 16 * 9  # Calculate height to maintain 16:9 aspect ratio

    # Create the figure with specified size
    fig, ax = plt.subplots(figsize=(fig_width, fig_height))
    ax.axis('tight')
    ax.axis('off')

    # Create the table
    the_table = ax.table(cellText=df.values, colLabels=df.columns, cellLoc='center', loc='center', fontsize=200)
    the_table.auto_set_font_size(False)
    the_table.set_fontsize(15)
    # Adjust column widths
    the_table.auto_set_column_width(col=list(range(len(df.columns))))  # Auto-set column widths
    the_table.auto_set_column_width(col=list(range(1)))# Set row heights
    # Set row heights
    for i in range(len(df)+1):
        for j in range(len(df.columns)):
            the_table._cells[(i, j)].set_height(0.08)

    # Manually apply cell colors
    for (i, j), val in np.ndenumerate(df.values):
        cell_color = highlighted_df.iloc[i, j]
        if cell_color:
            the_table[(i+1, j)].set_facecolor(cell_color.strip("background-color: "))

    file_name = 'car_stats.jpg'
    # Save the figure as "car_stats.jpg"
    plt.savefig(file_name, bbox_inches='tight', dpi=300)
    return file_name
    #plt.show()


def create_pie_charts(data, track_name: str):

    # Function to categorize small slices into "Others"
    def categorize_others(metric_data, threshold=3):
        total = sum(metric_data.values())
        others = {k: v for k, v in metric_data.items() if (v / total * 100) < threshold}
        metric_data = {k: v for k, v in metric_data.items() if (v / total * 100) >= threshold}
        if others:
            metric_data['Others'] = sum(others.values())
        return metric_data
    
    
    data = [
        entry for entry in data 
        if not (
            (entry['car_name'] == "Bentley Continental" or entry['car_name'] == "Mercedes-AMG GT3")
            and entry['year'] == 2015
        )
    ]    

    # Extract data for each metric and categorize others
    metrics = ['wins', 'podium', 'top5', 'top10', 'races']
    metric_data = {metric: {car['car_name']: car[metric] for car in data} for metric in metrics}
    metric_data = {metric: categorize_others(data) for metric, data in metric_data.items()}

    
    print(metric_data)

    def save_pie_chart(metric_data, title, filename):
        labels = metric_data.keys()
        sizes = metric_data.values()

        # Generate a list of colors using a colormap (e.g., tab20)
        cmap = plt.get_cmap('tab20')
        colors = cmap(np.linspace(0, 1, len(sizes)))  # Generate colors based on the number of slices

        fig, ax = plt.subplots(figsize=(25, 18))
        wedges, texts, autotexts = ax.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=140,
                                        pctdistance=0.85, labeldistance=1.05, colors=colors)

        for text in texts:
            text.set_fontsize(30)
        for autotext in autotexts:
            autotext.set_fontsize(30)
            autotext.set_color('white')

        plt.title(title, fontsize=30)
        plt.tight_layout()
        plt.savefig(filename)
        plt.close()

    # Create and save pie charts
    save_pie_chart(metric_data['wins'], 'Wins', f'{track_name}_pie_chart_wins.jpg')
    save_pie_chart(metric_data['podium'], 'Podiums', f'{track_name}_pie_chart_podiums.jpg')
    save_pie_chart(metric_data['top5'], 'Top5', f'{track_name}_pie_chart_top5.jpg')
    save_pie_chart(metric_data['top10'], 'Top10', f'{track_name}_pie_chart_top10.jpg')
    save_pie_chart(metric_data['races'], 'Entries', f'{track_name}_pie_chart_entries.jpg')


#### TESTING ####

# import Utils
# track = "all"
# logger.info(f"{track}")

# data = get_stats(track, "GT4") # type: ignore
# create_pie_charts(data, track)
# get_image(data)