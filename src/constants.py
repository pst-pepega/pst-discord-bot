import enum

PST_GUILD_ID = 1015601496743751740
KYLO_TEST_GUILD_ID = 430397412658380811
PST_GENERAL_CHANNEL_ID = 1015601499117727856
LFM_API = "https://api2.lowfuelmotorsport.com/api"
LFM_SEASON = 27 ## change accordingly


class Users(enum.Enum):
    MAX_VERSTAPPEN_SIMPS = "<@&1131921190940835870>"
    COOPER = "<@205991837188489216>"
    ZABBZI = "<@105462107755020288>"

    def __str__(self):
        return self.value


class Emotes(enum.Enum):
    SOB_CAT = "<a:sobcat:1116005972486410311>"
    AYAYA = "<:AYAYA:1102246176645988393>"
    KEKWBYE = "<a:KEKBye:1109520753763819660>"
    BURNS_COPIUM = "<a:CopiumBurns:1142414895632109608>"
    CUM = "<:cumm:1088430678523260930>"
    PEPOG = "<:PepoG:1016031408642326548>"
    CHAD = "<:donkChad:1015967748460908594>"
    CATJAM = "<a:catJAM:1109904362920231043>"
    PEEPOTOMATO = "<a:peepoTomato:1090947820468572251>"
    AMOEYES = "<:amoEyes:1016346504383959167>"
    OWOBELLE = "<:owoBelle:1016348306701561856>"
    KISS = "<a:Kiss:1017776062425473064>"
    HABIBI = "<:Habibi:1017076484302700616>"
    CATJAMGIGA = "<a:catjamGIGA:1140613934945153095>"
    PEEPOSMOKE = "<a:peeposmoke:1055431214229618708>"
    FRFR = "<:frfr:1084918119425900595>"
    CATGUN = "<:cat_gun:1102246188255805534>"
    GUNN = "<:gunn:1084918046436638730>"
    POPCAT = "<a:bopcat:1116003631855063071>"
    DONKCLAP = "<a:donkClap:1016349206442688532>"
    PEERGI = "<a:peergi:1171190874009522246>"
    PEEFLAP = "<a:peeflapp:1171190888060432444>"
    PEEROLL = "<a:peeRoll:1171190849065992192>"
    HARLEY = "<a:Harley:1214217568726949948>"
    PEEPOHEART = "<:peepoheart:1112477859680899122>"
    WICKED = "<:WICKED:1015967738985979925>"
    BINOCULARS = "<:Binoculars:1101918533811707914>"
    BOZO = "<a:bozo:1155535348277973062>"

    def __str__(self):
        return self.value


class WeatherSimDay(enum.Enum):
    Friday = 0
    Saturday = 1
    Sunday = 2


PST_SERVER_DRIVERS = [
        "Teemu Karppinen", "Dennis Schöps", "Florian Toenjes", "Jimmy Holm",
        "Bruhier Maedre", "Candide Moonen", "Alessio Melis",
        "Hubert Szymański", "Ryan Cooper", "Ahmet Serdar Kargulu",
        "Johnny Murasaki", "Janis Purviņš", "Geert Fischer",
        ]

ACC_TRACKS = (
        'Brands Hatch Circuit', 'Hungaroring', 'Misano',
        'Autodromo Nazionale di Monza', 'Nürburgring', 'Circuit de Paul Ricard',
        'Silverstone', 'Circuit de Spa Francorchamps', 'Zolder', 'Zandvoort',
        'Suzuka Circuit', 'Laguna Seca', 'Kyalami', 'Oulton Park',
        'Donington Park', 'Snetterton', 'Indianapolis', 'Watkins Glen',
        'Spielberg - Red Bull Ring', 'Nürburgring Nordschleife', 'Imola',
        'Cota', 'Bathurst', 'Valencia', 'Barcelona')


class Series(enum.StrEnum):
    GT2 = "GT2 Series"
    GT3 = "GT3 Series"
    GT4 = "GT4 Series"
    M2 = "BMW M2 Series"
    Duo = "DUO Cup"
    Pro = "PRO Series"
    Sprint = "Sprint Series"
    Single_Make = "Single Make Series"
    Endurance = "Endurance Series"


class Series_to_ID(enum.IntEnum):
    Endurance = 512
    Nords = 378
    GT3 = 505
    GT4 = 509
    M2 = 383
    Duo = 511
    Multiclass = 510
    Pro = 503
    Sprint = 507
    Single_Make = 513

    def get_id(series_name): # type: ignore
        return Series_to_ID[series_name].value # type: ignore
    
    def get_name(series_id): # type: ignore
        for name, member in Series_to_ID.__members__.items():
            if member.value == series_id:
                return name
        raise ValueError(f"No matching name found for ID: {series_id}")


    def __int__(self):
        return self.value
    
