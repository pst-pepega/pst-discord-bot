import requests

def get_team_standings(id, team = "Porsche S🅱️innala Team"):
	# Flag to check if team has been encountered
	porsche_encountered = False

	# Make an HTTP GET request to the URL
	response = requests.get(f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonTeamStandings/{id}")

	# Check if the request was successful (status code 200)
	if response.status_code == 200:
		# Parse the JSON data from the response content
		data = response.json()

		if 'GT3' in data:
			standings = "# GT3 Standings\n```\n"
			# Loop over the 'GT3' data
			for x in range(10):
				position = data['GT3'][x]['position']
				teamname = data['GT3'][x]['teamname']
				teampoints = data['GT3'][x]['teampoints']
				teamname = f"{position}. {teamname}"

				print(f"{position}: {teamname} {teampoints}")
				standings += f"{teamname.ljust(40)} {teampoints}\n"


				# Check if teamname is team
				if teamname == team:
					porsche_encountered = True
					

			# If team has not been encountered, continue until 15
			if not porsche_encountered:
				for x in range(10, min(16, len(data['GT3']))):
					position = data['GT3'][x]['position']
					teamname = data['GT3'][x]['teamname']
					teampoints = data['GT3'][x]['teampoints']
					teamname = f"{position}. {teamname}"

					print(f"{position}: {teamname} {teampoints}")
					standings += f"{teamname.ljust(40)} {teampoints}\n"

					# Break the loop if teamname is team
					if teamname == team:
						return f"{standings}\n```"
				
				return f"{standings}\n```"
						
			else:
				return f"{standings}\n```"
			
		elif 'GT4' in data:
			standings = "# GT4 Standings\n```\n"
			# Loop over the 'GT4' data
			for x in range(10):
				position = data['GT4'][x]['position']
				teamname = data['GT4'][x]['teamname']
				teampoints = data['GT4'][x]['teampoints']
				teamname = f"{position}. {teamname}"

				print(f"{position}: {teamname} {teampoints}")
				standings += f"{teamname.ljust(40)} {teampoints}\n"


				# Check if teamname is team
				if teamname == team:
					porsche_encountered = True
					

			# If team has not been encountered, continue until 15
			if not porsche_encountered:
				for x in range(10, min(16, len(data['GT4']))):
					position = data['GT4'][x]['position']
					teamname = data['GT4'][x]['teamname']
					teampoints = data['GT4'][x]['teampoints']
					teamname = f"{position}. {teamname}"

					print(f"{position}: {teamname} {teampoints}")
					standings += f"{teamname.ljust(40)} {teampoints}\n"

					# Break the loop if teamname is team
					if teamname == team:
						return f"{standings}\n```"
				
				return f"{standings}\n```"
						
			else:
				return f"{standings}\n```"
			
		else:
			standings = "# M2 Standings\n```\n"
			# Loop over the 'TCX' data
			for x in range(10):
				position = data['TCX'][x]['position']
				teamname = data['TCX'][x]['teamname']
				teampoints = data['TCX'][x]['teampoints']
				teamname = f"{position}. {teamname}"

				print(f"{position}: {teamname} {teampoints}")
				standings += f"{teamname.ljust(40)} {teampoints}\n"


				# Check if teamname is team
				if teamname == team:
					porsche_encountered = True
					

			# If team has not been encountered, continue until 15
			if not porsche_encountered:
				for x in range(10, min(16, len(data['TCX']))):
					position = data['TCX'][x]['position']
					teamname = data['TCX'][x]['teamname']
					teampoints = data['TCX'][x]['teampoints']
					teamname = f"{position}. {teamname}"

					print(f"{position}: {teamname} {teampoints}")
					standings += f"{teamname.ljust(40)} {teampoints}\n"

					# Break the loop if teamname is team
					if teamname == team:
						return f"{standings}\n```"
				
				return f"{standings}\n```"
						
			else:
				return f"{standings}\n```"

	else:
		print(f"Failed to fetch JSON data. Status code: {response.status_code}")
		return f"It's fucked"			