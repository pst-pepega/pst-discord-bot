import requests
from fuzzywuzzy import fuzz, process

def fetch_track_records():
	user_id = 1704
	series = "GT3"
	response = requests.get(f"https://api2.lowfuelmotorsport.com/api/users/getUserTrackRecords/{user_id}/{series}?car_id=&sim=1")
	if response.status_code == 200:
		json_data = response.json()
		return json_data
	else:
		print("Failed to fetch data")
		return None

def search_track(track_name):
	json_data = fetch_track_records()
	best_match = process.extractOne(track_name, [entry['track_name'] for entry in json_data])
	best_match_acc = process.extractOne(track_name, [entry['acc_track_name'] for entry in json_data])
	
	best_similarity = max(best_match[1], best_match_acc[1])
	
	if best_similarity >= 80:  # Adjust the threshold as needed
		if best_similarity == best_match[1]:
			matched_field = 'track_name'
			matched_value = best_match[0]
		else:
			matched_field = 'acc_track_name'
			matched_value = best_match_acc[0]
			
		for entry in json_data:
			if entry[matched_field] == matched_value:
				return entry
	else:
		return None

def calculate(track, incidents: int, laps: int):
	print(f"{track} {incidents} {laps}")
	data = search_track(track)
	turns = data['turns']
	total_turns = turns * laps
	incidents_per_turn = round(incidents/total_turns, 5)
	return f"{incidents_per_turn} IP/Turn [{incidents}/{total_turns}]"
