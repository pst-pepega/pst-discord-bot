import lfmhl
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def get_records():
	hotlappers_count = dict()
	still_available = ""

	track_list = lfmhl.get_all_tracks()
	car_list = {
		"Audi R8 LMS GT3 evo II 2022", 
		"AMR V8 Vantage 2019", 
		"Ferrari 296 GT3 2023", 
		"Bentley Continental 2018", 
		"Honda NSX GT3 Evo 2019", 
		"Lamborghini Huracan GT3 EVO 2 2023", 
		"Mercedes-AMG GT3 2020", 
		"BMW M4 GT3 2021", 
		"Porsche 992 GT3 R 2023", 
		"McLaren 720S GT3 Evo 2023"
}
	for track in track_list:
		hotlap_data = lfmhl.get_hotlaps(track)
		#logger.info(hotlap_data)
		
		for car in car_list:
			found = False  # Flag to check if the car was found for this track
			for hotlap in hotlap_data:
				if int(hotlap["gameversion"] > 49):
					if f'{hotlap["car_name"]} {hotlap["car_year"]}' == car:
						driver_name = f"{hotlap['vorname']} {hotlap['nachname']}"
						if driver_name in hotlappers_count:
							hotlappers_count[driver_name] += 1
						else:
							hotlappers_count[driver_name] = 1
						found = True  # Set the flag to True if the car was found
						break  # Break out of the hotlap loop as a record holder is found
			
			if not found:
				# Treat it as "Still available" and add or increment
				driver_name = "Still available"
				still_available += f"{car} + {track}\n"
				if driver_name in hotlappers_count:
					hotlappers_count[driver_name] += 1
				else:
					hotlappers_count[driver_name] = 1

	sorted_dict = dict(sorted(hotlappers_count.items(), key=lambda item: item[1], reverse=True))
	return sorted_dict, still_available
