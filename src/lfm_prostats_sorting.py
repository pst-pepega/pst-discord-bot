import csv


def get_headers():
    # Open the CSV file
    with open('resources/race_results.csv', 'r') as file:
        # Create a CSV reader object
        reader = csv.reader(file)
        
        # Get the header row
        headers = next(reader)
        
    return headers



def filter_csv(filters, input_file = 'resources/race_results.csv', output_file = "race_results_filtered.csv"):
    # Open the input CSV file
    with open(input_file, mode='r', newline='') as infile:
        reader = csv.DictReader(infile)
        
        # Get fieldnames from the input CSV
        fieldnames = reader.fieldnames
        
        # Open the output CSV file
        with open(output_file, mode='w', newline='') as outfile:
            writer = csv.DictWriter(outfile, fieldnames=fieldnames)
            
            # Write the header to the output file
            writer.writeheader()
            
            # Iterate through each row in the input file
            for row in reader:
                # Initialize a flag to determine if the row matches all filters
                match = True
                
                # Check each filter condition
                for filter_column, (condition, value) in filters.items():
                    if condition == "equals":
                        if str(row[filter_column]) != str(value):
                            match = False
                            break
                    elif condition == "greater_than":
                        if float(row[filter_column]) <= float(value):
                            match = False
                            break
                    elif condition == "less_than":
                        if float(row[filter_column]) >= float(value):
                            match = False
                            break
                    elif condition == "boolean":
                        if str(row[filter_column]).lower() != str(value).lower():
                            match = False
                            break
                    # Add more conditions as needed
                
                # If the row matches all filter conditions, write it to the output CSV
                if match:
                    writer.writerow(row)
    
    return output_file


def align_csvfile(input_file = "race_results_filtered.csv", output_file = "race_results_filtered_and_aligned.csv"):

    # Define the width of each column
    column_widths = {
        'season_week': 12,
        'vorname': 8,
        'nachname': 9,
        'origin': 6,
        'user_id': 8,
        'car_name': 25,
        'position': 10,
        'bestlap': 10,
        'time': 12,
        'time_penalty': 14,
        'dnf': 6,
        'points': 8,
        'gap': 10,
        'dns': 6,
        'dsq': 6,
        'car_number': 12,
        'bestOfTheWeek': 15,
        'track_name': 30,
        'competition': 15,
        'pole_position': 14,
        'fastest_lap': 12,
        'starting_position': 19
    }

    def format_row(row, widths):
        """Formats a single row to have aligned columns based on the specified widths."""
        formatted_row = []
        for key, value in row.items():
            formatted_row.append(f"{value:<{widths[key]}}")
        return ','.join(formatted_row)

    # Read the CSV file and format each row
    with open(input_file, 'r') as infile, open(output_file, 'w') as outfile:
        reader = csv.DictReader(infile)
        writer = csv.writer(outfile)

        # Write the header
        writer.writerow([f"{header:<{column_widths[header]}}" for header in reader.fieldnames])

        # Write the data rows
        for row in reader:
            formatted_row = format_row(row, column_widths)
            outfile.write(formatted_row + '\n')

        return output_file
