import time

# Record the start time
start_time = time.time()

# Your code or process
print("Doing some work...")
time.sleep(2)  # Simulating a time-consuming task

print(f"Time elapsed: {round(time.time() - start_time,3)} seconds")
