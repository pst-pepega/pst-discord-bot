import logging
from typing import Optional
import plotly.graph_objects as go
import pandas as pd
import matplotlib.pyplot as plt
import os
import requests
import csv
from datetime import datetime
import time

logging.basicConfig(level=logging.INFO)

def get_df(filename):     
    df = pd.read_csv(filename)

    return df


def get_drivers(df):
    drivers = df['driver'].unique().tolist()

    return drivers


def violin(drivers, df):
    fig = go.Figure()
    for driver in drivers:
        fig.add_trace(go.Violin(x=df['driver'][df['driver'] == driver],
                                y=df['time in seconds'][df['driver'] == driver],
                                spanmode="hard",
                                width=0,
                                name=driver,
                                box_visible=True,
                                meanline_visible=True)
        )    
    # Set figure size
    fig.update_layout(
        title='Laptime Distribution',
        title_x =0.5,
        width=len(df)*40,   # Set the width of the figure
        height=1500,   # Set the height of the figure        
        font=dict(
            size=len(df)/3  # Set the font size for all elements
        )
    )
    
    fig.write_image("violin.png")  # Save as PNG


def boxplot(drivers, df):
    fig = go.Figure()
    for stint in df['stint'].unique():
        filtered_data = df[df['stint'] == stint]
        driver = filtered_data['driver'].iloc[0]  # Get the driver for this stint
        fig.add_trace(go.Box(x=filtered_data['stint'],
                            y=filtered_data['time in seconds'],
                            name=f'{driver} - Stint {stint}',
                            boxmean=True))

    fig.update_layout(title='Boxplot of Lap Times by Stint and Driver',
                      title_x =0.5,
                    xaxis_title='Stint',
                    yaxis_title='Time in Seconds',
                    # Center the legend horizontally and vertically
                    legend=dict(x=0.5, y=-0.15, xanchor='center', 
                                yanchor='top', orientation='h'),  
    )
    
    # Set figure size
    fig.update_layout(
        width=len(df)*40,   # Set the width of the figure
        height=1500,   # Set the height of the figure        
        font=dict(
            size=len(df)/3  # Set the font size for all elements
        )
    )

    fig.write_image("boxplot.png")  # Save as PNG


def linegraph(drivers, df):
    fig = go.Figure()
    logging.info(len(df))
    # Create a line graph for each driver
    for driver in df['driver'].unique():
        for stint in df['stint'].unique():
            filtered_data = df[(df['driver'] == driver) & (df['stint'] == stint)]
            fig.add_trace(go.Scatter(x=filtered_data.index,
                                    y=filtered_data['time in seconds'],
                                    mode='lines',
                                    name=f'{driver} - Stint {stint}'))
            
    avg_lap_times = df.groupby('stint')['time in seconds'].mean()
    # Add average lap time line for each stint
    for stint, avg_time in avg_lap_times.items():
        filtered_data = df[df['stint'] == stint]
        min_lap = filtered_data.index.min()
        max_lap = filtered_data.index.max()
        fig.add_shape(type="line",
                    x0=min_lap,
                    y0=avg_time,
                    x1=max_lap,
                    y1=avg_time,
                    line=dict(color="black", width=1, dash="dash"),
                    xref='x', yref='y')

    fig.update_layout(
                    xaxis_title='Lap Number',
                    yaxis_title='Time in Seconds',
                    # Center the legend horizontally and vertically
                    legend=dict(x=0.5, y=-0.1, xanchor='center', yanchor='top', 
                                orientation='h'),  
                    # Width based on the number of laps
                    width=len(df['lap time'].unique()) * 15,  
                    title=dict(text='Line Graph of Lap Times by Stint and Driver', 
                               x=0.5, y=0.9))  # Center the title
    # Set figure size
    fig.update_layout(
        width=len(df)*40,   # Set the width of the figure
        height=1500,   # Set the height of the figure        
        font=dict(
            size=len(df)/3  # Set the font size for all elements
        )
    )


    fig.write_image("linegraph.png")  # Save as PNG


def get_laptimes(raceid, finishing_position: int, car_class: str = "GT3"):
    # Fetching data from the API
    api_url = f"https://api2.lowfuelmotorsport.com/api/race/{raceid}"
    logging.info(api_url)
    data = requests.get(api_url).json()

    # Extracting data for car
    try:
        car_data = data["race_results"][car_class]["OVERALL"][finishing_position-1]
    except:
        logging.info(f"Car {car_class} class not found")
    

    driver = f'{car_data["vorname"]} {car_data["nachname"]}'
    result_id = car_data['result_id']

    
    api_url = f"https://api3.lowfuelmotorsport.com/api/race/{raceid}/getLapDetails/{result_id}"
    lfm_lap_details = requests.get(api_url).json()
    lfm_lap_details = lfm_lap_details['laps']

    # Extracting lap details
    lap_details = []
    for lap in lfm_lap_details:
        try:
            lap_info = {
                "driver": f'{lap["vorname"]} {lap["nachname"]}',
                "lapTime": lap["lapTime"]
            }
        except:
            lap_info = {
                "driver": driver,
                "lapTime": lap["lapTime"]
            }
        lap_details.append(lap_info)

    # Printing lap details
    print("\nLap Details:")
    for lap in lap_details:
        print("Driver:", lap["driver"])
        print("Lap Time:", lap["lapTime"])
        print()

    return lap_details


def lap_time_to_seconds(lap_time):
    lap_time = datetime.strptime(lap_time, "%M:%S.%f")
    return lap_time.minute * 60 + lap_time.second + lap_time.microsecond / 1e6


def print_laptime_data_to_file(filename, raceid, finishing_position, car_class, delta = 0):
    data = get_laptimes(raceid, finishing_position, car_class)

    # Write data to CSV file
    with open('laps_data.csv', 'w', newline='') as csvfile:
        fieldnames = ['time in seconds', 'driver', 'stint', 'lap time']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        driver_stints = {}
        current_stint = 0
        previous_driver = None

        for lap in data:
            driver = lap['driver']
            lap_time_seconds = lap_time_to_seconds(lap['lapTime'])

            if driver != previous_driver:
                current_stint += 1
                driver_stints[driver] = current_stint
                previous_driver = driver

            writer.writerow({'time in seconds': lap_time_seconds, 'driver': driver, 
                             'stint': driver_stints[driver], 
                             'lap time': lap['lapTime']})

    # Read data from CSV file
    lap_data = []
    with open('laps_data.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            lap_data.append(row)

    # Calculate average lap time for each driver's stint
    driver_avg_lap_times = {}
    for lap in lap_data:
        driver_stint = (lap['driver'], lap['stint'])
        if driver_stint not in driver_avg_lap_times:
            driver_avg_lap_times[driver_stint] = []
        driver_avg_lap_times[driver_stint].append(lap_time_to_seconds(lap['lap time']))

    # Replace lap times that are too slow with empty lines
    for lap in lap_data:
        driver_stint = (lap['driver'], lap['stint'])
        avg_lap_time = sum(driver_avg_lap_times[driver_stint]) / len(driver_avg_lap_times[driver_stint])
        if lap_time_to_seconds(lap['lap time']) > avg_lap_time + delta:
            lap['lap time'] = ""

    # Write modified data to CSV file
    with open(f'{filename}', 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for lap in lap_data:
            if lap['lap time'] != '':
                writer.writerow(lap)
            else:
                csvfile.write("\n")  # Write an empty line


# Define a function to calculate the moving average
def moving_average(data, window_size):
    return data.rolling(window=window_size, min_periods=1).mean()


def calc_moving_average():
    for i in range(10):
        filename = f"{i}.csv"
        df = pd.read_csv(filename)
        print(df)
        # Calculate the 3-lap moving average and replace the original column
        df['time in seconds'] = moving_average(df['time in seconds'], 3)
        # Overwrite the original file with the updated DataFrame
        df.to_csv(filename, index=False)  
        print(df)

    print("Moving average lap times calculated and saved to individual files")


def create_image_top10(race_id, delta = 0):
    raceid = int(race_id)
    for i in range(10):
        print_laptime_data_to_file(f"{i}.csv", raceid, i+1, delta)

    calc_moving_average()
    # Set the size of the graph
    plt.figure(figsize=(50, 25))

    # Read and plot each CSV file with a different color
    for i in range(10):
        filename = f"{i}.csv"
        df = pd.read_csv(filename)
        
        # Extract the line name from the filename
        line_name = os.path.splitext(filename)[0]
        
        # Plot the lap times with the line name from the driver column
        for driver, data in df.groupby('driver'):
            # Adjust the line thickness here
            plt.plot(data.index, data['time in seconds'], 
                     label=f'P{int(line_name)+1} - {driver}', linewidth=5)


    # Add labels and legend
    plt.xlabel('Lap Number', fontsize=35)
    plt.ylabel('Time in Seconds', fontsize=35)
    plt.title(f'Lap Times for LFM Race #{raceid}', fontsize=25)
    plt.legend(loc='upper left', fontsize=25)
    plt.grid(True)
    # Increase the font size of the tick labels on the x-axis and y-axis
    plt.xticks(fontsize=32)
    plt.yticks(fontsize=32)

    # Save the plot as an image file
    plt.savefig('lap_times_plot.png')

    for i in range(0, 10):
        os.remove(f"{i}.csv")
        
    os.remove("laps_data.csv")


def create_image_team(raceid: int, finishing_position: int, car_class: str, delta = 0):
    
    FILENAME = "laps_data_modified.csv"
    print_laptime_data_to_file(FILENAME, raceid, finishing_position, car_class)
    df = get_df(FILENAME)
    drivers = get_drivers(df)
    linegraph(drivers, df)
    boxplot(drivers, df)
    violin(drivers, df)

    os.remove("laps_data.csv")
    os.remove("laps_data_modified.csv")


#create_image_team(172066, 2, "GT4")