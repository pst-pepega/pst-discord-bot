import re
import logging

def search_track(filename, search_term):
    result_string = ""
    result = []
    current_title = None

    with open(filename, 'r') as file:
        lines = file.readlines()

    for line in lines:
        title_match = re.match(r'^✓ (.+)', line)
        if title_match:
            current_title = title_match.group(1)
        elif search_term.lower() in line.lower():
            result.append((current_title, line.strip()))

            
    for title, line in result:
        result_string += "\n"
        result_string += (f'{title}\n{line}\n')

    return f"\n{result_string}"


def search_car(filename, search_term):
    result = []
    result_string = ""
    current_title = None
    current_content = []

    with open(filename, 'r') as file:
        lines = file.readlines()

    for line in lines:
        title_match = re.match(r'^✓ (.+)', line)
        if title_match:
            if current_title:
                result.append((current_title, current_content))
            current_title = title_match.group(1)
            current_content = []
        else:
            current_content.append(line.strip())

    # Check the last title and content
    if current_title:
        result.append((current_title, current_content))

    # Filter results based on the search term
    filtered_result = [(title, content) for title, content in result if search_term.lower() in title.lower()]

    for title, content_list in filtered_result:
        result_string += (f'✓ **{title}**\n')
        for line in content_list:
            result_string += f"{line}\n"

    return f"\n{result_string}"
