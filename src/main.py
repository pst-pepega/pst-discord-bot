# This bot requires the 'message_content' intent.
# Invite link: https://discord.com/api/oauth2/authorize?client_id=1131561281149730846&permissions=2214710336&scope=bot

import asyncio
import logging
import os
from dotenv import load_dotenv

from pst_bot import PstBot

logging.basicConfig(level=logging.INFO, format="[%(asctime)s] %(name)s: %(message)s", datefmt="%H:%M:%S")
logger = logging.getLogger(__name__)


async def main() -> None:

    logger.info("Loading env variables")
    load_dotenv()

    token = os.getenv("TOKEN_DEV")
    if token is None:
        token = os.getenv("TOKEN")

    if token is None:
        raise Exception("Missing TOKEN environment variable !!!")

    logger.info("Starting bot")

    client = PstBot()
    await client.start(token)

if __name__ == "__main__":
    asyncio.run(main())
