import requests
import json
from datetime import datetime, timezone
import math
import discord
from discord import Embed
import logging

logger = logging.getLogger(__name__)

from datetime import datetime, timedelta

def get_top(item, current_datetime, USERNAME = "Kylooooo"):
    # Get the current date and time    

    # Set the time to 00:00
    current_datetime = current_datetime.replace(hour=0, minute=0, second=0, microsecond=0)

    # Get the date from 7 days ago
    seven_days_ago = current_datetime - timedelta(days=7)

    # Format the dates as Unix timestamps
    to_date = int(current_datetime.timestamp())
    from_date = int(seven_days_ago.timestamp())

    # Set up the parameters
    API_KEY = "deea8100ae2c25c5a4120e86b70644d9"

    #from_date = int(datetime.strptime(FROM_DATE_STR, "%Y-%m-%d").timestamp())
    #to_date = int(datetime.strptime(TO_DATE_STR, "%Y-%m-%d").timestamp())

    # Calculate the total scrobbles for each recent artist
    recent_artists_url_template = "http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user={USERNAME}&api_key={API_KEY}&limit=200&from={from_date}&to={to_date}&page={page}&format=json"
    recent_artists = []
    page = 1
    while True:
        recent_artists_url = recent_artists_url_template.format(USERNAME=USERNAME, API_KEY=API_KEY, from_date=from_date, to_date=to_date, page=page)
        print(recent_artists_url)
        response = requests.get(recent_artists_url)
        # Check if the HTTP request was successful
        if response.status_code == 200:
            try:
                # Attempt to parse the response as JSON
                data = json.loads(response.text)

                # Check if the JSON response contains an error message
                if "error" in data and data["error"] == 8:
                    print("An error occurred: Operation failed. Please try again.")
                    return "The API is fucketh"
                else:
                    # Process the response data as needed
                    recent_artists += data["recenttracks"]["track"]
                    if int(data["recenttracks"]["@attr"]["page"]) >= int(data["recenttracks"]["@attr"]["totalPages"]):
                        break
            except json.JSONDecodeError:
                # Handle the case where the response is not valid JSON
                print("Invalid JSON response.")
        else:
            # Handle HTTP errors if the request was not successful
            print(f"HTTP error {response.status_code}: {response.text}")
        page += 1

    total_scrobbles_recent = {}
    for artist in recent_artists:
        artist_name = artist[item]["#text"]
        if artist_name in total_scrobbles_recent:
            total_scrobbles_recent[artist_name] += 1
        else:
            total_scrobbles_recent[artist_name] = 1
            
    total_scrobbles_recent = dict(sorted(total_scrobbles_recent.items(), key=lambda item: item[1], reverse=True))
    #print(json.dumps(total_scrobbles_recent, indent=4, ensure_ascii=False))
    return total_scrobbles_recent

def get_top10(dict):
    
    top_10 = {}
    count = 0
    for key, value in dict.items():
        if count < 10:
            top_10[key] = value
            count += 1
        else:
            break
    return top_10

def find_position(dict, search):

    # Initialize position
    position = None

    # Find the position and playcount of the artist
    for i, (artist, playcount) in enumerate(dict.items()):
        if artist == search:
            position = i
            break
    return position+1

def create_top10_embed(user, top_10, top_100_last_week):
    embed = Embed(title=f"**{user}'s Weekly Top 10!**", color=discord.Color.blue())

    field_text = "```"
    for position, (artist, plays) in enumerate(top_10.items(), start=1):
        try:
            position_delta = position - find_position(top_100_last_week, artist)
            position_delta = f"+{position_delta}" if position_delta > 0 else str(position_delta)
        except:
            position_delta = "NEW"

        try:
            last_week_plays = top_100_last_week[artist]
            delta_plays = plays - last_week_plays
        except:
            delta_plays = plays
 
        delta_plays = f"+{delta_plays}" if delta_plays > 0 else str(delta_plays)
        
        position_delta = f"[{position_delta}]"
        position = f"{position}."
        plays = str(plays)
        field_text += f"{position_delta:6}{position:4}{artist:40}{plays:5}{delta_plays}\n"
    field_text += "```"
    embed.add_field(name="\u200b", value=field_text, inline=False)

    return embed


user_list = {"Kylooooo", "sovietnugget", "emperalhero"}

for user in user_list:
    embed = create_top10_embed(user, get_top10(get_top("artist", datetime.now(), user)), get_top("artist", datetime.now() - timedelta(days=7), user))
    print(embed)


for user in user_list:
    top_100_last_week = get_top("artist", datetime.now() - timedelta(days=7), user)
    top_10 = get_top10(get_top("artist", datetime.now(), user))
    print(f"{user}'s Weekly Top 10!")
    for _ in range(10):
        artist = list(top_10.items())[_][0]
        position = _+1
        try:
            position_delta = position - find_position(top_100_last_week, artist)
            position_delta = f"+{position_delta}" if position_delta > 0 else f"{position_delta}"
            position_delta = f"[{position_delta}]"
        except:
            position_delta = "[NEW]"
        position = f"{position}."
        plays = list(top_10.items())[_][1]

        print(f"{position_delta:5} {position:3} {artist:30} {plays:3}")