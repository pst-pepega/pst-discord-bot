

class SDLSubmission(Command):

    def __init__(self, name, client: discord.client): # type: ignore
        super().__init__(name)
        self.client = client

    async def execute(self, message: discord.Message):
        submission = message.content.replace(f"!{self.name} ","")
        logger.info(submission)

        channel_id = 1166476358097051798
        channel = self.client.get_channel(channel_id) # type: ignore

        try:
            thread = await channel.send(f"{submission} by <@{message.author.id}>")
            await message.reply(f"<@{message.author.id}> thank you for your contribution.")
        except:
            await message.reply("It didn't work :(")
        

class ITBSubmission(Command):

    def __init__(self, name, client: discord.client): # type: ignore
        super().__init__(name)
        self.client = client

    async def execute(self, message: discord.Message):
        submission = message.content.replace(f"!{self.name} ","")
        logger.info(submission)

        channel_id = 1187492989048721428
        channel = self.client.get_channel(channel_id) # type: ignore

        try:
            thread = await channel.send(f"{submission} by <@{message.author.id}>")
            await message.reply(f"<@{message.author.id}> thank you for your contribution.")
        except:
            await message.reply("It didn't work :(")