# Counter end: 15/07/2024 13:30



if (
                " cope" in message.content.lower()
                or "copium" in message.content.lower()
            ) and "!checkcopium" not in message.content:
                # Perform an action, for example, sending a reply

                start_date = datetime.strptime("2023-06-01", "%Y-%m-%d")
                current_date = datetime.now()
                days_passed = (current_date - start_date).days

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                copium = await database.get_some_value("copium", pool)
                logging.info(f"copium:  {copium}")
                copium_remaining = 2 - copium * 0.001 + days_passed * 0.001
                copium_remaining = round(copium_remaining, 3)
                if copium_remaining > 0.0:
                    copium += 1
                    await database.update_some_value("copium", copium, pool)
                else:
                    await message.reply(
                        f"Unfortunately, we are all out of copium. The next patch of copium is set to arrive on **<t:1713615600:F>** {Emotes.SOB_CAT}"
                    )
                    logger.info(f"Copium remaining: {copium_remaining}")
                    return

                # copium = counters.count_up("copium")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                if "ferrari" in message.content.lower():
                    for _ in range(9):
                        pool = await database.create_pool()
                        logger.info("Database connected.")

                        # Read the counters.json file
                        copium = await database.get_some_value("copium", pool)
                        logging.info(f"copium:  {copium}")
                        copium_remaining = 2 - copium * 0.001 + days_passed * 0.001
                        copium_remaining = round(copium_remaining, 3)
                        if copium_remaining > 0.0:
                            copium += 1
                            await database.update_some_value("copium", copium, pool)
                        else:
                            await message.reply(
                                f"Unfortunately, we are all out of copium. The next patch of copium is set to arrive on **<t:1713615600:F>** {Emotes.SOB_CAT}"
                            )
                            logger.info(f"Copium remaining: {copium_remaining}")
                            return
                        if pool:
                            await pool.close()
                        logger.info("Database disconnected.")

                if int(copium_remaining * 1000) % 10 == 0:
                    logger.info(f"copium_remaining")

                    messages = f"*The Copium supply is low, please use Copium™ wisely.* {Emotes.BURNS_COPIUM} {copium_remaining}m³ of Copium™ remaining!"
                    messages += "📈" if copium_remaining >= 1.0 else "📉"
                else:
                    logger.info(f"copium_remaining")
                    messages = f"*The Copium supply is low, please use Copium™ wisely.*"
                    messages += "📈" if copium_remaining >= 1.0 else "📉"

                if copium_remaining == 0:
                    await message.reply(
                        f"Blud used up the last of the remaining Copium. 💀"
                    )
                else:
                    await message.reply(messages)

                if copium % 100 == 0:
                    await message.reply(
                        "https://tenor.com/view/copium-cat-gif-27161395"
                    )

            # Check if the message contains the words "cum"
            if "cum" in message.content.lower():
                # Perform an action, for example, sending a reply

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                cum = await database.get_some_value("cum", pool)
                logging.info(f"cum:  {cum}")
                cum += 1
                await database.update_some_value("cum", cum, pool)
                # cum = counters.count_up("cum")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                messages = f"{Emotes.CUM} {cum}"

                await message.channel.send(messages)

                if cum % 100 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/rebelnred-cum-gif-21461594"
                    )

                channel_id = 1240300121254531072
                channel = self.get_channel(channel_id) # type: ignore
                await channel.send(f"Cum count: {cum}")


# Check if the message contains the words "pepog"
            if "pepog" in message.content.lower():
                # Perform an action, for example, sending a reply

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                pepog = await database.get_some_value("pepog", pool)
                logging.info(f"pepog:  {pepog}")
                pepog += 1
                await database.update_some_value("pepog", pepog, pool)
                # pepog = counters.count_up("pepog")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                # Define the start date and time as a string
                start = "27/03/2023 17:08"
                # Convert the start string to a datetime object
                start_dt = datetime.strptime(start, "%d/%m/%Y %H:%M")
                # Get the current date and time as a datetime object
                now_dt = datetime.now()
                # Calculate the difference between the start and the current datetime objects
                diff = now_dt - start_dt
                # Convert the difference to days
                days = diff.total_seconds() / 86400

                pepog_per_day = pepog / days
                logger.info(f"Pepog per day: {pepog_per_day}")

                if pepog_per_day > 25:
                    await message.add_reaction("<:PepoG:1016031408642326548>")
                elif pepog_per_day < 25 and pepog_per_day > 20:
                    messages = f"{Emotes.PEPOG}"
                    await message.channel.send(messages)
                elif pepog_per_day < 20 and pepog_per_day > 15:
                    messages = f"{Emotes.PEPOG} {pepog} {Emotes.PEPOG}"
                    await message.channel.send(messages)
                elif pepog_per_day < 15 and pepog_per_day > 10:
                    messages = f"{Emotes.HABIBI} {pepog}"
                    await message.channel.send(messages)
                elif pepog_per_day < 10 and pepog_per_day > 5:
                    messages = f"{Emotes.PEERGI} {pepog}"
                    await message.channel.send(messages)
                elif pepog_per_day < 5 and pepog_per_day > 2:
                    messages = f"{Emotes.PEEROLL} {pepog}"
                    await message.channel.send(messages)
                else:
                    messages = f"{Emotes.PEPOG} {pepog}"
                    await message.channel.send(messages)

                if pepog % 300 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/noted-gif-26660577"
                    )
                elif pepog % 200 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/writing-notes-checklist-spongebob-squarepants-gif-16129592"
                    )
                elif pepog % 100 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/spongebob-patrick-star-noted-notes-gif-26231953"
                    )

                channel_id = 1240300121254531072
                channel = self.get_channel(channel_id) # type: ignore
                await channel.send(f"pepog count: {pepog} [{pepog_per_day}]")

            # Check if the message contains the words "chad"
            if "chad" in message.content.lower():
                # Perform an action, for example, sending a reply

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                chad = await database.get_some_value("chad", pool)
                logging.info(f"chad:  {chad}")
                chad += 1
                await database.update_some_value("chad", chad, pool)
                # chad = counters.count_up("chad")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                messages = f"{Emotes.CHAD} {chad}"

                await message.channel.send(messages)

            if "harley" in message.content.lower():
                # Perform an action, for example, sending a reply

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                karnimani = await database.get_some_value("karnimani", pool)
                logging.info(f"karnimani:  {karnimani}")
                karnimani += 1
                await database.update_some_value("karnimani", karnimani, pool)
                # chad = counters.count_up("chad")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                if karnimani == 100:
                    messages = "https://tenor.com/view/totodile-pokemon-dance-two-frame-gif-gif-617466149287403534"
                else:
                    messages = f"{Emotes.HARLEY}"

                await message.channel.send(messages)

                channel_id = 1240300121254531072
                channel = self.get_channel(channel_id) # type: ignore
                await channel.send(f"Harley count: {karnimani}")

            if "habibi" in message.content.lower():
                # Perform an action, for example, sending a reply

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                habibi = await database.get_some_value("habibi", pool)
                logging.info(f"habibi:  {habibi}")
                habibi += 1
                await database.update_some_value("habibi", habibi, pool)
                # chad = counters.count_up("chad")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")
                if habibi == 0:
                    messages = "https://tenor.com/view/ok-gif-21238001"
                else:
                    messages = f"{Emotes.HABIBI}"

                await message.channel.send(messages)

                channel_id = 1240300121254531072
                channel = self.get_channel(channel_id) # type: ignore
                await channel.send(f"Habibi count: {habibi}")

            # Check if the message contains the words "chad"
            if (
                "fr " in message.content.lower()
                or "<:frfr:1084918119425900595>" in message.content.lower()
            ):
                # Perform an action, for example, sending a reply

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                fr = await database.get_some_value("fr", pool)
                logging.info(f"chad:  {fr}")
                fr += 1
                await database.update_some_value("fr", fr, pool)
                # chad = counters.count_up("chad")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                messages = f"{Emotes.FRFR}"

                await message.channel.send(messages)

                if fr % 100 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/fr-fr-ong-gif-24732056"
                    )

                channel_id = 1240300121254531072
                channel = self.get_channel(channel_id) # type: ignore
                await channel.send(f"fr count: {fr}")

                # Check if the message contains the words "valencia"
            if "valencia" in message.content.lower():
                # Perform an action, for example, sending a reply

                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                valencia = await database.get_some_value("valencia", pool)
                logging.info(f"valencia:  {valencia}")
                valencia += 1
                await database.update_some_value("valencia", valencia, pool)
                # valencia = counters.count_up("valencia")

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                # Calculate the difference in days
                days_since = (datetime.now() - datetime(2023, 4, 19)).days
                valencia_rate = round(valencia / days_since, 3)

                messages = f"Are you talking about (Circuit) **Ricardo Tormo** <@{message.author.id}> ?"
                await message.channel.send(messages)
                # if valencia < 1000:
                #     messages = f"<@153146329554616320> double dares you *motherfucker*, say **Valencia** {1000-valencia} more god-damn times! {Emotes.PEEPOTOMATO}"
                #     await message.channel.send(messages, silent=True)
                # elif valencia == 1000:
                #     messages = "You have successfully made the Dennis mad. Called it by the wrong name too many times.."
                #     await message.channel.send(messages)
                if valencia_rate < 1.0:
                    if random.randint(0, 9) == 1:
                        await message.channel.send("Please keep the Valencia rate below 1 per day guys, we are doing a good job at the moment, but we have to keep it up. Our boy Dennis Schlops is allergic to it. Thank you.")
                elif valencia_rate < 2.5:
                    if random.randint(0, 9) == 1:
                        await message.channel.send("Red Alert! Too many mentions of **Valencia**. Deploying <@153146329554616320>")
                elif valencia_rate < 4:
                    if random.randint(0, 9) == 1:
                        await message.channel.send("POV: <@153146329554616320> seeing your message https://tenor.com/view/not-ok-bad-pulp-fiction-pretty-fucking-far-gif-17263982")
                elif valencia_rate < 5:
                    if random.randint(0, 9) == 1:
                        await message.channel.send("POV: <@153146329554616320> when you say **Valencia** one more god damn time. https://tenor.com/view/pulp-fiction-samuel-l-jackson-jules-vengeance-strike-gif-19959188")

            if "cooper" in message.content.lower():
                n = random.randint(0, 9)

                # Check the value of n and send the corresponding message
                if n == 8:
                    await message.channel.send(
                        "https://media.tenor.com/yhA2GZokGrAAAAAd/cooper-bradley.gif"
                    )
                elif n == 9:
                    await message.channel.send(
                        "https://tenor.com/view/bradley-cooper-wave-gif-19654703 "
                    )
                else:
                    await message.channel.send(
                        f"It's Ryan Cooooooooooooooooper? {Emotes.AYAYA}"
                    )

                    

            if "fuck bmw" in message.content.lower():
                await message.channel.send(
                    random.choice(open("resources/fuck_bmw.txt").readlines())
                )

            if "skill issue" in message.content.lower():
                await message.channel.send(
                    random.choice(open("resources/skill_issue.txt").readlines())
                )

            if "cry about it" in message.content.lower():
                await message.channel.send(
                    random.choice(open("resources/cry_about_it.txt").readlines())
                )

                

            if "car grips kerb" in message.content.lower():
                await message.channel.send(
                    "https://cdn.discordapp.com/attachments/498546852036083734/1025802547736367215/CARGRIPSKERB2.mp4"
                )

                if ((
                "ryan" in message.content.lower()
                or any(
                    member.name == "ryan" or member.display_name == "ryan"
                    for member in message.mentions
                )
                or any(
                    member.name == "gaufresausucre"
                    or member.display_name == "gaufresausucre"
                    for member in message.mentions
                )
                or any(
                    member.name == "emperalhero" or member.display_name == "emperalhero"
                    for member in message.mentions
                ))
                    and "cooper" not in message.content.lower()):
                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                ryan = await database.get_some_value("ryan", pool)
                logging.info(f"ryan:  {ryan}")
                ryan += 1
                await database.update_some_value("ryan", ryan, pool)

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                messages = [
                    f"{Emotes.AYAYA} **Ryan** {Emotes.CHAD}",
                    f"{Emotes.OWOBELLE} **Ryan** {Emotes.AMOEYES}",
                    f"{Emotes.KISS} **Ryan** {Emotes.KISS}",
                    "Astaghfirullah rayan habibi",
                ]

                selected_message = random.choice(messages)

                # Send the selected message to the same channel

                if random.random() <= 0.69:
                    message_sent = await message.channel.send(selected_message)
                try:
                    if selected_message == "Astaghfirullah rayan habibi":
                        await message_sent.add_reaction("<:Habibi:1017076484302700616>")
                except:
                    logger.info("Didn't manage to add Habibi to the Ryan")

                if ryan % 100 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/ryan-sign-ryan-sign-gif-20225832"
                    )

                channel_id = 1240300121254531072
                channel = self.get_channel(channel_id) # type: ignore
                await channel.send(f"Ryan count: {ryan}")

            if re.search(
                r"(?<![a-zA-Z])anna(?![a-zA-Z])", message.content, re.IGNORECASE
            ):
                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                anna = await database.get_some_value("anna", pool)
                logging.info(f"anna:  {anna}")
                anna += 1
                await database.update_some_value("anna", anna, pool)

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                messages = [
                    f"{Emotes.AYAYA} **Anna** {Emotes.AYAYA}",
                    f"{Emotes.PEERGI} **Anna** {Emotes.PEERGI}",
                    f"{Emotes.KISS} **Anna** {Emotes.KISS}",
                    f"{Emotes.KISS} **Anna** {Emotes.KISS}",
                    f"{Emotes.PEEFLAP} **Anna** {Emotes.PEEFLAP}",
                    f"{Emotes.PEEROLL} **Anna** {Emotes.PEEROLL}",
                    f"{Emotes.KISS} **Anna** {Emotes.KISS}",
                    "Astaghfirullah **Anna-chan** habibi",
                ]

                selected_message = random.choice(messages)

                # Send the selected message to the same channel
                if random.random() <= 0.42:
                    message_sent = await message.channel.send(selected_message)
                try:
                    if selected_message == "Astaghfirullah **Anna-chan** habibi":
                        await message_sent.add_reaction("<:Habibi:1017076484302700616>")
                except:
                    logger.info("Didn't manage to add Habibi to the Anna")

                if anna % 100 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/frozen-anna-gif-18589854"
                    )

                channel_id = 1240300121254531072
                channel = self.get_channel(channel_id) # type: ignore
                await channel.send(f"Anna count: {anna}")

                if (
                re.compile(r"AYAYA", re.IGNORECASE).search(message.content)
                or re.compile(r"owo", re.IGNORECASE).search(message.content)
                or re.compile(r"uwu", re.IGNORECASE).search(message.content)
            ):
                pool = await database.create_pool()
                logger.info("Database connected.")

                # Read the counters.json file
                anime = await database.get_some_value("anime", pool)
                logging.info(f"anime:  {anime}")
                anime += 1
                await database.update_some_value("anime", anime, pool)

                if pool:
                    await pool.close()
                logger.info("Database disconnected.")

                await message.channel.send(Emotes.AYAYA)

                if anime % 100 == 0:
                    await message.channel.send(
                        "https://tenor.com/view/anime-cheerleader-dance-weeb-weeaboo-gif-16445405"
                    )

            # List of former Soviet Union and communist countries
            soviet = [
                "Russia",
                "USSR",
                "Ukraine",
                "Belarus",
                "Kazakhstan",
                "Uzbekistan",
                "Armenia",
                "Azerbaijan",
                "Georgia",
                "Kyrgyzstan",
                "Tajikistan",
                "Turkmenistan",
                "Moldova",
                "Lithuania",
                "Latvia",
                "Estonia",
                "Armenia",
                "Azerbaijan",
                "Georgia",
                "Kazakhstan",
                "Kyrgyzstan",
                "Tajikistan",
                "Turkmenistan",
                "Uzbekistan",
                "Albania",
                "Angola",
                "Bulgaria",
                "Cambodia",
                "Cuba",
                "Czechoslovakia",
                "East Germany",
                "Hungary",
                "Laos",
                "Mongolia",
                "North Korea",
                "Poland",
                "Romania",
                "Vietnam",
                "Yugoslavia",
                "China",
                "Czech Republic",
                "Slovakia",
                "North Vietnam",
                "South Vietnam",
                "Communism",
                "Commie",
                "<:communist:1103055435826745364>",
                "<:AngeryCommunist:1103055614290165911>",
                "Stalin",
                "Communist",
            ]

            for country in soviet:
                if country.lower() in message.content.lower():
                    pool = await database.create_pool()
                    logger.info("Database connected.")

                    # Read the counters.json file
                    communism = await database.get_some_value("communism", pool)
                    logging.info(f"communism:  {communism}")
                    communism += 1
                    await database.update_some_value("communism", communism, pool)

                    if pool:
                        await pool.close()
                    logger.info("Database disconnected.")
                    if random.random() <= 0.1:
                        await message.channel.send("<:communist:1103055435826745364>")
                    if communism % 100 == 0:
                        await message.channel.send("https://tenor.com/view/stalin-head-turn-gif-27708670")

                    channel_id = 1240300121254531072
                    channel = self.get_channel(channel_id) # type: ignore
                    await channel.send(f"Communism count: {communism}")

            # List of former Soviet Union and communist countries
            nazi = ["Nazi", "Hitler", "88 ", "<:nazi:1081174725503488030>"]

            for country in nazi:
                if country.lower() in message.content.lower():
                    pool = await database.create_pool()
                    logger.info("Database connected.")

                    # Read the counters.json file
                    nazi = await database.get_some_value("nazi", pool)
                    logging.info(f"nazi:  {nazi}")
                    nazi += 1
                    await database.update_some_value("nazi", nazi, pool)

                    if pool:
                        await pool.close()
                    logger.info("Database disconnected.")
                    if random.random() <= 0.1:
                        await message.channel.send("<:nazi:1081174725503488030>")

                    channel_id = 1240300121254531072
                    channel = self.get_channel(channel_id) # type: ignore
                    await channel.send(f"Nazi count: {nazi}")

