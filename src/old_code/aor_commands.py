

class AorLfmWr(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):

        track_name = message.content.replace(f"!{self.name} ","")

        lfm_track_name = Utils.to_lfm_track_name(track_name)

        data = lfmhl.world_record(lfm_track_name)
        driver = f"{data['vorname']} {data['nachname']}"
        car = data['car_name']
        laptime = data['lap']
        date = data['lapdate']
        track = data['track_name']

        await message.channel.send(f"{driver} holds the world record at {track} in the {car} with a laptime of {laptime} ({date})")


class AorHotlap(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        # Whatever should happen goes here for example:    # Remove the !prediction part from the message content
        args = message.content[len('!aorhl'):].strip()
        arguments = [arg.lower() for arg in args.split(" - ")]

        logging.info(arguments)

        if ("help" in arguments):
            await message.channel.send("!aorhl [Track] - [Name] - [Car] - [Patch (optional)]")
            await message.channel.send("For Example: Imola Porsche ↪ !aorhl Imola - None - Porsche")
            await message.channel.send("Car and Track List: https://gitlab.com/pst-pepega/pst-twitch-bot/-/blob/main/resources/car%20and%20track%20list.txt")

        elif len(arguments) == 4:
            logging.info("4 Arguments")
            matching_hotlaps = aoruser.get_data(arguments[0], arguments[1], arguments[2], arguments[3])
            if len(matching_hotlaps) == 0:
                await message.channel.send("no results.")
            loop_count = 0
            result = "```"

            for hotlaps in matching_hotlaps:
                result += (f"{hotlaps['driver_name']} - {hotlaps['car_name']} - {aoruser.format_time(hotlaps['lap_time'])} - {hotlaps['acc_patch']} - {hotlaps['scrape_date']}\n")
                loop_count += 1

                if loop_count >= 15:
                    result += "```"
                    await message.channel.send(result)
                    result = "```"
                    loop_count = 0

            # Send the remaining result if there are any
            if loop_count > 0:
                result += "```"
                await message.channel.send(result)

        elif len(arguments) == 3:
            logging.info("3 Arguments")
            matching_hotlaps = aoruser.get_data(arguments[0], arguments[1], arguments[2], "")
            if len(matching_hotlaps) == 0:
                await message.channel.send("no results.")
                return
            loop_count = 0
            result = "```"

            for hotlaps in matching_hotlaps:
                result += (f"{hotlaps['driver_name']} - {hotlaps['car_name']} - {aoruser.format_time(hotlaps['lap_time'])} - {hotlaps['acc_patch']} - {hotlaps['scrape_date']}\n")
                loop_count += 1

                if loop_count >= 15:
                    result += "```"
                    await message.channel.send(result)
                    result = "```"
                    loop_count = 0

            # Send the remaining result if there are any
            if loop_count > 0:
                result += "```"
                await message.channel.send(result)

        elif len(arguments) == 2:
            logging.info("2 Arguments")
            matching_hotlaps = aoruser.get_data(arguments[0], arguments[1], "", "")
            if len(matching_hotlaps) == 0:
                await message.channel.send("no results.")
                return
            loop_count = 0
            result = "```"

            for hotlaps in matching_hotlaps:
                result += (f"{hotlaps['driver_name']} - {hotlaps['car_name']} - {aoruser.format_time(hotlaps['lap_time'])} - {hotlaps['acc_patch']} - {hotlaps['scrape_date']}\n")
                loop_count += 1

                if loop_count >= 15:
                    result += "```"
                    await message.channel.send(result)
                    result = "```"
                    loop_count = 0

            # Send the remaining result if there are any
            if loop_count > 0:
                result += "```"
                await message.channel.send(result)
        else:
            await message.channel.send("!aorhl [Track] - [Name] -  [Patch (optional)]")





class AorPrediction(Command):
    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        try:
            week, tier = message.content.lower().replace("!prediction ","").split()
        except:
            week = message.content.lower().replace("!prediction ","")
            tier = None

        logger.info(week)
        logger.info(tier)

        logger.info(f"Tier: {tier} - Week: {week}")
        results = await prediction_helper.process(f"week{int(week) - 1}")
        logger.info(results)        
        result = await prediction_helper.process(f"week{week}", results)
        logger.info(result)

        

        for item in result:
            logger.info(result)
        # Split the item by lines
            lines = item.split('\n')

            # Use the first line as the embed title
            title = lines[0]
            logger.info(title)

            if tier != None:
                if tier == "total":
                    if "total" not in title.lower():
                        continue
                else:
                    if str(tier) not in title.lower():
                        continue

            # Join the remaining lines as the embed description
            description = '\n'.join(lines[1:])
            logger.info(description)

            # Create a Discord embed with the title and description
            embed = discord.Embed(title=title, description=description, color=discord.Color.dark_purple())

            # Send the embed as a Discord message
            await message.channel.send(embed=embed)