

class Commands(Command):

    def __init__(self, name):
        super().__init__(name)

    @staticmethod
    def get_patches() -> list[str]:
        patches = []
        for file in os.listdir("resources"):
            if not file.startswith("Commands"):
                continue
            patches.append(file.replace("Commands_", "").replace(".md", ""))

        return patches

    async def execute(self, message: discord.Message):
        logging.info(message.content)
        patch = message.content.replace(f"!{self.name} ", "").lower()

        patches = Commands.get_patches()
        if patch == "help" or patch not in patches:
            result = "Available Command Help Pages are: "
            await message.channel.send(result + ", ".join(patches))
            return

        file_path = f"resources/Commands_{patch}.md"
        logging.info(f"File: {file_path}")

        with open(file_path, 'r') as file:
            file_contents = file.read()
        await message.channel.send(f'# {patch} **Commands:**\n{file_contents}')
        await message.reply("You can find help here: https://discord.gg/kAganQw6YE")


class GetPredictions(Command):

    def __init__(self, name, client: discord.Client):
        super().__init__(name)
        self.client = client

    async def execute(self, message: discord.Message):
        driver_name = message.content.replace("!getpredictions ","")

        try:
            if "tier" in driver_name:
                pool = await database.create_pool()
                result_dict = dict()
                tier = int(driver_name.replace("tier ",""))

                query_string = f"SELECT * from driver where tier = {tier};"
                logger.info(query_string)
                result = await database.get_reports(pool, query_string)

                for row in result:
                    name = row['driver_name']
                    list_positions = []
                    query_string2 = f"select * from prediction where driver_name = '{name}';"
                    logger.info(query_string2)
                    result2 = await database.get_reports(pool, query_string2)

                    for row2 in result2:
                        list_positions.append(int(row2['prediction']))
                    
                    average_position = round(sum(list_positions) / len(list_positions), 3)
                    median_position = statistics.median(list_positions)
                    result_dict[name] = (average_position, median_position)
                    logger.info(result_dict[name])

                logger.info(result_dict)
                result_dict = dict(sorted(result_dict.items(), key=lambda x: x[1][0]))

                headers = f"{'name':20}{'average':10}{'median':10}\n"
                result_string = headers
                for name, (average_position, median_position) in result_dict.items():
                    result_string += f"{name:20}{average_position:8}{median_position:8}\n"
                await message.channel.send(f"```\n{result_string}```")                    




            pool = await database.create_pool()
            query_string = f"select * from prediction where driver_name = '{driver_name}';"
            result = await database.get_reports(pool, query_string)

            output = f"**Predictions for {driver_name}:**\n```\n"

            max_length = 0
            list_positions = []

            for row in result:
                id = row['user_id']
                try:
                    user = await message.author.guild.fetch_member(id) # type: ignore
                    user = str(user.name)
                except:
                    user = await self.client.fetch_user(id)
                    user = str(user.name)
                    #user = f"userid: {id}"
        
                # Check if the current driver name is longer than the previous longest
                if len(user) > max_length:
                    longest_name = user
                    max_length = len(user)


            for row in result:
                id = row['user_id']    
                try:
                    user = await message.author.guild.fetch_member(id) # type: ignore
                    user = str(user.name)
                except: 
                    user = await self.client.fetch_user(id)
                    user = str(user.name)
                username = user
                prediction = row['prediction']
                output += f"{username.ljust(max_length)}: {prediction}\n"
                list_positions.append(int(prediction))
        

            output += "\n```"
            output += f"Average Position: {round(sum(list_positions) / len(list_positions), 3)}\n"
            output += f"Median Position: {statistics.median(list_positions)}"
            # Send the output as a Discord message
            await message.channel.send(output)
        
        except Exception as e:
            channel_id = 1240300121254531072
            channel = self.client.get_channel(channel_id) # type: ignore
            await channel.send(f"Error in GetPredictions: {e}")


        

class MyPredictions(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):

        try:
            pool = await database.create_pool()
            query_string = f"SELECT * FROM prediction where user_id = '{message.author.id}';"
            result = await database.get_reports(pool, query_string)

            output = f"**Driver Predictions for <@{message.author.id}>:**\n```\n"

            max_length = 0

            for row in result:
                driver_name = row['driver_name']
                
                # Check if the current driver name is longer than the previous longest
                if len(driver_name) > max_length:
                    longest_name = driver_name
                    max_length = len(driver_name)

            for row in result:
                driver_name = row['driver_name']
                prediction = row['prediction']
                output += f"{driver_name.ljust(max_length)}: {prediction}\n"

            output += "\n```"
            # Send the output as a Discord message
            await message.channel.send(output)
        except Exception as e:
            channel_id = 1240300121254531072
            channel = self.client.get_channel(channel_id) # type: ignore
            await channel.send(f"Error in MyPredictions: {e}")

        if pool:
            await pool.close()



allowed_names = ["Allen Drake Cruz",
                 "Valentin Barrier",
                 "Luke Whitehead", 
                 "Krzysztof Maczkowski", 
                 "Chris Bradbury", 
                 "Jaroslav Honzik", 
                 "Aidan Walsingham", 
                 "Kevin Niles", 
                 "Francesco Giardinetto", 
                 "Jonathan Clifford", 
                 "Abe Santema", 
                 "Luca Ziege", 
                 "Ryan Cooper"]
ALLOWED_NAMES = tuple(allowed_names)
@app_commands.command(name="predict",
                      description="Predict in the AOR Prediction game.")
async def predict_cmd(interaction: discord.Interaction,
                      driver_name: Literal[ALLOWED_NAMES],
                      position: int):
    
    if position > 50:    
        await interaction.response.send_message(f"Position needs to be between 1 and 50, you had {position}.", ephemeral=True)
        return
    
    if time.time() > 1720463400:
        await interaction.response.send_message("Predictions are closed. Sorry.", ephemeral=True)
        return

    pool = await database.create_pool()
    user_id = interaction.user.id
    user_name = interaction.user.name

    for driver in ALLOWED_NAMES:
        try:
            query_string = f"INSERT INTO prediction (user_id, prediction, driver_name) VALUES ('{user_id}', 0, '{driver}')"                
            await database.add_report(pool, query_string)
        except:
            logger.info("Guess not.")

    try:
        query_string = f"UPDATE prediction SET prediction = {position} WHERE user_id = '{user_id}' AND driver_name = '{driver_name}';"
        await database.add_report(pool, query_string)
        await interaction.response.send_message(f"Updated <@{user_id}>, {position}, '{driver_name}'", ephemeral=True)
    except Exception as e:
        await interaction.response.send_message(e)

    if pool:
        await pool.close()


class DeletePrediction(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        pool = await database.create_pool()
        await database.delete_prediction(message.author.id, pool)
        if pool:
            await pool.close()