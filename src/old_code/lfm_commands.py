class LfmHotlapCombo(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):

        try:
            counter = int(message.content.split("!lfmhlcombo "))
        except Exception as e:
            logger.debug(e)
            counter = 10

        logging.info(f"count: {counter}")
        initial_message = await message.reply("0/23")
        sorted_name_counts = await lfmhl_car_track_combinations.get_combos(initial_message)

        # Initialize variables for rank and previous count
        rank = 1
        prev_count = None
        result = "LFMHL Unique Car + Track Combos:\n```"
        rank_display = 1

        # Print the sorted name counts with ranks
        for name, count in sorted_name_counts.items():
            if int(rank_display) > counter+1:
                break
            if count != prev_count:
                prev_count = count
                rank_display = f"{rank}"
            result += (f"{rank_display:3} {name:<20} {count:4}\n")
            logger.info(f"{rank_display:3} {name:<20} {count:4}\n")
            rank += 1
        result += "```"
        await message.reply(result)


class LfmHotlap(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):

        args = message.content[len('!lfmhl'):].strip()
        arguments = [arg.lower() for arg in args.split(" - ")]

        if ("help" in arguments):
            await message.reply("!lfmhl [Track] - [Name] - [Car] ~~- [Patch (optional)]~~")
            result = f"""```
!lfmhl [track] - [driver] - [car]   → Shows lap(s) by driver on track with given car.
!lfmhl [track] - [driver]           → Shows lap(s) by driver on a given track.
!lfmhl none/all - [driver]          → Shows laprecords for the driver on every track where the data for the driver exists.
!lfmhl [track]                      → Shows the laprecords for each car on a given track.
                    ```"""
            await message.reply(result)
            return

        result = ""

        logging.info(arguments)
        arguments[0] = Utils.to_lfm_track_name(arguments[0])

        if len(arguments) == 4:
            logging.info("4 Arguments")
            matching_hotlaps = lfmhl.get_data(arguments[0], arguments[1], arguments[2], arguments[3])
            if len(matching_hotlaps) == 0:
                await message.reply("no results.")
            loop_count = 0
            result = "```"

            for hotlaps in matching_hotlaps:
                result += (f"{hotlaps['vorname']} {hotlaps['nachname']} - {hotlaps['car_name']} - {hotlaps['lap']} - {hotlaps['track_name']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC\n")
                loop_count += 1

                if loop_count >= 15:
                    result += "```"
                    await message.reply(result)
                    result = "```"
                    loop_count = 0

            # Send the remaining result if there are any
            if loop_count > 0:
                result += "```"
                await message.reply(result)

        elif len(arguments) == 3:
            logging.info("3 Arguments")
            matching_hotlaps = lfmhl.get_data(arguments[0], arguments[1], arguments[2], "")
            if len(matching_hotlaps) == 0:
                await message.reply("no results.")
                return
            loop_count = 0
            result = "```"
            max_car_length = 0

            if arguments[2] == "none" or arguments[2] == "all":
                max_car_length = max(len(f"{data['vorname']} {data['vorname']}") for track_name, data in matching_hotlaps.items())

            for hotlaps in matching_hotlaps:
                driver_name = f"{hotlaps['vorname']} {hotlaps['nachname']}"
                result += (f"{driver_name.ljust(max_car_length)} - {hotlaps['car_name']} - {hotlaps['lap']} - {hotlaps['track_name']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC\n")
                loop_count += 1

                if loop_count >= 15:
                    result += "```"
                    await message.reply(result)
                    result = "```"
                    loop_count = 0

            # Send the remaining result if there are any
            if loop_count > 0:
                result += "```"
                await message.reply(result)

        elif len(arguments) == 2:
            logging.info("2 Arguments")

            if arguments[0].lower() == "all" or arguments[0].lower() == "none":

                loop_count = 0
                result = "```"

                lap_list = lfmhl.get_all_track_results_for_user(arguments[1])
                if lap_list == None:
                    await message.reply("The API is broken or something.")
                else:
                    max_car_length = max(len(track_name) for track_name in lap_list.keys())
                    for track_name, laps in lap_list.items():
                        for lap_data in laps:
                            # Use lap_data to format your output as needed
                            result += (f"{track_name.ljust(max_car_length)} - {lap_data['lap']} - {lap_data['version']} - {lap_data['lapdate']} - ({lap_data['car_name']})\n")
                            logging.info(result)
                            logging.info(loop_count)
                            loop_count += 1

                            if loop_count >= 15:
                                result += "```"
                                await message.reply(result)
                                result = "```"
                                loop_count = 0

                    # Send the remaining result if there are any
                    if loop_count > 0:
                        result += "```"
                        await message.reply(result)

            elif arguments[0].lower() == "records":

                loop_count = 0
                result = "```"

                matching_hotlaps = lfmhl.get_user_track_records(arguments[1])
                if matching_hotlaps == None:
                    await message.reply("The API is broken or something.") 
                else:
                    max_car_length = max(len(data['track_name']) for track_name, data in matching_hotlaps.items())
                    for track_name, data in matching_hotlaps.items():
                        result += (f"{data['track_name'].ljust(max_car_length)} - {data['lap']} - {data['version']} - {data['lapdate']} - ({data['car_name']})\n")
                        loop_count += 1

                        if loop_count >= 15:
                            result += "```"
                            await message.reply(result)
                            result = "```"
                            loop_count = 0

                    # Send the remaining result if there are any
                    if loop_count > 0:
                        result += "```"
                        await message.reply(result)

            else:

                if arguments[1] == "pst":
                    matching_hotlaps = lfmhl.get_hotlaps_multiple_drivers_and_track(arguments[0], pst_server_driver_list, "", "")
                    if len(matching_hotlaps) == 0:
                        await message.reply("no results.")
                        return

                    loop_count = 0
                    result = "```"

                    for hotlaps in matching_hotlaps:
                        name = f"{hotlaps['vorname']} {hotlaps['nachname']}"
                        result += (f"{name.ljust(20)} - {hotlaps['car_name'].ljust(30)}- {hotlaps['lap']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC\n")
                        loop_count += 1

                        if loop_count >= 15:
                            result += "```"
                            await message.reply(result)
                            result = "```"
                            loop_count = 0

                    # Send the remaining result if there are any
                    if loop_count > 0:
                        result += "```"
                        await message.reply(result)
                else:
                    matching_hotlaps = lfmhl.get_data(arguments[0], arguments[1], "", "")
                    max_car_length = max(len(data['car_name']) for data in matching_hotlaps)
                    if len(matching_hotlaps) == 0:
                        await message.reply("no results.")
                        return

                    loop_count = 0
                    result = "```"

                    for hotlaps in matching_hotlaps:
                        result += (f"{hotlaps['vorname']} {hotlaps['nachname']} - {hotlaps['car_name'].ljust(max_car_length)} - {hotlaps['lap']} - {hotlaps['track_name']} - {hotlaps['version']} - {hotlaps['lapdate']} UTC\n")
                        loop_count += 1

                        if loop_count >= 15:
                            result += "```"
                            await message.reply(result)
                            result = "```"
                            loop_count = 0

                    # Send the remaining result if there are any
                    if loop_count > 0:
                        result += "```"
                        await message.reply(result)

        elif len(arguments) == 1:

            if arguments[0].lower() == "all":
                await message.reply("23 messages coming right up, Sir or Madame.")
                track_list = lfmhl.get_all_tracks()
                if track_list == None:
                    await message.reply("The API is broken or something.")
                else:
                    logger.info(track_list)
                    for track in track_list:
                        logging.info(f"Getting {track} hotlap.")
                        matching_hotlaps = lfmhl.car_records(track)
                        if len(matching_hotlaps) == 0:
                            await message.reply(f"no results for {track}.")
                            return
                        result = f"# {track}\n```"
                        max_car_length = max(len(car) for car in matching_hotlaps)

                        for car, lap_time in matching_hotlaps.items():
                            result += (f"{car.ljust(max_car_length)}: {lap_time}\n")

                        result += "```"
                        await message.reply(result)
                        await asyncio.sleep(1)

            else:
                logging.info("1 Arguments")
                matching_hotlaps = lfmhl.car_records(arguments[0])
                if len(matching_hotlaps) == 0:
                    await message.reply("no results.")
                    return
                max_car_length = max(len(car) for car in matching_hotlaps)

                for car, lap_time in matching_hotlaps.items():
                    result += (f"{car.ljust(max_car_length)}: {lap_time}\n")
                try:
                    await message.reply(f"```{result}```")
                except discord.errors.HTTPException as e:
                    filename = f"{arguments[0]}.json"
                    logging.info(f"HTTPException: {e.response.status} {e.response.reason}")
                    with open(filename, 'a', encoding="UTF-8") as file:
                        # Append your data
                        file.write(result)

                    # Send the file as an attachment in a Discord message
                    with open(filename, "rb") as file:
                        await message.reply(file=discord.File(file, filename=filename))

                    # Delete the local file
                    os.remove(filename)

        else:
            await message.reply("!lfmhl [Track] - [Name] - [Car] ~~- [Patch (optional)]~~")
            result = f"""````
                        !lfmhl [track] - [driver] - [car]   → Shows lap(s) by driver on track with given car.
                        !lfmhl [track] - [driver]           → Shows lap(s) by driver on a given track.
                        !lfmhl none/all - [driver]          → Shows laprecords for the driver on every track where the data for the driver exists.
                        !lfmhl [track]                      → Shows the laprecords for each car on a given track.
                    ```"""
            await message.reply(result)


class LfmHotlapCompetition(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        sorted_dict, still_available = lfmhl_competition.get_records()
        arg = message.content.strip("!lfmhlcomp ")
        logger.info(arg)

        if arg == "available":
            # Split the string into chunks based on commas and a maximum chunk size
            chunk_size = 1900
            chunks = []
            current_chunk = ""

            for item in still_available.split("\n"):
                if len(current_chunk) + len(item) + 1 <= chunk_size:  # +1 for the comma
                    if current_chunk:
                        current_chunk += "\n"
                    current_chunk += item
                else:
                    chunks.append(current_chunk)
                    current_chunk = item

            # Add the last chunk
            if current_chunk:
                chunks.append(current_chunk)

            # Send each chunk as a separate message
            for chunk in chunks:
                await message.reply(chunk)
            return

        try:
            arg = int(arg)
        except Exception:
            arg = 10

        result = "Driver with record laps for *competitive* cars```\n"
        max_key_width = max(len(str(key)) for key in sorted_dict.keys())
        count = 0

        # Print the sorted dictionary
        for key, value in sorted_dict.items():
            if count >= arg:
                break
            result += (f"{str(key):<{max_key_width}}: {value}\n")
            count += 1

        result += "```"

        await message.reply(result)


# Might be useless all together, because we got LFM Tracks List??
class LfmTracks(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        track_list = lfmhl.get_all_tracks()
        if track_list == None:
            await message.reply("The API is broken or something.")
        else:
            result = ""
            loop_count = 0

            for track in track_list:
                if loop_count < len(track_list)-1:
                    result += (f"{track} - ")
                else:
                    result += (f"{track}")
                loop_count += 1

            await message.reply(result)


## Need to redo it so it utilisies the weather command to predict the temperatures over time.
class LfmEndurance(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        input = message.content[len(f"!{self.name} "):]
        logger.info(f"{self.name} args: {input}")

        if input == "all":
            await message.reply("https://i.imgur.com/SxJ4Zmy.png")
            return

        if input != "" and input.lstrip("+-").isnumeric():
            if input.startswith("+") or input.startswith("-"):
                weather = lfmenduranceweather.getweather(None, int(input))
            else:
                weather = lfmenduranceweather.getweather(int(input))
        else:
            weather = lfmenduranceweather.getweather(None)

        track = weather['Track']
        degrees = weather['°C']
        hour = weather['Hour']
        eff_degrees = weather['Avg. °C']
        clouds = weather['Clouds']
        rain = weather['Rain']
        dry = weather['Dry']
        wet = weather['Wet']
        mixed = weather['Mixed']

        rain_emoji = emoji.emojize(':cloud_with_rain:')
        sun_emoji = emoji.emojize(':sunny:')

        # Create a combination of sun and rain cloud
        sun_with_rain_emoji = emoji.emojize(':white_sun_rain_cloud:')


        result = f'''
# {track}
* {degrees} °C at {hour}:00, average ~{eff_degrees} °C
* {sun_emoji} {dry} / {rain_emoji} {wet} / {sun_with_rain_emoji} {mixed}
'''
        await message.reply(result)


class LfmReports(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
           # Whatever should happen goes here for example:
        name = message.content.strip("!reports ")
        pool = await database.create_pool()
        data = await database.get_reports(pool, f"SELECT * FROM reports where reporter = '{name}'")
        processed_data = await database.process_report_data(data)

        logger.info(processed_data)

        result = f"""
Processed Report Data:
* Total Reports: {processed_data['amount_of_reports']}
* Total ELO: {processed_data['sum_elo']}
* Total SR: {processed_data['sum_sr']}
* Total TP: {processed_data['sum_tp']}
* Number of Bans: {processed_data['ban_count']}
* Number of PP: {processed_data['pp_count']}
* Number of Permabans: {processed_data['permaban_count']}
* Number of Back to Rookies: {processed_data['back_to_rookies_count']}
"""
        # Now you can work with the processed data
        await message.reply(result)


class LfmGetReports(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        # Whatever should happen goes here for example:
        args = message.content.replace("!getreports ", "")
        name, args = args.split(" - ")
        logger.info(name)
        pool = await database.create_pool()
        data = await database.get_reports(pool, f"SELECT * FROM reports where reporter = '{name}'")
        logger.info(len(data))

        if (args == "all"):
            # Create a file with the fetched data
            filename = "exported_data.txt"
            with open(filename, "w", encoding="UTF-8") as file:
                for row in data:
                    file.write(",".join(map(lambda x: str(round(x, 2)) if isinstance(x, float) else str(x), row)) + "\n")

            # Send the file as an attachment in a Discord message
            with open(filename, "rb") as file:
                await message.reply(file=discord.File(file, filename=filename))

            # Delete the local file
            os.remove(filename)

        elif (args == "sum"):
            processed_data = await database.process_report_data(data)

            logger.info(processed_data)

            result = f"""
Processed Report Data:
* Total Reports: {processed_data['amount_of_reports']}
* Total ELO: {processed_data['sum_elo']}
* Total SR: {processed_data['sum_sr']}
* Total TP: {processed_data['sum_tp']}
* Number of Bans: {processed_data['ban_count']}
* Number of PP: {processed_data['pp_count']}
* Number of Permabans: {processed_data['permaban_count']}
* Number of Back to Rookies: {processed_data['back_to_rookies_count']}
    """
            # Now you can work with the processed data
            await message.reply(result)


class AddReport(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        if "help" in message.content:
            allowed_names = ["name", "sr", "tp", "ban", "penalty point", "pp", "permaban", "back to rookies", "btr"]
            # Create an embed
            embed = discord.Embed(title="Help")

            # Add the allowed class names as a list
            class_names = ", ".join(allowed_names)

            # Define the syntax
            syntax = "!addreport name: [whoever is being reported] - sr: 0.5 - tp: 10"

            # Add the text to the embed
            embed.add_field(name="Allowed Variable Names", value=class_names, inline=False)
            embed.add_field(name="Syntax", value=syntax, inline=False)
            embed.add_field(name="Bonus", value="Ban takes integer (number of days) — pp or penalty points takes a number — permaban and btr or back to rookies only neet to be mentioned, will be set to true in that case.", inline=False)

            await message.reply(embed=embed)
            return
        # Whatever should happen goes here for example:
        logger.info("trying to add value to reports database")
        result = process_report_input.process(message.content)
        logger.info(result)
        logger.info(f"User ID:  {message.author.id}")
        user_name = get_lfm_name.get_username_by_id(str(message.author.id))
        logger.info(user_name)
        if user_name is None:
            await message.reply("You are not authorised, ask kylo to add you. :)")
            return
        date = datetime.now().strftime("%d/%m/%Y")
        logger.info(date)

        query = f"INSERT INTO reports (date, reporter, reportee, elo, sr, tp, ban, pp, permaban, back_to_rookies) VALUES ({date}, '{user_name}', '{result['name']}', 0, {result['sr']}, {result['tp']}, {result['ban']}, {result['pp']}, {result['permaban']}, '{result['btr']}')"
        logger.info(query)

        await database.add_report(await database.create_pool(), query)
        await message.reply(f"```\nadded report with VALUES ({date}, {user_name}, {result['name']}, 0, {result['sr']}, {result['tp']}, {result['ban']}, {result['pp']}, {result['permaban']}, {result['btr']}\n```")




class LfmSrCalculator(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):

        args = message.content[len('!sr'):].strip().split()
        if "help" in args:
            await message.reply("`!sr [track] [incidents] [laps]`")

        else:
            track = Utils.to_lfm_track_name(args[0])

            incidents = int(args[1])
            laps = int(args[2])
            result = safety_rating.calculate(track, incidents, laps)
            await message.reply(result)




class LFMRaceFilter(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        if "help" in message.content:
            await message.reply("Command should be something like `!find_race track=Kyalami maxtemp=25`, possible variables are track, maxtemp, mintemp, wet, dry, mixed. Last 3 are the chances that race will be dry, mixed or wet with the input from 0 to 100 being the minimum chance.")
        result = filter_race.get_data(message.content)
        await message.reply(f"```{result}```")


class LFMCarStatsAllCars(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        if "help" in message.content:
            await message.reply(f"!{self.name} [track] - [sort (optional)]")
            await message.reply("sorting: `wins, podiums, top5, top10, entries, wins_per_race, podium_per_race, top5_per_race or top10_per_race`")
            return

        sort = "entries"
        track = "Nordschleife"

        try:
            sort = message.content.replace(f"!{self.name} ", "")
        except:
            sort = "wins"

        track = Utils.to_lfm_track_name(track)
        sent = await message.reply(f"Getting car stats for {track}, sorted by {sort}.")

        logger.info(f"{track} {sort}")
        try:
            data1 = car_stats.get_stats(track, sort)
            data2 = car_stats.get_stats_gt4(track, sort)
        except:
            await message.reply("The sorting was not correct. I changed it to 'wins', the default sort. You can try again with one of the following: `wins, podiums, top5, top10, entries, wins_per_race, podium_per_race, top5_per_race or top10_per_race, entry_rate`")
            data1 = car_stats.get_stats(track, 'wins')
            data2 = car_stats.get_stats_gt4(track, 'wins')

        if data1 and data2:
            data = data1 + data2
        
        data = sorted(data, key=lambda x: x[sort], reverse=True)

        track = bop.find_closest_track(bop.get_all_tracks(), track)

        max_length = 40
        result = f"**{track}**\n"
        carname = f"Car"
        wins = f"Wins"
        podiums = f"Pod."
        top5 = f"Top 5"
        top10 = f"Top 10"
        races = f"Entries"
        winsratio = f"Wins"
        podiumsratio = f"Pod."
        top5ratio = f"Top 5"
        top10ratio = f"Top 10"
        entry_rate = f"Entries"
        result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

        for car in data:
            carname = f"{car['car_name']}"
            wins = f"{car['wins']}"
            podiums = f"{car['podium']}"
            top5 = f"{car['top5']}"
            top10 = f"{car['top10']}"
            races = f"{car['races']}"
            winsratio = f"{int(car['wins_per_race'] * 10000) / 100}%"
            podiumsratio = f"{int(car['podium_per_race'] * 10000) / 10000}"
            top5ratio = f"{int(car['top5_per_race'] * 10000) / 10000}"
            top10ratio = f"{int(car['top10_per_race'] * 10000) / 10000}"
            entry_rate = f"{int(car['entry_rate'] * 10000) / 100}%"
            result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

        filename = f"{track}.md"
        with open(filename, 'a', encoding="UTF-8") as file:
            # Append your data
            file.write(result)

        # Send the file as an attachment in a Discord message
        with open(filename, "rb") as file:
            await message.reply(file=discord.File(file, filename=filename))

        os.remove(filename)        

        data = data[:10]
        result = f"**{track}**\n```"
        carname = f"Car"
        wins = f"Wins"
        podiums = f"Pod."
        top5 = f"Top 5"
        top10 = f"Top 10"
        races = f"Entries"
        winsratio = f"Wins"
        podiumsratio = f"Pod."
        top5ratio = f"Top 5"
        top10ratio = f"Top 10"
        entry_rate = f"Entries"
        result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

        for car in data:
            carname = f"{car['car_name']}"
            wins = f"{car['wins']}"
            podiums = f"{car['podium']}"
            top5 = f"{car['top5']}"
            top10 = f"{car['top10']}"
            races = f"{car['races']}"
            winsratio = f"{int(car['wins_per_race'] * 10000) / 100}%"
            podiumsratio = f"{int(car['podium_per_race'] * 10000) / 10000}"
            top5ratio = f"{int(car['top5_per_race'] * 10000) / 10000}"
            top10ratio = f"{int(car['top10_per_race'] * 10000) / 10000}"
            entry_rate = f"{int(car['entry_rate'] * 10000) / 100}%"
            result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

        result += "```"
        await message.reply(f"{result}")
        await sent.delete()



class LfmSum(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        # Whatever should happen goes here for example:
        sorted_data = lfmhl_all_cars_all_tracks.get_data()

        loop_count = 0
        result = "```"
        max_car_length = max(len(f"{entry['Car']}") for entry in sorted_data)
        result += (f"{'Car'.ljust(max_car_length)} {'Total Time':<15} {'Average Time':<15} {'Count'}\n")

        for entry in sorted_data:
            result += (f"{entry['Car'].ljust(max_car_length)}\t{(lfmhl.format_time(entry['Total Time'])):<10}\t{lfmhl.format_time(entry['Average Time']):<10}\t{entry['Count']}\n")		
            loop_count += 1

            if loop_count >= 15:
                result += "```"
                await message.reply(result)
                result = "```"
                result += (f"{'Car'.ljust(max_car_length)} {'Total Time':<15} {'Average Time':<15} {'Count'}\n")
                loop_count = 0

        # Send the remaining result if there are any
        if loop_count > 0:
            result += "```"
            await message.reply(result)



class LfmOffSeasonStandings(Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        # Whatever should happen goes here for example:
        # Import the random module
        content = message.content.replace("!offseason ","")
        try:
            series, weeks = content.split(" ")
            result = off_season_standings.get_standings(series, int(weeks))
        except:
            series = content
            result = off_season_standings.get_standings(series)
        await message.reply(f"**Your standings good sir, or lady**:\n ```{result}```")


