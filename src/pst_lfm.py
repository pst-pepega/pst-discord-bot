import requests
import logging
from datetime import datetime
from dateutil import tz
from typing import List, Any
from constants import Emotes, LFM_API
from typing import Optional

import user_stat_comparison
import favourite_cars

logger = logging.getLogger(__name__)

LFM_ELO_RANKS = {
    "Iron+": 1000,
    "Bronze": 1300,
    "Bronze+": 1500,
    "Silver": 1700,
    "Silver+": 2000,
    "Gold": 2500,
    "Gold+": 3000,
    "Platinum": 4000,
    "Diamond": 5000,
    "Legend": 6000,
    "Alien": 8000
}


def get_users_past_races(user_id: int) -> List[dict[str, Any]]:

    url = f"{LFM_API}/users/getUsersPastRaces/{user_id}?start=0&limit=5"
    response = requests.get(url)
    return response.json()


def get_race_time(race_data: dict) -> datetime:

    race_date = datetime.strptime(race_data["race_date"], "%Y-%m-%d %H:%M:%S")

    race_date = race_date.replace(tzinfo=tz.tzstr("UTC+1"))
    race_date_local = race_date.astimezone(tz.tzlocal())

    return race_date_local


def rating_change_converter(rating: int) -> str:

    match rating:
        case rating if rating > 0:
            return "+ 📈"
        case rating if rating < 0:
            return "- 📉"
        case _:
            return "∆"


async def check_last_race(pool, discord_id, lfm_id) -> Optional[str]:

    past_races = get_users_past_races(lfm_id)

    if len(past_races) == 0:
        logger.info(f"LFM user {lfm_id} never raced")
        return None

    last_race = past_races[0]

    conn = await pool.acquire()

    row = await conn.fetchrow("SELECT race_id FROM last_lfm_race WHERE lfm_id = $1", lfm_id)

    if row is not None and row.get("race_id") == last_race.get("race_id"):
        logger.info(f"Already notified last race for {lfm_id}")
        await pool.release(conn)
        return None

    new_race_id = last_race.get("race_id")
    sim_id = last_race.get("sim_id")

    await conn.execute("""
                       INSERT INTO last_lfm_race(lfm_id, race_id)
                       VALUES ($1, $2)
                       ON CONFLICT (lfm_id) DO UPDATE
                       SET race_id = $2""",
                       lfm_id, new_race_id)

    await pool.release(conn)
    users_data = requests.get(f"{LFM_API}/users/getUserData/{lfm_id}").json()
    user_fullname = f"{users_data.get('vorname')} {users_data.get('nachname')}"
    if sim_id == 1:
        elo = users_data['rating_by_sim'][0]['rating']
    else:
        elo = users_data['rating_by_sim'][sim_id-2]['rating']

    sr = users_data['safety_rating']

    race_time = get_race_time(last_race)
    race_time_epoch = int(race_time.timestamp())
    position_delta = last_race['start_pos'] - last_race['finishing_pos']
    sr_change_diff = rating_change_converter(last_race.get("sr_change"))
    elo_change_diff = rating_change_converter(last_race.get("rating_change"))
    position_change_diff = rating_change_converter(position_delta)
    best_of_week = ""
    if last_race["best_of_week"] == True:
        best_of_week = f"{Emotes.AYAYA} Best result of the week for {user_fullname} {Emotes.AYAYA}\n"

    discord_message = f"""
### Latest LFM race for {user_fullname}
**{last_race['event_name']} in the {last_race['car']} at {last_race['track_name']}**
<t:{race_time_epoch}:R>
```diff
{position_change_diff} P{last_race['start_pos']} -> P{last_race['finishing_pos']} [{position_delta}]
{elo_change_diff} {last_race['rating_change']} Elo
→ {elo} Elo
{sr_change_diff} {last_race['sr_change']} SR [{last_race['incidents']} IP]
→ {sr} SR
```
"""
    
    elo_text_result = find_nearby_value(LFM_ELO_RANKS, elo, int(last_race.get("rating_change")))

    if elo_text_result:
        if int(last_race['rating_change']) > 0:
            elo_text_result = f"**You just achieved the rank of {elo_text_result[0]} {Emotes.HABIBI}**"
        elif int(last_race['rating_change']) < 0:
            elo_text_result = f"**You just lost the rank of {elo_text_result[0]} {Emotes.BOZO}**"
        else:
            elo_text_result = None

    if best_of_week:
        discord_message += best_of_week

    if sim_id == 1:
        car = last_race['car']
        try:
            milestone = f"{favourite_cars.get_milestone(int(lfm_id), car)}\n"
        except Exception as e:
            logger.info(e)

        if "GT4" in car:
            car = last_race['car']
            try:
                milestone = f"{milestone}\n{favourite_cars.get_milestone(int(lfm_id), 'GT4')}\n"
            except Exception as e:
                logger.info(e)

        if "GT2" in car:
            car = last_race['car']
            try:
                milestone = f"{milestone}\n{favourite_cars.get_milestone(int(lfm_id), 'GT2')}\n"
            except Exception as e:
                logger.info(e)
        
        try:
            milestone += favourite_cars.get_combined_cars_sorted_milestone(int(lfm_id), car)
        except Exception as e:
            logger.info(e)

    
    data = requests.get(f"{LFM_API}/users/getUserStats/{lfm_id}").json()
    podiums = data["podium"]
    wins = data["wins"]
    poles = data["poles"]
    races = data["starts"]
    leaderboard_podiums = None
    leaderboard_poles = None 
    leaderboard_wins = None 
    leaderboard_zeroip = None

    if last_race['finishing_pos'] == 1:
        discord_message += (f"Congratulations on your {get_ordinal(wins)} victory! {Emotes.PEEPOHEART} Well done!\n")
        leaderboard_wins = user_stat_comparison.get_stats("wins", lfm_id)
        if leaderboard_wins:
            discord_message += leaderboard_wins
    elif last_race['finishing_pos'] <= 3:
        discord_message += (f"You've clinched a spot on the podium! {Emotes.WICKED} Way to go, your {get_ordinal(podiums)}, congrats!\n")
        leaderboard_podiums = user_stat_comparison.get_stats("podium", lfm_id)
        if leaderboard_podiums:
            discord_message += leaderboard_podiums

    if last_race['start_pos'] == 1:
        discord_message += (f"{get_ordinal(poles)} Pole Position, Let's go! {Emotes.WICKED}\n")
        leaderboard_poles = user_stat_comparison.get_stats("poles", lfm_id)
        if leaderboard_poles:
            discord_message += leaderboard_poles

    if last_race['incidents'] == 0:
        discord_message += "0 Incident Points. Very nice.\n"
        leaderboard_zeroip = user_stat_comparison.get_stats("zero_ip", lfm_id)
        if leaderboard_zeroip:
            discord_message += leaderboard_zeroip

    discord_message += f"**{calculate_custom_milestone(races)}**\n"

    try:
        discord_message += milestone # type: ignore
    except Exception as e:
        logger.info(e)

    if elo_text_result:
        discord_message += f"\n{elo_text_result}"

    return discord_message


def get_ordinal(number: int) -> str:
    if 10 <= number % 100 <= 20:
        suffix = "th"
    else:
        suffix = {1: "st", 2: "nd", 3: "rd"}.get(number % 10, "th")

    return str(number) + suffix


def calculate_custom_milestone(current_races: int) -> str:
    if current_races <= 50:
        milestone_interval = 10
    elif current_races <= 100:
        milestone_interval = 20
    elif current_races <= 500:
        milestone_interval = 25
    elif current_races <= 1000:
        milestone_interval = 50
    else:
        milestone_interval = 100

    next_milestone = ((current_races // milestone_interval) + 1) * milestone_interval
    races_to_next_milestone = next_milestone - current_races

    # Check if a milestone has been achieved
    if current_races % milestone_interval == 0:
        message = f"You've just achieved a milestone of {current_races} races!\n"
    else:
        message = f"{races_to_next_milestone} race(s) to your next Milestone! Keep it up!\n"

    return message


def find_nearby_value(data_dict, current_elo, elo_delta):
    """
    Find the dict entry with a number in the list that is within the range [x, x + delta] 
    if delta > 0 or [x - delta, x] if delta < 0. 
    If delta is 0, the function returns None.

    :param data_dict: A dictionary where values are lists of numbers.
    :param x: The number to compare against.
    :param delta: The range within which to check for numbers near x.
    :return: The dict entry (key, value) if a number in the list is found in the specified range, None otherwise.
    """

    # If delta is 0, do nothing
    if elo_delta == 0:
        return None

    # Determine the lower and upper bounds based on the sign of delta
    lower_bound = current_elo - elo_delta if elo_delta > 0 else current_elo
    upper_bound = current_elo if elo_delta > 0 else current_elo - elo_delta

    print(lower_bound, upper_bound)

    # Iterate over dictionary items
    for key, value in data_dict.items():
        # Check if any number in the value list falls within the range
        if lower_bound <= value <= upper_bound:
            return key, value

    # If no matching entry is found, return None
    return None