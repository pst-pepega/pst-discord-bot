import requests
from constants import Series_to_ID

series_id = Series_to_ID.get_id('Pro')
url = f"https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeriesPenalties/{series_id}"

def get_list(limit = 10):
    result = ""

    try:
        # Fetch JSON data from the API
        response = requests.get(url)
        data = response.json()

        # Print the "nachname" and "penalty points" fields for the first 10 items
        for entry in data[:limit]:
            nachname = entry.get("nachname", "N/A")
            penalty_points = entry.get("penalty_points", "N/A")

            result += (f'{nachname:20}: {penalty_points}\n')

    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")
    return(result)

def get_name(driver):
    result = ""
    try:
        # Fetch JSON data from the API
        response = requests.get(url)
        data = response.json()

        # Print the "nachname" and "penalty points" fields for the first 10 items
        for entry in data:
            if entry.get("nachname", "N/A").lower() == driver:   
                penalty_points = entry.get("penalty_points", "N/A")
                return (f'{entry.get("vorname", "N/A")} {entry.get("nachname", "N/A")}: {penalty_points}')

        return "Clean driver detected, try again with a dirty driver."
    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")       


def get_point_reduction_list():
    dirty_list = []
    try:
        # Fetch JSON data from the API
        response = requests.get(url)
        data = response.json()

        # Print the "nachname" and "penalty points" fields for the first 10 items
        for entry in data:
            id = entry.get("user_id", "N/A")   
            penalty_points = entry.get("penalty_points", "N/A")
            if float(penalty_points) >= 7.0:
                dirty_list.append(int(id))
        return dirty_list

    except requests.exceptions.RequestException as e:
        print(f"Error fetching data: {e}")    