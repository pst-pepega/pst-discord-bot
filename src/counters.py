import json

def count_up(name):
	with open('resources/json/counters.json', 'r') as file:
		data = json.load(file)

	# Get the current value of copium
	copium = int(data.get(name, 0))+1

	# Update the data with the new copium value
	data[name] = copium

	# Save the updated data back to counters.json
	with open('resources/json/counters.json', 'w') as file:
		json.dump(data, file, indent=4)

	return data[name]