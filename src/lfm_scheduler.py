import json
from datetime import datetime, timedelta

def load_lfm_data():
    with open("resources/json/lfm_schedule.json") as f:
        lfm_data = json.load(f)
    return lfm_data


def find_track_by_input(input_text):
    lfm_data = load_lfm_data()
    matching_tracks = []

    for track in range(len(lfm_data)):
        print(lfm_data[track])
        if lfm_data[track]['track'] != None:
            if input_text.lower() in lfm_data[track]["track"].lower():
                date = lfm_data[track]["date"]
                series = lfm_data[track]["series"]
                matching_tracks.append(f"{date} — {series}")

    return matching_tracks


def find_next_week():
    lfm_data = load_lfm_data()
    next_week_data = []

    # Calculate the start and end dates for next week
    today = datetime.today()
    next_week_start = (today + timedelta(days=(7 - today.weekday()))).replace(hour=0, minute=0, second=0, microsecond=0)
    next_week_end = (next_week_start + timedelta(days=6)).replace(hour=0, minute=0, second=0, microsecond=0)

    # Append items to the list if their date falls within the next week's range
    for item in lfm_data:
        item_date = datetime.strptime(item['date'], '%d/%m/%Y')
        if next_week_start <= item_date <= next_week_end:
            next_week_data.append(item)

    return next_week_data


def find_current_week():
    lfm_data = load_lfm_data()
    next_week_data = []

    # Calculate the start and end dates for next week
    today = datetime.today()
    next_week_start = (today + timedelta(days=(7 - today.weekday()))).replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(-7)
    next_week_end = (next_week_start + timedelta(days=6)).replace(hour=0, minute=0, second=0, microsecond=0)

    print(next_week_end)

    # Append items to the list if their date falls within the next week's range
    for item in lfm_data:
        item_date = datetime.strptime(item['date'], '%d/%m/%Y')
        if next_week_start <= item_date <= next_week_end:
            next_week_data.append(item)

    return next_week_data

def schedule(input_series):
    lfm_data = load_lfm_data()
    # Filter the data based on the input series
    filtered_data = [item for item in lfm_data if item["series"] == input_series]

    result = ""
    # Print the filtered data
    for item in filtered_data:
        result += (f"{item['date']}\t{item['track']}\n")

    if result != "":
        result = f"```{result}```"

    return result or None

def get_series_schedule(series):
    lfm_data = load_lfm_data()
    return [item for item in lfm_data if item["series"] == series]
