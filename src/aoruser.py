import requests
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import json
import logging

def format_time(seconds):
	minutes, seconds = divmod(seconds, 60)
	seconds, milliseconds = divmod(seconds, 1)
	milliseconds = int(milliseconds * 1000)
	return f"{int(minutes):02d}:{int(seconds):02d}:{milliseconds:03d}"

def get_hotlaps(track_name):
	hotlap_url = f"https://aor-hotlap.skillissue.be/api/hotlaps/{track_name}?patch=1.9"
	response = requests.get(hotlap_url)

	if response.status_code == 200:
		hotlaps_data = response.json()
		return hotlaps_data
	else:
		print(f"Failed to retrieve hotlaps data for {track_name}. Status code: {response.status_code}")
		return None

def find_closest_track(match_tracks, input_track):
	closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.ratio) # type: ignore
	return closest_match

def find_closest_car(match_car, input_car):
	closest_match, _ = process.extractOne(input_car, match_car, scorer=fuzz.partial_token_sort_ratio) # type: ignore
	return closest_match

def find_hotlaps_for_driver(hotlaps_data, driver_name, acc_patch):
	matching_hotlaps = []
	
	print(len(hotlaps_data))
	
	for hotlap in hotlaps_data:
		if acc_patch == "":
			if hotlap["driver_name"].lower() == driver_name.lower():
				matching_hotlaps.append(hotlap)
		else:
			if hotlap["driver_name"].lower() == driver_name.lower():
				return hotlap
			
	return matching_hotlaps

def find_hotlaps_for_driver_and_car(hotlaps_data, driver_name, car_name, acc_patch):
	matching_hotlaps = []

	logging.info(f"{driver_name} - {car_name} - {acc_patch}")
	
	print(len(hotlaps_data))
	
	for hotlap in hotlaps_data:
		if acc_patch == "":
			if hotlap["driver_name"].lower() == driver_name.lower() and hotlap["car_name"].lower() == car_name.lower():
				matching_hotlaps.append(hotlap)
		else:
			if hotlap["driver_name"].lower() == driver_name.lower():
				return hotlap
			
	return matching_hotlaps

def find_hotlaps_for_car(hotlaps_data, car_name, acc_patch):
	matching_hotlaps = []
	
	print(len(hotlaps_data))
	
	for hotlap in hotlaps_data:
		if acc_patch == "":
			if hotlap["car_name"].lower() == car_name.lower():
				matching_hotlaps.append(hotlap)
		else:
			if hotlap["car_name"].lower() == car_name.lower():
				return hotlap
			
	return matching_hotlaps

def get_data(input_track, driver_name, input_car, acc_patch):
	logging.info(f"{input_track} - {driver_name} - {input_car} - {acc_patch}")

	track_list = ['barcelona', 'mount_panorama', 'brands_hatch', 'cota', 'donington',
          'hungaroring', 'imola', 'indianapolis', 'imola', 'kyalami',
          'laguna_seca', 'misano', 'monza', 'nurburgring', 'oulton_park',
          'paul_ricard', 'spa', 'snetterton', 'silverstone', 'suzuka',
          'valencia', 'watkins_glen', 'zandvoort', 'zolder']  # List of available tracks
	
	car_list = ['lamborghini huracan evo 2 (2023)', 'mclaren 720s gt3 evo (2023)', 
	     		'ferrari 296 gt3 (2023)', 'porsche 992 gt3 r  (2023)', 'audi r8 lms evo ii (2022)', 
				'lamborghini huracan supertrofeo evo2 (2022)', 'bmw m2 cs (2022)', 'bmw m4 gt3 (2022)', 
				'porsche 992 gt3 cup (2022)', 'mercedes-amg gt3 (2020)', 'ferrari 488 gt3 evo (2020)', 
				'amr v8 vantage (2019)', 'audi r8 lms evo (2019)', 'porsche 718 cayman gt4 (2019)', 
				'mclaren 720s gt3 (2019)', 'honda nsx gt3 evo (2019)', 'porsche 991 ii gt3 r (2019)', 
				'lamborghini huracan gt3 evo (2019)', 'alpine a110 gt4 (2018)', 'aston martin vantage gt4 (2018)', 
				'bmw m4 gt4 (2018)', 'bentley continental gt3 (2018)', 'nissan gt r nismo gt3 (2018)', 
				'porsche 991 gt3 r (2018)', 'chevrolet camaro gt4 (2017)', 'bmw m6 gt3 (2017)', 
				'reiter engineering r-ex gt3 (2017)', 'mercedes amg gt4 (2016)', 'maserati mc gt4 (2016)', 
				'lexus rc f gt3 (2016)', 'mercedes amg gt3 (2015)', 'ferrari 488 gt3 (2015)', 
				'emil frey jaguar g3 (2012)', 'ginetta g55 gt4 (2012)']
	
	closest_track = find_closest_track(track_list, input_track)
	
	if input_car != "":
		closest_car = find_closest_car(car_list, input_car)

	hotlaps_data = get_hotlaps(closest_track)

	if input_car == "" and driver_name.lower() != "none":
		matching_hotlaps = find_hotlaps_for_driver(hotlaps_data, driver_name, acc_patch)

	elif driver_name.lower() == "none" and input_car != "":
		matching_hotlaps = find_hotlaps_for_car(hotlaps_data, closest_car, acc_patch)
		matching_hotlaps = matching_hotlaps[:5]

	else:
		matching_hotlaps = find_hotlaps_for_driver_and_car(hotlaps_data, driver_name, closest_car, acc_patch)

	return matching_hotlaps
