import json
import requests
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_standings(series, rows = 10) -> str:
    logger.info(f"Series: {series} — Rows: {rows}")
    # Specify the API/URL from which to fetch JSON data
    if "gt3" in series.lower() or "gt4" in series.lower():
        url = "https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonStandings/245"  # Replace with the actual URL
        logger.info(url)

        # Fetch JSON data from the URL
        response = requests.get(url)
        if response.status_code == 200:
            json_data = response.json()
            logger.info("got data.")

            if "gt3" in series.lower():
                logger.info("GT3 Data")
                # Extract data for the table
                table_data = json_data["GT3"]["1"]
            else:
                logger.info("GT4 Data")
                # Extract data for the table
                table_data = json_data["GT4"]["1"]

        else:
            print(f"Failed to fetch data. Status code: {response.status_code}")
            return "The API didn't respond :("
    elif "nords" in series.lower() or "nordschleife" in series.lower():
        url = "https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonStandings/248"  # Replace with the actual URL
        logger.info(url)

        # Fetch JSON data from the URL
        response = requests.get(url)
        if response.status_code == 200:
            json_data = response.json()
            logger.info("got data.")

            logger.info("GT3 Data")
            # Extract data for the table
            table_data = json_data["GT3"]["1"]

        else:
            print(f"Failed to fetch data. Status code: {response.status_code}")
            return "The API didn't respond :("
    else:
        url = "https://api2.lowfuelmotorsport.com/api/v2/seasons/getSeasonStandings/246"  # Replace with the actual URL
        logger.info(url)

        # Fetch JSON data from the URL
        response = requests.get(url)
        if response.status_code == 200:
            json_data = response.json()
            logger.info("got data.")

            if "bmw" in series.lower() or "m2" in series.lower():
                logger.info("M2 Data")
                # Extract data for the table
                table_data = json_data["TCX"]["1"]
            else:
                logger.info("GT2 Data")
                # Extract data for the table
                table_data = json_data["GT2"]["1"]

        else:
            print(f"Failed to fetch data. Status code: {response.status_code}")
            return "The API didn't respond :("

    # Format header names
    position = "Pos."
    vorname = "Firstname"
    nachname = "Lastname"
    races = "Races"
    weeeks_counted = "Weeks"
    points = "Points"
    elo = "Elo"
    origin = "Origin"

    # Print formatted headers
    result = f"{position:4}\t{vorname[:10]:<10}\t{nachname[:10]:<10}\t{races:5}\t{weeeks_counted:5}\t{points:6}\t{elo:4}\t{origin[:6]:<6}\n"
    limiter = rows
    counter = 0
    # Print data
    for entry in table_data:
        row = [
            f"{entry['position']:4}",    # 2-character wide for position
            f"{entry['vorname'][:10]:<10}",  # 10-character wide for vorname
            f"{entry['nachname'][:10]:<10}",  # 10-character wide for nachname
            f"{entry['races']:5}",  # 2-character wide for races
            f"{entry['weeks_counted']:5}",  # 1-character wide for weeks counted
            f"{entry['points']:6}",  # 4-character wide for points
            f"{entry['elo']:4}",  # 4-character wide for elo
            f"{entry['origin'][:6]:<6}",  # 5-character wide for origin
        ]
        result += "\t".join(row)
        result += "\n"
        counter += 1
        logger.info(f"{counter} — {limiter}")
        if counter == limiter:
            return result
    return result