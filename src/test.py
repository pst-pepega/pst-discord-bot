

import json
import re

from matplotlib import pyplot as plt
import handle_pst_roles

def string_to_dict(string):
    """Converts a string in the given format to a Python dictionary."""

    # Remove the leading "INFO:root:{" and trailing "}"
    string = string.replace("INFO:root:{}", "").strip()

    # Split the string into key-value pairs
    pairs = re.findall(r'(\d+): (\d+)', string)

    # Create a dictionary from the key-value pairs
    return dict(pairs)

# Example usage
string = '{"Luca": 102, "Carpet": 65, "French Bozo": 7, "Washo": 4, "FBI Agent": 34, "trafka": 2, "Whitehead": 3, "Ryan Rennoir": 2, "Schubert": 1, "Ukraine Bozo": 22, "Kargulu": 2, "Jack Alexander": 1, "Haunted": 21, "Armento": 73, "Osama": 2, "Pran": 5, "Khepri": 9, "Flo": 31, "Ryan Cooper": 2, "Zabbzi": 40, "Schmeksi": 18, "Felix": 5, "Candy": 1}'
#result_dict = string_to_dict(string)
# Directly parse the string using json.loads
name_dict = json.loads(string)

# Sort the dictionary by message count in descending order
sorted_name_dict = dict(sorted(name_dict.items(), key=lambda item: item[1], reverse=True))

total_messages = sum(int(count) for count in sorted_name_dict.values())
others_count = 0
others_others_count = 0
filtered_names = []
filtered_counts = []
others_names = []
others_counts = []
others_others_names = []
others_others_counts = []



# Separate users with less than 1% messages into "Others"
for name, count in sorted_name_dict.items():
    if (int(count) / total_messages) >= 0.08:
        filtered_names.append(name)
        filtered_counts.append(count)
    
    elif (int(count) / total_messages) >= 0.016:
        others_count += int(count)
        others_names.append(name)
        others_counts.append(count)

    else:
        others_others_count += int(count)
        others_others_names.append(name)
        others_others_counts.append(count)

if others_count > 0:
    filtered_names.append("Others")
    filtered_counts.append(others_count+others_others_count)

if others_others_count > 0:
    others_names.append("Others")
    others_counts.append(others_others_count)


# Create the pie chart for proper people
plt.figure(figsize=(8, 8))
plt.pie(filtered_counts, labels=filtered_names, autopct='%1.1f%%', startangle=140, colors=plt.cm.Paired.colors)
plt.title('Messages by Users in last 1000 Server Messages', y=1.02)

# Adjust layout and remove some borders
plt.subplots_adjust(top=0.99, bottom=0.00)

# Save the chart to a file
plt.savefig("funny_pie_chart.png")
plt.close()

########

# Create the pie chart for others
plt.figure(figsize=(8, 8))
plt.pie(others_counts, labels=others_names, autopct='%1.1f%%', startangle=140, colors=plt.cm.Paired.colors)
plt.title(f'Messages by Others in {sum(others_counts)} of the last 1000 Server Messages', y=1.02)

# Adjust layout and remove some borders
plt.subplots_adjust(top=0.99, bottom=0.00)

# Save the chart to a file
plt.savefig("others.png")
plt.close()

########

# Create the pie chart for others
plt.figure(figsize=(8, 8))
plt.pie(others_others_counts, labels=others_others_names, autopct='%1.1f%%', startangle=140, colors=plt.cm.Paired.colors)
plt.title(f'Messages by Others in {sum(others_others_counts)} of the last 1000 Server Messages', y=1.02)

# Adjust layout and remove some borders
plt.subplots_adjust(top=0.99, bottom=0.00)

# Save the chart to a file
plt.savefig("others_others.png")
plt.close()