import requests
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from datetime import datetime, timedelta


def find_number_by_date(data_list, target_date):
    for date_str, number in data_list:
        if date_str == target_date:
            return number
    return None


def get_elo_graph(lfm_id):
    # Make the API request
    response = requests.get(f'https://api2.lowfuelmotorsport.com/api/users/getEloGraphData/{lfm_id}?sim_id=1')
    response2 = requests.get(f'https://api2.lowfuelmotorsport.com/api/users/getUserData/{lfm_id}?with_achievements=true')

    # Parse the JSON response
    data = response.json()
    data2 = response2.json()

    name = f"{data2['vorname']} {data2['nachname']}"

    # Extract the date and rating values
    dates = [item['date'] for item in data]
    ratings = [item['rating'] for item in data]

    first = dates[0]
    last = dates[len(dates)-1]

    # Create a list of tuples with the date and rating values
    elo_data = list(zip(dates, ratings))

    elo_data_temp = []
    last_elo = 0

    while first != last:
        temp_elo = find_number_by_date(elo_data, first)
        if temp_elo:
            elo_data_temp.append((first, temp_elo))
            last_elo = temp_elo
        else:
            elo_data_temp.append((first, last_elo))
        
        first = f"{(datetime.strptime(first, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')}"


    elo_data = elo_data_temp

    # Desired left margin in pixels
    left_margin_pixels = 50

    width = len(elo_data)/15

    # Create the figure with the specified size
    fig = plt.figure(figsize=(width, 10))

    # Create a graph using matplotlib
    plt.xlabel('Date')
    plt.ylabel('Elo Rating')
    plt.title(f'Elo Graph Data for {name}', fontsize=width/2)
    plt.xticks(range(len(elo_data)), [date for date, _ in elo_data], rotation=45)

    # Display date labels every tick
    loc = ticker.MultipleLocator(base=10)  # Change this base value as needed
    plt.gca().xaxis.set_major_locator(loc)

    # Adjust the subplot parameters for left margin in pixels
    fig_width, fig_height = fig.get_size_inches()
    left_margin_inches = left_margin_pixels / fig.dpi
    left_margin_relative = left_margin_inches / fig_width
    plt.subplots_adjust(left=left_margin_relative)
    first = dates[0]
    last = dates[len(dates)-1]


    # Plot the graph with appropriate line styles
    for i in range(1, len(elo_data)):
        if elo_data[i][1] == elo_data[i-1][1]:
            linestyle = 'dotted'
            colour = "black"
        elif elo_data[i][1] < elo_data[i-1][1]:
            linestyle = 'solid'
            colour = "red"
        else:
            linestyle = 'solid'
            colour = "green"
        
        # Extract the dates and ratings for the current segment
        dates = [elo_data[i-1][0], elo_data[i][0]]
        ratings = [elo_data[i-1][1], elo_data[i][1]]
        
        # Plot the segment
        plt.plot(dates, ratings, linestyle=linestyle, color=colour)


    plt.subplots_adjust(left=0.04)
    plt.subplots_adjust(right=0.96)
    # Get minimum and maximum dates from elo_data
    min_date = min(data[0] for data in elo_data)
    max_date = max(data[0] for data in elo_data)

    # Set x-axis limits with some buffer to avoid empty ticks
    plt.xlim([min_date, max_date])
    # Show the plot
    #plt.show()
    # Save the plot as an image file
    file_name = 'elo_graph.png'
    plt.savefig(file_name)

    return file_name


def get_safetyrating_graph(lfm_id):
    # Make the API request
    response = requests.get(f'https://api2.lowfuelmotorsport.com/api/users/getSrGraphData/{lfm_id}')
    response2 = requests.get(f'https://api2.lowfuelmotorsport.com/api/users/getUserData/{lfm_id}?with_achievements=true')

    # Parse the JSON response
    data = response.json()
    data2 = response2.json()

    name = f"{data2['vorname']} {data2['nachname']}"

    # Extract the date and rating values
    dates = [item['date'] for item in data]
    ratings = [item['rating'] for item in data]

    # Create a list of tuples with the date and rating values
    safetyrating_data = list(zip(dates, ratings))

    # Print the safetyrating_data list
    width = max(len(safetyrating_data)/15, 15)

        # Extract the date and rating values
    dates = [item['date'] for item in data]
    ratings = [item['rating'] for item in data]

    first = dates[0]
    last = dates[len(dates)-1]

    # Create a list of tuples with the date and rating values
    elo_data = list(zip(dates, ratings))

    elo_data_temp = []
    last_elo = 0

    while first != last:
        temp_elo = find_number_by_date(elo_data, first)
        if temp_elo:
            elo_data_temp.append((first, temp_elo))
            last_elo = temp_elo
        else:
            elo_data_temp.append((first, last_elo))
        
        first = f"{(datetime.strptime(first, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')}"


    elo_data = elo_data_temp

    # Desired left margin in pixels
    left_margin_pixels = 50

    width = len(elo_data)/15

    # Create the figure with the specified size
    fig = plt.figure(figsize=(width, 10))

    # Create a graph using matplotlib
    plt.xlabel('Date')
    plt.ylabel('Safety Rating')
    plt.title(f'Safety Rating Data for {name}', fontsize=width/2)
    plt.xticks(range(len(elo_data)), [date for date, _ in elo_data], rotation=45)

    # Display date labels every tick
    loc = ticker.MultipleLocator(base=10)  # Change this base value as needed
    plt.gca().xaxis.set_major_locator(loc)

    # Adjust the subplot parameters for left margin in pixels
    fig_width, fig_height = fig.get_size_inches()
    left_margin_inches = left_margin_pixels / fig.dpi
    left_margin_relative = left_margin_inches / fig_width
    plt.subplots_adjust(left=left_margin_relative)
    first = dates[0]
    last = dates[len(dates)-1]

    # Plot the graph with appropriate line styles
    for i in range(1, len(elo_data)):
        if elo_data[i][1] == elo_data[i-1][1]:
            linestyle = 'dotted'
            colour = "black"
        elif elo_data[i][1] < elo_data[i-1][1]:
            linestyle = 'solid'
            colour = "red"
        else:
            linestyle = 'solid'
            colour = "green"
        
        # Extract the dates and ratings for the current segment
        dates = [elo_data[i-1][0], elo_data[i][0]]
        ratings = [elo_data[i-1][1], elo_data[i][1]]
        
        # Plot the segment
        plt.plot(dates, ratings, linestyle=linestyle, color=colour)


    plt.subplots_adjust(left=0.04)
    plt.subplots_adjust(right=0.96)
    # Get minimum and maximum dates from elo_data
    min_date = min(data[0] for data in elo_data)
    max_date = max(data[0] for data in elo_data)

    # Set x-axis limits with some buffer to avoid empty ticks
    plt.xlim([min_date, max_date])
    plt.ylim(top=9.99)
    # Show the plot
    #plt.show()
    # Save the plot as an image file

    file_name = 'safetyrating_graph.png'
    plt.savefig(file_name)

    return file_name