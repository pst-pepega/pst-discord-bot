import discord
import logging
import Commands
import bop
import os

logger = logging.getLogger(__name__)


class get_bop_file(Commands.command.Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        # Whatever should happen goes here for example:
        track = message.channel.content.replace("!bopfile ", "") # type: ignore
        logger.info(track)
        filename = bop.get_json(track)
        logger.info(filename)
    
        # Check if a file was generated
        if filename:
            with open(filename, "rb") as file:
                await message.channel.send(file=discord.File(file))
        # Delete the file after uploading
        if filename:
            os.remove(filename)
