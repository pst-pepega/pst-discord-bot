import discord
import logging

from .command import Command
from discord import app_commands

logger = logging.getLogger(__name__)
POPOMETER_SETUPS = "https://popometer.io/setups?user="


@app_commands.command(name="popometer_teemu",
                      description="Sends link to Teemu's popometer setups.")
async def popometer_teemu_cmd(interaction: discord.Interaction):

    await interaction.response.send_message(f"[Link to Teemu's Popometer 👍]({POPOMETER_SETUPS}Teemu+Karppinen&ref=u428)")


@app_commands.command(name="popometer_geert",
                      description="Sends link to Teemu's popometer setups.")
async def popometer_geert_cmd(interaction: discord.Interaction):

    await interaction.response.send_message(f"[Link to Geerts's Popometer 👍]({POPOMETER_SETUPS}Geert+Fischer&ref=u428)")
