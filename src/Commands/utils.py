import asyncio
import json
from typing import Literal, Optional
from asyncpg import UniqueViolationError  # Import the exception class
import discord
import logging
import os
import requests
import re
import statistics
import time

from .command import Command
from discord import app_commands
import database
import fuel_calc
import multiclass_helper
import bop_commands_manager
import pitlane
import Utils
import replace_url
import download_video

logger = logging.getLogger(__name__)


@app_commands.command(name="embedder",
                      description="Convert twitter, tiktok, 𝕏, and twitch link into a link that can embed and viewed on discord.")
@app_commands.describe(link="The link.")
@app_commands.describe(text_only="type anything for text only twitter link.")
@app_commands.describe(media_only="type anything for media only twitter link.")
@app_commands.describe(gallery_view="type anything for gallery view twitter link.")
@app_commands.describe(link_or_file="If file then sends video as file.")
async def embedder_cmd(interaction: discord.Interaction,
                       link: str,
                       link_or_file: Optional[Literal[tuple(["link", "file"])]],
                       text_only: Optional[Literal[tuple(["Yes", "No"])]] = None,
                       media_only: Optional[Literal[tuple(["Yes", "No"])]] = None,
                       gallery_view: Optional[Literal[tuple(["Yes", "No"])]] = None):
    
    if '/?' in link:
        link = link.split('/?')[0]
    
    if "tiktok" in link:
        if link_or_file == "link" or link_or_file is None:
            await interaction.response.send_message(replace_url.replace_tiktok_link_with_tiktokez(link))
        else:
            file_name = download_video.download_video(link)
            if file_name:
                try:
                    await interaction.followup.send(file=discord.File(file_name))
                except:
                    await interaction.response.send_message(f"Didn't work, here is your link anyway: {replace_url.replace_tiktok_link_with_tiktokez(link)}")
    
    elif "twitter" in link:
        if text_only:
            await interaction.response.send_message(f"https://t.{replace_url.replace_twitter_link_with_fxtwitter(link)[8:]}")
        
        elif media_only:
            await interaction.response.send_message(f"https://d.{replace_url.replace_twitter_link_with_fxtwitter(link)[8:]}")

        elif gallery_view:
            await interaction.response.send_message(f"https://g.{replace_url.replace_twitter_link_with_fxtwitter(link)[8:]}")

        else:
            await interaction.response.send_message(replace_url.replace_twitter_link_with_fxtwitter(link))

    elif "twitch" in link:
        await interaction.response.defer(thinking=True, ephemeral=False)
        
        if link_or_file:
            if link_or_file == "link" or link_or_file is None:
                await interaction.followup.send(replace_url.replace_twitch_with_txitch(link))
            else:
                file_name = download_video.get_clip(link)
                if file_name:
                    try:
                        await interaction.followup.send(file=discord.File(file_name))
                    except:
                        await interaction.followup.send(f"Didn't work, here is your link anyway: {replace_url.replace_twitch_with_txitch(link)}")
            if file_name:
                os.remove(file_name)
        else:
            await interaction.followup.send(replace_url.replace_twitch_with_txitch(link))
    
    elif "x.com" in link:
        if text_only:
            await interaction.response.send_message(f"https://t.{replace_url.replace_x_link_with_fixupx(link)[8:]}")
        
        elif media_only:
            await interaction.response.send_message(f"https://d.{replace_url.replace_x_link_with_fixupx(link)[8:]}")

        elif gallery_view:
            await interaction.response.send_message(f"https://g.{replace_url.replace_x_link_with_fixupx(link)[8:]}")

        else:
            await interaction.response.send_message(replace_url.replace_x_link_with_fixupx(link))

    elif "reddit.com" in link:
        await interaction.response.send_message(re.sub(r'http(?:s)?://(?:[\w-]+?\.)?reddit\.com', r'https://rxddit.com', link))

    elif "instagram.com" in link:
        if "reel" in link:
            await interaction.response.send_message(link.replace("instagram.com", "instagramez.com"))
        else:
            if media_only:
                await interaction.response.send_message(link.replace("instagram.com", "d.ddinstagram.com"))
            if gallery_view:
                await interaction.response.send_message(link.replace("instagram.com", "g.ddinstagram.com"))

@app_commands.command(name="support",
                      description="Get link to discord server for support for this bot.")
async def support_cmd(interaction: discord.Interaction):

    await interaction.response.send_message("You can find help here: https://discord.gg/dAggwQcQXN")
    

@app_commands.command(name="how-to-report",
                      description="Get info on how to report on LFM.")
async def how_to_report_cmd(interaction: discord.Interaction):

    a = "[Reporting Guide](<https://youtu.be/w3DWudaNc5M>)"
    b = "[Reporting Sheet (not up to date)](<https://docs.google.com/spreadsheets/d/e/2PACX-1vQeLo3p4TpANLOvWngQY2Z-oRfJNtLtzbmsWRnCZE2lhrQQV0QovxNNthyABZOOtCPNN7mdyuWh7crC/pubhtml?gid=0&single=true>)"

    await interaction.response.send_message(f"{a}\n{b}")


# Load the JSON data
with open('resources/json/pitlane.json', 'r') as file:
    data = json.load(file)
TRACKS = tuple([track['name'] for track in data.get('tracks', [])])

async def track_autocomplete(interaction: discord.Interaction, current: str) -> list[app_commands.Choice[str]]:
    # Filter the TRACKS list based on the user's input
    filtered_tracks = [track for track in TRACKS if current.lower() in track.lower()]
    return [app_commands.Choice(name=track, value=track) for track in filtered_tracks[:25]]

PIT_INPUTS = tuple(["dt", "sg30", "tyre change", "1 litre refuel"])
@app_commands.command(name="pit",
                      description="Find out how long a pit stop takes.")
@app_commands.describe(track="Track you want to get the BOP for.")
@app_commands.autocomplete(track=track_autocomplete)
async def pit_cmd(interaction: discord.Interaction,
                       condition: Literal[PIT_INPUTS],
                       track: str):
    
    await interaction.response.defer(thinking=True, ephemeral=False)
    
    track = Utils.to_lfm_track_name(track)
    result = pitlane.find_most_similar_track(track)
    
    match condition:
            case "dt":
                time = result["DT Time"]

            case "sg30" | "tyre change":
                time = result["Tyre Change"]

            case "1 litre refuel":
                time = result["1 Litre Refuel"]        
                
    await interaction.followup.send(f"{time}s at {result['name']}")


@app_commands.command(name="fuel-calculator",
                      description="Calculate fuel for a race.")
@app_commands.describe(duration="How long is the race?")
@app_commands.describe(fuel_per_lap="How much fuel do you use per lap?")
@app_commands.describe(laptime="How long is a lap? (Example: 1:42.2 or 1:42.222 or 1:42.22)")
@app_commands.describe(ephemeral="If it should be publicly visible or only for you. Default is only you.")
async def fuel_calc_cmd(interaction: discord.Interaction,
                       duration: int,
                       fuel_per_lap: float,
                       laptime: str,
                       ephemeral: Optional[Literal[tuple(["True", "False"])]]):
    
    #result, result2 = fuel_calc.fuel_calc(duration, fuel_per_lap, laptime)
    embed = fuel_calc.fuel_calc(duration, fuel_per_lap, laptime)
    ephemeral_value = True if ephemeral == "True" else False

    print(ephemeral_value)
    
    if ephemeral:
        #await interaction.response.send_message(f"**{result}**\n{result2}", ephemeral=ephemeral_value)
        await interaction.response.send_message(embed=embed)
    else:
        #await interaction.response.send_message(f"**{result}**\n{result2}", ephemeral=True)
        await interaction.response.send_message(embed=embed, ephemeral=True)


@app_commands.command(name="invite",
                      description="Get invite link for the bot.")
async def invite_cmd(interaction: discord.Interaction):

    await interaction.response.send_message("Invite link: <https://pst-bot.skillissue.be/>")


@app_commands.command(name="code",
                      description="Get link to the code repository for the bot.")
async def code_link_cmd(interaction: discord.Interaction):

    await interaction.response.send_message("https://gitlab.com/pst-pepega")




@app_commands.command(name="multiclass",
                      description="Calculates when the overtakes happen between the classes.")
@app_commands.describe(race_length="Length of the race in minutes")
@app_commands.describe(gt3="Laptime for the GT3 Class")
@app_commands.describe(gt4="Laptime for the GT4 Class")
@app_commands.describe(porsche_cup="Laptime for the Porsche Cup Class")
@app_commands.describe(ferrari_challenge="Laptime for the Ferrari Challenge Class")
@app_commands.describe(lamborghini_st="Laptime for the Lamborghini ST Class")
@app_commands.describe(bmw_m2="Laptime for the BMW M2 Class")
async def multiclass_cmd(interaction: discord.Interaction,
                       gt3: Optional[int],
                       gt4: Optional[int],
                       porsche_cup: Optional[int],
                       ferrari_challenge: Optional[int],
                       lamborghini_st: Optional[int],
                       bmw_m2: Optional[int],
                       race_length: int):
    
    await interaction.response.defer(thinking=True, ephemeral=False)
    
    race_length *= 60
    multiclass_dict = dict()
    
    multiclass_dict.update({'length': race_length})
    if gt3:
        multiclass_dict.update({'gt3': gt3})
    if gt4:
        multiclass_dict.update({'gt4': gt4})
    if porsche_cup:
        multiclass_dict.update({'porsche': porsche_cup})
    if bmw_m2:
        multiclass_dict.update({'bmw': bmw_m2})
    if lamborghini_st:
        multiclass_dict.update({'lambo': lamborghini_st})
    if ferrari_challenge:
        multiclass_dict.update({'ferrari': ferrari_challenge})

    multiclass_result = multiclass_helper.calculate(multiclass_dict)
    result = ""
    for lap, cars in multiclass_result:
        car_list = cars.split(', ')
        if len(car_list) == 2:
            result += f"On Lap {lap}, {car_list[0].upper()}s lap {car_list[1].upper()}\n"
        else:
            result += f"On Lap {lap}, {car_list[0].upper()} laps {car_list[1].upper()}\n"

    try:
        await interaction.followup.send(result)
    except discord.errors.HTTPException as e:
        filename = f"multiclass.csv"
        logging.info(f"HTTPException: {e.response.status} {e.response.reason}") # type: ignore
        with open(filename, 'a') as file:
            # Append your data
            file.write(result)

        # Send the file as an attachment in a Discord message
        with open(filename, "rb") as file:
            await interaction.followup.send(file=discord.File(file, filename=filename))
        # Delete the local file
        os.remove(filename)


BOP_SETTING = tuple(["big", "small"])
@app_commands.command(name="setbop",
                      description="Set your default bop command output to either big or small")
@app_commands.describe(setting="The setting you want to set")
async def setbop_cmd(interaction: discord.Interaction,
                       setting: Literal[BOP_SETTING]):
    
    await interaction.response.defer(thinking=True, ephemeral=False)

    discord_id = float(interaction.user.id)
    discord_id_text = str(interaction.user.id)
    pool = await database.create_pool()
    value = await database.get_bop_setting_value(discord_id, pool)
    try:
        if value:
            await database.update_bop_setting(discord_id, setting, pool)
        else:
            await database.insert_bop_setting(discord_id, setting, pool)
    except Exception as e:
        channel_id = 1240300121254531072
        channel = self.get_channel(channel_id) # type: ignore
        await channel.send(f"Error upserting bop setting for discord_id: {discord_id}. Error: ```py\n {e} ```")

    await interaction.followup.send(f"Saved bop_setting for <@{str(discord_id_text)}>: __**{setting}**__.")

    if pool:
        await pool.close()