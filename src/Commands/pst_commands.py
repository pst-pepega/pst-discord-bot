import asyncio
from csv import reader
import csv
import json
import logging
import random
from typing import Literal, Optional
import discord
from discord import app_commands
import requests
import database

from constants import Emotes, Users
import handle_pst_roles
import driver_market_value_calc
import market_value_printer

@app_commands.command(name="ranks", description="Get the all of the ranks with the corresponding message count to achieve them.")
async def ranks_pstcmd(interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    await interaction.followup.send(f"```{await handle_pst_roles.get_ranks(interaction)}```")


@app_commands.command(name="messagesperday", description="Get your messages per day and the 2 users around you.")
async def messagesperday_pstcmd (interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    await interaction.followup.send(f"```{await handle_pst_roles.messages_per_day(interaction)}```")


@app_commands.command(name="next", description="Check when you level up to a new role")
async def next_pstcmd (interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    result = await handle_pst_roles.find_difference(interaction)
    
    if result:
        await interaction.followup.send(result)
    else:
        await interaction.followup.send("You won.")


@app_commands.command(name="overtake", description="Check when you overtake someone.")
async def overtake_pstcmd (interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    pool = await database.create_pool()
    try:
        result = await handle_pst_roles.get_overtake_counts(interaction, interaction.user.id, pool)
        await interaction.followup.send(result)
    except Exception as e:
        logging.info(e)
        await interaction.followup.send("You are in First.")

    if pool:
        await pool.close()


@app_commands.command(name="nextall", description="Check when the next 25 users are forecasted to level up.")
async def nextall_pstcmd (interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    result = f"```{await handle_pst_roles.find_difference_all(interaction)}```"
    
    if result:
        await interaction.followup.send(result)
    else:
        await interaction.followup.send("They won.")


@app_commands.command(name="join", description="Join the PST LFM Team by following these simple steps.")
async def join_pstcmd (interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
    await interaction.followup.send("<https://lowfuelmotorsport.com/teams/detail/227>\npassword: ryancooperisthe🐐")


@app_commands.command(name="minecraft", description="Information about the Minecraft server.")
async def minecraft_pstcmd (interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
    await interaction.followup.send(f"For Minecraft, ask {Users.COOPER} {Emotes.HABIBI}")


@app_commands.command(name="coc", description="Link to LFM Code of Conduct.")
async def coc_pstcmd (interaction: discord.Interaction):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
    await interaction.followup.send(f"https://drive.google.com/file/d/1d4al4hTtL7S3NGyAyULeVB7XFNqma2wA/view")


@app_commands.command(name="leaderboard", description="Show the messages leaderboard.")
async def leaderboard_pstcmd(interaction: discord.Interaction,
                             limit: Optional[int]):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    if limit:
        result = await handle_pst_roles.print_messages_leaderboard(interaction, limit)
    else:
        result = await handle_pst_roles.print_relative_messages_leaderboard(interaction)

    await interaction.followup.send(f"```{result}```")


@app_commands.command(name="leaderboard_voice", description="Show the voice activity leaderboard.")
async def leaderboard_voice_pstcmd(interaction: discord.Interaction,
                             limit: Optional[int]):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    if limit:
        result = await handle_pst_roles.print_voice_leaderboard(interaction, limit)
    else:
        result = await handle_pst_roles.print_voice_leaderboard(interaction)

    await interaction.followup.send(f"```{result}```")


@app_commands.command(name="haram_words_leaderboard", description="Show the leaderboard for naughty words.")
async def haram_pstcmd(interaction: discord.Interaction,
                             limit: Optional[int]):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    if limit:
        result = await handle_pst_roles.print_haram_leaderboard(interaction, limit)
    else:
        result = await handle_pst_roles.print_relative_haram_leaderboard(interaction)

    await interaction.followup.send(f"```{result}```")



@app_commands.command(name="f1", description="schedule f1 reminder.")
async def f1_pstcmd(interaction: discord.Interaction,
                             time: Optional[int]):
            

    if interaction.user.id != 83552350525980672:
        await interaction.channel.send("Brothermanbill you are looking for the /schedule command. (or you can also check the pins)") # type: ignore
    else:
        await interaction.response.defer(ephemeral=False, thinking=True)
        await interaction.followup.send(
            random.choice(
                json.load(open("resources/json/movie_quotes.json", "r"))))
        
        if not time:
            time = 5
        else:
            if time > 10:
                time = 10

        await asyncio.sleep(60 * time)
        await interaction.followup.send(
            f"{Users.MAX_VERSTAPPEN_SIMPS} Time for F1 Babes {Emotes.AYAYA}"
        )


@app_commands.command(name="add_driver_market_value", description="Add Driver to Market Value calculation.")
async def add_driver_market_value_pstcmd(interaction: discord.Interaction,
                             name: str,
                             birthday: str,
                             talent: float,
                             lfm_id: int):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    data = f"{name},{birthday},{talent:.1f},{lfm_id}"
    # Corresponding keys
    keys = ["name", "birthday", "talent", "user_id"]

    # Split the string by commas to get values
    values = data.split(',')

    # Create a dictionary by zipping keys and values together
    row = dict(zip(keys, values))
    
    value = driver_market_value_calc.calc(row)

    await interaction.followup.send(f"<@83552350525980672> add this `{name},{birthday},{talent:.1f},{lfm_id}`")
    await interaction.followup.send(f"{name} has a market value of {value}")


SORTINGS = tuple(["talent", "birthday", "score"])
@app_commands.command(name="print_driver_market_values", description="Print drivers on the market.")
async def print_driver_market_values_pstcmd(interaction: discord.Interaction,
                             sort_by: Literal[SORTINGS]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    if sort_by == 'talent':
        # Fetch the CSV data from the URL
        url = "https://gitlab.com/pst-pepega/pst-scripts/-/raw/main/lfm_driver_market_value/driver_data.csv"
        response = requests.get(url)
        decoded_content = response.content.decode('utf-8').splitlines()
        # Read the CSV data
        reader = csv.DictReader(decoded_content)
        # Convert CSV data into a list of dictionaries, excluding 'user_id'
        driver_data = [row for row in reader if 'user_id' in row]
        message = market_value_printer.sort_and_print_birthday_talent(driver_data, sort_by)
        if len(message) > 1900:
            messages = market_value_printer.split_message(message)
        else:
            messages = [message]

        for message in messages:
            await interaction.followup.send(f"```\n{message}```")
    
    elif sort_by == 'birthday':
        # Fetch the CSV data from the URL
        url = "https://gitlab.com/pst-pepega/pst-scripts/-/raw/main/lfm_driver_market_value/driver_data.csv"
        response = requests.get(url)
        decoded_content = response.content.decode('utf-8').splitlines()
        # Read the CSV data
        reader = csv.DictReader(decoded_content)
        # Convert CSV data into a list of dictionaries, excluding 'user_id'
        driver_data = [row for row in reader if 'user_id' in row]
        message = market_value_printer.sort_and_print_birthday_talent(driver_data, sort_by)
        if len(message) > 1900:
            messages = market_value_printer.split_message(message)
        else:
            messages = [message]

        for message in messages:
            await interaction.followup.send(f"```\n{message}```")

    elif sort_by == 'score':
        # Fetch the CSV data from the URL
        url = "https://gitlab.com/pst-pepega/pst-scripts/-/raw/main/lfm_driver_market_value/scores.csv"
        response = requests.get(url)
        decoded_content = response.content.decode('utf-8').splitlines()
        # Read the CSV data
        reader = csv.DictReader(decoded_content)
        # Convert CSV data into a list of dictionaries
        driver_data = [row for row in reader]
        message = market_value_printer.sort_and_print_score(driver_data)
        if len(message) > 1900:
            messages = market_value_printer.split_message(message)
        else:
            messages = [message]

        for message in messages:
            await interaction.followup.send(f"```\n{message}```")
