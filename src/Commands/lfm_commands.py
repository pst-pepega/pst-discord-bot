import asyncio
from datetime import datetime
import json
from sys import thread_info
from typing import Literal, Optional
import discord
from discord import app_commands
import emoji
import logging
import os
import re

import pandas as pd

import car_counts

from .command import Command
from constants import ACC_TRACKS, Series
import bop
import bop_commands_manager
import car_stats
import car_stats_string
import database
import elo_calc
import elo_calc_team_event
import entrylistprinter
import favourite_cars
import laprecords
import lfm_race_data
import lfm_scheduler
import lfm_penaltypoints
import lfmdata
import lfm_prostats_sorting
import lfmproweather
import lfmprostats
import lfmsearch
import LFM_laptimes_graphs
import penalties
import race_reminder
import user_graphs
import userstats
import Utils

logger = logging.getLogger(__name__)

#CAR_CLASSES = tuple(["GT3", "GT4", "CUP", "GT2", "TCX"])
### ADD OTHER CARS LATER
CAR_CLASSES = tuple(["GT3", "GT4"])


CARS = []
for file in os.listdir('resources/lfm_car_data'):
    if file.endswith('.json'):
        with open(os.path.join('resources/lfm_car_data', file), 'r') as f:
            json_data = json.load(f)
            CARS.append(file[:-5])

logger.info(CARS)

async def track_autocomplete(interaction: discord.Interaction, current: str) -> list[app_commands.Choice[str]]:
    # Filter the TRACKS list based on the user's input
    filtered_cars = [car for car in CARS if current.lower() in car.lower()]
    return [app_commands.Choice(name=car, value=car) for car in filtered_cars[:25]]


@app_commands.command(name="lfm-schedule-track", description="Shows you when a track is in the LFM schedule.")
@app_commands.describe(track="Track Name")
async def lfm_track_schedule_cmd(interaction: discord.Interaction, track: Literal[ACC_TRACKS]):
        result = ""

        message_to_print = lfm_scheduler.find_track_by_input(track)
        for _ in message_to_print:
            result += _
            result += "\n"  

        result += "For the full list of tracks for a series try /schedule Pro/GT3/Sprint/GT4/BMW M2 Series. :)"
        await interaction.response.send_message(result)


@app_commands.command(name="lfm-schedule-series", description="Shows you the schedule for a given LFM Series.")
@app_commands.describe(series="Series name")
async def lfm_series_schedule_cmd(interaction: discord.Interaction, series: Series):
        schedule = lfm_scheduler.get_series_schedule(series.value)

        embed = discord.Embed(title=f"{series.value} schedule:")
        for entry in schedule:
            embed.add_field(name=f"{entry.get('date')} - {entry.get('track')}",value="", inline=False)

        await interaction.response.send_message(embed=embed)


@app_commands.command(name="lfm-elo-calc", description="Calculate the ELO for an LFM Race.")
@app_commands.describe(race_id="Id of the race")
@app_commands.describe(id="Id of the player, can be left empty if linked to your accont with /add-lfm-id")
@app_commands.describe(short="Show brief elo gain")
async def lfm_elocalc_id_cmd(interaction: discord.Interaction, race_id: int, id: Optional[int] = None, short: Optional[bool] = True):

        if id is None:
            pool = await database.create_pool()
            id = await database.get_lfm_id_from_discord_id(interaction.user.id, pool)
            if pool:
                await pool.close()

        if id is None:
            await interaction.response.send_message("Unkown LFM id, specify id or link your LFM to your discord account with /add-lfm-id ")
            return

        info, small, big = elo_calc.main(race_id, True, short, id)
        line_count = big.count("\n")

        if not short and line_count > 10:
            filename = "temp_file.txt"
            with open(filename, "w", encoding="UTF-8") as file:
                file.write(f"{info}\n{big}")
            await interaction.response.send_message(file=discord.File(filename))
            os.remove(filename)
            
        else:
            await interaction.response.send_message(f"You can add your lfmid via `/add-lfm-id`\n{info}\n{small}")


@app_commands.command(name="current-week", description="Shows current LFM weekly schedule.")
async def current_week_cmd(interaction: discord.Interaction):
    tracks = lfm_scheduler.find_current_week()

    result = discord.Embed(title="LFM current week schedule")
    for track in tracks:
        result.add_field(name=f"{track['series']}", value=track['track'])

    await interaction.response.send_message(embed=result)


@app_commands.command(name="next-week", description="Shows next week LFM weekly schedule.")
async def next_week_cmd(interaction: discord.Interaction):
    tracks = lfm_scheduler.find_next_week()
    result = discord.Embed(title="LFM next week schedule")
    for track in tracks:
        result.add_field(name=f"{track['series']}", value=track['track'])

    await interaction.response.send_message(embed=result)


@app_commands.command(name="offseason", description="Shows offseason LFM weekly schedule.")
async def off_season_cmd(interaction: discord.Interaction):

    await interaction.response.defer()

    series2 = "GT3/GT4"
    series2_tracks = [
    {
        "day": "Monday",
        "track": "Spielberg"
    },
    {
        "day": "Tuesday",
        "track": "Paul Ricard"
    },
    {
        "day": "Wednesday",
        "track": "Nürburgring"
    },
    {
        "day": "Thursday",
        "track": "Silverstone",
    },
    {
        "day": "Friday",
        "track": "Spa"
    }
        ]
    
    series1 = "GT2/M2"
    series1_tracks = [
    {
        "day": "Monday",
        "track": "Kyalami"
    },
    {
        "day": "Tuesday",
        "track": "Ricardo Tormo"
    },
    {
        "day": "Wednesday",
        "track": "Suzuka"
    },
    {
        "day": "Thursday",
        "track": "Monza",
    },
    {
        "day": "Friday",
        "track": "Barcelona"
    }
    ]
    
    series3 = "Legacy Cup"
    series3_tracks = [
    {
        "day": "Monday",
        "track": "Watkins Glen"
    },
    {
        "day": "Tuesday",
        "track": "Zandvoort"
    },
    {
        "day": "Wednesday",
        "track": "Misano"
    },
    {
        "day": "Thursday",
        "track": "Hungaroring",
    },
    {
        "day": "Friday",
        "track": "Batthurst"
    }
    ]

    result = discord.Embed(title=f"LFM offseason {series1} schedule")
    for track in series1_tracks:
        result.add_field(name=f"{track['day']}", value=track['track'])

    await interaction.followup.send(embed=result)


    result = discord.Embed(title=f"LFM offseason {series2} schedule")
    for track in series2_tracks:
        result.add_field(name=f"{track['day']}", value=track['track'])

    await interaction.followup.send(embed=result)


    result = discord.Embed(title=f"LFM offseason {series3} schedule")
    for track in series3_tracks:
        result.add_field(name=f"{track['day']}", value=track['track'])

    await interaction.followup.send(embed=result)
    

@app_commands.command(name="proseries", description="Shows the conditions for the next Pro Series race.")
async def proseries_cmd(interaction: discord.Interaction, offset: Optional[str] = None, all: Optional[bool] = False):

    if all:
        await interaction.response.send_message("https://cdn.discordapp.com/attachments/1015611698348036096/1299117187969056799/jrrPo9kRTkJGJLXHYX1cnJ3oxC5tMlshCix42aB8r7SledU3utsj45xpdG3a9rcqNYdW9.png?ex=671c08aa&is=671ab72a&hm=c26ee4637fd82c5ca8d167e4e229ecc7390830608ac90c1e55ede234b4c8f8d3&")
        return

    if offset is not None and offset.lstrip("+-").isnumeric():
        if offset.startswith("+") or offset.startswith("-"):
            weather = lfmproweather.getweather(None, int(offset))
        else:
            weather = lfmproweather.getweather(int(offset))
    else:
        weather = lfmproweather.getweather(None)

    if weather is None:
        await interaction.response.send_message("No Pro series coming up")
        return

    track = weather['Track']
    degrees = weather['°C']
    hour = weather['Hour']
    eff_degrees = weather['Avg. °C']
    clouds = weather['Clouds']
    rain = weather['Rain']
    dry = weather['Dry']
    wet = weather['Wet']
    mixed = weather['Mixed']
    rain_emoji = emoji.emojize(':cloud_with_rain:')
    sun_emoji = emoji.emojize(':sunny:')

    # Create a combination of sun and rain cloud
    sun_with_rain_emoji = emoji.emojize(':white_sun_rain_cloud:')


    race_summary = f'''
# {track}
* {degrees} °C at {hour}:00, average ~{eff_degrees} °C
* {sun_emoji} {dry} / {rain_emoji} {wet} / {sun_with_rain_emoji} {mixed}
'''
    await interaction.response.defer(thinking=True, ephemeral=False)

####### BOPSMALL COMMAND STARTS HERE

    if 4 <= datetime.today().isoweekday():
        result = bop.bop_next(Utils.to_lfm_track_name(track))
        await interaction.followup.send(f"{race_summary}\n```ansi\n{result}```")
    else:
        result = bop_commands_manager.get_realbop(Utils.to_lfm_track_name(track))
        await interaction.followup.send(f"{race_summary}\n{result}")
    

@app_commands.command(name="lfm-elocalc-team")
@app_commands.describe(race_id="The race_id of the race")
@app_commands.describe(car="Car you want to check, need the number of the car in the entrylist here.")
async def lfmelo_team_cmd(interaction: discord.Interaction, 
                      car: int,
                      race_id: str):
    
    if race_id.startswith('#'):
        race_id = race_id[1:]

    result = elo_calc_team_event.main(race_id, car)

    await interaction.response.send_message(result)


@app_commands.command(name="favourite")
@app_commands.describe(car="Car you want to check")
@app_commands.autocomplete(car=track_autocomplete)
async def favourite_cmd(interaction: discord.Interaction, car: str):
            
    await interaction.response.defer(ephemeral=False, thinking=True)

    pool = await database.create_pool()
    lfm_id = str(await database.get_lfm_id_from_discord_id(interaction.user.id, pool))
    if pool:
        await pool.close()


    logger.info(interaction.user)

    if lfm_id:
        result = favourite_cars.get_data(lfm_id, car, interaction.user.name)
        timestamp, time_data = Utils.get_modified_date_from_file(f'resources/lfm_car_data/{car}.json')
        message = f"**{car}**\n```{result}```\nData from <t:{timestamp}> or {time_data[0]} days, {time_data[1]} hours and {time_data[2]} minutes ago.\nIf you want to get the data updated, drop a message in <#1162374457348849735>"
        logger.info(message)
        await interaction.followup.send(message)      
    else:
        await interaction.followup.send("use add command to add your lfm id.")          


image_tuple = tuple(["Yes", "No"])
SORTING = tuple(["wins", "podiums", "top5", "top10", "entries", "wins_per_race", "podium_per_race", "top5_per_race", "top10_per_race"])
@app_commands.command(name="lfm-carstats")
@app_commands.describe(car_class="The car class, default is GT3.")
@app_commands.describe(track="The track you want the stats for, or 'all'")
@app_commands.describe(sorting="The sorting you want to apply.")
@app_commands.describe(image="If you want it to be images or a table")
async def carstats_cmd(interaction: discord.Interaction, 
                      car_class: Optional[Literal[CAR_CLASSES]],
                      track: Optional[Literal[ACC_TRACKS]],
                      image: Optional[Literal[image_tuple]],
                      sorting: Optional[Literal[SORTING]],
                      season: Optional[Literal[tuple(["Yes", "No"])]]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    if track == None and season == None:
        await interaction.followup.send("One of season or track needs to be used..")

    if image:
        if image == "Yes":
            image = True
        else:
            image = False
    else:
        image = False

    if not sorting:
        sorting = "wins"
    
    if season == "Yes":
        track = "all"

    if car_class == None:
        car_class = "GT3"

    track = Utils.to_lfm_track_name(track) # type: ignore
    logger.info(f"{track} {sorting}")

    data = car_stats.get_stats(track, car_class, sorting) # type: ignore

    if image:
        try:
            file_name = car_stats.get_image(data)
            car_stats.create_pie_charts(data, track) # type: ignore
            file_names = [
                file_name,
                f'{track}_pie_chart_wins.jpg',
                f'{track}_pie_chart_podiums.jpg',
                f'{track}_pie_chart_top5.jpg',
                f'{track}_pie_chart_top10.jpg',
                f'{track}_pie_chart_entries.jpg'
                ]
        except Exception as e:
            await interaction.followup.send("For one reason or another I either have no data for this track or some other error occured.")

        try:
            files = []
            for file_name in file_names:
                with open(file_name, "rb") as file:
                    files.append(discord.File(file, filename=file_name))
            await interaction.followup.send(files=files)
            
            for file_name in file_names:
                    os.remove(file_name)
        except:
            for file_name in file_names:
                # Send the file as an attachment in a Discord message
                with open(file_name, "rb") as file:
                    await interaction.followup.send(file=discord.File(file, filename=file_name))
                os.remove(file_name)

    else:
        if season == "Yes":
            track = "Season"
        else:
            track = bop.find_closest_track(bop.get_all_tracks(), track)

        result = car_stats_string.get_carstats_result(data, track)

        filename = f"{track}.md"
        # Send the file as an attachment in a Discord message
        with open(filename, "rb") as file:
            await interaction.followup.send(content=result, file=discord.File(file, filename=filename))

        os.remove(filename)


@app_commands.command(name="race",
                      description="Get information about an LFM Race.")
@app_commands.describe(race_id="LFM Race ID")
async def race_cmd(interaction: discord.Interaction,
                       race_id: str):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    try:
        race_data, result = lfm_race_data.get_race(race_id)
    except:
        race_data, result = lfm_race_data.get_race_team_event(race_id)
        await interaction.followup.send(f"Fuck you <@244864531308740608>, fix yo shit bro")          
    
    await interaction.followup.send(f"{race_data}\n{result}")


@app_commands.command(name="entrylist",
                      description="Get entrylist (Split 1) for an LFM Race.")
@app_commands.describe(race_id="LFM Race ID")
async def lfm_entrylist_cmd(interaction: discord.Interaction,
                       race_id: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
        
    result = entrylistprinter.get_entrylist(race_id)
    filename = f"entrylist.txt"
    if result:
        with open(filename, 'a', encoding="UTF-8") as file:
            # Append your data
            file.write(result)
        with open(filename, "rb") as file:
            await interaction.followup.send(file=discord.File(file, filename=filename))
    else:
        await interaction.followup.send("Uh... There was an error acquiring the entrylist. I don't even know man. 🤷‍♀️")

    os.remove(filename)


@app_commands.command(name="entrylist-finddriver",
                      description="Find a driver in an entrylist for an LFM Race. (Split 1)")
@app_commands.describe(race_id="LFM Race ID")
@app_commands.describe(search="Driver you want to search for.")
async def lfm_entrylist_finddriver_cmd(interaction: discord.Interaction,
                       race_id: str,
                       search: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    result = entrylistprinter.find_driver(race_id, search)
    if result:
        await interaction.followup.send(f"`{result}`")
    else:
        await interaction.followup.send("No results.")


@app_commands.command(name="lfm_penalties",
                      description="Search for something in the LFM Penalties.")
@app_commands.describe(search="What you want to search for.")
async def lfm_penalties_cmd(interaction: discord.Interaction,
                       search: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
        
    embed_lists = penalties.get_embed(search)
    if len(embed_lists) > 0:
        for embed in embed_lists:
            await interaction.followup.send(embed=embed)
    else:
        await interaction.followup.send(f"Didn't find anything with {search}")


PRO_STATS_SORTING = tuple(['Races', 'Wins', 'Poles', 'Purple', 'Elo', 'SR', 'Inc.',
                            'Inc./Race', 'Podiums', 'Top5s', 'Top10s', 'Winrate', 'Podium %', 'Poles %', 'F. Lap %'])
ORDER = tuple(['ASC', 'DESC'])
@app_commands.command(name="lfm-prostats")
@app_commands.describe(sorting="The sorting you want to apply.")
@app_commands.describe(number_of_results="Number of results you want to show. Default is 10.")
async def prostats_cmd(interaction: discord.Interaction,
                    column1: Optional[Literal[PRO_STATS_SORTING]],
                    column2: Optional[Literal[PRO_STATS_SORTING]],
                    column3: Optional[Literal[PRO_STATS_SORTING]],
                    column4: Optional[Literal[PRO_STATS_SORTING]],
                    column5: Optional[Literal[PRO_STATS_SORTING]],
                    order: Optional[Literal[ORDER]],
                    minimum_races: Optional[int],
                    sorting: Literal[PRO_STATS_SORTING] = "Wins",
                    number_of_results: app_commands.Range[int, 1, 16] = 10):

    columns = [column1, column2, column3, column4, column5]
    columns_to_keep = [col for col in columns if col is not None]
    columns_to_keep.insert(0, "First Name")
    columns_to_keep.insert(1, "Last Name")

    if not minimum_races:
        minimum_races = 0
    
    result = lfmprostats.get_lfmprostats(sorting.title(), number_of_results, columns_to_keep, order, minimum_races) # type: ignore
    await interaction.response.defer(ephemeral=False, thinking=True)

    await interaction.followup.send(f"```\n{result.to_string(index=False)}```\nFor more stats: <https://tinyurl.com/lfmprostats>")


@app_commands.command(name="lfm-pro_carstats")
@app_commands.describe(sorting="The sorting you want to apply.")
@app_commands.describe(number_of_results="Number of results you want to show. Default is 10.")
async def prostats_cars_cmd(interaction: discord.Interaction, 
                      sorting: Literal[PRO_STATS_SORTING] = "Wins",
                      number_of_results: app_commands.Range[int, 1, 16] = 10):
    
    result = lfmprostats.lfm_pro_car_stats(sorting.title(), number_of_results) # type: ignore
    await interaction.response.defer(ephemeral=False, thinking=True)

    await interaction.followup.send(f"```\n{result.to_string(index=False)}```\nFor more stats: <https://tinyurl.com/lfmprostats>")


PRO_STATS_CARS = tuple(pd.read_csv('resources/race_results.csv')['car_name'].unique())
@app_commands.command(name="lfm-pro-driver-cars")
@app_commands.describe(sorting="The sorting you want to apply.")
@app_commands.describe(number_of_results="Number of results you want to show. Default is 10.")
async def prostats_driver_cars_cmd(interaction: discord.Interaction,
                            car_filter: Optional[Literal[PRO_STATS_CARS]],
                            sorting: Literal[PRO_STATS_SORTING] = "Wins",
                            number_of_results: app_commands.Range[int, 1, 16] = 10):
    
    result = lfmprostats.get_lfmprostats_by_car_and_user(sorting.title(), number_of_results, car_filter) # type: ignore
    await interaction.response.defer(ephemeral=False, thinking=True)

    await interaction.followup.send(f"```\n{result.to_string(index=False)}```\nFor more stats: <https://tinyurl.com/lfmprostats>")


@app_commands.command(name="random-relevant-car",
                      description="Get random relevant (bop) car.")
async def random_relevant_car_cmd(interaction: discord.Interaction):
    
    import random

    # Define the list of cars
    cars = ["Audi R8 Evo II", "Aston Martin V8 GT3", "Ferrari 296 GT3", "Bentley Contintental 2018", "Honda NSX GT3 2020", "Lamborghini Huracan GT3 Evo 2", "Mercedes AMG GT3 2020", "BMW M4 GT3", "Porsche 992 GT3", "McLaren 720S GT3 Evo", "Ford Mustang GT3", "Nissan GT-R Nismo GT3"]

    # Choose a random item from the list
    random_car = random.choice(cars)

    await interaction.response.defer(ephemeral=False, thinking=True)
    await interaction.followup.send(f"Bonsoir <@{interaction.user.id}>, your new car is the {random_car}")


@app_commands.command(name="random-irrelevant-car",
                      description="Get random irrelevant (legacy cup) car.")
async def random_irrelevant_car_cmd(interaction: discord.Interaction):
    
    import random

    # Define the list of cars
    cars = ["AMR V12 Vantage GT3", "Audi R8 LMS", "Bentley Continental 2015", "BMW M6 GT3", "Emil Frey Jaguar G3", "Ferrari 488 GT3", "Honda NSX GT3 2017", "Lamorghini Huracan GT3", "McLaren 650S GT3", "Mercedes AMG GT3 2015", "Nissan GT-R Nismo GT3 2015", "Porsche 991 GT3 R", "Reiter Engineering R-EX GT3"]

    # Choose a random item from the list
    random_car = random.choice(cars)

    await interaction.response.defer(ephemeral=False, thinking=True)
    await interaction.followup.send(f"Bonsoir <@{interaction.user.id}>, your new car is the {random_car}")


@app_commands.command(name="personal_lap_record_lfm",
                      description="Get your lap record on LFM")
@app_commands.describe(track="Track you want the lap record from.")
@app_commands.describe(year="Current lap records (2024) or older lap records (2023).")
async def personal_lap_record_lfm_cmd(interaction: discord.Interaction,
                                      year: Optional[Literal[tuple([2023, 2024])]],
                                      track: Literal[ACC_TRACKS]):

    track = Utils.to_lfm_track_name(track)
    await interaction.response.defer(ephemeral=False, thinking=True)

    if year is None:
        year = 2024

    pool = await database.create_pool()

    try:
        lfm_id = await database.get_lfm_id_from_discord_id(interaction.user.id, pool)
    except:
        await interaction.followup.send("Please add lfm id with /add-lfm-id")

    if pool:
        await pool.close()
    rows = laprecords.get_laprecords(lfm_id, track, year) # type: ignore

    if rows is None:
        await interaction.followup.send("Didn't find no records.")
    else:
        result = ""
        for row in rows:
            result += f"{row}\n"

        await interaction.followup.send(result)


@app_commands.command(name="add-lfm-id")
async def add_lfm_id_cmd(interaction: discord.Interaction, lfm_id: int):
    pool = await database.create_pool()
    try:
        await database.update_lfm_id(interaction.user.id, int(lfm_id), pool)
        await interaction.response.send_message(f"Added {lfm_id} for <@{interaction.user.id}>.", ephemeral=True)
    except:
        await interaction.response.send_message("Something is fucked. Blame <@244864531308740608>")

    if pool:
        await pool.close()


@app_commands.command(name="delete-lfm-id")
async def delete_lfm_id_cmd(interaction: discord.Interaction):
    pool = await database.create_pool()
    await database.delete_lfmid(interaction.user.id, pool)
    await interaction.response.send_message(f"Deleted <@{interaction.user.id}>.", ephemeral=True)
    if pool:
        await pool.close()


@app_commands.command(name="lfm-userstats",
                      description="Get your lap record on LFM")
async def lfm_userstats_cmd(interaction: discord.Interaction):
    
    await interaction.response.defer(ephemeral=False, thinking=True)

    pool = await database.create_pool()
    try:
        lfm_id = await database.get_lfm_id_from_discord_id(interaction.user.id, pool)
    except:
        await interaction.followup.send("Please add lfm id with /add-lfm-id")    
    if pool:
        await pool.close()
    rows = userstats.get_stats(lfm_id)
    if rows:
        await interaction.followup.send(rows)
    else:
        await interaction.followup.send("Sorry, didn't find anything")


@app_commands.command(name="elocalc-position",
                      description="Calculate elo gain/loss for an LFM race for one position only.")
@app_commands.describe(name="Driver you want to check.")
@app_commands.describe(race_id="The Race ID of the LFM Race.")
@app_commands.describe(position="The finishing position")
async def elo_calc_position_cmd(interaction: discord.Interaction,
                                  name: str,
                                  race_id: int,
                                  position: int,
                                  car_class: Optional[Literal[CAR_CLASSES]]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    if car_class:
        await interaction.followup.send(elo_calc.calc_for_position(race_id, name, position, car_class))
    else:
        await interaction.followup.send(elo_calc.calc_for_position(race_id, name, position))


@app_commands.command(name="lfm_all_data",
                      description="Get *all* **statistical** data from LFM.")
@app_commands.describe(everything="If you want all the data or just top/bottom 5.")
async def lfm_all_data_cmd(interaction: discord.Interaction,
                            everything: Optional[Literal[tuple(["True", "False"])]]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)

    if not everything:
        everything = False
    
    if everything == "True":
        everything = True
    
    if everything == "False":
        everything = False
            
    pool = await database.create_pool()
    lfm_id = await database.get_lfm_id_from_discord_id(interaction.user.id, pool)
    if pool:
        await pool.close()
    if lfm_id:
        data = await lfmdata.give_data(lfm_id, everything)
        if data:
            filename = f"{interaction.user.name}.json"
            with open(filename, 'a', encoding="UTF-8") as file:
                # Append your data
                file.write(data)

            # Send the file as an attachment in a Discord message
            with open(filename, "rb") as file:
                await interaction.followup.send(file=discord.File(file, filename=filename))

            # Delete the local file
            os.remove(filename)

    else:
        await interaction.followup.send("Add your lfm id with `/add-lfm-id`")       


@app_commands.command(name="lfm-search",
                      description="Search for a driver on LFM.")
@app_commands.describe(search="Search for a driver")
async def lfm_search_cmd(interaction: discord.Interaction,
                            search: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)

    drivers_data = lfmsearch.get_user_info(search)
    drivers_data = drivers_data[:10]

    embed = discord.Embed(title="Search Results", description="", color=discord.Color.blue())

    # Add a table to the embed
    Name = "Name".ljust(30)
    Elo = "Elo".ljust(5)
    table = f"{Name} {Elo} | SR\n"
    for driver in drivers_data:
        table += f"{driver['Name']} {str(driver['Rating']).ljust(5)} | {driver['Safety Rating']}\n"

    embed.add_field(name="", value=f"```\n{table}\n```", inline=False)
   
    await interaction.followup.send(embed=embed)


@app_commands.command(name="lfm-penaltypoints",
                      description="Get the LFM Penalty points table.")
@app_commands.describe(search="Search for a driver")
@app_commands.describe(help="Put some value here to get the penalty points help text, see what time penalty results in how many penaltypoints.")
@app_commands.describe(limit="limit the result list.")
async def lfm_penalty_points_cmd(interaction: discord.Interaction,
                            search: Optional[str],
                            help: Optional[str],
                            limit: Optional[int]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)

    if not search and not help and not limit:
        await interaction.followup.send(f"```{lfm_penaltypoints.get_list()}```")

    if help:
        await interaction.followup.send("5s → 1 Pts || 10s → 1.5 Pts || 15s → 2 Pts || DT → 2.5 Pts || StopGo → 3 Pts")

    if search:
        await interaction.followup.send(lfm_penaltypoints.get_name(search.lower())) # type: ignore

    if limit:
        await interaction.followup.send(f"```{lfm_penaltypoints.get_list(limit)}```")


@app_commands.command(name="elo-distribution",
                      description="Get the elo distribution (awarded elo) for a race.")
@app_commands.describe(car_class="The car class you want to check.")
@app_commands.describe(race_id="The Race ID of the LFM Race.")
async def elo_distribution_cmd(interaction: discord.Interaction,
                            car_class: Optional[Literal[CAR_CLASSES]],
                            race_id: int):
    
    await interaction.response.defer(ephemeral=False, thinking=True)

    if car_class:
        elo_calc.get_image(race_id, car_class)
    else:
        elo_calc.get_image(race_id)
    filename = "elo_distribution.png"
    await interaction.followup.send(file=discord.File(filename))
    os.remove(filename)


type_of_race = tuple(["team", "solo"])
@app_commands.command(name="lfm-race-graphs",
                      description="Make graphs for top 10 of a split 1 race or for a specific split 1 team in a team race.")
@app_commands.describe(race_id="LFM Race ID (Can have # at start)")
@app_commands.describe(type="If it's team race or a 'normal' race")
@app_commands.describe(finishing_position="If it's team race, put your finishing position here.")
@app_commands.describe(delta="How much slower than average is a lap allowed to be, before the lap is thrown out?")
@app_commands.describe(car_class="The car class, default is GT3.")
@app_commands.choices(type=[app_commands.Choice(name=choice, value=choice) for choice in type_of_race])
async def lfm_race_graphs_cmd(interaction: discord.Interaction,
                       race_id: str,
                       type: str,
                       car_class: Optional[Literal[CAR_CLASSES]],
                       finishing_position: Optional[int],
                       delta: Optional[int]):

    await interaction.response.defer(ephemeral=False, thinking=True)

    logger.info(type)
    logger.info(race_id)
    logger.info(finishing_position)

    if not car_class:
        car_class = "GT3"

    if type == "team":
        if finishing_position == None:
            await interaction.followup.send("We do kinda need a finishing position.")
            return

        if delta:
            LFM_laptimes_graphs.create_image_team(race_id, finishing_position, car_class, delta) # type: ignore
        else:
            LFM_laptimes_graphs.create_image_team(race_id, finishing_position, car_class) # type: ignore


        # Delete data file
        if os.path.exists("laps_data_modified.csv"):
            os.remove("laps_data_modified.csv")

        # Delete data file
        if os.path.exists("laps_data.csv"):
            os.remove("laps_data.csv")

        # Assuming you have three files named file1.csv, file2.csv, and file3.csv in the same directory as your bot script
        files = ["linegraph.png", "boxplot.png", "violin.png"]

        # Creating a list to store all files to be sent
        files_to_send = []
        for file in files:
            with open(file, 'rb') as f:
                files_to_send.append(discord.File(f))
        
        # Sending all files in one message
        await interaction.followup.send(files=files_to_send)

        for file in files:
            
            # Delete image file
            if os.path.exists(file):
                os.remove(file)

    if type == "solo":
        if delta:
            LFM_laptimes_graphs.create_image_top10(race_id, delta)
        else:
            LFM_laptimes_graphs.create_image_top10(race_id)

        # Delete data file
        if os.path.exists("laps_data.csv"):
            os.remove("laps_data.csv")

        file_path = "lap_times_plot.png"

        # Opening the image file and sending it
        with open(file_path, 'rb') as f:
            await interaction.followup.send(file=discord.File(f, filename=file_path))

        
        # Delete image file
        if os.path.exists(file_path):
            os.remove(file_path)


type_of_graph = tuple(["elo", "safety_rating"])
@app_commands.command(name="lfm-user-graphs",
                      description="Get graph of a user's safety rating or elo progression.")
@app_commands.describe(lfm_id="LFM ID if you haven't added it via the **add** command.")
@app_commands.describe(lfm_id="Elo graph or safety rating graph")
@app_commands.choices(type=[app_commands.Choice(name=choice, value=choice) for choice in type_of_graph])
async def lfm_user_graphs_cmd(interaction: discord.Interaction,
                       lfm_id: Optional[int],
                       type: str):

    await interaction.response.defer(ephemeral=False, thinking=True)

    if lfm_id is None:
        pool = await database.create_pool()
        
        try:
            lfm_id = await database.get_lfm_id_from_discord_id(interaction.user.id, pool)
        except:
            await interaction.followup.send("Please add lfm id with /add-lfm-id")   

        if pool:
            await pool.close()

    if lfm_id is None:
        await interaction.response.send_message("Unkown LFM id, specify id or link your LFM to your discord account with /add-lfm-id ")
        return
    
    if type == "elo":
        file_name = user_graphs.get_elo_graph(lfm_id)
        await interaction.followup.send(file=discord.File(file_name))
        os.remove(file_name)
    elif type == "safety_rating":
        file_name = user_graphs.get_safetyrating_graph(lfm_id)
        await interaction.followup.send(file=discord.File(file_name))
        os.remove(file_name)
    else:
        await interaction.followup.send("You put in neither elo nor safety_rating")

PRO_STATS_SORTING_FILTER = tuple(['First Name', 'Last Name', 'user_id', 'Races', 'Wins', 'Poles', 'Purple', 'Elo', 'SR', 'Inc.',
                            'Inc./Race', 'Podiums', 'Top5s', 'Top10s', 'Winrate', 'Podium %', 'Poles %', 'F. Lap %'])
CONDITIONS = tuple(["equals", "greater_than", "less_than", "boolean"])
@app_commands.command(name="lfm-prostats-filter")
async def prostatsfilter_cmd(interaction: discord.Interaction,
                       filter1_field: Literal[PRO_STATS_SORTING_FILTER],
                       filter1_condition: Literal[CONDITIONS],
                       filter1_value: str,
                       filter2_field: Optional[Literal[PRO_STATS_SORTING_FILTER]],
                       filter2_condition: Optional[Literal[CONDITIONS]],
                       filter2_value: Optional[str],
                       filter3_field: Optional[Literal[PRO_STATS_SORTING_FILTER]],
                       filter3_condition: Optional[Literal[CONDITIONS]],
                       filter3_value: Optional[str]):
    

    await interaction.response.defer(ephemeral=False, thinking=True)

    if (
        (filter1_condition is None and (filter1_value is not None or filter1_field is not None)) or
        (filter1_value is None and (filter1_condition is not None or filter1_field is not None)) or
        (filter1_field is None and (filter1_condition is not None or filter1_value is not None))
    ):
        await interaction.followup.send("Use all of the filter1 parameters. Thanks.")
        return

    if (
        (filter2_condition is None and (filter2_value is not None or filter2_field is not None)) or
        (filter2_value is None and (filter2_condition is not None or filter2_field is not None)) or
        (filter2_field is None and (filter2_condition is not None or filter2_value is not None))
    ):
        await interaction.followup.send("Use all of the filter2 parameters. Thanks.")
        return

    if (
        (filter3_condition is None and (filter3_value is not None or filter3_field is not None)) or
        (filter3_value is None and (filter3_condition is not None or filter3_field is not None)) or
        (filter3_field is None and (filter3_condition is not None or filter3_value is not None))
    ):
        await interaction.followup.send("Use all of the filter3 parameters. Thanks.")
        return

    filters = {
        filter1_field: (filter1_condition, filter1_value),  
        filter2_field: (filter2_condition, filter2_value),  
        filter3_field: (filter3_condition, filter3_value), 
    }

    filename = lfm_prostats_sorting.filter_csv(filters)
    filename = lfm_prostats_sorting.align_csvfile()

    await interaction.followup.send(file=discord.File(filename))
    os.remove(filename)


@app_commands.command(name="add_race_reminder",
                      description="Get a reminder in current channel for an LFM Race at 12h, 6h, 3h, 1h, 30min, 15min until race start.")
async def add_race_reminder_cmd(interaction: discord.Interaction,
                                  race_id: int,
                                  description: str):
    
    await interaction.response.defer(ephemeral=True)
    
    member = interaction.user
    channel = interaction.channel
    if channel:
        permissions = channel.permissions_for(member)

    if permissions.manage_messages:
        try:
            await race_reminder.add_race_to_database(race_id, description, interaction.channel_id)
            await interaction.followup.send(f"Added race reminder for race {race_id} with description `{description}`")
        except Exception as e:
            logging.info(e)
    else:
        await interaction.followup.send("You need the Manage Messages permissions to add a race reminder.", ephemeral=True)


@app_commands.command(name="lfm-car-counts")
@app_commands.describe(race_id="The race_id of the race")
async def lfmcar_counts_cmd(interaction: discord.Interaction, 
                      race_id: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    if race_id.startswith('#'):
        race_id = race_id[1:]

    result = car_counts.get_cars(race_id)

    await interaction.followup.send(result)