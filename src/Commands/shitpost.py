import discord
import logging
import os
import random
import json
import database
from discord import Embed

from .command import Command
from discord import app_commands

logger = logging.getLogger(__name__)


@app_commands.command(name="send-copypasta",
                      description="send a copy pasta from the database.")
@app_commands.describe(title="Give your copy pasta a title.")
@app_commands.describe(copypasta="Paste your pasta here.")
async def sendcopypasta_cmd(interaction: discord.Interaction,
                       title: str,
                       copypasta: str):
    
    await interaction.response.defer(thinking=True, ephemeral=False)
    
    pool = await database.create_pool()
        
    try:    
        # Replace YOUR_CHANNEL_ID with the actual ID of the channel you want to send the message to
        channel_id = 1162385010200948787
        channel = self.client.get_channel(channel_id) # type: ignore

        thread = await channel.create_thread(name=title, auto_archive_duration=1440, content=f"{copypasta} from {interaction.user.name}")

        logger.info(thread)
        await interaction.followup.send(f"Thank you for your contribution. Your message has been received. [<https://discord.com/channels/1015601496743751740/{thread.message.id}>]")
    except:
        logger.info("Something went wrong with creating the thread.")

    if pool:
        await pool.close()


@app_commands.command(name="get-copypasta",
                      description="get a copy pasta from the database.")
@app_commands.describe(query="Your search query")
async def getcopypasta_cmd(interaction: discord.Interaction,
                       query: str):
    
    await interaction.response.defer(thinking=True, ephemeral=False)
        
    sql_query = f"SELECT * FROM copypasta WHERE Name ILIKE '%{query}%' OR Text ILIKE '%{query}%';"  
    if query == "all of the pastas":
        result = await database.get_reports(await database.create_pool(), "SELECT * from copypasta;")
    else:
        result = await database.get_reports(await database.create_pool(), sql_query)
        # Specify the filename where you want to write the data
    output_file = "copypastas.txt"

    # Open the file in write mode and write the rows to it
    with open(output_file, "w") as file:
        for row in result:
            file.write(row['name'] + "\n")
            file.write(row['text'] + "\n\n\n")

    with open(output_file, "rb") as file:
        await interaction.followup.send(file=discord.File(file))
        # Delete the file after uploading
    os.remove(output_file)


@app_commands.command(name="define",
                      description="get a definition")
@app_commands.describe(query="Your search query")
async def define_cmd(interaction: discord.Interaction,
                       query: str):
    
    await interaction.response.defer(thinking=True, ephemeral=False)

    pool = await database.create_pool()
    row = await database.get_reports(pool, f"SELECT * FROM definition WHERE title ILIKE '%{query}%' LIMIT 1;")
    if pool:
        await pool.close()
    logger.info(len(row))
    try:
        title = row['title']
        subtitle = row['subtitle']
        description = row['description']
    except:
        row = row[0]
        
        title = row['title']
        subtitle = row['subtitle']
        description = row['description']

    # Create an embed
    embed = Embed(title=title, color=discord.Color.blue())

    # Add a field for the subtitle
    embed.add_field(name=f"*{subtitle}*", value=description, inline=True)

    # Send the embed to the same channel where the command was invoked
    await interaction.followup.send(embed=embed)


@app_commands.command(name="apology",
                      description="get a definition")
@app_commands.describe(name="Name of the guy you want to apologise to.")
async def apology_cmd(interaction: discord.Interaction,
                       name: str):
    
    await interaction.response.defer(thinking=True, ephemeral=False)
    with open('resources/apologies.json') as f:
        data = json.load(f)
    sentence = random.choice(data)
    sentence = sentence.replace("$NAME", name)
    await interaction.followup.send(f"```{sentence}```")
