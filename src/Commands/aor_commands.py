from typing import Literal, Optional
import discord
import logging

from .command import Command
from discord import app_commands
import aor_prediction
import prediction_helper
import aoruser
import lfmhl
import Utils


logger = logging.getLogger(__name__)


@app_commands.command(name="aor_prediction_results",
                      description="Shows AOR Prediction Results from past seasons")
@app_commands.describe(season="The season you want to see the standings for.")
async def aor_prediction_results_cmd(interaction: discord.Interaction,
                       season: Literal[tuple(["9", "10", "12", "13", "podiums"])]):
    
    if season == "podiums":
        with open("resources/aor_prediction_results/AOR_Prediction_Awards.txt", 'r') as file:
            content = file.read()
        await interaction.response.send_message(f"```\n{content}\n```")
    else:
        await interaction.response.send_message(f"```\n{aor_prediction.printerino(season)}\n```")