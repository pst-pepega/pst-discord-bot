
from typing import Literal, Optional
import discord
from discord import app_commands
import logging

from constants import ACC_TRACKS, Series
from .command import Command
import driver_standings
import standings
from constants import Series_to_ID

logger = logging.getLogger(__name__)

LFM_SERIES = tuple(member.name for member in Series_to_ID.__members__.values())

@app_commands.command(name="lfm-teamstandings",
                      description="Get team standings for a series on LFM")
@app_commands.describe(series="The series.")
async def teamstandings_cmd(interaction: discord.Interaction,
                                      series: Series):
    await interaction.response.defer(thinking=True, ephemeral=False)

    series_name = Series[series].name
    logging.info(f"Series Input {series}")

    series_id = Series_to_ID.get_id(series_name)
    logging.info(f"Series ID: {series_id}")

    result = standings.get_team_standings(series_id)
    message = "Your team is used from your added lfm-id, use /add-lfm-id to add your ID if you want to use **your team**. If team not found, we will print the top 16."

    message = f"{message}\n{result}"

    await interaction.followup.send(message)


@app_commands.command(name="lfm-standings",
                      description="Get team standings for a series on LFM")
@app_commands.describe(series="The series.")
async def personalstandings_cmd(interaction: discord.Interaction,
                                      series: Series,
                                      rows: Optional[int]):
    await interaction.response.defer(thinking=True, ephemeral=False)

    logging.info(series.name)
    series_name = series.name

    #series_name = Series[series].name
    #logging.info(f"Series Input {series}")

    series_id = Series_to_ID.get_id(series_name)
    logging.info(f"Series ID: {series_id}")

    if rows:
        result = driver_standings.get_standings(series_id, rows)
    else:
        result = driver_standings.get_standings(series_id)

    embed = discord.Embed(title=f"{series_name.title()} Standings", description="", color=discord.Color.blue())
    embed.add_field(name="", value=f"```\n{result}\n```", inline=False)

    await interaction.followup.send(f"{series_name.title()} Standings\n```{result}```")