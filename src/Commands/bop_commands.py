from datetime import datetime
import re
import discord
from discord import app_commands
import logging
import os
import requests
import pandas
import time
from typing import Literal, Optional

from .command import Command
import bop
import BOP_score
import database
import lfmhl
import gt4_bop
import slow_laps
import Utils
from Utils import Ansi
import bop_commands_manager

logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"

CAR_CLASSES = tuple(["GT3", "GT4"])
TRACKS = tuple(bop_commands_manager.get_filtered_tracks()) # type: ignore
BOP_REQUEST_CARS = tuple(["McLaren 720S GT3 Evo", "AMR V8 Vantage", "BMW M4 GT3", "Audi R8 LMS GT3 evo II", "Mercedes-AMG GT3", "Porsche 992 GT3 R", "Lamborghini Huracan GT3 EVO 2", "Ferrari 296 GT3", "Bentley Continental", "Honda NSX GT3 Evo", "Nissan GT-R Nismo GT3", "Ford Mustang GT3", "Alpine A110 GT4", "Porsche 718 Cayman GT4 Clubsport", "BMW M4 GT4", "Mercedes AMG GT4", "McLaren 570S GT4", "Maserati MC GT4", "Chevrolet Camaro GT4", "Audi R8 LMS GT4", "Aston Martin Vantage GT4", "Ginetta G55 GT4", "KTM X-Bow GT4"])


async def track_autocomplete(interaction: discord.Interaction, current: str) -> list[app_commands.Choice[str]]:
    # Filter the TRACKS list based on the user's input
    filtered_tracks = [track for track in TRACKS if current.lower() in track.lower()]
    return [app_commands.Choice(name=track, value=track) for track in filtered_tracks[:25]]


@app_commands.command(name="bigbop", description="Get BOP (in big format).")
@app_commands.describe(track="Track you want to get the BOP for.")
@app_commands.autocomplete(track=track_autocomplete)
async def bigbop_cmd(interaction: discord.Interaction, track: str):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    if track == "all":
        result = bop_commands_manager.all_bop()
        await interaction.followup.send(content=result)
    
    elif track == "all all":
        result = bop_commands_manager.all_all_bop()
        filename = f"all.csv"
        with open(filename, 'a') as file:
            # Append your data
            file.write(result)

        # Send the file as an attachment in a Discord message
        with open(filename, "rb") as file:
            await interaction.followup.send(file=discord.File(file))

        # Delete the local file
        os.remove(filename)
    
    else:
        message1, message2 = bop_commands_manager.get_bop(track)

        filename = f"{track}.csv"
        with open(filename, 'a') as file:
            # Append your data
            file.write(message1)

        # Send the file as an attachment in a Discord message
        with open(filename, "rb") as file:
            await interaction.followup.send(content=message2, file=discord.File(file))

        # Delete the local file
        os.remove(filename)


@app_commands.command(name="bopfile",
                      description="Get BOP JSON.")
@app_commands.describe(track="Track you want to get the JSON for.")
@app_commands.autocomplete(track=track_autocomplete)
async def bopfile_cmd(interaction: discord.Interaction,
                       car_class: Optional[Literal[CAR_CLASSES]],
                       track: str):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
        
    track = Utils.to_lfm_track_name(track)

    if track == "all":
        filename = bop.get_json_all_tracks(bop.get_all_tracks())
    else:
        if car_class:
            filename = bop.get_json(track, car_class)
        else:
            filename = bop.get_json(track)

    logger.info(filename)
    # Check if a file was generated
    if filename:
        with open(filename, "rb") as file:
            await interaction.followup.send(content="Here is your JSON, good sir.", file=discord.File(file))
        # Delete the file after uploading
        if filename:
            os.remove(filename)


@app_commands.command(name="smallbop",
                      description="Get BOP (in small format).")
@app_commands.describe(track="Track you want to get the BOP for.")
@app_commands.autocomplete(track=track_autocomplete)
async def bop_small_cmd(interaction: discord.Interaction,
                       track: str):
            
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    logger.info(track)
    message = bop_commands_manager.get_bop_small(track)
    await interaction.followup.send(content=message)


BOPADJUST_CARS = tuple(["McLaren 720S GT3 Evo", "AMR V8 Vantage", "BMW M4 GT3", "Audi R8 LMS GT3 evo II", "Mercedes-AMG GT3", "Porsche 992 GT3 R", "Lamborghini Huracan GT3 EVO 2", "Ferrari 296 GT3", "Bentley Continental", "Honda NSX GT3 Evo", "Nissan GT-R Nismo GT3", "Ford Mustang GT3"])   
@app_commands.command(name="bopadjust",
                      description="Adjust the laptime for a car in the BOP, and see the consequences.")
@app_commands.describe(track="Track you want to get the BOP changed for.")
@app_commands.describe(car="Car you want to get the BOP changed for.")
@app_commands.describe(laptime="Laptime you want to get the BOP changed to.")
@app_commands.autocomplete(track=track_autocomplete)
async def bop_adjust_cmd(interaction: discord.Interaction,
                       track: str,
                       car: Literal[BOPADJUST_CARS], # type: ignore
                       laptime: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)

    result = bop.get_updated_bop(car, laptime, Utils.to_lfm_track_name(track))
    await interaction.followup.send(content=result)


@app_commands.command(name="realbop",
                      description="Get current active BOP")
@app_commands.describe(track="Track you want to get the BOP for.")
@app_commands.autocomplete(track=track_autocomplete)
async def real_bop_cmd(interaction: discord.Interaction,
                        car_class: Optional[Literal[CAR_CLASSES]],
                        track: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    if car_class:
        await interaction.followup.send(content=bop_commands_manager.get_realbop(track, car_class)) # type: ignore
    else:
        await interaction.followup.send(content=bop_commands_manager.get_realbop(track)) # type: ignore


@app_commands.command(name="slowlaps",
                      description="Get Track + Laptimes of relevant cars that are 1 percent slower than fastest.")
@app_commands.describe(car="Car you want to check for.")
async def slow_laps_cmd(interaction: discord.Interaction,
                       car: Literal[BOPADJUST_CARS]): # type: ignore
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    await interaction.followup.send(content=slow_laps.get_records(car)) # type: ignore


@app_commands.command(name="bopcar",
                      description="Get BOP and laptime for every track for a specific car")
@app_commands.describe(car="Car you want to check for.")
async def bopcar_cmd(interaction: discord.Interaction,
                       car: Literal[BOP_REQUEST_CARS]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    await interaction.followup.send("This may take a while...")
    data = bop_commands_manager.get_bopcar(car)
    try:
        await interaction.followup.send(data) # type: ignor
    except:
        # Open the file in write mode
        with open(f"{car}.csv", mode='w', newline='') as file:
            file.write(data)
        with open(f"{car}.csv", "rb") as file:
            await interaction.followup.send(file=discord.File(file))


@app_commands.command(name="bopnext",
                      description="Get next week's BOP")
@app_commands.describe(track="Track you want to get the BOP for.")
@app_commands.autocomplete(track=track_autocomplete)
async def bop_next_cmd(interaction: discord.Interaction,
                        car_class: Optional[Literal[CAR_CLASSES]],
                        track: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)  
    try:
        if car_class:
            result = bop.bop_next(Utils.to_lfm_track_name(track), car_class)
        else:
            result = bop.bop_next(Utils.to_lfm_track_name(track))
        await interaction.followup.send(f"```ansi\n{result}```")
    except:
        await interaction.followup.send("Boris says you cannot have the bop right now.")


@app_commands.command(name="target-time",
                      description="Get the target time for a given track")
@app_commands.describe(track="Track you want to get the target time for.")
@app_commands.autocomplete(track=track_autocomplete)
async def target_time_cmd(interaction: discord.Interaction,
                       track: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)  
    await interaction.followup.send(f"Target time for input {track}: {bop.get_targettime(Utils.to_lfm_track_name(track))}") # type: ignore
       

@app_commands.command(name="enabledtracks",
                      description="Get the tracks where the BOP is enabled.")
async def enabled_tracks_cmd(interaction: discord.Interaction):
    await interaction.response.defer(ephemeral=False, thinking=True)  
    await interaction.followup.send(bop.get_enabled_tracks())       


@app_commands.command(name="bopscore",
                      description="Get the BOP Score for one or all tracks.")
@app_commands.describe(track="Track you want to get the BOP Score for.")
@app_commands.autocomplete(track=track_autocomplete)
async def bop_score_cmd(interaction: discord.Interaction,
                       track: str):
    await interaction.response.defer(ephemeral=False, thinking=True)
    if track == "all":
        result = BOP_score.get_all()
        await interaction.followup.send(f"```{result}```")
    else:
        track = Utils.to_lfm_track_name(track)
        result = BOP_score.get_score(track, bop.get_bop(track))
        await interaction.followup.send(f"BOP-Score for {track}: {result}")


@app_commands.command(name="bop-season",
                      description="Get current (theoretical) BOP averaged out over a season.")
@app_commands.describe(track1="Track you want to get the BOP for.")
@app_commands.describe(track2="Track you want to get the BOP for.")
@app_commands.describe(track3="Track you want to get the BOP for.")
@app_commands.describe(track4="Track you want to get the BOP for.")
@app_commands.describe(track5="Track you want to get the BOP for.")
@app_commands.describe(track6="Track you want to get the BOP for.")
@app_commands.describe(track7="Track you want to get the BOP for.")
@app_commands.describe(track8="Track you want to get the BOP for.")
@app_commands.describe(track9="Track you want to get the BOP for.")
@app_commands.describe(track10="Track you want to get the BOP for.")
@app_commands.describe(track11="Track you want to get the BOP for.")
@app_commands.describe(track12="Track you want to get the BOP for.")
@app_commands.autocomplete(track1=track_autocomplete)
@app_commands.autocomplete(track2=track_autocomplete)
@app_commands.autocomplete(track3=track_autocomplete)
@app_commands.autocomplete(track4=track_autocomplete)
@app_commands.autocomplete(track5=track_autocomplete)
@app_commands.autocomplete(track6=track_autocomplete)
@app_commands.autocomplete(track7=track_autocomplete)
@app_commands.autocomplete(track8=track_autocomplete)
@app_commands.autocomplete(track9=track_autocomplete)
@app_commands.autocomplete(track10=track_autocomplete)
@app_commands.autocomplete(track11=track_autocomplete)
@app_commands.autocomplete(track12=track_autocomplete)
async def bop_season_cmd(interaction: discord.Interaction,
                       track1: Optional[str] = None,
                       track2: Optional[str] = None,
                       track3: Optional[str] = None,
                       track4: Optional[str] = None,
                       track5: Optional[str] = None,
                       track6: Optional[str] = None,
                       track7: Optional[str] = None,
                       track8: Optional[str] = None,
                       track9: Optional[str] = None,
                       track10: Optional[str] = None,
                       track11: Optional[str] = None,
                       track12: Optional[str] = None):
    await interaction.response.defer(ephemeral=False, thinking=True)

    track_list = []

    for i in range(1, 13):  # range(1, 13) to iterate from track1 to track12
        track = f"track{i}"  # Construct the variable name using a f-string
        if locals()[track] is not None:  # Check if the variable is not None
            track_list.append(locals()[track])  

    all_df, track_list = bop.get_bop_for_season(track_list)
    relevant_tracks_df = all_df[all_df['rel'] == 'X']

    filename = f"bop.csv"
    with open(filename, 'a') as file:
        # Append your data
        file.write(all_df.to_string(index=True))

    with open(filename, "rb") as file:
        await interaction.followup.send(content=f"```\n{relevant_tracks_df.to_string()}\n```", file=discord.File(file))
    
    os.remove(filename)


@app_commands.command(name="bop",
                      description="Get current (theoretical) BOP")
@app_commands.describe(track="Track you want to get the BOP for.")
@app_commands.autocomplete(track=track_autocomplete)
async def bop_cmd(interaction: discord.Interaction,
                       track: str,
                       car_class: Optional[Literal[CAR_CLASSES]]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)

    message = ""
    
    if not car_class:
        car_class = "GT3"

    if car_class == "GT3":

        discord_id = float(interaction.user.id)
        pool = await database.create_pool()
        value = await database.get_bop_setting_value(discord_id, pool)

        if pool:
            await pool.close()

        if value == None:
            message += "You can automatically get the bopsmall message if you `/setbop small` or the big (normal) bop message if you don't do anything or `/setbop big`👍\n"
            message += "Did you know you can add your LFM ID to the bot with `/add-lfm-id lfm_id:` and add lfm race results to every channel with `/register-race-result`👍"
            value = "big"
            
        if value == "big":
            if track == "all" or track == "all all":
                await interaction.followup.send(content="use /bopbig for all or all all :)")
            else:
                message1, message2 = bop_commands_manager.get_bop(track)

                filename = f"{track}.csv"
                with open(filename, 'a') as file:
                    # Append your data
                    file.write(message1)

                # Send the file as an attachment in a Discord message
                with open(filename, "rb") as file:
                    if len(message2) < 1500:
                        await interaction.followup.send(content=f"{message}\n{message2}", file=discord.File(file))
                    else:
                        await interaction.followup.send(content=f"{message2}", file=discord.File(file))

                # Delete the local file
                os.remove(filename)

        if value == "small":

            logger.info(track)
            message = bop_commands_manager.get_bop_small(track)
            await interaction.followup.send(content=message)

    elif car_class == "GT4":
        
        if track == "all":
            #result = bop_commands_manager.all_bop()
            #await interaction.followup.send(content=result)
            await interaction.followup.send("No.")

        elif track == "all all":
            # result = bop_commands_manager.all_all_bop()
            # filename = f"all.csv"
            # with open(filename, 'a') as file:
            #     # Append your data
            #     file.write(result)

            # # Send the file as an attachment in a Discord message
            # with open(filename, "rb") as file:
            #     await interaction.followup.send(file=discord.File(file))

            # # Delete the local file
            # os.remove(filename)
            await interaction.followup.send("No.")
        
        else:
            message1, message2 = bop_commands_manager.get_gt4bop(track)

            await interaction.followup.send(message2)

    
    else:
        await interaction.followup.send("Eh. Something went wrong..")

@app_commands.command(name="bop_request_insert",
                      description="Insert a request for bopping.")
@app_commands.describe(track="Track you want to request the BOP for.")
@app_commands.describe(car="Car you want to request the BOP for.")
@app_commands.describe(laptime="Syntax: x:xx.xxx")
@app_commands.autocomplete(track=track_autocomplete)
async def bop_request_insert_cmd(interaction: discord.Interaction,
                       track: str,
                       car: Literal[BOP_REQUEST_CARS],
                       laptime: str):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    user_id = interaction.user.id

    
    if not bool(re.match(r"^\d:\d{2}\.\d{3}$", laptime)):
        await interaction.followup.send("Laptime invalid.")
        return

    pool = await database.create_pool()
    if await database.insert_bop_request(user_id, track, car, laptime, pool):
        await interaction.followup.send(f"Congratulations you have successfully requested the {car} to be adjusted on {track} to {laptime}. ~~We Will get back to you shortly with the payment request.~~")
    else:
        await interaction.followup.send("Combination has already been requested.")
        await interaction.followup.send("https://tenor.com/view/%D0%BF%D0%BE%D1%82%D1%83%D1%88%D0%B8%D0%BB%D0%B8-%D0%BA%D1%80%D0%B8%D0%BC%D0%B8%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%B5%D1%87%D1%82%D0%B8%D0%B2%D0%BE-%D1%87%D0%B8%D0%BB%D0%BB%D1%81%D0%BA%D0%B2%D0%B0%D0%B4-pulp-fiction-chillbro-gif-23566190")
    if pool:
        await pool.close()


@app_commands.command(name="bop_request_select",
                      description="Check the bop requests.")
@app_commands.describe(track="Track you want to check the BOP requests for.")
@app_commands.describe(car="Car you want to check the BOP requests for.")
@app_commands.autocomplete(track=track_autocomplete)
async def bop_request_select_cmd(interaction: discord.Interaction,
                       track: Optional[str],
                       car: Optional[Literal[BOP_REQUEST_CARS]]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    result = ""
    user_id = interaction.user.id
    pool = await database.create_pool()
    rows = await database.get_bop_requests(pool, None, car, track)

    headers = ["id", "track", "car", "laptime", "user_id/user_name"]
    column_widths = {
        "id": 5,
        "track": 40,
        "car": 30,
        "laptime": 10,
        "user_id/user_name": 25
    }
    
    # Create the header row
    header_row = " ".join(f"{header:<{column_widths[header]}}" for header in headers)
    result += header_row + "\n"
    result += "-" * len(header_row) + "\n"  # Separator line
    
    # Process each row, replacing user_id with the user's display name
    for row in rows:
        # Fetch the user's guild member info based on the user_id in the row
        try:
            user = await interaction.guild.fetch_member(row["user_id"])
            user_name = user.display_name  # Use display name or username as needed
        except Exception:
            user_name = "Unknown User"  # Fallback if the user can't be fetched

        # Construct the formatted row with user_name replacing user_id
        formatted_row = (
            f"{str(row['id']):<{column_widths['id']}} "
            f"{row['track']:<{column_widths['track']}} "
            f"{row['car']:<{column_widths['car']}} "
            f"{row['laptime']:<{column_widths['laptime']}} "
            f"{user_name:<{column_widths['user_id/user_name']}} "  # Use user_name instead of user_id
        )
        result += formatted_row + "\n"

    await interaction.followup.send(f"```{result}```")


@app_commands.command(name="bop_request_delete",
                      description="Delete one of your bop requests.")
@app_commands.describe(entry_id="ID of the entry you want to delete.")
async def bop_request_delete_cmd(interaction: discord.Interaction,
                       entry_id: Optional[int]):
    
    await interaction.response.defer(ephemeral=False, thinking=True)
    
    user_id = interaction.user.id
    
    pool = await database.create_pool()
    if await database.delete_bop_request(entry_id, user_id, pool):
        await interaction.followup.send(f"Entry {entry_id} deleted.")
    else:
        await interaction.followup.send("https://media0.giphy.com/media/v1.Y2lkPTc5MGI3NjExbG5hdXhxY3JlNGxreDkwYnZ6MXZ6ZTdmbm9vcjN3ZTBzY29vbnRmMSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/xT9KVImcHatLPzx1VC/giphy.webp")


