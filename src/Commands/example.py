import discord
import logging
import Commands
import bop
import car_stats

logger = logging.getLogger(__name__)


class example(Commands.command.Command):

    def __init__(self, name):
        super().__init__(name)

    async def execute(self, message: discord.Message):
        track, sort = message.content.replace("!carstats ").split(" - ")
        data = car_stats.get_stats(track)
        track = bop.find_closest_track(bop.get_all_tracks(), track)
        print(data)
        max_length = 30
        result = f"**{track}**\n"
        carname = f"Car"
        wins = f"Wins"
        podiums = f"Pod."
        top5 = f"Top 5"
        top10 = f"Top 10"
        races = f"Entries"
        winsratio = f"Wins"
        podiumsratio = f"Pod."
        top5ratio = f"Top 5"
        top10ratio = f"Top 10"
        result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8}\n"

        for car in data:
            carname = f"{car['car_name']}"
            wins = f"{car['wins']}"
            podiums = f"{car['podium']}"
            top5 = f"{car['top5']}"
            top10 = f"{car['top10']}"
            races = f"{car['races']}"
            winsratio = f"{int(car['wins_per_race']*100000)/1000}%"
            podiumsratio = f"{int(car['podium_per_race']*100000)/1000}%"
            top5ratio = f"{int(car['top5_per_race']*100000)/1000}%"
            top10ratio = f"{int(car['top10_per_race']*100000)/1000}%"
            result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8}\n"

        await message.channel.send(f"{result}")
