import discord
import logging

from Commands.command import Command

logger = logging.getLogger(__name__)


class CommandManager:

    def __init__(self, prefix: str = "!"):
        self.prefix = prefix
        self.commands: dict[str, Command] = {}

    def add_command(self, command: Command) -> None:
        self.commands[command.name] = command

    def get_commands(self) -> dict[str, Command]:
        return self.commands

    async def dispatch(self, message: discord.Message) -> bool:

        if not message.content.startswith(self.prefix):
            return False

        logger.info(f"Dispatching message {message.content} from {message.author}")

        command_name = message.content.split()[0][1:]

        if command_name not in self.commands.keys():
            return False

        await self.commands.get(command_name).execute(message)

        return True
