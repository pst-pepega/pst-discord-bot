import re

input_string = 'Total:\n\n8.35523505259806e+16: 14\n8.355335052598067e+16: 14\n8.354235052598067e+16: 14\n8.355235052598057e+16: 14\n8.355235052598067e+16: 24\n8.355235052598057e+16: 14\n'

# Define the username you want to search for
username_to_find = '8.355235052598067e+16'

# Create a regular expression pattern to find the username and total_diff
pattern = r'{}: (\d+)'.format(re.escape(username_to_find))

# Search for the pattern in the input string
match = re.search(pattern, input_string)

# Check if a match was found
if match:
    total_diff = match.group(1)
    print(f"Total Difference for {username_to_find}: {total_diff}")
else:
    print(f"Username {username_to_find} not found")
