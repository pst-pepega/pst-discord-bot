import discord
from discord import app_commands
import logging
import os
from typing import Literal, Optional

from constants import WeatherSimDay
import Utils
import Utils.Ansi
import search_patch
import weathersim
from LfmAPI import LfmApi

logger = logging.getLogger(__name__)


@app_commands.command(name="acc-status",
                      description="Check if this fucking game is down")
async def acc_down_cmd(interaction: discord.Interaction):

    lfm_api = LfmApi()
    acc_status = lfm_api.get_acc_status()
    if acc_status is None:
        return

    status = "ONLINE"
    color = Utils.Ansi.Foreground.GREEN  # type: ignore
    if acc_status:
        status = "OFFLINE"
        color = Utils.Ansi.Foreground.RED  # type: ignore

    ansi_text = Utils.Ansi.Text().set_foreground(color).add_text(
        f"ACC lobby servers are {status}").reset()  # type: ignore
    reply = f"```ansi\n{ansi_text.render()}\n```"

    await interaction.response.send_message(reply)


raw_patch_notes = os.listdir("./resources/acc_patch/")
patch_notes = tuple([raw_patch_note.rsplit('.', 1)[0]
                    for raw_patch_note in raw_patch_notes])


@app_commands.command(name="acc-patch")
@app_commands.describe(patch="ACC version")
@app_commands.describe(search_term="Search for term in the ACC patch notes")
async def acc_patch_cmd(interaction: discord.Interaction,
                    patch: Literal[patch_notes],
                    search_term: Optional[str] = None):

    file_path = f"resources/acc_patch/{patch}.md"
    logging.info(f"File: {file_path}")

    if search_term is None:
        with open(file_path, 'r') as file:
            await interaction.response.send_message(f'ACC Patch: {patch}:', file=discord.File(file))
        return

    car_data = search_patch.search_car(file_path, search_term)
    track_data = search_patch.search_track(file_path, search_term)

    if car_data == "\n":
        car_data = ""
    else:
        car_data = f"```{car_data}```"

    if track_data == "\n":
        track_data = ""
    else:
        track_data = f"```{track_data}```"

    await interaction.response.send_message(f"{car_data}\n\n{track_data}")


@app_commands.command(name="weather-sim",
                      description="Calculate weather for your ACC Race")
@app_commands.describe(rain="Rain intensity, from 0 to 100")
@app_commands.describe(clouds="Cloud cover, from 0 to 100")
@app_commands.describe(randomness="Weather randomness, from 0 to 7")
@app_commands.describe(ambient="Ambient temperature, from 12 to 50°C")
@app_commands.describe(day="Day of the session")
@app_commands.describe(hour="Hour at which the session start")
@app_commands.describe(duration="Session duration in minutes")
@app_commands.describe(simulations="Number of weather simulation, from 1 to 100 000, default is 1000")
@app_commands.describe(info="Additional Info to put in the title of the image.")
async def weather_sim_cmd(interaction: discord.Interaction,
                      rain: app_commands.Range[int, 0, 100],
                      clouds: app_commands.Range[int, 0, 100],
                      randomness: app_commands.Range[int, 0, 7],
                      ambient: app_commands.Range[int, 12, 50], day: WeatherSimDay,
                      hour: app_commands.Range[int, 0, 23], duration: int,
                      info: str = "",
                      simulations: app_commands.Range[int, 1, 100_000] = 1000):

    rain = rain / 100
    clouds = clouds / 100

    await interaction.response.defer(ephemeral=False, thinking=True)

    result = weathersim.run_simulations(rain, clouds, randomness, ambient,
                                        day.value, hour, duration, info, simulations)
    with open('graph.png', 'rb') as f:
        await interaction.followup.send(content=result, file=discord.File(f))

    os.remove('graph.png')