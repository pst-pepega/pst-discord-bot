from discord import app_commands
import discord
import database


@app_commands.command(name="register-race-result",
                      description="Register to get race result notifications")
async def race_result_register_cmd(interaction: discord.Interaction):
    pool = await database.create_pool()
    async with pool.acquire() as conn:

        user_record = await conn.fetchrow("SELECT * FROM lfm_ids WHERE discord_id = $1",
                                          interaction.user.id)
        message = "Registered !"
        if user_record is None:
            message = "Fist add your lfm id using the /add-lfm-id command then register to the race result notification system"

        else:
            result = await conn.execute("""
                               INSERT INTO race_results(discord_id, channel_id)
                               VALUES ($1, $2)
                               ON CONFLICT(discord_id, channel_id) DO NOTHING
                               """, interaction.user.id, interaction.channel_id)
            if result.split(" ")[-1] == "0":
                message = "Already registered !"

    await interaction.response.send_message(message, ephemeral=True)
    await pool.close()


@app_commands.command(name="unregister-race-result",
                      description="Unregister from race result notifications")
@app_commands.describe(all="Unregister from all channels")
async def race_result_unregister_cmd(interaction: discord.Interaction,
                                     all: bool = False):
    pool = await database.create_pool()
    message = "Unregistered"
    async with pool.acquire() as conn:
        query = "DELETE FROM race_results WHERE discord_id = $1"
        if not all:
            query += f" AND channel_id = {interaction.channel_id}"
        else:
            message += " from all channel"
        await conn.execute(query, interaction.user.id)
    await pool.close()
    await interaction.response.send_message(f"{message} !", ephemeral=True)
