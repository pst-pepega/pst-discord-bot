import bop
import BOP_score
import pandas
import time
import requests
import logging
import Utils
from Utils import Ansi
from datetime import datetime
import lfmhl



logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def get_filtered_tracks():
    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")
    data = track_list_response.json()
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}
#   track_list = list(track_dict.keys())
    tracks = list(track_dict.keys())
    tracks.append("Nords")
    tracks.append("Imola")
    tracks.append("Cota")
    tracks.append("Bathurst")
    tracks.append("Valencia")
    tracks.append("Barcelona")
    tracks.append("Barça")
    tracks.append("Barca")
    tracks.append("all")
    tracks.append("all all")
        
    return tracks


def all_bop():
    result = bop.get_all_bop()
    result = result[(result['relevant'] == 'X')]
    result = result.to_string(index=True)

    return result


def all_all_bop():
    result = bop.get_all_bop()
    result = result.to_string(index=True)
    return result


def get_bop(track):
    start_time = time.time()
    track = Utils.to_lfm_track_name(track)
    bop_data = bop.get_bop_data(track)
    result = bop.get_bop(track, bop_data)
    intermediate_time = round(time.time() - start_time, 3)
    logger.info(f"```\n{result}\n```")

    hotlap_data = bop.fetch_hotlaps_data(track)
    result['driver_name'] = result.apply(lambda row: bop.extract_data(hotlap_data, row['car_name'], row['lap']), axis=1) # type: ignore
        
    message = result.to_string(index=True) # type: ignore

    result['bop'] = pandas.to_numeric(result['bop'], errors='coerce') # type: ignore
    result = result[(result['relevant'] == 'X') | (result['bop'] > 0)] # type: ignore
    
    message2 =f'''
**{track.title()}**
```\n{result.to_string(index=True)}
```\n**Target Laptime:** {bop.get_targettime(track, bop_data)}
**Seconds per kg:** {bop.get_kg_per_second(track, result)}
BOP Score: {BOP_score.get_score(track, result)}/10
This request took {intermediate_time} seconds. Adding Hotlap drivers took {round(time.time() - start_time - intermediate_time, 3)} seconds.           
            '''

    return (message, message2)


def get_bop_small(track):
    track = Utils.to_lfm_track_name(track)
    bop_data = bop.get_bop_data(track)        
    result = bop.get_bop(track, bop_data)        
    copy_of_bop_data = result
    target = bop.get_targettime(track, bop_data)

    result.drop("car_year", axis=1, inplace=True) # type: ignore
    result = result.rename(columns={'relevant': 'rel'}) # type: ignore
    result = result.rename(columns={'bop_raw': 'raw'})
    result['target_dif'] = result['target_dif'].apply(lambda x: x[0] + x[4:] if x[1:3] == '00' else x)
    result = result.rename(columns={'target_dif': 'diff'})                            
    result['bop'] = pandas.to_numeric(result['bop'], errors='coerce') # type: ignore
    result = result[(result['rel'] == 'X') | (result['bop'] > 0)] 

    # make car names smaller
    result['car_name'] = result['car_name'].replace(to_replace='Bentley Continental', value='Bentley')
    result['car_name'] = result['car_name'].replace(to_replace='McLaren 720S GT3 Evo', value='McLaren 720S Evo')
    result['car_name'] = result['car_name'].replace(to_replace='Mercedes-AMG GT3', value='Mercedes-AMG')
    result['car_name'] = result['car_name'].replace(to_replace='Audi R8 LMS GT3 evo II', value='Audi evo II')
    result['car_name'] = result['car_name'].replace(to_replace='BMW M4 GT3', value='BMW M4')
    result['car_name'] = result['car_name'].replace(to_replace='Lamborghini Huracan GT3 EVO 2', value='Lambo EVO 2')
    result['car_name'] = result['car_name'].replace(to_replace='Porsche 992 GT3 R', value='Porsche 992')
    result['car_name'] = result['car_name'].replace(to_replace='Ferrari 296 GT3', value='Ferrari 296')
    result['car_name'] = result['car_name'].replace(to_replace='AMR V8 Vantage', value='AMR V8')
    result['car_name'] = result['car_name'].replace(to_replace='Honda NSX GT3 Evo', value='Honda Evo')
    result['car_name'] = result['car_name'].replace(to_replace='Ford Mustang GT3', value='Ford')
    result['car_name'] = result['car_name'].replace(to_replace='Nissan GT-R Nismo GT3', value='Nissan')

    try:
        result['car_name'] = result['car_name'].replace(to_replace='Lexus RC F GT3', value='Lexus')
    except:
        logger.info("Lexus RC F GT3 not found.")

    try:
        result['car_name'] = result['car_name'].replace(to_replace='Ferrari 488 GT3 Evo', value='Ferrari 488')
    except:
        logger.info("Ferrari 488 not found.")

    logger.info(f"```\n{result}\n```")

    bop_score = BOP_score.get_score(track, copy_of_bop_data)

    message = f"**{track}**```\n{result.to_string(index=True)}\n```\n**Target Laptime:** {target}\nBOP Score: {bop_score}/10"

    return message


def get_realbop(track, car_class = "GT3"):
    data = bop.get_realbop(bop.find_closest_track(bop.get_all_tracks(), track))
        
    result = ""

    if data:
        cars = data['bop'][car_class]
        # Sort the cars by "bop" (ballast) in ascending order
        sorted_cars = sorted(cars, key=lambda x: x['ballast'], reverse=True)

        for car in sorted_cars:
            car_name = car['car_name']
            ballast = car['ballast']
            change = int(car['ballast_change'])

            ansi_text = Ansi.Text()
            if change > 0:
                foreground = Ansi.Foreground.RED
                text = f"+{change}"
            elif change < 0:
                foreground = Ansi.Foreground.GREEN
                text = f"{change}"
            else:
                foreground = Ansi.Foreground.YELLOW
                text = f" {change}"

            ansi_text.set_foreground(foreground).add_text(text).reset()

            ballast = f"{ballast:4} [{ansi_text.render()}]"

            # Use string formatting to align car names
            result += (f'{ballast:10}  {car_name}\n') # type: ignore
        
        kg = "kg"    
        temp = f"{kg:4} [Δ]"
        result = f"**{track}**```ansi\n {temp:10} car\n{result}```\n Δ == Delta from last version."

        bop_version = data['bop_version']
        active_since = data['active_since']
        active_since_datetime = datetime.strptime(active_since, '%Y-%m-%d %H:%M:%S')
        active_since_timestamp = active_since_datetime.timestamp()
        result += (f'\nBOP Version {bop_version}, active since <t:{int(active_since_timestamp)}>')

        return result


def get_bopcar(car):
    #car = lfmhl.find_closest_car(lfmhl.get_cars(), car)

    # get all tracks for later
    tracks = bop.get_all_tracks()
    
    # define dataframes
    total = pandas.DataFrame()

    if tracks:
        count_tracks = len(tracks)
        count = 0
        for track in tracks:
            count += 1
            new = pandas.DataFrame()
            logger.info(f"Trying {track}")
            try:
                new = bop.get_bop(track)       
                new = new[new['car_name'] == car] # type: ignore
                new.insert(0, 'track', track)
                total = pandas.concat([total, new], ignore_index=True, sort=False)
            except:
                logger.info("Probably didn't find the input car in dataframe.")
    
    total = total.sort_values(by="bop_raw", ascending=False)
    total.drop("car_year", axis=1, inplace=True)
    #total.drop("lap", axis=1, inplace=True)
    total.drop("car_name", axis=1, inplace=True)
    total = total.rename(columns={'relevant': 'rel'})
    total = total.rename(columns={'bop_raw': 'raw'})
    total['target_dif'] = total['target_dif'].apply(lambda x: x[0] + x[4:] if x[1:3] == '00' else x)
    total = total.rename(columns={'target_dif': 'diff'})

    return f"```\n{total.to_string(index=False)}\n```"


def get_gt4bop(track: str):
    
    track = Utils.to_lfm_track_name(track)
    bop_data = bop.get_gt4bop_data(track)
    print(bop_data)
    result = bop.get_bop(track, bop_data)
    logger.info(f"```\n{result}\n```")

    hotlap_data = bop.fetch_hotlaps_data(track, "GT4")
    result['driver_name'] = result.apply(lambda row: bop.extract_data(hotlap_data, row['car_name'], row['lap']), axis=1) # type: ignore
        
    message = result.to_string(index=True) # type: ignore

    result['bop'] = pandas.to_numeric(result['bop'], errors='coerce') # type: ignore
    result = result[(result['relevant'] == 'X') | (result['bop'] > 0)] # type: ignore
    
    message2 =f'''
**{track.title()}**\n
```\n{result.to_string(index=False)}
```\n**Target Laptime:** {bop.get_targettime(track, bop_data)}
**Seconds per kg:** {bop.get_kg_per_second(track, result)}
BOP Score: {BOP_score.get_score(track, result)}/10     
            '''

    return (message, message2)

