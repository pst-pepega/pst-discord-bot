import car_stats

def get_carstats_result(data, track):

    result = ""

    max_length = 30
    result = f"**{track}**\n"
    carname = f"Car"
    wins = f"Wins"
    podiums = f"Pod."
    top5 = f"Top 5"
    top10 = f"Top 10"
    races = f"Entries"
    winsratio = f"Wins"
    podiumsratio = f"Pod."
    top5ratio = f"Top 5"
    top10ratio = f"Top 10"
    entry_rate = f"Entries"
    result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

    for car in data:
        carname = f"{car['car_name']}"
        wins = f"{car['wins']}"
        podiums = f"{car['podium']}"
        top5 = f"{car['top5']}"
        top10 = f"{car['top10']}"
        races = f"{car['races']}"
        winsratio = f"{int(car['wins_per_race'] * 10000) / 100}%"
        podiumsratio = f"{int(car['podium_per_race'] * 10000) / 10000}"
        top5ratio = f"{int(car['top5_per_race'] * 10000) / 10000}"
        top10ratio = f"{int(car['top10_per_race'] * 10000) / 10000}"
        entry_rate = f"{int(car['entry_rate'] * 10000) / 100}%"
        result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

    filename = f"{track}.md"
    with open(filename, 'a', encoding="UTF-8") as file:
        # Append your data
        file.write(result)

        result = ""

        data = data[:10]
        max_length = 30
        result = f"**{track}**\n```"
        carname = f"Car"
        wins = f"Wins"
        podiums = f"Pod."
        top5 = f"Top 5"
        top10 = f"Top 10"
        races = f"Entries"
        winsratio = f"Wins"
        podiumsratio = f"Pod."
        top5ratio = f"Top 5"
        top10ratio = f"Top 10"
        entry_rate = f"Entries"
        result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

        for car in data:
            carname = f"{car['car_name']}"
            wins = f"{car['wins']}"
            podiums = f"{car['podium']}"
            top5 = f"{car['top5']}"
            top10 = f"{car['top10']}"
            races = f"{car['races']}"
            winsratio = f"{int(car['wins_per_race'] * 10000) / 100}%"
            podiumsratio = f"{int(car['podium_per_race'] * 10000) / 10000}"
            top5ratio = f"{int(car['top5_per_race'] * 10000) / 10000}"
            top10ratio = f"{int(car['top10_per_race'] * 10000) / 10000}"
            entry_rate = f"{int(car['entry_rate'] * 10000) / 100}%"
            result += f"{carname:{max_length}} {wins:6} {podiums:6} {top5:7} {top10:7} {races:7} {winsratio:8} {podiumsratio:8} {top5ratio:8} {top10ratio:8} {entry_rate:8}\n"

        result += "```"

    return result

