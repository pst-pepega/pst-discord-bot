import asyncio
import asyncpg
import csv
import logging

logger = logging.getLogger(__name__)

async def main():
    logger.info("Start report database.")
    # Create a connection to the database
    conn = await asyncpg.connect(user='ryan_the_rennoir', password='ryancooper',
                                  database='counters', host='postgres')

	
    logger.info("Connected?")

    # Create the table
    await conn.execute('''CREATE TABLE IF NOT EXISTS reports
                          (id SERIAL PRIMARY KEY,
                           Date TEXT,
                           Reporter TEXT,
                           Reportee TEXT,
                           ELO REAL,
                           SR REAL,
                           TP REAL,
                           Ban TEXT,
                           PP TEXT,
                           Permaban TEXT,
                           Back_to_Rookies TEXT)''')
    
    logger.info("Created Reports Table")

    # Open the TSV file and insert the data into the table
    with open('resources/report data.tsv', newline='') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        next(reader)  # Skip the header row
        for row in reader:
           # Check if the row has enough elements before accessing them
            if len(row) > 0:
                Date = row[0] if row[0] else ""
            else:
                Date = ""

            if len(row) > 1:
                Reporter = row[1] if row[1] else ""
            else:
                Reporter = ""

            if len(row) > 2:
                Reportee = row[2] if row[2] else ""
            else:
                Reportee = ""

            if len(row) > 3:
                Elo = float(row[3]) if row[3] else 0
            else:
                Elo = 0

            if len(row) > 4:
                Sr = float(row[4]) if row[4] else 0
            else:
                Sr = 0

            if len(row) > 5:
                Tp = float(row[5]) if row[5] else 0
            else:
                Tp = 0

            if len(row) > 6:
                Ban = row[6] if row[6] else ""
            else:
                Ban = ""

            if len(row) > 7:
                pp = row[7] if row[7] else ""
            else:
                pp = ""

            if len(row) > 8:
                permaban = row[8] if row[8] else ""
            else:
                permaban = ""

            if len(row) > 9:
                btr = row[9] if row[9] else ""
            else:
                btr = ""

            await conn.execute('''INSERT INTO reports (Date, Reporter, Reportee, ELO, SR, TP, Ban, PP, Permaban, Back_to_Rookies)
                                  VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)''', Date, Reporter, Reportee, Elo, Sr, Tp, Ban, pp, permaban, btr)
            logger.info(f"INSERT INTO reports (Date, Reporter, Reportee, ELO, SR, TP, Ban, PP, Permaban, Back_to_Rookies) VALUES ({Date}, {Reporter}, {Reportee}, {Elo}, {Sr}, {Tp}, {Ban}, {pp}, {permaban}, {btr})")
            
    logger.info("Data inserted??")

    # Close the connection
    await conn.close()

# # Run the main function
# asyncio.run(main())
