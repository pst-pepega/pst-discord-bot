import math
import json
from urllib.request import urlopen 
import sys
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)  # Set the logging level
# Create a console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)  # Set the logging level for the handler
# Create a formatter and set it for the handler
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
# Add the handler to the logger
logger.addHandler(console_handler)


def main(race_id, team_id):
	url = ("https://api2.lowfuelmotorsport.com/api/race/" + str(race_id))
	return_text = ""
  
	response = urlopen(url)
	data = json.loads(response.read())

#	print("Data loaded.")

	return_text += ("Race at " + data["track"]["track_name"] + " at " + data["race_date"] + "\n")
	return_text += (data["server_settings"]["server_settings"]["settings"]["data"]["serverName"] + "\n")


	# drivers = [5368, 5018, 4922, 4726, 4697, 4319, 4170, 3799, 3633, 3624, 3409, 3322, 3092, 3060, 2913, 2757, 2735, 2701, 2611, 2516, 2372, 2368, 2358, 2299, 2290, 2277, 2257, 2255, 2249, 2232, 2226, 2190, 2168, 2102, 2097]
	drivers = []
	driver = 0
	driver_amount = 0

	for x in data["participants"]["entries"]:
		drivers.append(x["car_elo"])
		if(x["car_number"] == int(team_id)):
			driver = x["car_elo"]
			logger.info(driver)
			driver_amount = len(x["drivers"])
			logger.info(driver_amount)
			return_text += ("Your team is " + x["teamname"] +  "\n")

	rBase = 1600 / math.log(2)
	elogain = 0
	k_factor = float(data["event"]["k_factor"])
	drivers_count = len(drivers)
	return_text += ("Drivers: " + str(drivers_count) + "\n")
	
	return_text += ("K-factor: " + str(k_factor) + "\n")

	for y in range(1, drivers_count+1):
		elogain = 0
		fudgeFactor = ((drivers_count - 0) / 2 - y) / 100	
		for x in drivers:
			if (x != driver):
				a = (1 - math.exp(-driver / rBase)) * math.exp(-x / rBase)
				b = (1 - math.exp(-x / rBase)) * math.exp(-driver / rBase)
				c = (1 - math.exp(-driver / rBase)) * math.exp(-x / rBase)
				score = (a / (b + c)) 
				logger.info(score)
				#score = (drivers_count - driver_pos - score - fudgeFactor) * 200 / drivers_count * k_factor
				# return_text += (str(score))
				elogain = (elogain + score)
		
		logger.info(((drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * k_factor))
		elogain = ((drivers_count - y - elogain - fudgeFactor) * 200 / drivers_count * k_factor) * 35 / 72


		#print("P" + str(y) + ": " + str(round(elogain, 4)))
		return_text += ("P" + str(y) + ": " + str(round(elogain, 4)) +  "\n")
		
	return return_text

if __name__ == "__main__":
    print(main(139909,1))        




