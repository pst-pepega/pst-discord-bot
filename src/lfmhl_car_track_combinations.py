import lfmhl

async def get_combos(initial_message):
    # List of tracks
    track_list = lfmhl.get_all_tracks()

    # Initialize a dictionary to count names
    name_counts = {}
    count = 1

    # Loop through the track list and retrieve data for each track
    for track_name in track_list:
        await initial_message.edit(content=f"{count}/23")
        track_data = lfmhl.get_hotlaps(track_name)  # Replace with your data retrieval function
        # Extract the data for the current track
        data_for_track = track_data
        # Loop through the data for the current track and count names
        for item in data_for_track:    
            name = f"{item['vorname']} {item['nachname']}"
            if name in name_counts:
                name_counts[name] += 1
            else:
                name_counts[name] = 1
    
        count += 1

    # Sort the name counts dictionary by count in descending order
    sorted_name_counts = dict(sorted(name_counts.items(), key=lambda item: item[1], reverse=True))

    return sorted_name_counts