import requests
import json

def find_user_id(user_id, data, sort_by):
    for i, user in enumerate(data):
        if user["user_id"] == user_id:
            if i == 0:
                prev_user = {'vorname': 'Your', 'nachname': 'Mum', sort_by: 42069}
            else:
                prev_user = data[i-1]
                        
            return (user, i+1), (prev_user, i)
    return None, None

def get_diff(user, prev_user, sort_by):
    if user and prev_user:
        user_poles = user.get(sort_by, 0)
        prev_user_poles = prev_user.get(sort_by, 0)
        return user_poles - prev_user_poles
    return None


def get_stats(sort_by, user_id):
#    try:
        url = "https://api2.lowfuelmotorsport.com/api/statistics/driverperformance?sort=wins&country="
        response = requests.get(url)
        data = json.loads(response.text)
        data["data"].sort(key=lambda x: x[sort_by], reverse=True)
        user, prev_user = find_user_id(user_id, data["data"], sort_by)

        if user:
            user, user_pos  = user

            if user['patreon'] == 0:
                return None

        if prev_user:
            prev_user, prev_user_pos = prev_user

        if user and prev_user:
            diff = get_diff(prev_user, user, sort_by)
            if diff > 0: # type: ignore
                return (f"You need {diff} more {sort_by} to overtake {prev_user['vorname']} {prev_user['nachname']} for position {prev_user_pos}\n")
        else:
            return None
        
#    except:
#        return "Key not found, try one of the following: wins, podium, top5t, top10, dnf, poles, racestarts, ranked, zero_ip"
