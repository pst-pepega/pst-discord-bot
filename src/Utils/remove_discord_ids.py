import logging

# Define a function to remove the substrings that match the patterns
def check_string(message, string_to_match, patterns = ["<@\d+>", "<@&\d+>", "<#\d+>", "<:[^>]+:\d+>", "<a:[^>]+:\d+>"]):
    # Import the re module for regular expressions
    import re
    # Loop through each pattern and replace it with an empty string
    for pattern in patterns:
        message = re.sub(pattern, "", message)
        
    if string_to_match in message:
        return True
    else:
        return False    