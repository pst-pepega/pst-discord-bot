def file_starts_with(file_path, target_string = '{\n\t"carName"'):
    try:
        with open(file_path, 'r') as file:
            # Read the beginning part of the file (length of target_string)
            start_of_file = file.read(len(target_string))
            # Check if it matches the target string
            return start_of_file == target_string
    except FileNotFoundError:
        print(f"The file {file_path} does not exist.")
        return False
    except Exception as e:
        print(f"An error occurred: {e}")
        return False
