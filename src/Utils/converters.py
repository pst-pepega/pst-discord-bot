import logging

logger = logging.getLogger(__name__)


def to_lfm_track_name(track_name: str) -> str:

    track_name = track_name.lower()

    match track_name:
        case "imola":
            return "Autodromo Enzo e Dino Ferrari"

        case "cota":
            return "Circuit of The Americas"

        case "bathurst":
            return "Mount Panorama"

        case "valencia":
            return "Ricardo Tormo"

        case "barcelona" | "barca":
            return "catalunya"
        
        case "nords":
            return "nordschleife"
        
        case "nürburgring nordschleife 24h":
            return "nordschleife"

        case _:
            return track_name


def to_lfm_series_name(series: str) -> str:
    match series.lower():
        case "gt3" | "gt3 series":
            return "GT3 Series"

        case "gt4" | "gt4 series":
            return "GT4 Series"

        case "gt2" | "gt2 series":
            return "GT2 Series"

        case "m2" | "bmw m2 series":
            return "BMW M2 Series"

        case "pro" | "pro series":
            return "Pro Series"

        case "sprint" | "sprint series":
            return "Sprint Series"

        case "duo" | "duo cup":
            return "Duo Cup"

        case "aor" | "apex online racing":
            return "AOR"

        case _:
            logger.warning(f"Unknown series {series}")
            return series
