from datetime import datetime

def calculate_current_week(start_date, current_date):
    # Convert string dates to datetime objects
    start_date = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
    current_date = datetime.strptime(current_date, "%Y-%m-%d %H:%M:%S")
    
    # Calculate the difference in days between the start date and current date
    delta_days = (current_date - start_date).days
    print(delta_days)
    
    # Calculate the week number
    current_week = delta_days // 7 + 1
    
    return current_week