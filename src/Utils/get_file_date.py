import os
import datetime
import time

def get_modified_date_from_file(file_path: str):

    # Get the last modification time of the file in seconds since the epoch
    modification_time = os.path.getmtime(file_path)

    # Convert the modification time to a human-readable format
    last_modified = datetime.datetime.fromtimestamp(modification_time)

    # Convert the modification time to a Unix timestamp
    unix_timestamp = int(modification_time)

    # Calculate the time difference in seconds
    current_time = time.time()
    time_difference = int(current_time - modification_time)

    # Calculate the days, hours, and minutes
    days = time_difference // (24 * 3600)
    hours = (time_difference % (24 * 3600)) // 3600
    minutes = (time_difference % 3600) // 60

    # Display the results
    # returns unix time stamp and days/hours/minutes
    return (unix_timestamp, [days, hours, minutes])