import json

def extract_track_bop_type(json_data):
    # Extract the "trackBopType" value
    track_bop_type = json_data.get("trackBopType")
    return track_bop_type

def find_track_info(file_name):
    # Find the track info based on the extracted BopType
    
    with open('resources/TrackBop.json', 'r', encoding='utf-8-sig') as file:
        tracks_json = json.load(file)

    # Load the second JSON data
    with open(file_name, 'r', encoding='utf-8-sig') as file:
        bop_type = json.load(file)
    
    bop_type = extract_track_bop_type(bop_type)

    track_info = tracks_json.get(str(bop_type))
    return track_info

