from enum import Enum
from typing import Self


# Escape char
ESCAPE = "\u001b"


class Format(Enum):
    NORMAL = 0
    BOLD = 1
    UNDERLINE = 4


class Foreground(Enum):
    GRAY = 30
    RED = 31
    GREEN = 32
    YELLOW = 33
    BLUE = 34
    PINK = 35
    CYAN = 36
    WHITE = 37


class Background(Enum):
    FIREFLY_DARK_BLUE = 40
    ORANGE = 41
    MARBLE_BLUE = 42
    GREYISH_TURQUOISE = 43
    GRAY = 44
    INDIGO = 45
    LIGHT_GRAY = 46
    WHITE = 47


class Text:
    def __init__(self):
        self.text = ""

    def add_text(self, text: str) -> Self:
        self.text += text
        return self

    def set_format(self, format: Format) -> Self:
        self.text += f"{ESCAPE}[{format.value}m"
        return self

    def set_foreground(self, foreground: Foreground) -> Self:
        self.text += f"{ESCAPE}[{foreground.value}m"
        return self

    def set_background(self, background: Background) -> Self:
        self.text += f"{ESCAPE}[{background.value}m"
        return self

    def reset(self) -> Self:
        self.text += f"{ESCAPE}[0m"
        return self

    def render(self) -> str:
        return self.text
