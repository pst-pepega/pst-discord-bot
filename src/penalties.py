import json
import logging

with open(f'resources/json/lfm_penalties.json', 'r') as file:
    data = json.load(file)

# Function to search for a string in the JSON data
def search_description(input_string):
    result_list = []
    for category in data["categories"]:
        for penalty in category["penalties"]:
            if input_string.lower() in penalty["description"].lower():
                result_list.append(category)  # Return the category object if found
    return result_list
    return None  # Return None if no match is found


def get_embed(search_string):
    # Search for the string in the JSON data
    result_list = search_description(search_string)
    embed_list = []

    for result in result_list: 
        import discord

        # Assuming you have a result dictionary as described in your code

        # Create a Discord embed
        embed = discord.Embed(title=f"{result['name']}", color=0x00ff00)  # You can change the color to your preference

        # Add fields for the penalties
        penalty = result['penalties'][0]

        # Create a custom field separator (a blank field with spaces)
        custom_separator = '\u200b\u200b\u200b\u200b\u200b'  # You can adjust the number of spaces as needed

        # Add the fields
        try:
            embed.add_field(name=f"**{penalty['amount']}{penalty['unit']} {penalty['type']}**", value=custom_separator)
        except:
             embed.add_field(name=f"**{penalty['type']}**", value=custom_separator)
        
        try:
            embed.add_field(name=custom_separator, value=f"{penalty['points']} LFM Pro Penalty Points")
        except:
            logging.info("No penalty points for this offense.")

        # Add the description as a separate field
        embed.add_field(name="Description", value=f"{penalty['description']}", inline=False)

        # Send the embed message
        # Replace `ctx.send` with your own way of sending messages in your Discord bot
        embed_list.append(embed)

    return embed_list
