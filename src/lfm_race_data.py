from datetime import datetime
import json
import urllib.error
import urllib.request

from LfmAPI import LfmApi
import weathersim


def timestamp(date_time: datetime) -> int:
    return int(date_time.timestamp() - 3600)


# TODO continue converting LfmRace to dataclass 
def get_race(race_id) -> tuple[str, str]:
    api = LfmApi()
    race_info = api.get_race(race_id)
    if race_info is None:
        return ("Invalid race ID", "")

    date_and_time = timestamp(race_info.date)

    track = race_info.track.name
    race_time = f"<t:{date_and_time}> <t:{date_and_time}:R>"
    ambient = race_info.server_settings.event["data"]["ambientTemp"]
    clouds = race_info.server_settings.event["data"]["cloudLevel"]
    rain = race_info.server_settings.event["data"]["rain"]
    randomness = race_info.server_settings.event["data"]["weatherRandomness"]
    hour = race_info.server_settings.event['data']['sessions'][2]['hourOfDay']
    duration = int(race_info.server_settings.event["data"]["sessions"][2]["sessionDurationMinutes"]) + int(race_info.server_settings.event["data"]["sessions"][1]["sessionDurationMinutes"]) + 5
    day = race_info.server_settings.event['data']['sessions'][2]['dayOfWeekend']
    eff_ambient = 0
    if isinstance(ambient, (int, float)):
        eff_ambient = (
            ambient
            * max((1.232 - abs(14 - hour) * 0.05), 0.65)
            * (1 - clouds / 10 * 0.15)
        )


    result = weathersim.run_simulations(float(rain), float(clouds), int(randomness), int(ambient), int(day), int(hour), int(duration))

    race_info = f"""# Race info for \\#{race_id}\n
{track} @ {race_time}\n
{ambient}°C @ {hour}:00 with {int(clouds*100)}% Clouds and {int(rain*100)}% Rain [Randomness: {randomness}]
"""
    return (race_info, result)

## TEMP cause Ryan is a lil bitch
def get_race_team_event(race_id):
    url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    date_and_time = data['race_date']
    eff_ambient = 0.0
    
    track = data['track']['track_name']
    race_time = f'<t:{timestamp(datetime.strptime(date_and_time, "%Y-%m-%d %H:%M:%S"))}> <t:{timestamp(datetime.strptime(date_and_time, "%Y-%m-%d %H:%M:%S"))}:R>'
    ambient = data['server_settings']['server_settings']['event']['data']['ambientTemp']
    clouds = data['server_settings']['server_settings']['event']['data']['cloudLevel']
    rain = data['server_settings']['server_settings']['event']['data']['rain']
    randomness = data['server_settings']['server_settings']['event']['data']['weatherRandomness']
    hour = data['server_settings']['server_settings']['event']['data']['sessions'][2]['hourOfDay']
    duration = int(data['server_settings']['server_settings']['event']['data']['sessions'][2]['sessionDurationMinutes']) + int(data['server_settings']['server_settings']['event']['data']['sessions'][1]['sessionDurationMinutes']) + 5
    day = data['server_settings']['server_settings']['event']['data']['sessions'][2]['dayOfWeekend']

    result = weathersim.run_simulations(float(rain), float(clouds), int(randomness), int(ambient), int(day), int(hour), int(duration))
      

    race_info = f"""# Race info for \\#{race_id}\n
{track} @ {race_time}\n
{ambient}°C @ {hour}:00 with {int(clouds*100)}% Clouds and {int(rain*100)}% Rain [Randomness: {randomness}]
"""

    return (race_info, result)


def race_weather_to_json(race_id):
    try:
        api = LfmApi()
        race_info = api.get_race(race_id)

        date_and_time = timestamp(race_info.date)

        track = race_info.track.name
        race_time = f"<t:{date_and_time}> <t:{date_and_time}:R>"
        ambient = race_info.server_settings.event["data"]["ambientTemp"]
        clouds = race_info.server_settings.event["data"]["cloudLevel"]
        rain = race_info.server_settings.event["data"]["rain"]
        randomness = race_info.server_settings.event["data"]["weatherRandomness"]
        hour = race_info.server_settings.event['data']['sessions'][2]['hourOfDay']
        duration = int(race_info.server_settings.event["data"]["sessions"][2]["sessionDurationMinutes"]) + int(race_info.server_settings.event["data"]["sessions"][1]["sessionDurationMinutes"]) + 5
        day = race_info.server_settings.event['data']['sessions'][2]['dayOfWeekend']

    except:
        url = f"https://api2.lowfuelmotorsport.com/api/race/{race_id}"
        response = urllib.request.urlopen(url)
        data = json.loads(response.read())
        date_and_time = data['race_date']
        eff_ambient = 0.0

        track = data['track']['track_name']
        race_time = f'<t:{timestamp(datetime.strptime(date_and_time, "%Y-%m-%d %H:%M:%S"))}> <t:{timestamp(datetime.strptime(date_and_time, "%Y-%m-%d %H:%M:%S"))}:R>'
        ambient = data['server_settings']['server_settings']['event']['data']['ambientTemp']
        clouds = data['server_settings']['server_settings']['event']['data']['cloudLevel']
        rain = data['server_settings']['server_settings']['event']['data']['rain']
        randomness = data['server_settings']['server_settings']['event']['data']['weatherRandomness']
        hour = data['server_settings']['server_settings']['event']['data']['sessions'][2]['hourOfDay']
        duration = int(data['server_settings']['server_settings']['event']['data']['sessions'][2]['sessionDurationMinutes']) + int(data['server_settings']['server_settings']['event']['data']['sessions'][1]['sessionDurationMinutes']) + 5
        day = data['server_settings']['server_settings']['event']['data']['sessions'][2]['dayOfWeekend']
    
    eff_ambient = 0
    if isinstance(ambient, (int, float)):
        eff_ambient = (
            ambient
            * max((1.232 - abs(14 - hour) * 0.05), 0.65)
            * (1 - clouds / 10 * 0.15)
        )

    date = datetime.utcfromtimestamp(date_and_time).date()
    wet, dry, mixed, avg_temps = weathersim.run_simulation_for_json(10000, float(rain), float(clouds), int(randomness), int(ambient), int(((day-1)*24+hour*3600)), int(duration))

    return date, track, ambient, avg_temps, clouds, rain, dry, wet, mixed, hour

import json

def collect_weather_data(start_id, end_id):
    weather_data = []

    for race_id in range(start_id, end_id + 1):
        date, track, ambient, avg_temps, clouds, rain, dry, wet, mixed, hour = race_weather_to_json(race_id)
        print(date, track, ambient, avg_temps, clouds, rain, dry, wet, mixed, hour)
        weather_data.append({
            "Date": str(date),
            "Track": track,
            "°C": ambient,
            "Hour": hour,  # Assuming a fixed hour; adjust if necessary
            "Avg. °C": avg_temps,
            "Clouds": clouds,
            "Rain": rain,
            "Dry": dry,
            "Wet": wet,
            "Mixed": mixed
        })

    return weather_data


# if __name__ == "__main__":
#     start_id = 152241
#     end_id = 152252
#     weather_data = collect_weather_data(start_id, end_id)

#     # Write to JSON file
#     with open('weather_data.json', 'w') as json_file:
#         json.dump(weather_data, json_file, indent=4, ensure_ascii=False)

#     print("Weather data has been collected and saved to 'weather_data.json'.")


def get_race_reminder_info(race_id) -> str:
    api = LfmApi()
    race_info = api.get_race(race_id)
    if race_info is None:
        return ("Invalid race ID")

    date_and_time = timestamp(race_info.date)

    track = race_info.track.name
    race_time = f"<t:{date_and_time}> <t:{date_and_time}:R>"
    ambient = race_info.server_settings.event["data"]["ambientTemp"]
    clouds = race_info.server_settings.event["data"]["cloudLevel"]
    rain = race_info.server_settings.event["data"]["rain"]
    randomness = race_info.server_settings.event["data"]["weatherRandomness"]
    hour = race_info.server_settings.event['data']['sessions'][2]['hourOfDay']
    duration = int(race_info.server_settings.event["data"]["sessions"][2]["sessionDurationMinutes"]) + int(race_info.server_settings.event["data"]["sessions"][1]["sessionDurationMinutes"]) + 5
    day = race_info.server_settings.event['data']['sessions'][2]['dayOfWeekend']
    eff_ambient = 0
    if isinstance(ambient, (int, float)):
        eff_ambient = (
            ambient
            * max((1.232 - abs(14 - hour) * 0.05), 0.65)
            * (1 - clouds / 10 * 0.15)
        )


    result = weathersim.run_simulations(float(rain), float(clouds), int(randomness), int(ambient), int(day), int(hour), int(duration))

    race_info = f"""
{track} @ {race_time}\n
{ambient}°C @ {hour}:00 with {int(clouds*100)}% Clouds and {int(rain*100)}% Rain [Randomness: {randomness}]
__{race_id}__
"""
    return result