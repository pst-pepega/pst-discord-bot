from datetime import datetime, timezone
import asyncpg
import logging
import math

logger = logging.getLogger(__name__)

table_created = False

async def create_pool():
    global table_created
    # Provide connection details as separate arguments
    pool = await asyncpg.create_pool(
        user='ryan_the_rennoir',
        password='ryancooper',
        database='counters',
        host='postgres',
    )

    # Check if the counters table exists
    if pool != None:
        async with pool.acquire() as connection:
            table_exists = await connection.fetchval('''
                SELECT EXISTS (
                    SELECT 1
                    FROM information_schema.tables
                    WHERE table_name = 'counters'
                )
            ''')

        # If the counters table doesn't exist, create it with initial values
        if not table_exists and table_created == False:
            await setup_counters_table(pool)
            table_created = True

    await print_counters(pool)

    return pool


async def setup_counters_table(pool):
    async with pool.acquire() as connection:
        await connection.execute('''
            CREATE TABLE counters (
                counter_name TEXT PRIMARY KEY,
                count INT
            )
        ''')

        initial_counters = {
            "copium": 500,
            "cum": 144,
            "pepog": 1920,
            "chad": 10,
            "valencia": 522
        }

        for counter_name, count in initial_counters.items():
            await connection.execute('''
                INSERT INTO counters (counter_name, count)
                VALUES ($1, $2)
                ON CONFLICT (counter_name)
                DO NOTHING
            ''', counter_name, count)


async def print_counters(pool):
    return_value = ""
    async with pool.acquire() as connection:
        rows = await connection.fetch('SELECT * FROM counters')

    for row in rows:
        counter_name = row['counter_name']
        count = row['count']
        return_value += f'Counter "{counter_name}" has a count of {count}\n'
    return return_value


async def get_some_value(name, pool):
    async with pool.acquire() as conn:
        value = await conn.fetchval(f"SELECT count FROM counters WHERE counter_name = '{name}'")
        logger.info(f"SELECT count FROM counters WHERE counter_name = '{name}'")
        logger.info(f"{name} has the value {value}")
        return value
    

async def update_predictions(name, position, week, pool):
    async with pool.acquire() as conn:
        await conn.execute(f"UPDATE standings SET week{week} = $1 WHERE driver_name = '{name}'", position)
        logger.info(f"UPDATE standings SET week{week} = {position} WHERE driver_name = '{name}'")
    

async def update_some_value(name, new_value, pool):
    async with pool.acquire() as conn:
        await conn.execute(f"UPDATE counters SET count = $1 WHERE counter_name = '{name}'", new_value)
        logger.info(f"Updating counters table - {name} with {new_value}")


async def get_lfm_id_from_discord_id(discord_id, pool) -> int:
    async with pool.acquire() as conn:
        value = await conn.fetchval(f"SELECT lfmid FROM lfm_ids WHERE discord_id = '{discord_id}'")
        logger.info(f"SELECT lfmid FROM lfm_ids WHERE discord_id = '{discord_id}'")
        logger.info(f"{discord_id} has the value {value}")
        return value


async def delete_lfmid(discord_id, pool):
    async with pool.acquire() as conn:
        await conn.execute(f"DELETE from lfm_ids where discord_id = '{discord_id}'")
        logger.info(f"DELETE from lfm_ids where discord_id = '{discord_id}'")


async def delete_prediction(discord_id, pool):
    async with pool.acquire() as conn:
        await conn.execute(f"DELETE from prediction where user_id = '{discord_id}'")
        await conn.execute(f"DELETE from users where user_id = '{discord_id}'")
        logger.info(f"DELETE from prediction where user_id = '{discord_id}'")
        logger.info(f"DELETE from users where user_id = '{discord_id}'")

        
async def fetch_value(query, pool) -> int:
    async with pool.acquire() as conn:
        value = await conn.fetchval(query)
        logger.info(f"Successfully executed query: {query} and fetched: {value}")
        return value    
    

async def fetch_values(query, pool):
    async with pool.acquire() as conn:
        rows = await conn.fetch(query)
        return rows


async def update_lfm_id(discord_id: float, lfm_id: int, pool):
    async with pool.acquire() as conn:
        value = await conn.fetchval(f"SELECT lfmid FROM lfm_ids WHERE discord_id = '{discord_id}'")

        # Check if a row was updated
        if value is None:
            # If no rows were updated, perform an insert
            await conn.execute(
                "INSERT INTO lfm_ids (discord_id, lfmid) VALUES ($1, $2)",
                discord_id, lfm_id
            )
            logger.info(f"Inserting into lfm_ids table - {discord_id} with {lfm_id}")
        else:
            await conn.execute(
                "UPDATE lfm_ids SET lfmid = $1 WHERE discord_id = $2",
                lfm_id, discord_id
            )
            logger.info(f"Updating lfm_ids table - {discord_id} with {lfm_id}")


async def update_messages_count(user_id, new_value, pool):
    async with pool.acquire() as conn:
        query = """
            INSERT INTO messages (user_id, message_count)
            VALUES ($1, $2)
            ON CONFLICT (user_id)
            DO UPDATE SET message_count = EXCLUDED.message_count;
        """
        await conn.execute(query, user_id, new_value)
        #logger.info(f"Updating counters table - {user_id} with {new_value}")


async def get_message_count(user_id, pool):
    async with pool.acquire() as conn:
        value = await conn.fetchval(f"SELECT message_count FROM messages WHERE user_id = {user_id}")
        #logger.info(f"SELECT count FROM counters WHERE counter_name = '{user_id}'")
        #logger.info(f"{user_id} has the value {value}")
        return value


async def get_all_message_count(limit, pool):
    async with pool.acquire() as conn:
        value = await conn.fetch(f"select * from messages order by message_count DESC limit {limit};")
        return value 


async def get_overtake_message_count(pool):
    async with pool.acquire() as conn:
        value = await conn.fetch(f"SELECT user_id, message_count, CASE WHEN LAG(message_count) OVER (ORDER BY message_count DESC) IS NULL THEN 'N/A' ELSE (LAG(message_count) OVER (ORDER BY message_count DESC) - message_count)::TEXT END AS difference FROM messages ORDER BY message_count DESC;")
        return value 


async def add_report(pool, query):
    async with pool.acquire() as conn:
        await conn.execute(query)
        logger.info(query)


async def add_copypastas(pool, query, *args):
    async with pool.acquire() as conn:
        logging.info(f"Executing query: {query}")
        logging.info(f"Arguments: {args}")
        await conn.execute(query, *args)        


async def get_reports(pool, query):
    async with pool.acquire() as conn:
        values = await conn.fetch(query)
        logger.info(query)
        logger.info(values)
        logger.info(f"Length of Query: {len(values)}")
        return values
    
    
async def insert_bop_setting(discord_id, setting_value, pool):
    async with pool.acquire() as conn:
        query = """
        INSERT INTO bop_setting (discord_id, setting_value)
        VALUES ($1, $2)
        """
        await conn.execute(query, discord_id, setting_value)
        logger.info(f"INSERT INTO bop_setting (discord_id, setting_value) VALUES ({discord_id}, {setting_value})")


async def update_bop_setting(discord_id, setting_value, pool):
    async with pool.acquire() as conn:
        query = """
        UPDATE bop_setting
        SET setting_value = $1
        WHERE discord_id = $2
        """
        await conn.execute(query, setting_value, discord_id)
        logger.info(f"UPDATE bop_setting SET setting_value = {setting_value} WHERE discord_id = {discord_id}")


async def get_bop_setting_value(discord_id, pool):
    async with pool.acquire() as conn:
        query = """
        SELECT setting_value
        FROM bop_setting
        WHERE discord_id = $1
        """
        result = await conn.fetchrow(query, discord_id)
        if result:
            setting_value = result['setting_value']
            logger.info(f"Retrieved setting_value: {setting_value} for discord_id: {discord_id}")
            return setting_value
        else:
            logger.info(f"No setting found for discord_id: {discord_id}")
            return None


async def process_report_data(data):
    sum_elo = 0
    sum_sr = 0
    sum_tp = 0
    ban_count = 0
    pp_count = 0
    permaban_count = 0
    back_to_rookies_count = 0
    amount_of_reports = len(data)

    for row in data:
        logger.info(row)
        sum_elo += row['elo'] if row['elo'] else 0
        sum_sr += row['sr'] if row['sr'] else 0
        sum_tp += row['tp'] if row['tp'] else 0

        if row['ban'] == 'TRUE':
            ban_count += 1

        if row['pp'] == 'TRUE':
            pp_count += 1

        if row['permaban'] == 'TRUE':
            permaban_count += 1

        if row['back_to_rookies'] == 'TRUE':
            back_to_rookies_count += 1
            
    sum_elo = int(sum_elo)
    sum_sr = round(sum_sr, 1)
    sum_tp = int(sum_tp)
    
    return {
        'sum_elo': sum_elo,
        'sum_sr': sum_sr,
        'sum_tp': sum_tp,
        'ban_count': ban_count,
        'pp_count': pp_count,
        'permaban_count': permaban_count,
        'back_to_rookies_count': back_to_rookies_count,
        'amount_of_reports': amount_of_reports
    }       


async def get_voice_activity(discord_id, pool) -> int: 
    async with pool.acquire() as conn:
        query = """
        SELECT time_in_voice_channel 
        FROM voice_channel_time 
        WHERE user_id = $1;
        """
        result = await conn.fetchval(query, discord_id)
        if result:
            logger.info(f"Voice Activity for {discord_id}: {result}")
            return result
        else:
            logger.info(f"No voice activity found for discord_id: {discord_id}")
            return 0
        

async def update_voice_activity(discord_id, new_voice_time, pool):
    async with pool.acquire() as conn:
        query = """
        INSERT INTO voice_channel_time (user_id, time_in_voice_channel)
        VALUES ($1, $2)
        ON CONFLICT (user_id) 
        DO UPDATE 
        SET time_in_voice_channel = EXCLUDED.time_in_voice_channel;
        """
        await conn.execute(query, discord_id, new_voice_time)
        logger.info(f"Upsert voice activity time = {new_voice_time} WHERE discord_id = {discord_id}")


async def get_all_voice_activity(pool, limit = 10):
    async with pool.acquire() as conn:
        value = await conn.fetch(f"select * from voice_channel_time order by time_in_voice_channel DESC limit {limit};")
        return value 
    

async def update_last_1000_messages(discord_id, pool):
    async with pool.acquire() as conn:
        query = """
        INSERT INTO last_1000_messages (user_id, messages)
        VALUES ($1, 1)
        ON CONFLICT (user_id)
        DO UPDATE SET messages = last_1000_messages.messages + 1;
        """
        await conn.execute(query, discord_id)
        #logger.info(f"INSERT INTO last_1000_messages (user_id, messages) VALUES ({discord_id}, 1) ON CONFLICT (user_id) DO UPDATE SET messages = last_1000_messages.messages + 1;")


async def get_last_1000_messages_message_count(pool):
    async with pool.acquire() as conn:
        # fetchval returns the first column of the first row directly
        total_messages = await conn.fetchval("SELECT SUM(messages) FROM last_1000_messages;")
        return total_messages
    

async def recreate_last_1000_messages_table(pool):
    async with pool.acquire() as conn:
        query = """
        DROP TABLE IF EXISTS last_1000_messages;
        """
        await conn.execute(query)
        logger.info(f"DROP TABLE IF EXISTS last_1000_messages;")

        query = """
        CREATE TABLE last_1000_messages (
            user_id BIGINT UNIQUE,
            messages INT,
            creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );
        """
        await conn.execute(query)
        logger.info(f"CREATE TABLE last_1000_messages (user_id BIGINT, messages INT, creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);")


async def fetch_last_1000_messages_dict(pool):
    # Acquire a connection from the pool
    async with pool.acquire() as conn:
        # Execute a query to fetch all rows from the table
        rows = await conn.fetch("SELECT user_id, messages FROM last_1000_messages")

        # Convert rows into a dictionary where user_id is the key and messages is the value
        messages_dict = {int(row['user_id']): row['messages'] for row in rows}

        logging.info(messages_dict)

    return messages_dict


async def fetch_user_last_1000_message_count(pool, user_id):
    async with pool.acquire() as conn:
        # Query to get messages and creation_date for the specific user_id
        row = await conn.fetchrow(
            "SELECT messages, creation_date FROM last_1000_messages WHERE user_id = $1", user_id
        )

        # Check if user_id exists, then calculate minutes since creation
        if row:
            messages = row['messages']
            creation_date = row['creation_date']
            
            # Ensure creation_date is timezone-aware
            if creation_date.tzinfo is None:
                creation_date = creation_date.replace(tzinfo=timezone.utc)
            
            current_time = datetime.now(timezone.utc)  # Timezone-aware UTC datetime
            minutes_since_creation = (current_time - creation_date).total_seconds() / 60
            #logging.info(minutes_since_creation)
            return messages, minutes_since_creation
        else:
            return 0, 0
        

async def insert_bop_request(user_id: int, track: str, car: str, laptime: str, pool) -> bool:
    async with pool.acquire() as conn:
        query = """
        INSERT INTO bop_requests (track, car, laptime, user_id)
        VALUES ($1, $2, $3, $4);

        """
        try:
            await conn.execute(query, str(track), str(car), str(laptime), int(user_id))
            logging.info(query.replace("$1", track).replace("$2", car).replace("$3", laptime).replace("$4", str(user_id)))
            return True
        except Exception as e:
            logging.error(f"Failed to insert BOP request: {e}")
            return False


async def get_bop_requests(pool, user_id = None, car = None, track = None):
    # Acquire a connection from the pool
    async with pool.acquire() as conn:
        if user_id:
            # Execute a query to fetch all rows from the table
            rows = await conn.fetch(f"SELECT * FROM bop_requests WHERE user_id = {user_id};")

        if car and not track:
            # Execute a query to fetch all rows from the table
            rows = await conn.fetch(f"SELECT * FROM bop_requests WHERE car = '{car}';")
        
        if track and not car:
            # Execute a query to fetch all rows from the table
            rows = await conn.fetch(f"SELECT * FROM bop_requests WHERE track = '{track}';")

        if car and track:
            # Execute a query to fetch all rows from the table
            rows = await conn.fetch(f"SELECT * FROM bop_requests WHERE car = '{car}' AND track = '{track}';")

        if not car and not track and not user_id:
            # Execute a query to fetch all rows from the table
            rows = await conn.fetch(f"SELECT * FROM bop_requests;")

    return rows


async def delete_bop_request(id: int, user_id: int, pool) -> bool:
    async with pool.acquire() as conn:
        query = """
        DELETE FROM bop_requests
        WHERE id = $1 AND user_id = $2;
        """
        try:
            await conn.execute(query, id, user_id)
            logging.info(f"Deleted BOP request with id={id} and user_id={user_id}")
            return True
        except Exception as e:
            logging.error(f"Failed to delete BOP request: {e}")
            return False


async def update_haram_count(user_id, new_value, pool):
    async with pool.acquire() as conn:
        query = """
            INSERT INTO haram_words (user_id, violations )
            VALUES ($1, $2)
            ON CONFLICT (user_id)
            DO UPDATE SET violations  = EXCLUDED.violations ;
        """
        await conn.execute(query, user_id, new_value)
        #logger.info(f"Updating counters table - {user_id} with {new_value}")


async def get_haram_count(user_id, pool):
    async with pool.acquire() as conn:
        value = await conn.fetchval(f"SELECT violations  FROM haram_words WHERE user_id = {user_id}")
        #logger.info(f"SELECT count FROM counters WHERE counter_name = '{user_id}'")
        #logger.info(f"{user_id} has the value {value}")
        return value


async def get_all_haram_count(limit, pool):
    async with pool.acquire() as conn:
        value = await conn.fetch(f"select * from haram_words order by violations DESC limit {limit};")
        return value 
    

async def execute_query(query, pool):
    async with pool.acquire() as conn:
        await conn.execute(query)