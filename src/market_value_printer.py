import csv
import requests
from operator import itemgetter
from datetime import datetime


# Function to format a row with specific width constraints
def format_row_birthday_talent(name, birthday, talent):
    name = (name[:32] + '...') if len(name) > 35 else name.ljust(35)
    birthday = birthday.ljust(10)
    talent = f"{float(talent):.1f}".rjust(9)
    return f"{name}{birthday}{talent}"


# Parse the birthday to a consistent format
def parse_birthday(birthday_str):
    for fmt in ("%d/%m/%Y", "%d/%m/%y"):
        try:
            return datetime.strptime(birthday_str, fmt)
        except ValueError:
            continue
    return None  # Return None if no valid format found


# Collect and sort data by either 'birthday' or 'talent'
def sort_and_print_birthday_talent(data, sort_by):
    message = ""
    if sort_by == 'birthday':
        # Parse and sort by birthday as datetime
        sorted_data = sorted(data, key=lambda row: parse_birthday(row['birthday']) or datetime.max)
    elif sort_by == 'talent':
        # Convert talent to float and sort
        sorted_data = sorted(data, key=lambda row: float(row['talent']), reverse=True)
    else:
        print("Invalid sort_by value. Use 'birthday' or 'talent'.")
        return
    

    # Print the formatted rows
    message += (f"{'Name'.ljust(35)}{'Birthday'.ljust(10)}{'Talent'.rjust(9)}\n")
    message += ('-' * 54)
    message += "\n"
    for row in sorted_data:
        message += (format_row_birthday_talent(row['name'], row['birthday'], row['talent']))
        message += "\n"

    return message


# Function to format a row with specific width constraints
def format_row_score(name, score):
    name = (name[:32] + '...') if len(name) > 35 else name.ljust(35)
    score = f"{float(score):.1f}".rjust(9)
    return f"{name}{score}"


# Collect and sort data by 'score'
def sort_and_print_score(data):
    message = ""
    # Convert score to float and sort
    sorted_data = sorted(data, key=lambda row: float(row['score']), reverse=True)
    
    # Print the formatted rows
    message += (f"{'Name'.ljust(35)}{'Score'.rjust(9)}\n")
    message += ('-' * 44)
    message += "\n"
    for row in sorted_data:
        message += (format_row_score(row['name'], row['score']))
        message += "\n"

    return message


def split_message(message, max_length=1900):
    messages = []  # List to store the message parts
    while len(message) > max_length:
        # Find the last occurrence of '\n' before or at the max_length
        split_index = message.rfind('\n', 0, max_length)

        if split_index == -1:
            # If no '\n' is found within max_length, just split at max_length
            split_index = max_length

        # Append the part of the message up to the split index
        messages.append(message[:split_index])

        # Update the message to be the remaining part after the split
        message = message[split_index + 1:]  # Skip the '\n'

    # Append any remaining part of the message
    messages.append(message)

    return messages