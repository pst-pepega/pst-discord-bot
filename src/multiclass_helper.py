import re

def calculate(cars):

    result = []

    for key1 in cars:
        if key1 != "length":
            for key2 in cars:
                if key2 != "length":
                    print(f"{key1} vs {key2}")
                    temp_a = 0
                    temp_b = 0
                    laps_class_a = 0
                    laps_class_b = 0
                    race_finished = False
                    if key1 != key2:
                        while not race_finished:
                            temp_a += cars[key1]
                            laps_class_a += 1
                            temp_b += cars[key2]
                            laps_class_b += 1
                            print(f"{temp_b} {temp_b}")
                            if temp_a + cars[key1] < temp_b:
                                result.append((laps_class_a, f"{key1}, {key2}"))
                                temp_a += cars[key1]
                            if temp_a > int(cars['length']) and temp_b > int(cars['length']):
                                race_finished = True

    # Use sorted with a lambda function as the key
    sorted_data = sorted(result, key=lambda x: x[0])

    # Now sorted_data contains the sorted list of tuples
    return sorted_data