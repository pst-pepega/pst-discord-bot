import re
import database
import logging
import discord

logger = logging.getLogger(__name__)

def parse_prediction_command(command):
    command = command.replace("!predict ", "")
    allowed_names = ["Valentin Barrier", "Luke Whitehead", "Joshua Allan", "Samir Foch", "Jaroslav Honzik", "Aidan Walsingham", "Jimmy Holm", "Geert Fischer", "Jonathan Clifford", "Ahmet Serdar Kargulu", "Luca Ziege", "Ryan Cooper"]
    # Initialize an empty dictionary to store the results
    results = {}

    # Use regular expressions to find name-value pairs in the command
    name_value_pairs = re.findall(r'\s*([\w\s]+):\s*(\d+)', command)

    # Iterate through the name-value pairs and add them to the dictionary
    for name, value in name_value_pairs:
        # Check if the name is allowed
        if name in allowed_names:
            results[name] = int(value)
        else:
            logger.info(f"Ignoring invalid name: {name}")

    return results


async def process(week, client: discord.Client, week_before_data = None):

    if week_before_data != None:
        #logger.info(f"week_before 0: {week_before_data[0]}")
        # Sort the list of tuples based on the second element of each tuple
        week_before_data[0] = dict(sorted(week_before_data[0].items(), key=lambda x: x[1]))
        #logger.info(f"week_before 1: {week_before_data[1]}")
        for tier, tier_dict in week_before_data[1].items():
            # Sort the dictionary by values
            sorted_dict = sorted(tier_dict.items(), key=lambda x: x[1])
            week_before_data[1][tier] = dict(sorted_dict)



    query = f"""
    SELECT
    p.user_id,
    d.tier,
    SUM(ABS(p.prediction - s.{week})) AS total_prediction_difference
    FROM
        prediction AS p
    JOIN
        driver AS d
    ON
        d.driver_name = p.driver_name
    JOIN
        standings AS s
    ON
        s.driver_name = d.driver_name
    WHERE
        s.{week} IS NOT NULL
    GROUP BY
        p.user_id, d.tier;
    """
    pool = await database.create_pool()
    result = await database.get_reports(pool, query)

    # Dictionaries to store differences for each tier and all tiers combined
    tier_dicts = {}
    all_tiers_dict = {}

    # Loop through the result and store differences in the dictionaries
    for row in result:
        user_id = row['user_id']
        username = client.fetch_user(user_id)
        tier = row['tier']
        total_prediction_difference = row['total_prediction_difference']

        # Create or update the dictionary for the specific tier
        if tier not in tier_dicts:
            tier_dicts[tier] = {}
        if username in tier_dicts[tier]:
            tier_dicts[tier][username] += total_prediction_difference
        else:
            tier_dicts[tier][username] = total_prediction_difference

        # Update the dictionary for all tiers combined
        if username in all_tiers_dict:
            all_tiers_dict[username] += total_prediction_difference
        else:
            all_tiers_dict[username] = total_prediction_difference

    # Now you have 'tier_dicts' containing differences for each tier separately
    # and 'all_tiers_dict' containing differences for all tiers combined

    # Sort the dictionary by values
    #logger.info(f"all tier dict: {all_tiers_dict}")
    all_tiers_dict = dict(sorted(all_tiers_dict.items(), key=lambda x: x[1]))
    #logger.info(f"tier_dict: {tier_dicts}")
    for tier, tier_dict in tier_dicts.items():
        sorted_dict = sorted(tier_dict.items(), key=lambda x: x[1])
        tier_dicts[tier] = dict(sorted_dict)  # Update the original dictionary

    if week_before_data == None:
        result = []
        result.append(all_tiers_dict)
        result.append(tier_dicts)
        if pool:
            await pool.close()
        logger.info("Database disconnected.")
        return result
    
    to_print = []
    to_print.append("")
    num_tiers = len(tier_dicts)
    for _ in range(num_tiers):
        to_print.append("")
    
    
    # To print the results for each tier
    tier_count = 0
    for tier, diff_dict in tier_dicts.items():

        tier_count += 1

        #logger.info(f'Tier {tier}:\n')
        to_print[tier_count] += f'Tier {tier}:\n\n```'
        Delta = 'Δ'
        Position = "Pos."
        Name = "Name"
        Score = "Score"
        Delta2 = "Δ"
        to_print[tier_count] += f'{Delta:3} {Position:4} {Name:30} {Score:6} {Delta2}\n'
        position = 0

        for username, total_diff in diff_dict.items():

            delta_score = total_diff - week_before_data[1][tier_count][username]
            formatted_number = f"+{delta_score}" if delta_score >= 0 else str(delta_score)
            user = f"{(username)}"
            position += 1
            #logger.info(week_before_data[1][tier_count])
            #logger.info(week_before_data[1])
            delta_position = position - find_position(week_before_data[1][tier_count], username) # type: ignore
            formatted_position = f"+{delta_position}" if delta_position >= 0 else str(delta_position)


            #logger.info(f'[{formatted_position}]{position}. {user:20}: {total_diff} {formatted_number}')
            to_print[tier_count] += f"{formatted_position:3} {position:4} {user:30} {total_diff:6} {formatted_number}\n"
        
        to_print[tier_count] += "```"

    # To print the results for all tiers combined
    #logger.info('Total:')
    to_print[0] += f'Total:\n\n```'
    Delta = 'Δ'
    Position = "Pos."
    Name = "Name"
    Score = "Score"
    Delta2 = "Δ"
    to_print[0] += f'{Delta:3} {Position:4} {Name:30} {Score:6} {Delta2}\n'
    position = 0

    for username, total_diff in all_tiers_dict.items():

        delta_score = total_diff - week_before_data[0][username]
        formatted_number = f"+{delta_score}" if delta_score >= 0 else str(delta_score)
        user = f"{username}"
        position += 1
        delta_position = position - find_position(week_before_data[0], username) # type: ignore
        #logger.info(week_before_data)
        formatted_position = f"+{delta_position}" if delta_position >= 0 else str(delta_position)


        #logger.info(f'[{formatted_position}]{position}. {user:20}: {total_diff} {formatted_number}')
        to_print[0] += f"{formatted_position:3} {position:4} {user:30} {total_diff:6} {formatted_number}\n"

    to_print[0] += "```"
    if pool:
        await pool.close()
    #logger.info("Database disconnected.")
    return to_print


def find_score(input_string, username_to_find):

    # Create a regular expression pattern to find the username and total_diff
    pattern = r'{}: (\d+)'.format(re.escape(username_to_find))

    # Search for the pattern in the input string
    match = re.search(pattern, input_string)

    # Check if a match was found
    if match:
        total_diff = match.group(1)
        print(f"Total Difference for {username_to_find}: {total_diff}")
    else:
        print(f"Username {username_to_find} not found")


def find_position(dictionary, search):
    # Find the position of a specific key in a dictionary
    keys = list(dictionary.keys())
    if search in keys:
        return keys.index(search) + 1
    return None  # Return None if the key is not found
