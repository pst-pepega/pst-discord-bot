import datetime
import math
import random
import threading
from queue import Queue
import logging
import statistics
import matplotlib.pyplot as plt

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

class WeatherSim:
    def __init__(self, rain_level_config, cloud_level_config, randomness, ambient_temp_config):
        self.rain_level_config = rain_level_config
        self.cloud_level_config = cloud_level_config
        self.ambient_temp_config = ambient_temp_config
        self.randomness = max(0, min(randomness, 7))

        # Factor used for scaling the randomness
        randomness_factor = self.randomness / 7

        # How many passes the noise function does
        self.noise_passes = 0
        if self.randomness > 0:
            self.noise_passes = math.floor(randomness_factor * 10) + 4

        # Generates the noise coefficient array
        self.noise_coefficients = []
        if self.randomness > 0:
            variance = randomness_factor * 0.4
            r = self._constrain(self._rand_normal_dist(0, variance), -2 * variance, 2 * variance)
            self.noise_coefficients.append(r * 0.2)
            for i in range(1, self.noise_passes):
                r = self._constrain(self._rand_normal_dist(0, variance), -2 * variance, 2 * variance)
                self.noise_coefficients.append((2.5 / self.noise_passes) * r)

        # Other coefficients
        self.other_coefficients = [self._rand_normal_dist(self._constrain(0, -1.6, 1.6), 1.3)]

        # Base temperature for temperature calculation
        self.base_temp = ambient_temp_config + 1 - 2 * random.random() * randomness_factor


    def calculate_weather(self, weektime):
        result = {
            'noise': 0,
            'rain_level': self.rain_level_config,
            'cloud_level': self.cloud_level_config,
            'brightness': 0,
            'ambient_temp': 0,
            'track_temp': 0,
        }

        if self.noise_passes == 0:
            # Skip rain and cloud calculation
            result['noise'] = self.rain_level_config
            result['rain_level'] = self.rain_level_config
            result['cloud_level'] = self.cloud_level_config
        else:
            # Calculate noise function
            noise = self.rain_level_config
            noise += weektime / 86400 * self.noise_coefficients[0]
            for i in range(1, self.noise_passes):
                s = 0
                s = math.sin(weektime * 3.14156 / 86400 + self.other_coefficients[0])
                s = math.sin((s * 86400 / 3.14156 + weektime) * i * 2 * 3.14156 * 2 / 86400)
                noise += s * self.noise_coefficients[i]

            result['noise'] = noise

            # Cloud level is cloud level config + noise
            cloud_level = noise + self.cloud_level_config
            result['cloud_level'] = self._constrain(cloud_level, 0, 1)

            # Rain is only allowed to exist if the cloud level is at least 0.6
            # If the rain level is zero and the randomness is less than four,
            # rain is always zero.
            result['rain_level'] = 0
            if (self.rain_level_config > 0 or self.randomness > 3) and result['cloud_level'] >= 0.6:
                rain_level = (cloud_level - 0.6) * 1.4875 + 0.15
                rain_level = self._constrain(rain_level, 0, noise)
                result['rain_level'] = self._constrain(rain_level, 0, 1)

        # Brightness is a cosine with a period of one day (86400 seconds)
        # and offset by two hours (7200 seconds)
        result['brightness'] = -0.2 - math.cos((weektime - 7200) / 86400 * 3.14156 * 2)

        # Hotness factor goes up with increasing temperature.
        # Hotness factor is equal to one when the temperature is 25 degrees.
        hotness_factor = self.base_temp / 25

        # Ambient temperature
        result['ambient_temp'] = self.base_temp + (6 - result['cloud_level'] * 3) * result['brightness'] - \
                                (result['rain_level'] * 4 + result['cloud_level']) * hotness_factor

        # Track temperature
        brightness_clipped = max(result['brightness'], 0)
        result['track_temp'] = result['ambient_temp'] + \
                               (result['ambient_temp'] + 20) * (0.25 - 0.125 * (result['cloud_level'] + result['rain_level']) * hotness_factor) * brightness_clipped

        return result


    def set_seeds(self, noise_coefficients, other_coefficients, base_temp):
        self.noise_coefficients = noise_coefficients
        self.other_coefficients = other_coefficients
        self.base_temp = base_temp


    def set_rain_level_config(self, new_rain_level_config):
        self.rain_level_config = new_rain_level_config


    def set_cloud_level_config(self, new_cloud_level_config):
        self.cloud_level_config = new_cloud_level_config


    def set_ambient_temp_config(self, new_ambient_temp_config):
        self.ambient_temp_config = new_ambient_temp_config
        variance = self.randomness / 7
        self.base_temp = self.ambient_temp_config + 1 - random.random() * 2 * variance


    def get_noise_coefficients(self):
        return self.noise_coefficients


    def get_other_coefficients(self):
        return self.other_coefficients


    def get_base_temp(self):
        return self.base_temp


    def _constrain(self, value, minimum, maximum):
        return max(minimum, min(value, maximum))


    def _rand_normal_dist(self, base_value, variance):
        a, b, c = 0, 0, 2
        while c > 1:
            a = random.uniform(-1, 1)
            b = random.uniform(-1, 1)
            c = a * a + b * b

        aa = math.sqrt(math.log(c) * -2 / c) * a
        return base_value + variance * aa
    

    def simulate_weather_duration(self, start_time, duration, results_queue):
        current_time = start_time
        wet_count = 0
        dry_count = 0
        average_wet = []
        average_dry = []
        wet_duration = 0
        dry_duration = 0
        dry = 0
        wet = 0

        start_temps = []
        end_temps = []
        max_temps = []
        min_temps = []
        avg_temps = []

        for _ in range(duration + 1):
            weather_result = self.calculate_weather(current_time)

            #logging.info(weather_result['rain_level'])
            if weather_result['rain_level'] > 0:
                wet_count += 1
            else:
                dry_count += 1
   
            if weather_result['rain_level'] > 0:
                wet += 1
                if dry > 0: average_dry.append(dry)
                dry = 0
            else:
                dry += 1
                if wet > 0: average_wet.append(wet)
                wet = 0

            start_temps.append(weather_result['ambient_temp'])
            end_temps.append(weather_result['ambient_temp'])
            max_temps.append(weather_result['ambient_temp'])
            min_temps.append(weather_result['ambient_temp'])
            avg_temps.append(weather_result['ambient_temp'])

            current_time += 60  # Assuming each iteration is 5 minutes (300 seconds)
        

        if wet > 0: average_wet.append(wet)
        if dry > 0: average_dry.append(dry)
        if len(average_wet) > 0:
            wet_duration = sum(average_wet) / len(average_wet)
        if len(average_dry) > 0:
            dry_duration = sum(average_dry) / len(average_dry)
        #logging.info(f"Dry: {average_dry} — Wet: {average_wet}")
        #logging.info(f"Dry: {dry_duration} — Wet: {wet_duration}")
        results_queue.put({
            'weather_condition': 'mixed' if wet_count > 0 and dry_count > 0 else 'wet' if wet_count > 0 else 'dry',
            'start_temp': start_temps[0],
            'end_temp': end_temps[-1],
            'max_temp': max(max_temps),
            'min_temp': min(min_temps),
            'avg_temp': sum(avg_temps) / len(avg_temps),
            'wet_count': wet_count,
            'dry_count': dry_count,
            'wet_duration': wet_duration,
            'dry_duration': dry_duration,
            'temps': avg_temps
        })


def run_simulation(simulation_count, rain, clouds, randomness, ambient, start, duration, info):
    results_queue = Queue()
    threads = []
    return_string = ""

    # Set the start time and duration for which you want to simulate the weather (in seconds)
    start_time = start  # day 2, hour 17
    simulation_duration = duration  # 60 minutes

    for simulation_number in range(1, simulation_count + 1):
        # Create a new instance of WeatherSim for each simulation
        weather_sim = WeatherSim(rain_level_config=rain, cloud_level_config=clouds, randomness=randomness, ambient_temp_config=ambient)

        thread = threading.Thread(target=weather_sim.simulate_weather_duration, args=(start_time, simulation_duration, results_queue))
        threads.append(thread)
        thread.start()

        #print(f"Simulation {simulation_number} started.")

    for thread in threads:
        thread.join()

    # Process results
    wet_count = dry_count = mixed_count = 0
    avg_start_temp = avg_end_temp = avg_max_temp = avg_min_temp = avg_avg_temp = wet_duration = dry_duration = 0
    all_temps = []

    while not results_queue.empty():
        result = results_queue.get()

        if result['weather_condition'] == 'wet':
            wet_count += 1
        elif result['weather_condition'] == 'dry':
            dry_count += 1
        elif result['weather_condition'] == 'mixed':
            mixed_count += 1

        avg_start_temp += result.get('start_temp', 0)
        avg_end_temp += result.get('end_temp', 0)
        avg_max_temp += result.get('max_temp', 0)
        avg_min_temp += result.get('min_temp', 0)
        avg_avg_temp += result.get('avg_temp', 0)
        wet_duration += result.get('wet_duration', 0)
        dry_duration += result.get('dry_duration', 0)
        all_temps.append(result.get('temps', []))

        #print(f"Simulation {result['weather_condition']} | Wet Count: {result['wet_count']} | Dry Count: {result['dry_count']}")

    avg_start_temp /= simulation_count
    avg_end_temp /= simulation_count
    avg_max_temp /= simulation_count
    avg_min_temp /= simulation_count
    avg_avg_temp /= simulation_count
    wet_duration /= simulation_count
    dry_duration /= simulation_count

    return_string += f"Wet: {round(wet_count / simulation_count * 100, 1)}% — "
    return_string += f"Dry: {round(dry_count / simulation_count * 100, 1)}% — "
    return_string += f"Mixed: {round(mixed_count / simulation_count * 100, 1)}% — "
    #return_string += f"Avg Start Temp: {round(avg_start_temp,3)} — "
    #return_string += f"Avg End Temp: {round(avg_end_temp,3)} — "
    #return_string += f"Avg Max Temp: {round(avg_max_temp,3)} — "
    #return_string += f"Avg Min Temp: {round(avg_min_temp,3)} — "
    #return_string += f"Avg Avg Temp: {round(avg_avg_temp,3)}"
    return_string += f"Avg Temp: {round(avg_avg_temp,3)}"

    if duration > 100:
        return_string += f" — Avg Start Temp: {round(avg_start_temp,3)} — "
        return_string += f"Avg End Temp: {round(avg_end_temp,3)} — "
        return_string += f"Avg Rain Duration: {int(wet_duration)} minutes — "
        return_string += f"Avg Dry Duration: {int(dry_duration)} minutes"

    averages = []
    for sublist in zip(*all_temps):
        averages.append(statistics.mean(sublist))

    print(averages)

    x_values = []  
    y_values = []

    duration = len(averages) * 1 # assuming 1 minute interval

    for i, value in enumerate(averages):

        if i == 0:
            x_values.append(0)
        else:
            x_values.append(x_values[-1] + 1)

        y_values.append(value)

    interval = 60
    plt.figure(figsize=(max(duration/60, 16), 8), dpi=500)
    plt.subplots_adjust(left=0.05, right=0.95, bottom=0.10, top=0.9) 

    for i, (x, y) in enumerate(zip(x_values, y_values)):

        if i % interval == 0:

            y_rounded = round(y, 2)

            plt.text(x+1, y, f'x: {x}\ny: {y_rounded}', fontsize=10,
                    bbox=dict(facecolor='white', edgecolor='black'))

        plt.plot(x_values, y_values) 

    # rest of plotting code

    plt.xlabel('Time (minutes since start of the race)')
    plt.ylabel('Temperature')
    plt.title(f'Averaged Temperature Over Time During The Race: {info}')   
    plt.savefig('graph.png')
 
    return return_string

# Run 1000 simulations
def run_simulations(rain, clouds, randomness, ambient, day, hour, duration, info = "", simulations = 1000):
    logging.info("Simulation started with:\n"
        f"Rain: {rain}\n"
        f"Clouds: {clouds}\n"
        f"Randomness: {randomness}\n"
        f"Temps: {ambient}\n"
        f"Day: {day} & Hour {hour}\n"
        f"Duration: {duration} minutes\n")
    result = run_simulation(simulations, rain, clouds, randomness, ambient, ((day-1)*24+hour*3600), duration, info)

    return result

#print(run_simulations(0.25, 0.3, 0, 17, 3, 12, 1440))



def run_simulation_for_json(simulation_count, rain, clouds, randomness, ambient, start, duration):
    results_queue = Queue()
    threads = []
    return_string = ""

    # Set the start time and duration for which you want to simulate the weather (in seconds)
    start_time = start  # day 2, hour 17
    simulation_duration = duration  # 60 minutes

    for simulation_number in range(1, simulation_count + 1):
        # Create a new instance of WeatherSim for each simulation
        weather_sim = WeatherSim(rain_level_config=rain, cloud_level_config=clouds, randomness=randomness, ambient_temp_config=ambient)

        thread = threading.Thread(target=weather_sim.simulate_weather_duration, args=(start_time, simulation_duration, results_queue))
        threads.append(thread)
        thread.start()

        #print(f"Simulation {simulation_number} started.")

    for thread in threads:
        thread.join()

    # Process results
    wet_count = dry_count = mixed_count = 0
    avg_start_temp = avg_end_temp = avg_max_temp = avg_min_temp = avg_avg_temp = wet_duration = dry_duration = 0
    all_temps = []

    while not results_queue.empty():
        result = results_queue.get()

        if result['weather_condition'] == 'wet':
            wet_count += 1
        elif result['weather_condition'] == 'dry':
            dry_count += 1
        elif result['weather_condition'] == 'mixed':
            mixed_count += 1

        avg_start_temp += result.get('start_temp', 0)
        avg_end_temp += result.get('end_temp', 0)
        avg_max_temp += result.get('max_temp', 0)
        avg_min_temp += result.get('min_temp', 0)
        avg_avg_temp += result.get('avg_temp', 0)
        wet_duration += result.get('wet_duration', 0)
        dry_duration += result.get('dry_duration', 0)
        all_temps.append(result.get('temps', []))

        #print(f"Simulation {result['weather_condition']} | Wet Count: {result['wet_count']} | Dry Count: {result['dry_count']}")

    avg_start_temp /= simulation_count
    avg_end_temp /= simulation_count
    avg_max_temp /= simulation_count
    avg_min_temp /= simulation_count
    avg_avg_temp /= simulation_count
    wet_duration /= simulation_count
    dry_duration /= simulation_count

    return f"{round(wet_count / simulation_count * 100, 1)}%", f"{round(dry_count / simulation_count * 100, 1)}%", f"{round(mixed_count / simulation_count * 100, 1)}%", f"{round(avg_avg_temp,3)}"