from matplotlib import pyplot as plt
import numpy as np
import requests
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging
import json
import datetime
from Utils import Ansi, to_lfm_track_name

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def get_track_id(track_name):

    if "Nordschleife" in track_name:
        return 249

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio) # type: ignore
    return closest_match


def get_all_tracks():

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track ID from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    return track_list


def get_bop_data(track):
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        return data
    except Exception as e:
        print(f'An error occurred: {e}')

        
def get_gt4bop_data(track):
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}&class=GT4'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        return data
    except Exception as e:
        print(f'An error occurred: {e}')


def get_bop(track, bop_data = None):
        if bop_data:
            data = bop_data
        else:
            data = get_bop_data(track)

        # Check if the JSON data has the expected structure
        if 'laps_relevant' in data and 'laps_others' in data: # type: ignore
            relevant_laps = data['laps_relevant'] # type: ignore
            others_laps = data['laps_others'] # type: ignore

            # Convert the relevant laps data to a Pandas DataFrame
            relevant_df = pd.DataFrame(relevant_laps)
            # Add a "relevant" column and set it to True
            relevant_df['relevant'] = 'X'

            # Convert the others laps data to a Pandas DataFrame
            others_df = pd.DataFrame(others_laps)
            # Add a "relevant" column and set it to an empty string
            others_df['relevant'] = ''

            try:
                # Sort the others DataFrame by "bop_raw" in descending order
                others_df = others_df.sort_values(by='bop_raw', ascending=False)
            except:
                others_df = pd.DataFrame()

            # Combine both DataFrames into one
            combined_df = pd.concat([relevant_df, others_df])
            combined_df = combined_df.sort_values(by='bop_raw', ascending=False)
            

            # Reorder columns
            combined_df = combined_df[['car_name', 'car_year', 'target_dif', 'lap', 'bop', 'bop_raw', 'relevant']]
            # Reset the row numbering to start from 1
            combined_df.reset_index(drop=True, inplace=True)
            # Add 1 to the index to start from 1
            combined_df.index += 1

            # Print the combined DataFrame
            return combined_df
        else:
            print('JSON data does not have the expected structure.')


def get_all_bop():
    # Initialize an empty DataFrame to store the combined results
    combined_df = pd.DataFrame()

    # Assuming you have a list of tracks
    track_list = get_all_tracks()

    # Iterate through the track_list
    if track_list:
        for i, track in enumerate(track_list):
            # Calculate the bop for the current track
            df1 = get_bop(track)

            # Calculate the bop for the next track, if available
            #if i < len(track_list) - 1:
            #    next_track = track_list[i + 1]
            #    print(f"Next track: {next_track}")
            #    df2 = get_bop(next_track)
            #else:
                # If there is no next track, create an empty DataFrame
                #df2 = pd.DataFrame()

            # Concatenate the data for the current track and the next track
            combined_track_df = pd.concat([df1, combined_df]) # type: ignore
            combined_df = combined_track_df.groupby(['car_name', 'car_year', 'relevant'], as_index=False)['bop_raw'].sum()
            #print(f"{track}:")
            #print(combined_df)
            #print("-----")

            # Append the combined data for the current track to the combined DataFrame
            #combined_df = pd.concat([combined_df, combined_track_df], ignore_index=True)

    # Group by car_name and car_year and calculate the sum of bop_raw
    #grouped = combined_df.groupby(['car_name', 'car_year', 'relevant'])['bop_raw'].sum().reset_index()

    grouped = combined_df
    # Calculate the bop as described (limited to -40 and 40)
    grouped['bop'] = (grouped['bop_raw'] / 23).clip(lower=-40, upper=40)
    grouped['bop_raw'] = (grouped['bop_raw'] / 23)
    grouped = grouped[['car_name', 'car_year', 'bop', 'bop_raw', 'relevant']]

    # Drop the bop_raw column
    combined_df.drop(columns=['bop_raw'], inplace=True)            

    # Convert 'bop' column to numeric
    grouped['bop'] = pd.to_numeric(grouped['bop'], errors='coerce')
    grouped['bop'] = grouped['bop'].round(2)


    # Convert 'bop' column to numeric
    grouped['bop_raw'] = pd.to_numeric(grouped['bop_raw'], errors='coerce')
    grouped['bop_raw'] = grouped['bop_raw'].round(2)


    # Sort by 'bop' column
    grouped = grouped.sort_values(by='bop_raw', ascending=False)

    # Reset the index
    grouped.reset_index(drop=True, inplace=True)
    grouped.index += 1

    # Now grouped contains the sum of bop_raw values for each car across all tracks
    return grouped


def get_json(track, car_class = 'GT3'):
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}&class={car_class}'
    logging.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        
        track = get_acc_track_name(track)
        if track:
            track = track[:-5]

        output_json = {
            "entries": []
        }

        # Iterate through laps_relevant and laps_others
        for entry in data['laps_relevant'] + data['laps_others']:
            output_entry = {
                "track": track,  # You can set the track name here
                "carModel": entry['car_id'],
                "ballastKg": int(entry['bop'])
            }
            output_json["entries"].append(output_entry)

        # Convert the result to JSON format
        result_json = json.dumps(output_json, indent=4)

        # Write the JSON data to a file
        filename = f"bop.json"
        with open(filename, "w") as file:
            file.write(result_json)

        return filename

    except Exception as e:
        print(f'An error occurred: {e}')


def get_acc_track_name(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['acc_track_name'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_acc_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_acc_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio) # type: ignore
    return closest_match


def get_all_bop_tsv():
    # Initialize an empty list to store DataFrames
    dfs = []

    # List of tracks
    track_list = get_all_tracks()

    # Loop through the tracks
    if track_list:
        for track in track_list:
            # Replace this line with your get_bop(track) function to get the data
            # For demonstration, we'll create a sample DataFrame
            data = get_bop(track)
            
            # Create a DataFrame from the sample data
            df = pd.DataFrame(data)
            
            # Add the track name as a column
            df['track'] = track
            
            # Append the DataFrame to the list
            dfs.append(df)

    # Concatenate the DataFrames
    result_df = pd.concat(dfs, ignore_index=True)

    df = pd.DataFrame(result_df)

    # Pivot the DataFrame
    pivot_df = df.pivot(index=['car_name', 'car_year'], columns=['track'], values='bop')

    # Replace NaN values with "—"
    pivot_df = pivot_df.fillna('—')

    # Flatten the multi-level columns
    # pivot_df.columns = [f'{col[0]}_{col[1]}' for col in pivot_df.columns]

    # Reset index
    pivot_df.reset_index(inplace=True)

    # Display the result
    return pivot_df


def get_json_all_tracks(tracklist):
    # URL to fetch JSON data from
    output_json = {
        "entries": []
    }
    for track in tracklist:
        url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
        try:
            # Send an HTTP GET request to the URL and parse the JSON response
            data = requests.get(url).json()
            
            track = get_acc_track_name(track)
            if track:
                track = track[:-5]

            # Iterate through laps_relevant and laps_others
            for entry in data['laps_relevant'] + data['laps_others']:
                output_entry = {
                    "track": track,  # You can set the track name here
                    "carModel": entry['car_id'],
                    "ballastKg": int(entry['bop'])
                }
                output_json["entries"].append(output_entry)

        except Exception as e:
            print(f'An error occurred: {e}')

            

    # Convert the result to JSON format
    result_json = json.dumps(output_json, indent=4)

    # Write the JSON data to a file
    filename = f"bop.json"
    with open(filename, "a") as file:
        file.write(result_json)

    return filename


def get_targettime(track, bop_data = None):

    # URL to fetch JSON data from
    if bop_data:
        data = bop_data
    else:
        data = get_bop_data(track)
    try:
        if data:
            kgtime = data['bopdata']['target_time']
        return kgtime

    except Exception as e:
        print(f'An error occurred: {e}')

    ##### Old Code #####

    # difference = float(str(result['target_dif'].iloc[0])[4:])
    # laptime = (str(result['lap'].iloc[0]))

    # # Split the time string into minutes and seconds
    # minutes_str, seconds_str = laptime.split(":")
    # minutes = int(minutes_str)
    # seconds = float(seconds_str)

    # # Create a timedelta object with minutes and seconds
    # time_delta = datetime.timedelta(minutes=minutes, seconds=seconds + difference)

    # # Get the total time in seconds as a float
    # total_seconds = time_delta.total_seconds()

    # # Calculate minutes and seconds
    # minutes = int(total_seconds // 60)
    # seconds = round(total_seconds % 60, 3)
    # if seconds < 10:
    #     seconds = f"0{seconds}"

    # # Format as "mm:ss"
    # return f"{minutes}:{seconds}"


def get_realbop(input_track):
    # Send a GET request to the URL to fetch the JSON data
    url = 'https://api2.lowfuelmotorsport.com/api/hotlaps/getAccBop'

    response = requests.get(url)
    data = response.json()

    # Iterate through the tracks in the JSON data
    for track in data:
        if track['track_name'] == input_track:
            return track  # Return the data for the specified track

    # If the track is not found, return None or raise an exception
    return None  # or raise an exception with a custom message


BOPADJUST_CARS = ["McLaren 720S GT3 Evo", "AMR V8 Vantage", "BMW M4 GT3", "Audi R8 LMS GT3 evo II", "Mercedes-AMG GT3", "Porsche 992 GT3 R", "Lamborghini Huracan GT3 EVO 2", "Ferrari 296 GT3", "Bentley Continental", "Honda NSX GT3 Evo", "Nissan GT-R Nismo GT3", "Ford Mustang GT3"]
def get_updated_bop(car, laptime, track):
    car = find_closest_acc_track(BOPADJUST_CARS, car)
    logger.info(car)
    if laptime.startswith("0"):
        pass
    else:
        laptime = f"0{laptime}"
    result_string = ""
    result = get_bop(track)
    result = result[(result['relevant'] == 'X')] # type: ignore

    difference = float(str(result['target_dif'].iloc[0])[4:])
    kg = float(str(result['bop'].iloc[0])[-3:])

    bop_per_kg = round(difference/kg, 5)
    bop_max = round(bop_per_kg * 40, 2)

    result_string += f"{bop_per_kg}s per kg\n"
    result_string += f"BOP Max is {bop_max} seconds\n"

    laptimes_dict = {}
    for index, row in result.iterrows():
        laptimes_dict[row['car_name']] = row['lap']
 
    total_seconds = 0
    result_string += f"The laptime of {car} will be adjusted from {laptimes_dict[car]} to {laptime}\n"
    laptimes_dict[car] = laptime

    for car, lap in laptimes_dict.items():
        minutes_str, seconds_str = lap.split(":")
        minutes = int(minutes_str)
        seconds = float(seconds_str)

        # Get the total time in seconds as a float
        total_seconds += minutes * 60 + seconds 

    # Calculate minutes and seconds
    total_seconds = total_seconds / 12

    minutes = int(total_seconds // 60)
    seconds = round(total_seconds % 60, 3)
    if seconds < 10:
        seconds = f"0{seconds}"

    result_string += (f"Target time is now: {minutes}:{seconds}\n")
    print(laptimes_dict)
    laptimes_dict = dict(sorted(laptimes_dict.items(), key=lambda item: item[1]))
    print(laptimes_dict)


    for car, lap in laptimes_dict.items():
        minutes_str, seconds_str = lap.split(":")
        minutes = int(minutes_str)
        seconds = float(seconds_str)

        # if minutes * 60 + seconds > total_seconds + bop_max:
        #     result_string += f"The slowest car {car} is too slow. We will adjust.\n"
        #     total_seconds = minutes * 60 + seconds - bop_max
        #     minutes = int(total_seconds // 60)
        #     seconds = round(total_seconds % 60, 3)
        #     if seconds < 10:
        #         seconds = f"0{seconds}"
        #     result_string += (f"The Target laptime is now: {minutes}:{seconds}\n")

    fastest_car = list(laptimes_dict.items())[0]
    print(fastest_car)
    print(fastest_car[1])
        
    minutes_str, seconds_str = fastest_car[1].split(":")
    minutes = int(minutes_str)
    seconds = float(seconds_str)
    if minutes * 60 + seconds < total_seconds - bop_max:
        result_string += f"The fastest car {fastest_car[0]} is too fast. We will adjust.\n"
        total_seconds = minutes * 60 + seconds + bop_max
        minutes = int(total_seconds // 60)
        seconds = round(total_seconds % 60, 3)
        if seconds < 10:
            seconds = f"0{seconds}"
        result_string += (f"The Target laptime is now: {minutes}:{seconds}\n")

    ballast_dict = {}

    for car, lap in laptimes_dict.items():
        minutes_str, seconds_str = lap.split(":")
        minutes = int(minutes_str)
        seconds = float(seconds_str)
        ballast_dict[car] = round((total_seconds - minutes * 60 - seconds) / bop_per_kg, 0)


    max_car_name_length = len("Lamborghini Huracan GT3 EVO 2")  # Adjust this as needed
    result_string += "**BOP now as follows:**\n"
    result_string += "```ansi\n"
    old_data = get_bop(track)
    print(old_data)
    for car, ballast in ballast_dict.items():
        # Truncate or pad the car name to fit the maximum length
        formatted_car_name = car[:max_car_name_length] + ':' if len(car) > max_car_name_length else car.ljust(max_car_name_length)
        old_ballast = result = old_data.loc[old_data['car_name'] == car, 'bop'].values[0]
        difference = int(ballast)-int(old_ballast)

        ansi_text = Ansi.Text()
        if difference > 0:
            foreground = Ansi.Foreground.RED
            text = f"{difference}"
        elif difference < 0:
            foreground = Ansi.Foreground.GREEN
            text = f"{difference}"
        else:
            foreground = Ansi.Foreground.YELLOW
            text = f"{difference}"

        ansi_text.set_foreground(foreground).add_text(text).reset()

        ballast = f"+{ballast}" if ballast > 0 else ballast
        result_string += f"{formatted_car_name}\t{ballast:5}\t[{ansi_text.render()}]\n"

    result_string += "```\n"

    return result_string


def get_kg_per_second(track, bop_data_from_function = pd.DataFrame()):
    
    # # URL to fetch JSON data from
    # url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    # logger.info(url)
    # try:
    #     # Send an HTTP GET request to the URL and parse the JSON response
    #     data = requests.get(url).json()

    # except Exception as e:
    #     print(f'An error occurred: {e}')

    # kgtime = data['bopdata']['KgTime']

    # return kgtime
    result = bop_data_from_function
        
    try:
        result = result[(result['relevant'] == 'X')] # type: ignore
    except:
        result = get_bop(track)
        result = result[(result['relevant'] == 'X')] # type: ignore


    difference = float(str(result['target_dif'].iloc[0])[4:])
    kg = float(str(result['bop'].iloc[0])[-3:])

    bop_per_kg = round(difference/kg, 4)
    return bop_per_kg


def time_to_seconds(time_str):
    # Split the time string into minutes, seconds, and milliseconds
    minutes, seconds = time_str.split(':')
    
    # Calculate the total time in seconds
    total_seconds = int(minutes) * 60 + float(seconds)
    
    return total_seconds


def float_to_time(float_value):
    minutes, seconds = divmod(float_value, 60)
    return f"{int(minutes):02d}:{seconds:06.3f}".lstrip("0")


def bop_gt4(track):
    result = ""
        
    # URL to fetch JSON data from
    url = f'{LFM_API}/hotlaps/getFastestLapsPerCar?track={get_track_id(track)}&class=GT4'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()

        # Extract the car_name and lap from the data
        car_names_and_laps = []

        for lap_data in data["laps_others"]:
            car_name = lap_data["car_name"]
            lap_time = round(time_to_seconds(lap_data["lap"]), 3)
            car_names_and_laps.append((car_name, lap_time))
        
        
        # Extract lap times and convert them to seconds
        lap_times_seconds = [time[1] for time in car_names_and_laps]

        # Calculate the average lap time
        average_lap_time = sum(lap_times_seconds) / len(lap_times_seconds)

        for time in car_names_and_laps:
            offset = round((1-(time[1] / average_lap_time)) * 100, 3)
            bop_raw = int(40*offset)
            bop = max(min(bop_raw, 40), -40)
            bop_raw = f"{bop_raw}"
            bop = f"{bop}"
            result += (f"{time[0]:35}\t{float_to_time(time[1])}\t{bop:3}\t{bop_raw:4}\n")
            
        result2 = (float_to_time(average_lap_time))
        return (result, result2)
    
    except:
        print("Error")


def format_number(number):
    # Check if the number is positive or negative
    sign = '+' if number >= 0 else '-'
    
    # Add a space before the sign if the number is a single digit
    formatted_number = f" {sign}{abs(number)}" if abs(number) < 10 else f"{sign}{number}"
    
    # Print the result
    return formatted_number


def bop_next(track, car_class = "GT3"):
    result = ""
    data1 = get_realbop(find_closest_track(get_all_tracks(), track))

    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}&class={car_class}'
    # Send an HTTP GET request to the URL and parse the JSON response
    data2 = requests.get(url).json()

    cars = dict()
    if data1:
        for entry in data1['bop'][car_class]:
            car_name = entry['car_name']
            cars[car_name] = entry['ballast']
        
    for entry in data2['laps_relevant']:
        car_name = entry['car_name']
        if car_name not in cars:
            cars[car_name] = 0
        
    for entry in data2['laps_relevant']:
        if int(entry['bop']) > 0:
            car_name = entry['car_name']
            if car_name not in cars:
                cars[car_name] = 0

    logger.info(cars)
    cars = sorted(cars.items(), key=lambda x: x[1], reverse=True)
    
    # Extract car_name and ballast from the first JSON
    for car_name, ballast in cars:

        # Find the corresponding entry in the second JSON
        bop = None  # Initialize to None in case the entry is not found in either laps_relevant or laps_others

        for lap_entry in data2['laps_relevant']:
            if lap_entry['car_name'] == car_name:
                bop = lap_entry['bop']
                break

        # If the entry is not found in laps_relevant, search in laps_others
        if bop is None:
            for lap_entry in data2['laps_others']:
                if lap_entry['car_name'] == car_name:
                    bop = lap_entry['bop']
                    break
                
        try:        
            diff = f"[{format_number(int(bop) - int(ballast))}]" # type: ignore
            bop = f"{bop}"
            
            # Print the results

            ansi_text = Ansi.Text()
            if int(bop) - int(ballast) > 0:
                foreground = Ansi.Foreground.RED
                text = f"[{(diff[2:])}"
            elif int(bop) - int(ballast) < 0:
                foreground = Ansi.Foreground.GREEN
                text = f"[{(diff[2:])}"
            else:
                foreground = Ansi.Foreground.YELLOW
                text = f"[{(diff[2:])}"

            ansi_text.set_foreground(foreground).add_text(text).reset()

            result += (f"{car_name:30} {bop.rjust(5)} {ansi_text.render()}\n")
            #result += (f"{car_name:30} {bop.rjust(5)} {diff}\n")
        except:
            logger.info("Error, no bop found for this.")
            bop = "N/A"
            result += (f"{car_name:30} {bop.rjust(5)}\n")

    logger.info(result)
    return result


async def get_bop_test_async(track):
        
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()

        # Check if the JSON data has the expected structure
        if 'laps_relevant' in data and 'laps_others' in data:
            relevant_laps = data['laps_relevant']
            others_laps = data['laps_others']

            # Convert the relevant laps data to a Pandas DataFrame
            relevant_df = pd.DataFrame(relevant_laps)
            # Add a "relevant" column and set it to True
            relevant_df['relevant'] = 'X'

            # Convert the others laps data to a Pandas DataFrame
            others_df = pd.DataFrame(others_laps)
            # Add a "relevant" column and set it to an empty string
            others_df['relevant'] = ''

            # Sort the others DataFrame by "bop_raw" in descending order
            others_df = others_df.sort_values(by='bop_raw', ascending=False)

            # Combine both DataFrames into one
            combined_df = pd.concat([relevant_df, others_df])
            combined_df = combined_df.sort_values(by='bop_raw', ascending=False)

            # Reorder columns
            combined_df = combined_df[['car_name', 'car_year', 'target_dif', 'lap', 'bop', 'bop_raw', 'relevant']]
            # Reset the row numbering to start from 1
            combined_df.reset_index(drop=True, inplace=True)
            # Add 1 to the index to start from 1
            combined_df.index += 1

            # Print the combined DataFrame
            return combined_df
        else:
            print('JSON data does not have the expected structure.')
    except Exception as e:
        print(f'An error occurred: {e}')


def get_enabled_tracks():
    track_list = get_all_tracks()
    bop_tracks = []
    result = "Enabled Tracks: "

    if track_list != None:
        for track in track_list:
            if get_realbop(track) != None:
                bop_tracks.append(track)    
            
        bop_tracks.sort()
        for track in bop_tracks:
            result += f"{track}, "

    result = f"{result[:-2]}."
    return result


def fetch_hotlaps_data(track, car_class = "GT3"):
    url = f"https://api2.lowfuelmotorsport.com/api/hotlaps?track={get_track_id(track)}&class={car_class}&car=&source="
    print(url)
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        print("Failed to fetch data from API")
        return None


# Function to cross-reference and extract data
def extract_data(hotlaps_data, car_name, lap_time):
    for entry in hotlaps_data['data']:
        if entry['lap'] == lap_time and entry['car_name'] == car_name:
            return f"{entry.get('vorname')} {entry.get('nachname')}"
    return f"🤷"


def get_bop_for_season(tracks):

    # Initialize an empty DataFrame
    final_df = pd.DataFrame()

    # List of tracks
    actual_tracks = []
    track_list = get_all_tracks()

    # Iterate over tracks
    for track in tracks:

        track = to_lfm_track_name(track)
        
        real_track = find_closest_track(track_list, track)
        actual_tracks.append(real_track)
        # Get bop for the track
        track_df = get_bop(track)
        
        # Rename 'bop' column to the track name
        track_df.rename(columns={'bop': track[:4]}, inplace=True) # type: ignore
        
        # Merge with final_df
        if not final_df.empty:
            final_df = pd.merge(final_df, track_df[['car_name', 'relevant', track[:4]]], on=['car_name', 'relevant'], how='left') # type: ignore
        else:
            final_df = track_df[['car_name', 'relevant', track[:4]]] # type: ignore
        
        # Display intermediate result
        logger.info("Intermediate DataFrame after merging track", track[:4], ":")
        logger.info(final_df)
        logger.info("")

    # Display the final DataFrame
    logger.info("Final DataFrame:")
    logger.info(final_df)

    # Exclude non-numeric columns from average calculation
    exclude_cols = ['car_name', 'car_year', 'relevant']
    numeric_cols = [col for col in final_df.columns if col not in exclude_cols]
    final_df[numeric_cols] = final_df[numeric_cols].apply(pd.to_numeric, errors='coerce', downcast='integer').fillna(0)

    # Convert 'bop' columns to numeric
    final_df[numeric_cols] = final_df[numeric_cols].apply(pd.to_numeric, errors='coerce', downcast='integer')

    # Calculate average bop across all tracks
    final_df['average_bop'] = final_df[numeric_cols].mean(axis=1)

    # Sort by average_bop column in descending order
    final_df['average_bop'] = final_df['average_bop'].astype(float)
    final_df.sort_values(by='average_bop', ascending=False, inplace=True)
    final_df['average_bop'] = final_df['average_bop'].map(lambda x: f"{x:.2f}")

    # Reset index
    final_df.reset_index(drop=True, inplace=True)

    # Display the final DataFrame
    logger.info("Final DataFrame with average BOP column and sorted by average BOP:")
    final_df.rename(columns={'relevant': 'rel'}, inplace=True)
    logger.info(final_df)
    return (final_df, actual_tracks)



# print(get_updated_bop("Bentley", "1:58.444", "Suzuka"))
# nords_bop_data = (get_bop("Nordschleife"))
# print(nords_bop_data)
# nords_bop_data['driver_name'] = nords_bop_data['lap'].apply(lambda x: extract_data(fetch_hotlaps_data("Nordschleife"), x)) # type: ignore
# print(nords_bop_data)

#print(get_bop("Misano"))
# track_list = ["Misano", "Monza", "Spa", "Suzuka", "Hungaroring", "Glen", "Nordschleife"]
# df, track_list = get_bop_for_season(track_list)
# print(f"We used the following tracks for the calculation: {track_list}")
# print(df.to_string())

#print(get_bop("Imola"))
# import time
# input_track = track = "Autodromo Enzo e Dino Ferrari"

# start_time = time.time()
# bop_data = get_bop_data(track)
# result = get_bop(track, bop_data)

# intermediate_time = round(time.time() - start_time, 3)
# print(f"**{input_track}**\n"
#                                                         f"```\n{result.to_string(index=True)}\n" # type: ignore
#                                                         f"```\n**Target Laptime:** {get_targettime(track, bop_data)}\n"
#                                                         f"**Seconds per kg:** {get_kg_per_second(track, result)}\n"
#                                                         f"!realbop [track] or !bopcurrent [track] might be useful for you, if you just want to see the bop, and you don't care about the lap times. 👍\n"
#                                                         f"!bopsmall [track] is smaller, if you have a smaller screen.\nThis request took {intermediate_time} seconds.")
# hotlap_data = fetch_hotlaps_data(track)                  
# result['driver_name'] = result.apply(lambda row: extract_data(hotlap_data, row['car_name'], row['lap']), axis=1) # type: ignore
# print(f"**{input_track}**\n"
#                                                         f"```\n{result.to_string(index=True)}\n" # type: ignore
#                                                         f"```\n**Target Laptime:** {get_targettime(track, bop_data)}\n"
#                                                         f"**Seconds per kg:** {get_kg_per_second(track, result)}\n"
#                                                         f"!realbop [track] or !bopcurrent [track] might be useful for you, if you just want to see the bop, and you don't care about the lap times. 👍\n"
#                                                         f"!bopsmall [track] is smaller, if you have a smaller screen.\n"
#                                                         f"This request took {intermediate_time} seconds. Adding Hotlap drivers took {round(time.time() - start_time - intermediate_time, 3)} seconds.")                   # type: ignore

