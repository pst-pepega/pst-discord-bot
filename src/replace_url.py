import re

def replace_twitch_with_txitch(url):
    if "/clip/" in url:
        temp = re.sub(r'https?:\/\/(?:clips\.twitch\.tv|(?:www\.)?twitch\.tv\/\w+\/clip)\/([A-Za-z0-9\-_]+)', r'https://clips.fxtwitch.tv/\1', url)
        return f"[You post it, maybe it will work then](<{temp}>)"
    else:
        return url.replace("twitch", "fxtwitch")

def replace_twitter_link_with_fxtwitter(link):
    return link.replace("twitter", "fxtwitter")

def replace_x_link_with_fixupx(link):
    return link.replace("x", "fixupx")

def replace_tiktok_link_with_tiktokez(link):
    return link.replace("tiktok", "tiktokez")

def replace_instagram_link_with_ddinstagram(link):
    return link.replace("instagram", "ddinstagram")

def replace_fandom_with_breezewiki(link):
    if ".com/wiki/" in link:
        return link.replace("fandom", "breezewiki")
    return None   

def replace_instagram_with_instagramez(link):
    # Use regular expression to find the URL in the message content
    url_pattern = r"(https?://\S+)"
    match = re.search(url_pattern, link)

    if match:
        url = match.group()
        if "instagram.com" in url and "/reel" in url:
            # Replace only the specific part of the URL
            new_url = url.replace("instagram", "instagramez")
            new_message = link.replace(url, new_url)
            return new_message
    return None