import requests
import logging
from typing import Optional

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def get_stats(user_id):
        
    url = f"{LFM_API}/users/getUserStats/{user_id}"
    result = ""
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        wins = data['wins']
        podiums = wins + data['podium']
        top5 = podiums + data['top5']
        top10 = top5 + data['top10']
        try:
            result = f"Wins: {wins}, Podiums: {podiums}, Top 5: {top5}, Top 10: {top10}, Poles: {data['poles']}, Races: {data['starts']} ({data['ranked']}), AVG IP: {data['avg_ip']},  Zero IP: {data['zero_ip']}"
        except:
            result = f"Wins: {wins}, Podiums: {podiums}, Top 5: {top5}, Top 10: {top10}, Poles: {data['poles']}, Races: {data['starts']} ({data['ranked']})"
        return result
# {"wins":34,"podium":145,"top5":148,"top10":259,"poles":31,"starts":916,"dnf":27,"podiumrate":20,"ranked":773,"avg_ip":10.5089,"zero_ip":114}
            