import requests
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging
import json
import datetime
from Utils import Ansi

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
LFM_API = "https://api2.lowfuelmotorsport.com/api"


def get_track_id(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def get_all_tracks():

    # Make a GET request to the API
    response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if response.status_code != 200:
        logger.warning("Failed to get track ID from LFM API")
        return None

    # Parse the JSON response
    data = response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155 or track['track_id'] == 223 or track['track_id'] == 249]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['track_id'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    return track_list


def get_bop(track):
        
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}&class=GT4'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        print(data)

        # Check if the JSON data has the expected structure
        if 'laps_relevant' in data and 'laps_others' in data:
            relevant_laps = data['laps_relevant']
            others_laps = data['laps_others']
            

            # Convert the relevant laps data to a Pandas DataFrame
            relevant_df = pd.DataFrame(relevant_laps)
            # Add a "relevant" column and set it to True
            relevant_df['relevant'] = 'X'

            print(relevant_df)

            # Convert the others laps data to a Pandas DataFrame
            others_df = pd.DataFrame(others_laps)


            if others_df.empty:
                relevant_df = relevant_df.sort_values(by='bop_raw', ascending=False)

                # Reorder columns
                relevant_df = relevant_df[['car_name', 'car_year', 'target_dif', 'lap', 'bop', 'bop_raw', 'relevant']]
                # Reset the row numbering to start from 1
                relevant_df.reset_index(drop=True, inplace=True)
                # Add 1 to the index to start from 1
                relevant_df.index += 1
                
                # shorten names
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='Alpine A110 GT4', value='Alpine')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='Porsche 718 Cayman GT4 Clubsport', value='Porsche')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='Mercedes AMG GT4', value='Mercedes')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='McLaren 570S GT4', value='McLaren')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='Ginetta G55 GT4', value='Ginetta')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='Audi R8 LMS GT4', value='Audi')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='KTM X-Bow GT4', value='KTM')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='Chevrolet Camaro GT4', value='Chevrolet')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='Aston Martin Vantage GT4', value='Aston Martin')
                relevant_df['car_name'] = relevant_df['car_name'].replace(to_replace='BMW M4 GT4', value='BMW')
                
                relevant_df = relevant_df.rename(columns={'relevant': 'rel'})
                relevant_df = relevant_df.rename(columns={'bop_raw': 'raw'})


                return relevant_df

            else:

                # Add a "relevant" column and set it to an empty string
                others_df['relevant'] = ''

                # Sort the others DataFrame by "bop_raw" in descending order
                others_df = others_df.sort_values(by='bop_raw', ascending=False)

                # Combine both DataFrames into one
                combined_df = pd.concat([relevant_df, others_df])
                combined_df = combined_df.sort_values(by='bop_raw', ascending=False)

                # Reorder columns
                combined_df = combined_df[['car_name', 'car_year', 'target_dif', 'lap', 'bop', 'bop_raw', 'relevant']]
                # Reset the row numbering to start from 1
                combined_df.reset_index(drop=True, inplace=True)
                # Add 1 to the index to start from 1
                combined_df.index += 1
                
                # shorten names
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Alpine A110 GT4', value='Alpine')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Porsche 718 Cayman GT4 Clubsport', value='Porsche')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Mercedes AMG GT4', value='Mercedes')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='McLaren 570S GT4', value='McLaren')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Ginetta G55 GT4', value='Ginetta')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Audi R8 LMS GT4', value='Audi')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='KTM X-Bow GT4', value='KTM')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Chevrolet Camaro GT4', value='Chevrolet')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Aston Martin Vantage GT4', value='Aston Martin')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='BMW M4 GT4', value='BMW')
                combined_df['car_name'] = combined_df['car_name'].replace(to_replace='Maserati MC GT4', value='Maserati')
                
                combined_df = combined_df.rename(columns={'relevant': 'rel'})
                combined_df = combined_df.rename(columns={'bop_raw': 'raw'})


                # Print the combined DataFrame
                return combined_df
        else:
            print('JSON data does not have the expected structure.')
    except Exception as e:
        print(f'An error occurred: {e}')


def get_all_bop():
    # Initialize an empty DataFrame to store the combined results
    combined_df = pd.DataFrame()

    # Assuming you have a list of tracks
    track_list = get_all_tracks()

    # Iterate through the track_list
    for i, track in enumerate(track_list):
        # Calculate the bop for the current track
        df1 = get_bop(track)

        # Calculate the bop for the next track, if available
        #if i < len(track_list) - 1:
        #    next_track = track_list[i + 1]
        #    print(f"Next track: {next_track}")
        #    df2 = get_bop(next_track)
        #else:
            # If there is no next track, create an empty DataFrame
            #df2 = pd.DataFrame()

        # Concatenate the data for the current track and the next track
        combined_track_df = pd.concat([df1, combined_df])
        combined_df = combined_track_df.groupby(['car_name', 'car_year', 'relevant'], as_index=False)['bop_raw'].sum()
        #print(f"{track}:")
        #print(combined_df)
        #print("-----")

        # Append the combined data for the current track to the combined DataFrame
        #combined_df = pd.concat([combined_df, combined_track_df], ignore_index=True)

    # Group by car_name and car_year and calculate the sum of bop_raw
    #grouped = combined_df.groupby(['car_name', 'car_year', 'relevant'])['bop_raw'].sum().reset_index()

    grouped = combined_df
    # Calculate the bop as described (limited to -40 and 40)
    grouped['bop'] = (grouped['bop_raw'] / 23).clip(lower=-40, upper=40)
    grouped['bop_raw'] = (grouped['bop_raw'] / 23)
    grouped = grouped[['car_name', 'car_year', 'bop', 'bop_raw', 'relevant']]

    # Drop the bop_raw column
    combined_df.drop(columns=['bop_raw'], inplace=True)            

    # Convert 'bop' column to numeric
    grouped['bop'] = pd.to_numeric(grouped['bop'], errors='coerce')
    grouped['bop'] = grouped['bop'].round(2)


    # Convert 'bop' column to numeric
    grouped['bop_raw'] = pd.to_numeric(grouped['bop_raw'], errors='coerce')
    grouped['bop_raw'] = grouped['bop_raw'].round(2)


    # Sort by 'bop' column
    grouped = grouped.sort_values(by='bop_raw', ascending=False)

    # Reset the index
    grouped.reset_index(drop=True, inplace=True)
    grouped.index += 1

    # Now grouped contains the sum of bop_raw values for each car across all tracks
    return grouped


def get_json(track):
    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        
        track = get_acc_track_name(track)
        track = track[:-5]

        output_json = {
            "entries": []
        }

        # Iterate through laps_relevant and laps_others
        for entry in data['laps_relevant'] + data['laps_others']:
            output_entry = {
                "track": track,  # You can set the track name here
                "carModel": entry['car_id'],
                "ballastKg": int(entry['bop'])
            }
            output_json["entries"].append(output_entry)

        # Convert the result to JSON format
        result_json = json.dumps(output_json, indent=4)

        # Write the JSON data to a file
        filename = f"bop.json"
        with open(filename, "w") as file:
            file.write(result_json)

        return filename

    except Exception as e:
        print(f'An error occurred: {e}')


def get_acc_track_name(track_name):

    track_list_response = requests.get(f"{LFM_API}/lists/getTracks")

    # Check if the request was successful (status code 200)
    if track_list_response.status_code != 200:
        logger.warn("Failed to get list of track IDs from LFM API")
        return None

    data = track_list_response.json()

    # Filter tracks with track_id between 124 and 155
    filtered_tracks = [track for track in data if 124 <= track['track_id'] <= 155]

    # Create a dictionary with track_name as keys and track_id as values
    track_dict = {track['track_name']: track['acc_track_name'] for track in filtered_tracks}

    # Convert track_dict keys to a list of strings
    track_list = list(track_dict.keys())

    # Find the closest matching track name
    closest_track = find_closest_acc_track(track_list, track_name)

    # Get the corresponding track_id
    track_id = track_dict.get(closest_track)

    return track_id


def find_closest_acc_track(match_tracks, input_track):
    closest_match, _ = process.extractOne(input_track, match_tracks, scorer=fuzz.partial_ratio)
    return closest_match


def get_all_bop_tsv():
    # Initialize an empty list to store DataFrames
    dfs = []

    # List of tracks
    track_list = get_all_tracks()

    # Loop through the tracks
    for track in track_list:
        # Replace this line with your get_bop(track) function to get the data
        # For demonstration, we'll create a sample DataFrame
        data = get_bop(track)
        
        # Create a DataFrame from the sample data
        df = pd.DataFrame(data)
        
        # Add the track name as a column
        df['track'] = track
        
        # Append the DataFrame to the list
        dfs.append(df)

    # Concatenate the DataFrames
    result_df = pd.concat(dfs, ignore_index=True)

    df = pd.DataFrame(result_df)

    # Pivot the DataFrame
    pivot_df = df.pivot(index=['car_name', 'car_year'], columns=['track'], values='bop')

    # Replace NaN values with "—"
    pivot_df = pivot_df.fillna('—')

    # Flatten the multi-level columns
    # pivot_df.columns = [f'{col[0]}_{col[1]}' for col in pivot_df.columns]

    # Reset index
    pivot_df.reset_index(inplace=True)

    # Display the result
    return pivot_df


def get_json_all_tracks(tracklist):
    # URL to fetch JSON data from
    output_json = {
        "entries": []
    }
    for track in tracklist:
        url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
        try:
            # Send an HTTP GET request to the URL and parse the JSON response
            data = requests.get(url).json()
            
            track = get_acc_track_name(track)
            track = track[:-5]

            # Iterate through laps_relevant and laps_others
            for entry in data['laps_relevant'] + data['laps_others']:
                output_entry = {
                    "track": track,  # You can set the track name here
                    "carModel": entry['car_id'],
                    "ballastKg": int(entry['bop'])
                }
                output_json["entries"].append(output_entry)

        except Exception as e:
            print(f'An error occurred: {e}')

            

    # Convert the result to JSON format
    result_json = json.dumps(output_json, indent=4)

    # Write the JSON data to a file
    filename = f"bop.json"
    with open(filename, "a") as file:
        file.write(result_json)

    return filename


def get_targettime(track):

    # URL to fetch JSON data from
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}&class=GT4'
    logger.info(url)
    try:
        # Send an HTTP GET request to the URL and parse the JSON response
        data = requests.get(url).json()
        kgtime = data['bopdata']['target_time']
        return kgtime

    except Exception as e:
        print(f'An error occurred: {e}')

    ##### Old Code #####

    # difference = float(str(result['target_dif'].iloc[0])[4:])
    # laptime = (str(result['lap'].iloc[0]))

    # # Split the time string into minutes and seconds
    # minutes_str, seconds_str = laptime.split(":")
    # minutes = int(minutes_str)
    # seconds = float(seconds_str)

    # # Create a timedelta object with minutes and seconds
    # time_delta = datetime.timedelta(minutes=minutes, seconds=seconds + difference)

    # # Get the total time in seconds as a float
    # total_seconds = time_delta.total_seconds()

    # # Calculate minutes and seconds
    # minutes = int(total_seconds // 60)
    # seconds = round(total_seconds % 60, 3)
    # if seconds < 10:
    #     seconds = f"0{seconds}"

    # # Format as "mm:ss"
    # return f"{minutes}:{seconds}"


def get_realbop(input_track):
    # Send a GET request to the URL to fetch the JSON data
    url = 'https://api2.lowfuelmotorsport.com/api/hotlaps/getAccBop'

    response = requests.get(url)
    data = response.json()

    # Iterate through the tracks in the JSON data
    for track in data:
        if track['track_name'] == input_track:
            return track  # Return the data for the specified track

    # If the track is not found, return None or raise an exception
    return None  # or raise an exception with a custom message


def get_updated_bop(car, laptime, track):
    if laptime.startswith("0"):
        pass
    else:
        laptime = f"0{laptime}"
    result_string = ""
    result = get_bop(track)
    result = result[(result['relevant'] == 'X')]

    difference = float(str(result['target_dif'].iloc[0])[4:])
    kg = float(str(result['bop'].iloc[0])[-3:])

    bop_per_kg = round(difference/kg, 5)
    bop_max = round(bop_per_kg * 40, 2)

    result_string += f"{bop_per_kg}s per kg\n"
    result_string += f"BOP Max is {bop_max} seconds\n"

    laptimes_dict = {}
    for index, row in result.iterrows():
        laptimes_dict[row['car_name']] = row['lap']
 
    total_seconds = 0
    result_string += f"The laptime of {car} will be adjusted from {laptimes_dict[car]} to {laptime}\n"
    laptimes_dict[car] = laptime

    for car, lap in laptimes_dict.items():
        minutes_str, seconds_str = lap.split(":")
        minutes = int(minutes_str)
        seconds = float(seconds_str)

        # Get the total time in seconds as a float
        total_seconds += minutes * 60 + seconds 

    # Calculate minutes and seconds
    total_seconds = total_seconds / 10

    minutes = int(total_seconds // 60)
    seconds = round(total_seconds % 60, 3)
    if seconds < 10:
        seconds = f"0{seconds}"

    result_string += (f"Target time is now: {minutes}:{seconds}\n")
    print(laptimes_dict)
    laptimes_dict = dict(sorted(laptimes_dict.items(), key=lambda item: item[1]))
    print(laptimes_dict)


    for car, lap in laptimes_dict.items():
        minutes_str, seconds_str = lap.split(":")
        minutes = int(minutes_str)
        seconds = float(seconds_str)

        # if minutes * 60 + seconds > total_seconds + bop_max:
        #     result_string += f"The slowest car {car} is too slow. We will adjust.\n"
        #     total_seconds = minutes * 60 + seconds - bop_max
        #     minutes = int(total_seconds // 60)
        #     seconds = round(total_seconds % 60, 3)
        #     if seconds < 10:
        #         seconds = f"0{seconds}"
        #     result_string += (f"The Target laptime is now: {minutes}:{seconds}\n")

    fastest_car = list(laptimes_dict.items())[0]
    print(fastest_car)
    print(fastest_car[1])
        
    minutes_str, seconds_str = fastest_car[1].split(":")
    minutes = int(minutes_str)
    seconds = float(seconds_str)
    if minutes * 60 + seconds < total_seconds - bop_max:
        result_string += f"The fastest car {fastest_car[0]} is too fast. We will adjust.\n"
        total_seconds = minutes * 60 + seconds + bop_max
        minutes = int(total_seconds // 60)
        seconds = round(total_seconds % 60, 3)
        if seconds < 10:
            seconds = f"0{seconds}"
        result_string += (f"The Target laptime is now: {minutes}:{seconds}\n")

    ballast_dict = {}

    for car, lap in laptimes_dict.items():
        minutes_str, seconds_str = lap.split(":")
        minutes = int(minutes_str)
        seconds = float(seconds_str)
        ballast_dict[car] = round((total_seconds - minutes * 60 - seconds) / bop_per_kg, 0)


    max_car_name_length = len("Lamborghini Huracan GT3 EVO 2")  # Adjust this as needed
    result_string += "**BOP now as follows:**\n"
    result_string += "```\n"
    for car, ballast in ballast_dict.items():
        # Truncate or pad the car name to fit the maximum length
        formatted_car_name = car[:max_car_name_length] + ':' if len(car) > max_car_name_length else car.ljust(max_car_name_length)
        ballast = f"+{ballast}" if ballast > 0 else ballast
        result_string += f"{formatted_car_name}\t{ballast}\n"

    result_string += "```\n"

    return result_string


def get_kg_per_second(track):
    
    # # URL to fetch JSON data from
    # url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    # logger.info(url)
    # try:
    #     # Send an HTTP GET request to the URL and parse the JSON response
    #     data = requests.get(url).json()

    # except Exception as e:
    #     print(f'An error occurred: {e}')

    # kgtime = data['bopdata']['KgTime']

    # return kgtime
    result = get_bop(track)
    result = result[(result['rel'] == 'X')]

    difference = float(str(result['target_dif'].iloc[0])[4:])
    kg = float(str(result['bop'].iloc[0])[-3:])

    bop_per_kg = round(difference/kg, 4)
    return bop_per_kg


def time_to_seconds(time_str):
    # Split the time string into minutes, seconds, and milliseconds
    minutes, seconds = time_str.split(':')
    
    # Calculate the total time in seconds
    total_seconds = int(minutes) * 60 + float(seconds)
    
    return total_seconds


def float_to_time(float_value):
    minutes, seconds = divmod(float_value, 60)
    return f"{int(minutes):02d}:{seconds:06.3f}".lstrip("0")


def format_number(number):
    # Check if the number is positive or negative
    sign = '+' if number >= 0 else '-'
    
    # Add a space before the sign if the number is a single digit
    formatted_number = f" {sign}{abs(number)}" if abs(number) < 10 else f"{sign}{number}"
    
    # Print the result
    return formatted_number


def bop_next(track):
    result = ""
    data1 = get_realbop(find_closest_track(get_all_tracks(), track))
    url = f'https://api2.lowfuelmotorsport.com/api/hotlaps/getBopPrediction?track={get_track_id(track)}'
    # Send an HTTP GET request to the URL and parse the JSON response
    data2 = requests.get(url).json()

    # Extract car_name and ballast from the first JSON
    for entry in data1['bop']['GT3']:
        car_name = entry['car_name']
        ballast = entry['ballast']

        # Find the corresponding entry in the second JSON
        bop = None  # Initialize to None in case the entry is not found in either laps_relevant or laps_others

        for lap_entry in data2['laps_relevant']:
            if lap_entry['car_name'] == car_name:
                bop = lap_entry['bop']
                break

        # If the entry is not found in laps_relevant, search in laps_others
        if bop is None:
            for lap_entry in data2['laps_others']:
                if lap_entry['car_name'] == car_name:
                    bop = lap_entry['bop']
                    break
                
        diff = f"[{format_number(int(bop) - int(ballast))}]"
        bop = f"{bop}"
        
        # Print the results
        

        ansi_text = Ansi.Text()
        if int(bop) - int(ballast) > 0:
            foreground = Ansi.Foreground.RED
            text = f"[{(diff[2:])}"
        elif int(bop) - int(ballast) < 0:
            foreground = Ansi.Foreground.GREEN
            text = f"[{(diff[2:])}"
        else:
            foreground = Ansi.Foreground.YELLOW
            text = f"[{(diff[2:])}"

        ansi_text.set_foreground(foreground).add_text(text).reset()

        result += (f"{car_name:30} {bop.rjust(5)} {ansi_text.render()}\n")
        #result += (f"{car_name:30} {bop.rjust(5)} {diff}\n")


    return result


#print(get_bop("catalunya"))
#print(get_kg_per_second("Enzo"))
#print(get_targettime("Enzo"))