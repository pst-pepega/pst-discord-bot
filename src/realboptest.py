import requests

def get_track_data(track_name, url):
    # Send a GET request to the URL to fetch the JSON data
    response = requests.get(url)
    data = response.json()

    # Iterate through the tracks in the JSON data
    for track in data:
        if track['track_name'] == track_name:
            return track  # Return the data for the specified track

    # If the track is not found, return None or raise an exception
    return None  # or raise an exception with a custom message

# Example usage
url = 'https://api2.lowfuelmotorsport.com/api/hotlaps/getAccBop'
track_name = 'Autodromo Enzo e Dino Ferrari'

track_data = get_track_data(track_name, url)

if track_data:
    # Process the track data here
    print(track_data)
else:
    print(f"Track '{track_name}' not found in the data.")