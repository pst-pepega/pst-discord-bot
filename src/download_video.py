import requests
import yt_dlp

def get_clip(clip_url: str):
    # Download clip using yt-dlp
    with yt_dlp.YoutubeDL({}) as ydl:
        info = ydl.extract_info(clip_url, download=False)
        if info:
            video_url = info['url']
            title = info['title']

    # Upload clip to Discord webhook
    if info:
        file_name = f"{title}.mp4"
        response = requests.get(video_url)
        with open(file_name, 'wb') as f:
            f.write(response.content)

    return file_name


def download_video(url):
    # Define a dictionary to hold the options for yt-dlp
    ydl_opts = {
        'outtmpl': '%(title)s.%(ext)s',  # Save the file with the title of the video
        'format': 'best',                # Download the best quality
    }

    # Create a yt-dlp object with the given options
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        # Download the video and get the information dictionary
        info_dict = ydl.extract_info(url, download=True)
        
        # Get the filename from the info dictionary
        filename = ydl.prepare_filename(info_dict)
    
    return filename
