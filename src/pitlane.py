from fuzzywuzzy import fuzz, process
import json


def find_most_similar_track(input_name):
    track_names = []
    for i in range(len(tracks_data["tracks"])):
        track_names.append(tracks_data["tracks"][i])
    closest_match = process.extractOne(input_name, track_names)
    return closest_match[0]

# Load the JSON data
with open("resources/json/pitlane.json", "r") as json_file:
    tracks_data = json.load(json_file)