from dataclasses import dataclass
from typing import Any


@dataclass
class LfmTrack:
    id: int
    name: str
    acc_name: str
    year: int
    sim_id: int
    country: int
    length: int
    turns: int
    turn_factor: float

    @classmethod
    def from_dict(cls, data: dict[str, Any]):
        return LfmTrack(
                id=data["track_id"],
                name=data["track_name"],
                acc_name=data["acc_track_name"],
                year=data["track_year"],
                sim_id=data["sim_id"],
                country=data["country"],
                length=data["km"],
                turns=data["turns"],
                turn_factor=float(data["turn_factor"])
                )
