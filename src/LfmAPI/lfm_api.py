from collections.abc import Sequence
from fuzzywuzzy import process, fuzz
import logging
import requests
from typing import Optional, Any

from .lfm_race import LfmRace
from .lfm_track import LfmTrack


logger = logging.getLogger(__name__)


class LfmApi:
    LFM_API = "https://api2.lowfuelmotorsport.com/api"
    ACC_SIM_ID = 1
    ACC_TRACK_YEAR = 2023

    def __init__(self) -> None:
        pass

    def _get(self, path: str, args: Optional[tuple] = None) -> Optional[Any]:
        args_path = ""
        if args is not None and len(args) > 0:
            args_path = "?" + "&".join(
                [f"{arg['name']}={arg['value']}" for arg in args]
            )

        request = requests.get(f"{self.LFM_API}{path}{args_path}")
        if request.status_code != 200:
            logger.error(
                f"Failed to execute request {path}, {request.status_code}: {request.text}"
            )
            return None

        return request.json()

    def get_tracks(self) -> list[LfmTrack]:
        data: Optional[Sequence[dict[str, Any]]] = self._get("/lists/getTracks")
        if data is None:
            return list()

        return [LfmTrack.from_dict(track) for track in data]

    @classmethod
    def find_closest_track(cls, match_tracks: Sequence[str], input_track: str):
        matchs = process.extractOne(
            input_track, match_tracks, scorer=fuzz.partial_ratio
        )
        if matchs is None:
            return ""
        return matchs[0]

    def get_acc_status(self) -> Optional[bool]:
        status = self._get("/accstatus")
        if status is None:
            return None

        text = status.get("text")
        if not text:
            logger.error("No 'text' field found in the JSON response.")
            return None

        return "offline" in text.lower()

    def get_acc_track_id(self, track_name: str) -> int:
        acc_tracks = self.get_acc_tracks()

        # Find the closest matching track name
        closest_track = LfmApi.find_closest_track(list(acc_tracks.keys()), track_name)

        # Get the corresponding track_id
        track_id = acc_tracks[closest_track]

        return track_id

    def get_acc_tracks(self) -> dict[str, int]:
        data = self.get_tracks()

        return {
            track.name: track.id
            for track in data
            if track.sim_id == self.ACC_SIM_ID
            and track.year == self.ACC_TRACK_YEAR
        }

    def get_acc_track_names(self) -> tuple[str, ...]:
        return tuple(self.get_acc_tracks().keys())

    def get_bop(self, track) -> Optional[dict[str, Any]]:
        args = ({"name": "track", "value": self.get_acc_track_id(track)},)
        return self._get("/hotlaps/getBopPrediction", args)

    def get_race(self, race_id: int) -> Optional[LfmRace]:
        data = self._get(f"/race/{race_id}")
        if data is None:
            return None

        return LfmRace.from_dict(data)
