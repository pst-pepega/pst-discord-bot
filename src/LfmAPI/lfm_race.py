from dataclasses import dataclass
from datetime import datetime
from typing import Any
from .lfm_track import LfmTrack

@dataclass
class LfmServerSettings:
    event: dict
    assist_rules: dict
    event_rules: dict
    settings: dict

    @classmethod
    def from_dict(cls, data: dict[str, Any]):
        return LfmServerSettings(
                    event=data["event"],
                    assist_rules=data["assistRules"],
                    event_rules=data["eventRules"],
                    settings=data["settings"],
                )


# TODO fill other field
@dataclass
class LfmEvent:
    id: int
    name: str
    k_factor: float

    @classmethod
    def from_dict(cls, data: dict[str, Any]):
        return LfmEvent(
                    id=data["event_id"],
                    name=data["event_name"],
                    k_factor=float(data["k_factor"]),
                )

@dataclass
class LfmEntry:
    race_number: int
    first_name: str
    last_name: str
    short_name: str
    elo: int
    car_model: int
    user_id: int
    car_class: str

    @classmethod
    def from_dict(cls, data: dict[str, Any]):
        return LfmEntry(
                    race_number=data["raceNumber"],
                    first_name=data["vorname"],
                    last_name=data["nachname"],
                    short_name=data["shortname"],
                    elo=data["elo"],
                    car_model=data["car_model"],
                    user_id=data["user_id"],
                    car_class=data['car_class']
                )

@dataclass
class LfmParticipants:
    entries: list[LfmEntry]

    @classmethod
    def from_dict(cls, data: dict[str, Any]):
        return LfmParticipants(
                    entries=[LfmEntry.from_dict(entry) for entry in data["entries"]],
                )


# TODO fill other field
@dataclass
class LfmRace:
    date: datetime
    track: LfmTrack
    server_settings: LfmServerSettings
    event: LfmEvent
    participants: LfmParticipants
    
    @classmethod
    def from_dict(cls, data: dict[str, Any]):
        return LfmRace(
                    date=datetime.strptime(data["race_date"], "%Y-%m-%d %H:%M:%S"),
                    track=LfmTrack.from_dict(data["track"]),
                    server_settings=LfmServerSettings.from_dict(data["server_settings"]["server_settings"]),
                    event=LfmEvent.from_dict(data["event"]),
                    participants=LfmParticipants.from_dict(data["participants"])
                )
