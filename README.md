# PST Discord bot

## Setup poetry

### Install poetry

```sh
# Via package manager (PIP 668)
apt install python-poetry

# Via pip
pip install poetry
```

### Install venv inside project directory

```sh
poetry config virtualenvs.in-project true
```

### Install project dependencies

```sh
poetry install
```

### Add / remove dependencies to the project

```sh
poetry add something

poetry remove other-thing
```

### Load the virtual env

```sh
poetry shell
```

### Run the script without loading the environment

```sh
poetry run python src/main.py
```

